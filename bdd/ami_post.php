<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (isset($_GET['ami']))
{
	// on regarde s'il ne sont pas d�j� amis 
	$re = $bdd->prepare('SELECT id FROM membres WHERE nom_de_compte=:nom_de_compte')or die(print_r($bdd->errorInfo()));
	$re->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))or die(print_r($bdd->errorInfo()));
	$id = $re->fetch();
	// on insert l'ami ds la bdd
	$req = $bdd->prepare('INSERT INTO amis(id_jeu,id_jeu_ami) 
	VALUES(:id_jeu,:id_jeu_ami)') or die(print_r($bdd->errorInfo()));
	$req->execute(array(
	'id_jeu' => $_SESSION['id_jeu'],
	'id_jeu_ami' => $_GET['ami'])) or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	// supprimer les messages de demande d'ami
	$req = $bdd->prepare('DELETE FROM message WHERE titre_message=:titre_message AND destinataire=:destinataire')or die(print_r($bdd->errorInfo()));
	$req->execute(array(
	'titre_message' => 'Demande d\'amiti� !',
	'destinataire' => $_SESSION['id_jeu'])) or die(print_r($bdd->errorInfo()));
	header('Location: message.php');
}
else
{
	header('Location: message.php?erreur');
}