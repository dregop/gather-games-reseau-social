<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
	die('Erreur : '.$e->getMessage());
}

//ON TEST L'IMAGE///////////////////////////////////////////////////////////////
include('test_image.php');
////////////////////////////////////////////////////////////////////////////////


// MODIFICATION SOUS COMPTE INFORMATIONS CONNECTE ------------------------------
if (isset($_FILES['photo_profil']['name'],$_POST['description'],
$_POST['informations_connecte'],$_POST['id_jeu']) 
AND $_FILES['photo_profil']['size']<=1000000 
AND strlen($_POST['description'])<251)
{
	$reqs = $bdd->prepare('SELECT pseudo,description FROM jeu 
						WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $reqs->fetch();	
	
	if ($_FILES['photo_profil']['name'] != '')
	{
		if (isset($presence_photo) AND $presence_photo == 'off')  { $photo=''; }
		
		$reqa = $bdd->prepare('UPDATE jeu SET description=:description, 
							photo_profil=:photo_profil WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$reqa->execute(array('description' => $_POST['description'], 
							'photo_profil' => $photo,
							'id_jeu' => $_POST['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
		$reqa->closeCursor(); // Termine le traitement de la requ�te
		
		if (isset($presence_photo) AND $presence_photo = 'on'  
		AND $_POST['id_jeu'] == $_SESSION['id_jeu'])
		{
			header('Location: choix-profil.html');
		}
		elseif (isset($presence_photo) AND $presence_photo == 'on'
		AND $_POST['id_jeu'] != $_SESSION['id_jeu'])
		{// LE 315 signifie co => connecte (c => 3 ; o => 15) 
			header('Location: informations315-sc.html');
		}
		else
		{
			$info4 ='invalide';
			header('Location: informations315-sc-'.$info4.'.html');
		}
	}
	elseif ($_POST['description'] != $donnees_jeu['description']) 
	{
		$reqa = $bdd->prepare('UPDATE jeu SET description=:description 
							WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$reqa->execute(array('description' => $_POST['description'],
							'id_jeu' => $_POST['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
		$description_modifiee = 'scd';
		header('Location: informations315-'.$_POST['id_jeu'].'-'.$description_modifiee.'.html#'.$_POST['id_jeu'].'');
	}
	else
	{
		header('Location: informations315-sc.html');
	}
	
}
elseif (isset($_POST['description']))
{
	$erreur_description='scde';
	header('Location: informations315-'.$_POST['id_jeu'].'-'.$erreur_description.'.html#'.$_POST['id_jeu'].'');
}
elseif (isset($_POST['informations_connecte']))
{
	header('Location: informations315-sc.html');
}

include('supprimer_sous_compte.php');
?>