<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
	die('Erreur : '.$e->getMessage());
}

//ON TEST L'IMAGE///////////////////////////////////////////////////////////////
include('test_image.php');
////////////////////////////////////////////////////////////////////////////////

$req = $bdd->prepare('SELECT COUNT(*) AS nbre 
					FROM jeu 
					WHERE nom_de_compte=:nom_de_compte')
					or die(print_r($bdd->errorInfo()));
$req->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))
					or die(print_r($bdd->errorInfo()));
$do = $req->fetch();

if ($do['nbre'] > 4 AND !(isset($_POST['informations_connecte'])))
{
	$sous_compte_max = 1;
}

if (!isset($_GET['supprimer'], $_GET['id']) AND isset($sous_compte_max) 
AND $sous_compte_max = 1)
	header('Location: informations-max.html');

if (!isset($sous_compte_max) AND !isset($_GET['supprimer'])) 					// ON VERIFIE QUE LE NBRE MAX DE SOUS COMPTE N'EST PAS ATTEINT
{
	 // LES ENREGISTREMENT AVEC LE RENVOIS DE MESSAGE D'ERREUR !!! 
	 // POUR FORMAT VALIDE, FORMAT INVALIDE  ET AUCUNE IMAGE CHOISIE !!!
	if (isset($_POST['pseudo']) AND preg_match("#['\"]#",$_POST['pseudo'])) 	// SI LE PSEUDO CONTIENT " et ' 
	{
		$erreur='e1';
		header('Location: informations-'.$erreur.'.html');
	} 	
	elseif (isset($_POST['jeu'], $_POST['plateforme'], $_POST['pseudo']) 
	AND $_POST['pseudo'] !='' AND $_POST['jeu'] !=''
	AND $_POST['plateforme'] != 'S�lectionnez'AND strlen($_POST['pseudo'])>1 
	AND strlen($_POST['pseudo'])<21 AND strlen($_POST['description_jeu'])<251 )
	{	
		if(isset($_FILES['photo_profil']['name']) 
		AND $_FILES['photo_profil']['name'] != '')
		{		
		   $infosfichier = pathinfo($_FILES['photo_profil']['name']);
		   $extension_upload = $infosfichier['extension'];
		   $extensions_autorisees = array('jpg', 'jpeg', 'png', 'PNG', 'JPEG', 'JPG');	
		 
			if ($_FILES['photo_profil']['size']<=1000000 
			AND isset($_FILES['photo_profil']['name']) 
			AND !(empty($_FILES['photo_profil']['name'])) 
			AND in_array($extension_upload, $extensions_autorisees))// ENREGISTREMENT NORMAL AVEC IMAGE INFERIEUR 1MO 
			{
				if (isset($presence_photo) AND $presence_photo == 'off') 
				{ $photo=''; }
				
				$req = $bdd->prepare('INSERT INTO jeu(nom_jeu, pseudo, 
									description, nom_de_compte, plateforme,
									photo_profil) 
									VALUES(:nom_jeu, :pseudo, :description, 
									:nom_de_compte, :plateforme, 
									:photo_profil)');
				$req->execute(array('nom_jeu' => $_POST['jeu'],
									'pseudo' => $_POST['pseudo'],
									'description' => $_POST['description_jeu'],
									'nom_de_compte' => $_SESSION['nom_de_compte'],
									'plateforme' => $_POST['plateforme'],
									'photo_profil' => $photo));
				$req->closeCursor(); // Termine le traitement de la requ�te	
			
				if (isset($_SESSION['id_jeu']))
				{
					// ON INSERT DANS ACTUALITE
					$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,
										date_actu) 
										VALUES(:id_jeu,:actu,NOW())') 
										or die(print_r($bdd->errorInfo()));
					$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
										'actu' => 'sous_compte')) 
										or die(print_r($bdd->errorInfo()));
				}
				if (isset($presence_photo) AND $presence_photo == 'on')
				{
					header('Location: informations.html');	
				}
				elseif (isset($presence_photo) AND $presence_photo == 'off')
				{
					$erreur_img ='e9';
					header('Location: informations-'.$erreur_img.'.html');
				}
			}
			else // ENREGISTREMENT AVEC IMAGE PAR DEFAULT ON RENVOIE MESSAGE 
			{    // D'ERREUR PAS LE BON FORMAT OU SUPERIEUR A 1 MO
			
				$photo = '';
				$req = $bdd->prepare('INSERT INTO jeu(nom_jeu, pseudo, 
									description, nom_de_compte, plateforme,
									photo_profil) 
									VALUES(:nom_jeu, :pseudo, :description, 
									:nom_de_compte, :plateforme, 
									:photo_profil)');
				$req->execute(array('nom_jeu' => $_POST['jeu'],
									'pseudo' => $_POST['pseudo'],
									'description' => $_POST['description_jeu'],
									'nom_de_compte' => $_SESSION['nom_de_compte'],
									'plateforme' => $_POST['plateforme'],
									'photo_profil' => $photo));
				$req->closeCursor(); // Termine le traitement de la requ�te	
				
				if (isset($_SESSION['id_jeu']))
				{
					// on insert dans actu
					$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,date_actu) 
										VALUES(:id_jeu,:actu,NOW())') 
										or die(print_r($bdd->errorInfo()));
					$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
										'actu' => 'sous_compte')) 
										or die(print_r($bdd->errorInfo()));
				}
				$erreur_img ='e9';
				header('Location: informations-'.$erreur_img.'.html');
			}
				
		 }
		 else // SI IL N'Y A PAS D'IMAGE SELECTIONNEE (PAS DE MESSAGE D'ERREUR)
		 {
			if (isset($presence_photo) AND $presence_photo == 'off') 
			{ $photo=''; }	
			
			$req = $bdd->prepare('INSERT INTO jeu(nom_jeu, pseudo, description, 
								nom_de_compte, plateforme,photo_profil) 
								VALUES(:nom_jeu, :pseudo, :description, 
								:nom_de_compte, :plateforme, :photo_profil)');
			$req->execute(array('nom_jeu' => $_POST['jeu'],
								'pseudo' => $_POST['pseudo'],
								'description' => $_POST['description_jeu'],
								'nom_de_compte' => $_SESSION['nom_de_compte'],
								'plateforme' => $_POST['plateforme'],
								'photo_profil' => $photo));
			$req->closeCursor(); // Termine le traitement de la requ�te	
			if (isset($_SESSION['id_jeu']))
			{
				// ON INSERT DANS ACTUALITE
				$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,date_actu) 
				VALUES(:id_jeu,:actu,NOW())') or die(print_r($bdd->errorInfo()));
				$req->execute(array(
				'id_jeu' => $_SESSION['id_jeu'],
				'actu' => 'sous_compte')) or die(print_r($bdd->errorInfo()));
			}
			header('Location: informations.html');		
		 }

	}
	else // TOUT N'EST PAS REMPLIS
	{
		$erreur='e1';
		header('Location: informations-'.$erreur.'.html');
	}	
}

include('supprimer_sous_compte.php');
?>
