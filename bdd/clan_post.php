<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if(isset($_POST['supprimer_membre'], $_POST['id_jeu']))
{
	$req_membre = $bdd->prepare('UPDATE jeu SET id_clan=:id_clan WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
	$req_membre->execute(array('id_clan' => 0,
								'id' => $_POST['id_jeu']))
								or die(print_r($bdd->errorInfo()));	
	$req_membre->closeCursor(); // Termine le traitement de la requ�te
	
	header('Location: team.html');
}

if (isset($_POST['nommer'], $_POST['id_jeu'], $_POST['id_clan']))
{
	$req1 = $bdd->prepare('UPDATE clan SET id_jeu=:id_jeu WHERE id = :id_clan')
							or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id_jeu' => $_POST['id_jeu'],
						'id_clan' => $_POST['id_clan']))
						or die(print_r($bdd->errorInfo()));	
	$req1->closeCursor(); // Termine le traitement de la requ�te
	$req2 = $bdd->prepare('UPDATE jeu SET id_clan=:id_clan WHERE id = :id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req2->execute(array('id_clan' => 0,
						'id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));	
	$req2->closeCursor(); // Termine le traitement de la requ�te
	
	header('Location: team-i'.$_POST['id_clan'].'.html');
}	

if(isset($_GET['id_clan'], $_GET['supprimer']))
{
	$req1 = $bdd->prepare('DELETE FROM clan WHERE id=:id_clan')
							or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id_clan' => $_GET['id_clan'])) 
							or die(print_r($bdd->errorInfo()));
	$req1->closeCursor(); // Termine le traitement de la requ�te
	
	$req2 = $bdd->prepare('UPDATE jeu SET id_clan=:id_clan WHERE id = :id_jeu')
							or die(print_r($bdd->errorInfo()));
	$req2->execute(array('id_clan' => 0,
							'id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$req2->closeCursor(); // Termine le traitement de la requ�te
	header('Location: team.html');	
}

if(isset($_GET['quitter'], $_GET['id_clan']))
{
	$req = $bdd->prepare('UPDATE jeu SET id_clan=:id_clan WHERE id = :id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => 0,
						'id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te
	header('Location: team-i'.$_GET['id_clan'].'.html');
}
// Testons si le fichier a bien �t� envoy� et s'il n'y a pas d'erreur
// Testons si le fichier n'est pas trop gros
if (isset($_FILES['photo_clan']) AND $_FILES['photo_clan']['size'] <= 1000000 
AND isset($_FILES['photo_clan']['name']) AND !empty($_FILES['photo_clan']['name']))
{
	// Testons si l'extension est autoris�e
	$infosfichier = pathinfo($_FILES['photo_clan']['name']);
	$extension_upload = $infosfichier['extension'];
	$extensions_autorisees = array('jpg', 'jpeg', 'png', 'PNG', 'JPEG', 'JPG');				
	if (in_array($extension_upload, $extensions_autorisees))
	{
						// ouverture du fichier compteur
			$monfichier = fopen('compteur.txt', 'r+');				 
			$compteur = fgets($monfichier); 
			$compteur++; 
			fseek($monfichier, 0); 
			fputs($monfichier, $compteur); 
			fclose($monfichier);
			// On peut valider le fichier et le stocker d�finitivement
		if ($extension_upload == 'png' OR $extension_upload == 'PNG')
		{
			move_uploaded_file($_FILES['photo_clan']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefrompng('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		elseif ($extension_upload == 'jpeg' OR $extension_upload == 'jpg')
		{
			move_uploaded_file($_FILES['photo_clan']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefromjpeg('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
	}
	else
			header('Location: team-creer.html'); 

include('redimention_image.php');
$redimOK = fctredimimage(400,200,'images_utilisateurs/','clan_1_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
$redimOK = fctredimimage(800,400,'images_utilisateurs/','clan_2_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
$redimOK = fctredimimage(120,60,'images_utilisateurs/','clan_3_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
}
		
// CHANGER INFO DU CLAN	
if(isset($_POST['candidature'], $_POST['id_clan']) 
AND $_POST['candidature'] != '')
{

	$req = $bdd->prepare('INSERT INTO candidature (id_jeu,id_clan,message)
						VALUES (:id_jeu,:id_clan,:message)')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'id_clan' => $_POST['id_clan'], 
						'message' => $_POST['candidature']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te
	
	if (isset($_POST['id_clan'])) 
	{
		header('Location: team-i'.$_POST['id_clan'].'.html');
	}
	else
	{
		header('Location: team.html');
	}
}
elseif(isset($_POST['candidature'], $_POST['id_clan']) 
AND $_POST['candidature'] == '' AND !isset($_POST['changer_image']))
{
	if(isset($_POST['annonce_recrutements']))
		header('Location: recrutements-i'.$_POST['id_clan'].'-ec.html');
	else
		header('Location: team-i'.$_POST['id_clan'].'-ec.html');
}

if(isset($_POST['changer_description'], $_POST['modifier_info_clan']) 
AND strlen($_POST['modifier_info_clan'])<251)
{
	$req = $bdd->prepare('UPDATE clan SET information=:information 
						WHERE id_jeu = :id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('information' => $_POST['modifier_info_clan'],
						'id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te
	header('Location: team.html');
}	
elseif(isset($_POST['changer_description'], $_POST['modifier_info_clan']) 
AND strlen($_POST['modifier_info_clan'])>251)
{
	header('Location: team-ed.html');
}

if(isset($_POST['changer_image'],$_FILES['photo_clan']['name']))
{
	$monfichier = fopen('compteur.txt', 'r+');				 
	$compteur = fgets($monfichier); 
	fclose($monfichier);
	$photo = $compteur.'.'.$extension_upload;	
	if ($source == '')
	{
		$photo = 0;
		$erreur_image = 0;
	}	
	
	$req = $bdd->prepare('UPDATE clan SET photo_clan=:photo_clan 
						WHERE id_jeu = :id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('photo_clan' => $photo,
						'id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te
	if (isset($erreur_image))
	{
		$erreur_image = 0;
		header('Location: team-'.$erreur_image.'.html'); 
	}
	else
		header('Location: team.html'); 
}
// FIN CHANGER INFO DU CLAN

// CREATION DU CLAN
if(isset($_POST['nom_clan'],$_POST['plateforme'],$_POST['information'],
$_POST['jeu'])AND $_POST['nom_clan'] !='' AND $_POST['information'] !=''
AND $_POST['plateforme'] != 'S�lectionnez' AND $_POST['jeu'] !=''
AND strlen($_POST['information'])< 251 AND strlen($_POST['nom_clan'])>1 
AND strlen($_POST['nom_clan'])<21)
{
	$monfichier = fopen('compteur.txt', 'r+');				 
	$compteur = fgets($monfichier); 
	fclose($monfichier);
	$photo = $compteur.'.'.$extension_upload;
	
	if ($source == '')
	{
		$photo = 0;
		$erreur_image = 0;
	}			
	$req = $bdd->prepare('INSERT INTO clan(id_jeu, nom_clan, photo_clan, 
						information, jeu, plateforme) 
						VALUES(:id_jeu, :nom_clan, :photo_clan, :information, 
						:jeu, :plateforme)')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'nom_clan' => $_POST['nom_clan'],
						'photo_clan' => $photo,
						'information' =>$_POST['information'],
						'jeu' => $_POST['jeu'],
						'plateforme' => $_POST['plateforme']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te
	
	$requete = $bdd->prepare('SELECT id FROM clan WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
	$donnees_t = $requete->fetch();
	
	$req = $bdd->prepare('UPDATE jeu SET id_clan=:id_clan WHERE id = :id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $donnees_t['id'],
						'id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te
	header('Location: team.html');
													
	// on insert dans actualit� !
	$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,id_table, date_actu) 
						VALUES(:id_jeu,:actu,:id_table, NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'actu' => 'creer_clan',
						'id_table' => $donnees_t['id'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te	
}
elseif(isset($_POST['nom_clan'],$_POST['plateforme'],$_POST['information'],
$_POST['jeu'])AND $_POST['nom_clan'] =='' AND $_POST['information'] =='' 
AND $_POST['plateforme'] == 'S�lectionnez' AND $_POST['jeu'] ==''
AND strlen($_POST['information'])> 250 AND strlen($_POST['nom_clan'])<2 
AND strlen($_POST['nom_clan'])>20)
{
	header('Location: team-creer-fail.html'); 
}
elseif (!isset($_POST['changer_image']) 
AND !isset($_POST['changer_description']) AND !isset($_POST['candidature']) 
AND !isset($_POST['supprimer_membre']) AND !isset($_POST['nommer']) 
AND !isset($_GET['id_clan']))
{
	header('Location: team-creer-fail.html'); 
}
//FIN CREATION DU CLAN	
