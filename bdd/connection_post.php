<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (isset($_POST['nom_de_compte']) AND isset($_POST['mot_de_passe']))
{
	$requete = $bdd->prepare('SELECT nom_de_compte,mot_de_passe FROM membres 
							WHERE nom_de_compte = :nom_de_compte')
	or die(print_r($bdd->errorInfo()));
	$requete->execute(array('nom_de_compte' => $_POST['nom_de_compte']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $requete->fetch();
	$mdp_hache = sha1('qw' . $_POST['mot_de_passe']); // on hache le mdp
	
	if ($_POST['nom_de_compte'] == $donnees['nom_de_compte'] 
	AND $mdp_hache == $donnees['mot_de_passe']) 								// si le nom_de_compte et le mdp sont bon 
	{
		$_SESSION['nom_de_compte'] = $_POST['nom_de_compte'];
		if ($_POST['connecte'] == 'on' AND isset($_POST['connecte']))
		{
			setcookie('nom_de_compte', $_POST['nom_de_compte'], time() + 365*24*3600, null, null, false, true);
		}
		header('Location: choix-profil.html');
	}
	else
	{
		$erreur_connection ='fail';
		header('Location: index-'.$erreur_connection.'.html');
	}

}
else
{	
	$erreur_connection ='fail';
	header('Location: index-'.$erreur_connection.'.html');
}