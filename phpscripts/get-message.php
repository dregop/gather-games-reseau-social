<?php
session_start();
include('functions.php');
// Appel de la fonction de connexion à la base de donn�es
$db = db_connect();
	


/* Si vous voulez faire appraître les messages depuis l'actualisation
de la page, laissez l'AVANT-DERNIERE ligne de la requete, sinon, supprimez-la */
// Gestion de l'affichage 
if(isset($_GET['general'],$_GET['team']) 
AND $_GET['general'] == 0 AND $_GET['team'] == 0)
{
$query = $db->query("
	SELECT *
	FROM chat_messages
	LEFT JOIN jeu ON jeu.id = chat_messages.message_user
	WHERE affichage != 'Général'
	ORDER BY message_time ASC");
}
elseif(isset($_GET['general'],$_GET['team']) 
AND $_GET['general'] == 1 AND $_GET['team'] == 1)
{
$query = $db->query("
	SELECT *
	FROM chat_messages
	LEFT JOIN jeu ON jeu.id = chat_messages.message_user
	WHERE affichage != 'Team'
	ORDER BY message_time ASC");
}
elseif(isset($_GET['general'],$_GET['team']) 
AND $_GET['general'] == 0 AND $_GET['team'] == 1)
{
$query = $db->query("
	SELECT *
	FROM chat_messages
	LEFT JOIN jeu ON jeu.id = chat_messages.message_user
	WHERE affichage != 'Team' AND affichage != 'Général'
	ORDER BY message_time ASC");
}
elseif(isset($_GET['general'],$_GET['team']) 
AND $_GET['general'] == 1 AND $_GET['team'] == 0)
{
$query = $db->query("
	SELECT *
	FROM chat_messages
	LEFT JOIN jeu ON jeu.id = chat_messages.message_user
	ORDER BY message_time ASC");
}
$count = $query->rowCount();
if($count != 0) {
	$json['messages'] = '<div id="messages_content">';
	// On cr�e un tableau qui continendra notre...tableau
	// Afin de placer les messages en bas du chat
	// On triche un peu mais c'est plus simple :D
	$json['messages'] .= '<table><tr><td style="height:200px;" valign="bottom">';
	$json['messages'] .= '<table>';

	$i = 1;
	$e = 0;
	$prev = 0;
	$text = '';
	$nbre_message = 0;
	while ($data = $query->fetch()) {
	$nbre_message++;
	// on selectionne le pseudo et id_clan
	$reque = $db->prepare('SELECT pseudo FROM jeu
					WHERE id=:id_jeu') or die(print_r($bdd->errorInfo()));
	$reque->execute(array('id_jeu' => $data['id_message_prive']))
										   or die(print_r($bdd->errorInfo()));
	$donnees_pseudo = $reque->fetch();
	
	$reques = $db->prepare('SELECT id_clan FROM jeu
					WHERE id=:id_jeu') or die(print_r($bdd->errorInfo()));
	$reques->execute(array('id_jeu' => $data['message_user']))
										   or die(print_r($bdd->errorInfo()));
	$donnees_clan = $reques->fetch();	
	
	$requet = $db->prepare('SELECT id_clan FROM jeu
					WHERE id=:id_jeu') or die(print_r($bdd->errorInfo()));
	$requet->execute(array('id_jeu' => $_SESSION['id_jeu']))
										   or die(print_r($bdd->errorInfo()));
	$donnees = $requet->fetch();
	
	if (($data['id_message_prive'] == 0 AND $donnees_clan['id_clan'] == $donnees['id_clan'] AND $data['affichage'] == 'Team') // clan
	OR ($data['id_message_prive'] == 0 AND $data['affichage'] == 'Général') // g�n�ral
	OR $data['id_message_prive'] == $_SESSION['id_jeu'] 
	OR $data['message_user'] == $_SESSION['id_jeu'])
	{
		// On supprime les balises HTML
		if ($data['affichage'] == 'Team')
		{
			$message = '<span style="color:#fbf584;margin-left:10px;" >'.htmlspecialchars($data['message_text']).'</span>';
		}
		elseif ($data['affichage'] == 'Privé')
		{
			$message = '<span style="color:#fbb5a4;margin-left:10px;" >'.htmlspecialchars($data['message_text']).'</span>';
		}
		else
		{
			$message = '<span style="color:#b8c6ce;margin-left:10px;" >'.htmlspecialchars($data['message_text']).'</span>';
		}

		// On transforme les liens en URLs cliquables
		$message = urllink($message);
			
	
		// Change la couleur dès que l'ID du membre est diff�rent du pr�c�dent
		$id = $data['message_user'];

		$text .= '<div style="width:330px; word-wrap= break-word; margin-top:10px;"><tr><td style=" " valign="top">';
		
		if ($data['affichage'] == 'Team')
		{
			$text .= date('[H:i]', $data['message_time']);
			$text .= '&nbsp;<span style="color:#fbf584" >'.utf8_encode($data['pseudo']).' :</span>';
		}
		elseif ($data['affichage'] == 'Privé')
		{
			$text .= date('[H:i]', $data['message_time']);
			$text .= '&nbsp;<span style="color:#fbb5a4" >'.utf8_encode($data['pseudo']).' :</span>';
		}
		else
		{
			$text .= date('[H:i]', $data['message_time']);
			$text .= '&nbsp;<span style="color:#b8c6ce" >'.utf8_encode($data['pseudo']).' :</span>';
		}
			
		$text .= '</td>';			
		$text .= '<td style="" valign="top">';

			
		// affichage message priv�, on selectionne les amis
				$requete2_c = $db->prepare('SELECT id_jeu_ami FROM amis 
											WHERE id_jeu=:id_jeu')
											or die(print_r($bdd->errorInfo()));
				$requete2_c->execute(array('id_jeu' => $_SESSION['id_jeu']))
											or die(print_r($bdd->errorInfo()));
				while($donnees_amis_c = $requete2_c->fetch())
				{
					if ($data['affichage'] == 'Privé' AND $donnees_amis_c['id_jeu_ami'] == $data['id_message_prive'])
					{
						$message = preg_replace('#'.$donnees_pseudo['pseudo'].'#is', 
						'<span style="color:#fbb5a4;">'.utf8_encode($donnees_pseudo['pseudo']).'</span>', $message);
						$stop = 0;
						break;
					}
				}
				$requete3_c = $db->prepare('SELECT id_jeu FROM amis 
											WHERE id_jeu_ami=:id_jeu_ami')
											or die(print_r($bdd->errorInfo()));
				$requete3_c->execute(array('id_jeu_ami' => $_SESSION['id_jeu']))
											or die(print_r($bdd->errorInfo()));
				while($donnees2_amis_c = $requete3_c->fetch())
				{
					if ( !isset($stop) AND $data['affichage'] == 'Privé' 
					AND $donnees2_amis_c['id_jeu'] == $data['id_message_prive'])
					{
							$message = preg_replace('#'.$donnees_pseudo['pseudo'].'#is', 
						'<span style="color:#fbb5a4;">'.utf8_encode($donnees_pseudo['pseudo']).'</span>', $message);
						break;
					}				
				}
				
			
		// On ajoute le message en remplaçant les liens par des URLs cliquables
		$text .= $message.'<br />';
		$text .= '</td></tr>';
		$i++;
		$prev = $data['id'];
	}
	}
	/* On cr�e la colonne messages dans le tableau json
	qui contient l'ensemble des messages */
	$json['messages'] = $text;

	$json['messages'] .= '</table>';
	$json['messages'] .= '</td></tr></div></table>';
	$json['messages'] .= '</div>';			
} else {
	$json['messages'] = 'Aucun message n\'a �t� envoy� pour le moment.';
}
	$json['nbre_message'] = $nbre_message;
$query->closeCursor();

// Encodage de la variable tableau json et affichage
echo json_encode($json);