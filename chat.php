<?php
session_start();
include('phpscripts/functions.php');
$db = db_connect();
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
<style>
body {
	background: #d2d2d2;
}
/* Pour que les liens ne soient pas soulign�s */
a {
	text-decoration: none;
}
img {
	vertical-align: middle;
}
/* Conteneur principal des blocs de la page */
#container {
	width: 80%;
	margin: 50px auto;
	padding: 2px 20px 20px 20px;
	background: #fff;
}

/* Bloc contenant la zone de texte et bouton */
.post_message  {
	width: 95%;
	margin: auto;
	border: 1px solid #d2d2d2;
	background: #f8fafd;
	padding: 3px;
}
/* Zone de texte */
.post_message #message {
	width: 80%;
}
/* Bouton d'envoi */
.post_message #post {
	width: 18%;
}

/* La zone o� sont affich�s les messages
et utilisateurs connect�s */
.chat {
	width: 95%;
	margin: 10px auto;
	border: 1px solid #d2d2d2;
	padding: 0px;
}
/* Bloc de chargement */
.chat #loading {
	margin-top: 50px;
}
/* Annonce */
.chat #annonce {
	background: #f8fafd;
	margin: -6px -7px 5px -7px;
	padding: 5px;
	height:20px;
	box-shadow: 8px 8px 12px #aaa;
	-webkit-box-shadow: 0px 8px 15px #aaa;
}
/* Zone des messages */
.chat #text-td {
	margin: 0px; 
	padding: 5px; 
	width: 80%; 
	background: #fff; 
}
/* Zone des utilisateurs connect�s */
.chat #users-td, .chat #users-chat-td {
	margin: 0px; 
	padding: 5px; 
	width: 20%; 
	background: #ddd;
}
.chat #text, .chat #users, .chat #users-chat {
	height:500px; 
	overflow-y: auto;
}

/* Modification du statut */
.status {
	width: 95%;
	border: none;
	background: #fff;
	margin: auto;
	text-align: right;
}

.info {
	color: green;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="chat.js"></script>
 </head>
   
<body>


<div id="container">
	<h1>Mon super chat</h1>
<?php
// permettra de cr�er l'utilisateur lors de la validation du formulaire
if(isset($_POST['login']) AND !preg_match("#^[-. ]+$#", $_POST['login'])) {
/* On cr�e la variable login qui prend la valeur POST envoy�e
car on va l'utiliser plusieurs fois */
$login = $_POST['login'];
$pass = $_POST['pass'];
			
// On cr�e une requ�te pour rechercher un compte ayant pour nom $login
$query = $db->prepare("SELECT * FROM chat_accounts WHERE account_login = :login");
$query->execute(array(
	'login' => $login
));
// On compte le nombre d'entr�es
$count=$query->rowCount();
			
// Si ce nombre est nul, alors on cr�e le compte, sinon on le connecte simplement
if($count == 0) {			
	// Cr�ation du compte
	$insert = $db->prepare('
		INSERT INTO chat_accounts (account_id, account_login, account_pass) 
		VALUES(:id, :login, :pass)
	');
	$insert->execute(array(
		'id' => '',
		'login' => htmlspecialchars($login),
		'pass' => md5($pass)
	));
				
	/* Cr�ation d'une session id ayant pour valeur le dernier ID cr��
	par la derni�re requ�te SQL effectu�e */
	$_SESSION['id'] = $db->lastInsertId();
	// On cr�e une session time qui prend la valeur de la date de connexion
	$_SESSION['time'] = time();
	$_SESSION['login'] = $login;
} else {
	$data = $query->fetch();	
				
	if($data['account_pass'] == md5($pass)) {			
		$_SESSION['id'] = $data['account_id'];
		// On cr�e une session time qui prend la valeur de la date de connexion
		$_SESSION['time'] = time();
		$_SESSION['login'] = $data['account_login'];
	}
}
			
// On termine la requ�te
$query->closeCursor();
}

/* Si l'utilisateur n'est pas connect�, 
d'o� le ! devant la fonction, alors on affiche le formulaire */
if(!user_verified()) {
?>
<div class="unlog">
	<form action="" method="post">
	Indiquez votre pseudo afin de vous connecter au chat. 
	Aucun mot de passe n'est requis. Entrez simplement votre pseudo.<br><br>
				
	<center>
		<input type="text" name="login" placeholder="Pseudo" /><br />
                <input type="password" name="pass" placeholder="Mot de passe" /><br /> 
		<input type="submit" value="Connexion" />
	</center>
	</form>
</div>
<?php
}
?>
        <!-- Statut //////////////////////////////////////////////////////// -->				
	<table class="status"><tr>
		<td>
			<span id="statusResponse"></span>
			<select name="status" id="status" style="width:200px;" onchange="setStatus(this)">
				<option value="0">Absent</option>
				<option value="1">Occup�</option>
				<option value="2" selected>En ligne</option>
			</select>
		</td>
	</tr></table>
<table class="chat"><tr>	
	<!-- zone des messages -->
	<div id="messages_content"></div>
	<td valign="top" id="text-td">
            	<div id="annonce"></div>
		<div id="text">
			<div id="loading">
				<center>
				<span class="info" id="info">Chargement du chat en cours...</span><br />
				<img src="images/ajax-loader.gif" alt="patientez...">
				</center>
			</div>
		</div>
	</td>		
	<!-- colonne avec les membres connect�s au chat -->
	<td valign="top" id="users-td"><div id="users">Chargement</div></td>
</tr></table>
<!-- Zone de texte //////////////////////////////////////////////////////// -->
        <a name="post"></a>
	<table class="post_message"><tr>
		<td>
			<input type="text" id="message" maxlength="255" />
			<input type="button" onclick="postMessage()" value="Envoyer" id="post" />
                <div id="responsePost" style="display:none"></div>
		</td>
	</tr></table>
</div>
</body>
</html>