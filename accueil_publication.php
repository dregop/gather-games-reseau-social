<?php
// ON DETRUIT LES SESSIONS PROVENANT DE COMMENTAIRE
unset($_SESSION['id_publi']);
unset($_SESSION['id_lieu_publication']);
unset($_SESSION['lieu_page']);
unset($_SESSION['page']);

foreach($tableau_amis as $element)
{
	$requete = $bdd->prepare('SELECT * FROM jeu WHERE id=:id_jeu')
								or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $element))
								or die(print_r($bdd->errorInfo()));
	$donnees_p = $requete->fetch();
	$requete1 = $bdd->prepare('SELECT * , 
								DATE_FORMAT(date_actu, \'%d/%m/%Y � %H:%i \') 
								AS date_affichee 
								FROM actu 
								WHERE id_jeu=:id_jeu')
								or die(print_r($bdd->errorInfo()));
	$requete1->execute(array('id_jeu' => $element))
								or die(print_r($bdd->errorInfo()));
	while($donnees = $requete1->fetch())
	{	
		if ($donnees['actu'] == 'publication')///////////PUBLICATION////////////
		{
			$requete6 = $bdd->prepare('SELECT * ,
										DATE_FORMAT(date_publication, \'%d/%m/%Y � %H:%i \')
										AS date_publication
										FROM publication WHERE id=:id_table')
										or die(print_r($bdd->errorInfo()));
			$requete6->execute(array('id_table' => $donnees['id_table']))
										or die(print_r($bdd->errorInfo()));
			$donnees_t = $requete6->fetch(); 	
			$to_display[$donnees_t['id']] = $donnees_t;
			$to_display[$donnees_t['id']]['auteur'] = $donnees_p;
		}
	} 
}
if(isset($to_display))// SI PAS D'AMIS => PAS DE TABLEAU
{	
krsort($to_display);// classement selon la cl�, donc selon l'id des publications
	
if(isset($_GET['page']) AND $_GET['page'] > 0)
	$fu = $_GET['page']-1;
else
	$fu = 0;

$to_display = array_slice ($to_display, $fu*15, 15, true);

foreach($to_display as $affichage)
{
$r_id = $bdd->prepare('SELECT id 
						FROM actu 
						WHERE id_table=:id_table
						AND actu=:actu')
						or die(print_r($bdd->errorInfo()));
$r_id->execute(array('id_table' => $affichage['id'], 
						'actu' => 'publication'))
						or die(print_r($bdd->errorInfo()));
while($d_id = $r_id->fetch())
{
	if($affichage['id_jeu'] == $affichage['lieu_publication_id_jeu']) 			/// SI LA PUBLICATION EST SUR SON PROFIL
	{
		echo '
		
		<div class="bloc_publication_commentaire" id="'.$affichage['id'].'"> ';
		
		if($affichage['auteur']['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'
			<a  
				href="id'.$affichage['auteur']['id'].'-'.urlencode(stripslashes(htmlspecialchars($affichage['auteur']['pseudo']))).'" 
				title="'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'
			">';
		}
		elseif($affichage['auteur']['id'] == $_SESSION['id_jeu'])
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
		else
			echo'<span  title="Mon sous compte">';
			
		if ($affichage['auteur']['photo_profil'] != 0)
		{	
			$source = getimagesize('images_utilisateurs/'.$affichage['auteur']['photo_profil']); // La photo est la source
			echo'<div class="centre_image60">';
			//[if IE 6]><span class="tampon"></span><![endif]
			if ($source[0] <= 60 AND $source[1] <= 60)	
				echo'<img src="images_utilisateurs/'.$affichage['auteur']['photo_profil'].'" alt="Photo de profil"/>';
			else
				echo'<img src="images_utilisateurs/mini_2_'.$affichage['auteur']['photo_profil'].'" alt="Photo de profil"/>';
				
		   echo'</div>';
		}
		else
			echo'<img class="image_publication" src="images/tete3.png" alt="Photo de profil"/>';
		
		if($affichage['auteur']['nom_de_compte'] != $_SESSION['nom_de_compte']
		OR $affichage['auteur']['id'] == $_SESSION['id_jeu'])
			echo'</a>';
		else
			echo'</span>';
			
		if($affichage['auteur']['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'<a  href="id'.$affichage['auteur']['id'].'-'.urlencode(stripslashes(htmlspecialchars($affichage['auteur']['pseudo']))).'">
					<span class="pseudo_publication">
						'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'
					</span>
				</a>';
		}
		elseif($affichage['auteur']['id'] == $_SESSION['id_jeu'])
		{
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
					<span class="pseudo_publication">
						'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'
					</span>
				</a>';	
		}
		else
		{
			echo'<span class="pseudo_publication" style="color:#102c3c;" title="Mon sous compte">
					'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'
				</span>';
		}

		echo '
		<p class="date_publication">le '.$affichage['date_publication'].'</p>';
		
		echo '<div class="publication">';
		$message = nl2br(stripslashes(htmlspecialchars($affichage['message'])));
		$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);	
		$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
		$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
		$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
		if (preg_match('#\[img\](.+)\[/img\]#', $message))
		{
			$message = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message);			
		}
		elseif (preg_match("#http://(.+)#isU", $message))
		{
			$message = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);					
		}
		elseif (preg_match("#https://(.+)#isU", $message))
		{
			$message = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);
		}
		else
		{
			$message = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$message);
		}
		$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
		$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
		$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
		$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
		$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
		$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
		$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
		$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
		$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
		$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);
	echo'<span class="texte_publication">'.$message.'</span>';		

			// TEXTE + IMAGE
			if ($affichage['photo'] != 0 AND $affichage['photo'] != '')
			{
				$source = getimagesize('images_utilisateurs/'.$affichage['photo']); // La photo est la source
				if ($source[0] <= 900 AND $source[1] <= 600)
				{
					echo '<p class="img_bloc">';
					//[if IE 6]><span class="tampon"></span><![endif]
					echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$affichage['photo'].'">';
				}
				else
				{
					echo '<p class="img_bloc">';
					//[if IE 6]><span class="tampon"></span><![endif]
					echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$affichage['photo'].'">';
				}

				if ($source[0] <= 200 AND $source[1] <= 200)
					echo '<img class="img_publication" src="images_utilisateurs/'.$affichage['photo'].'" /></p></a>'; 
				else		
					echo '<img class="img_publication" src="images_utilisateurs/mini_3_'.$affichage['photo'].'" /></p></a>'; 
			}

			if (isset($affichage['video']) AND $affichage['video'] != '')
			{
				$video = stripslashes(htmlspecialchars($affichage['video']));
				$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
				<object	type="application/x-shockwave-flash" width="500" height="350" data="http://www.youtube.com/v/$1&hl=fr">							
					<param name="wmode" value="transparent" />
					<param name="allowFullScreen" value="true" />
				</object>',$video);
				echo $video;
			}
			
		echo'</div>';
		
	// affichage j'aime 
	$nbre_jaime = 0;
	$like = 0;
	$tableau_pseudo_jaime = array();
	$tableau_id_jaime = array();
	$re_c = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_publication=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_publi' => $affichage['id']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch()) 
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 1;
		}
		$re_c2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $donnees_c['id_jeu']))
		or die(print_r($bdd->errorInfo()));	
        $donnees_c2 = $re_c2->fetch();	
		$tableau_pseudo_jaime[$nbre_jaime] = $donnees_c2['pseudo'];
		$tableau_id_jaime[$nbre_jaime] = $donnees_c['id_jeu'];
		$nbre_jaime++; 
	}
		
	if($like == 0)
		echo'<p> <a id="'.$affichage['id'].'"  class="like">
		<img src="images/pouce.png" title="J\'aime"/></a>';
	else
		echo'<p> <a id="'.$affichage['id'].'"  class="like">
		<img src="images/pouce_vert.png" title="Je n\'aime plus"/></a>';

	if ($nbre_jaime > 1)
		echo ' (<span id="'.$affichage['id'].'" class="nbre_jaime">'.$nbre_jaime.'</span>)';
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] != $_SESSION['id_jeu'])
		echo ' (<a href="profil-i'.$tableau_id_jaime[0].'.html">'.$tableau_pseudo_jaime[0].'</a>)';	
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] == $_SESSION['id_jeu'])
		echo '<span> Vous aimez</span>';		
	// fin affichage j'aime	
				
		///////////////////////////// COMMENTAIRE //////////////////////////////
		echo '<form action="profil_post.php" method="post">';
		echo '<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />';
		echo '<input type="hidden" name="id_publication" value="'.$affichage['id'].'"/>';
		echo '<input type="hidden" name="id_jeu" value="'.$_SESSION['id_jeu'].'"/>';
		echo '<input type="hidden" name="profil_ou_clan" value="profil" />';
		echo '<input type="hidden" name="accueil"/>';
		if(isset($_GET['page']) AND $_GET['page'] != 0)
		{
			echo '<input type="hidden" name="page" value="'.$_GET['page'].'"/>';
		}
		echo '</form></span>';
		
		$reponse = $bdd->prepare('SELECT commentaire, id_jeu ,id, 
								DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
								AS date_commentaire 
								FROM commentaire 
								WHERE id_publication=:id_publication 
								ORDER BY id LIMIT 0,5')
								or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_publication' => $affichage['id']))
								or die(print_r($bdd->errorInfo()));
		$i=0;
		while ($donnees3 = $reponse->fetch()) 									// ON AFFICHE COMMENTAIRE PAR COMMENTAIRE
		{
			$i++;
			$rs = $bdd->prepare('SELECT pseudo, photo_profil, id, nom_de_compte 
								FROM jeu 
								WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
			$rs->execute(array('id' => $donnees3['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			$ds = $rs->fetch();
			echo '
			<div class="bloc_commentaire">';
				if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
				OR $affichage['lieu_publication_id_jeu'] == $_SESSION['id_jeu'] 
				OR $affichage['id_jeu'] == $_SESSION['id_jeu'])
				{
					if(isset($_GET['page']))
					{
						echo'
						<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
							 if (sup == 0)return false;" href="accueil.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$affichage['id'].'&page='.$_GET['page'].'" title="Supprimer le commentaire">
							<div id="supprimer_commentaire_profil"></div>
						</a>';
					}
					else
					{
						echo'
						<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
							 if (sup == 0)return false;" href="accueil.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$affichage['id'].'" title="Supprimer le commentaire">
							<div id="supprimer_commentaire_profil"></div>
						</a>';
					}
				}
				if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				{
					echo'<a href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'" 
							title="'.stripslashes(htmlspecialchars($ds['pseudo'])).'">';
				}
				elseif($ds['id'] == $_SESSION['id_jeu'])
					echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
				else
					echo'<span  title="Mon sous compte">';
					
				if ($ds['photo_profil'] != 0)
				{ 
					$source = getimagesize('images_utilisateurs/'.$ds['photo_profil']); // La photo est la source
					echo'<div class="centre_image40">';
					
					if ($source[0] <= 40 AND $source[1] <= 40)
						echo '<img src="images_utilisateurs/'.$ds['photo_profil'].'" />'; 
					else				
						echo'<img src="images_utilisateurs/mini_4_'.$ds['photo_profil'].'"  />';
						
					echo'</div>';
				}
				else
					echo '<img  class="img_commentaire" src="images/tete2.png" alt="Planete"/>';
					
				if($ds['nom_de_compte'] != $_SESSION['nom_de_compte']
				OR $ds['id'] == $_SESSION['id_jeu'])
					echo'</a>';
				else
					echo'</span>';
					
				if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				{
					echo'<a  href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'">
							<span class="pseudo_commentaire">
								'.stripslashes(htmlspecialchars($ds['pseudo'])).'
							</span>
						</a>';
				}
				elseif($ds['id'] == $_SESSION['id_jeu'])
				{
					echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
							<span class="pseudo_commentaire">
								'.stripslashes(htmlspecialchars($ds['pseudo'])).'
							</span>
						</a>';
				}
				else
				{
					echo'<span class="pseudo_commentaire" style="color:#102c3c;" title="Mon sous compte">
							'.stripslashes(htmlspecialchars($ds['pseudo'])).'
						</span>';
				}
				
				echo'
				<br/>
				<span class="date_commentaire">
					le '.$donnees3['date_commentaire'].'
				</span>';
				
			$com = stripslashes(htmlspecialchars($donnees3['commentaire']));
			
			if (preg_match("#http://(.+)#isU", $com))
			{
				$com = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);					
			}
			elseif (preg_match("#https://(.+)#isU", $com))
			{
				$com = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);
			}
			else
			{
				$com = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$com);
			}
			
			$com = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $com);		
			$com = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $com);	
			$com = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $com);		
			$com = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $com);
			$com = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $com);
			$com = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $com);
			$com = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $com);
			$com = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $com);
			$com = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $com);
			$com = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $com);		
				echo ' <span class="message_commentaire">'.$com.'</span>';

					
			echo '
			</div>';				
		}
		if ($i > 4)
		{
			if(isset($_GET['page']))
			{
				echo '
				<a href="commentaires.php?id_table='.$affichage['id'].'&lieu_publication='.$affichage['lieu_publication_id_jeu'].'&page_accueil='.$_GET['page'].'&lieu_page=accueil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
			else
			{
				echo '
				<a href="commentaires.php?id_table='.$affichage['id'].'&lieu_publication='.$affichage['lieu_publication_id_jeu'].'&lieu_page=accueil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
		}
		echo'
		</div>';
	}
	else // SUR LE PROFIL D'UN AUTRE MAIS ON EST PAS AMI AVEC / ON EST AMI AVEC
	{
		$r_inconnue = $bdd->prepare('SELECT * FROM jeu WHERE id=:id_jeu')
									or die(print_r($bdd->errorInfo()));
		$r_inconnue->execute(array('id_jeu' => $affichage['lieu_publication_id_jeu']))
									or die(print_r($bdd->errorInfo()));
		$donnees_inconnue = $r_inconnue->fetch();
		
		if($donnees_inconnue['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'<a href="id'.$donnees_inconnue['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_inconnue['pseudo']))).'" 
					title="'.stripslashes(htmlspecialchars($donnees_inconnue['pseudo'])).'">';
		}
		elseif($donnees_inconnue['id'] == $_SESSION['id_jeu'])
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
		else
			echo'<span  title="Mon sous compte">';
			
		echo '
		<div class="bloc_publication_commentaire" id="'.$affichage['id'].'"> ';
		// IMAGE DU SOUS COMPTE OU IL Y A EU LA PUBLICATION/////////////////
		echo'<div class="centre_image60_inconnue">';
		
			if ($donnees_inconnue['photo_profil'] != 0)
			{	
				$source = getimagesize('images_utilisateurs/'.$donnees_inconnue['photo_profil']); // La photo est la source
				if ($source[0] <= 60 AND $source[1] <= 60)	
				{
					echo'<img src="images_utilisateurs/'.$donnees_inconnue['photo_profil'].'" 
							alt="Photo de profil"
						 />';
				}
				else
				{
					echo'<img src="images_utilisateurs/mini_2_'.$donnees_inconnue['photo_profil'].'" 
							alt="Photo de profil"
						 />';
				}
			}
			else
				echo'<img src="images/tete3.png" alt="Photo de profil"/>';
				
		echo'</div>';
			
		if($donnees_inconnue['nom_de_compte'] != $_SESSION['nom_de_compte']
		OR $donnees_inconnue['id'] == $_SESSION['id_jeu'])
			echo'</a>';
		else
			echo'</span>';
			
		echo'<img 
				style="float:right;margin-top:10px; margin-right:125px;"
				src="images/fleche_accueil.png" 
				alt=" "
			 />';
			
		if($affichage['auteur']['nom_de_compte']!=$_SESSION['nom_de_compte'])
		{
			echo'<a href="id'.$affichage['auteur']['id'].'-'.urlencode(stripslashes(htmlspecialchars($affichage['auteur']['pseudo']))).'" 
					title="'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'"
				 >';
		}
		elseif($affichage['auteur']['id'] == $_SESSION['id_jeu'])
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
		else
			echo'<span  title="Mon sous compte">';
			
		echo'<div class="centre_image60">';	
		
		if ($affichage['auteur']['photo_profil'] != 0)
		{	
			$source = getimagesize('images_utilisateurs/'.$affichage['auteur']['photo_profil']); // La photo est la source
			if ($source[0] <= 60 AND $source[1] <= 60)						
				echo'<img src="images_utilisateurs/'.$affichage['auteur']['photo_profil'].'" alt="Photo de profil"/>';
			else
				echo'<img src="images_utilisateurs/mini_2_'.$affichage['auteur']['photo_profil'].'" alt="Photo de profil"/>';
		}
		else
			echo'<img src="images/tete3.png" alt="Photo de profil"/>';

		echo'</div>';
		
		if($affichage['auteur']['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'</a>';
		elseif($affichage['auteur']['id'] == $_SESSION['id_jeu'])
			echo'</a>';
		else
			echo'</span>';
			
		if($affichage['auteur']['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'<a  href="id'.$affichage['auteur']['id'].'-'.urlencode(stripslashes(htmlspecialchars($affichage['auteur']['pseudo']))).'">
					<span class="pseudo_publication">
						'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'
					</span>
				</a>';
		}
		elseif($affichage['auteur']['id'] == $_SESSION['id_jeu'])
		{
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
					<span class="pseudo_publication">
						'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'
					</span>
				</a>';	
		}
		else
		{
			echo'<span class="pseudo_publication" style="color:#102c3c;" title="Mon sous compte">
					'.stripslashes(htmlspecialchars($affichage['auteur']['pseudo'])).'
				</span>';
		}

		echo '<p class="date_publication">
				le '.$affichage['date_publication'].'
			  </p>';
		echo'<div class="publication">';
		$message = nl2br(stripslashes(htmlspecialchars($affichage['message'])));
		$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);	
		$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
		$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
		$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
		$message = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message);
		if (preg_match("#\[a\]http://(.+)\[/a\]#isU", $message)
		OR preg_match("#\[a\]https://(.+)\[/a\]#isU", $message) )
		{
			$message = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="$1" target="_blank">$1</a>', $message);
		}
		else
		{
			$message = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="http://$1" target="_blank">$1</a>', $message);
		}
		$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
		$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
		$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
		$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
		$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
		$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
		$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
		$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
		$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
		$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);
	
	echo'<span class="texte_publication">'.$message.'</span>';	

		// TEXTE + IMAGE
		if ($affichage['photo'] != 0 AND $affichage['photo'] != '')
		{
			$source = getimagesize('images_utilisateurs/'.$affichage['photo']); // La photo est la source
			if ($source[0] <= 900 AND $source[1] <= 600)
			{
				echo '<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$affichage['photo'].'">';
			}
			else
			{
				echo'<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$affichage['photo'].'">';
			}

			if ($source[0] <= 200 AND $source[1] <= 200)
			{
				echo '<img class="img_publication" src="images_utilisateurs/'.$affichage['photo'].'" />
					 </a>
					 </p>'; 
			}
			else
			{
				echo '<img class="img_publication" src="images_utilisateurs/mini_3_'.$affichage['photo'].'" />
					  </a>
					  </p>'; 
			}
		}

		if (isset($affichage['video']) AND $affichage['video'] != '')
		{
			$video = stripslashes(htmlspecialchars($affichage['video']));
			$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
			<object	type="application/x-shockwave-flash" width="500" height="350" data="http://www.youtube.com/v/$1&hl=fr">							
				<param name="wmode" value="transparent" />
				<param name="allowFullScreen" value="true" />
			</object>',$video);
			echo $video;
		}
		echo'</div>';
		
	// affichage j'aime 
	$nbre_jaime = 0;
	$like = 0;
	$tableau_pseudo_jaime = array();
	$tableau_id_jaime = array();
	$re_c = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_publication=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_publi' => $affichage['id']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch()) 
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 1;
		}
		$re_c2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $donnees_c['id_jeu']))
		or die(print_r($bdd->errorInfo()));	
        $donnees_c2 = $re_c2->fetch();	
		$tableau_pseudo_jaime[$nbre_jaime] = $donnees_c2['pseudo'];
		$tableau_id_jaime[$nbre_jaime] = $donnees_c['id_jeu'];
		$nbre_jaime++; 
	}
		
	if($like == 0)
		echo'<p> <a id="'.$affichage['id'].'"  class="like">
		<img src="images/pouce.png" title="J\'aime"/></a>';
	else
		echo'<p> <a id="'.$affichage['id'].'"  class="like">
		<img src="images/pouce_vert.png" title="Je n\'aime plus"/></a>';

	if ($nbre_jaime > 1)
		echo ' (<span id="'.$affichage['id'].'" class="nbre_jaime">'.$nbre_jaime.'</span>)';
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] != $_SESSION['id_jeu'])
		echo ' (<a href="profil-i'.$tableau_id_jaime[0].'.html">'.$tableau_pseudo_jaime[0].'</a>)';	
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] == $_SESSION['id_jeu'])
		echo '<span> Vous aimez</span>';		
	// fin affichage j'aime	
		
		$r1_ami = $bdd->prepare('SELECT * FROM amis 
								WHERE id_jeu=:id_jeu 
								AND id_jeu_ami=:id_jeu_ami')
								or die(print_r($bdd->errorInfo()));
		$r1_ami->execute(array('id_jeu' => $donnees_inconnue['id'],
								'id_jeu_ami' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));	
		$d1_ami = $r1_ami->fetch();
		
		$r2_ami = $bdd->prepare('SELECT * FROM amis 
								WHERE id_jeu=:id_jeu_ami 
								AND id_jeu_ami=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$r2_ami->execute(array('id_jeu' => $donnees_inconnue['id'],
								'id_jeu_ami' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));	
		$d2_ami = $r2_ami->fetch(); 
		
		if ($d2_ami['id_jeu'] != '' OR $d1_ami['id_jeu'] != '' 
		OR $affichage['lieu_publication_id_jeu'] == $_SESSION['id_jeu']) 		// CONDITION POUR AFFICHER LE BLOC POUR COMMENTER SI ON EST AMI
		{
			// COMMENTAIRE
			echo '<form action="profil_post.php" method="post">';
			echo '<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />';
			echo '<input type="hidden" name="id_publication" value="'.$affichage['id'].'"/>';
			echo '<input type="hidden" name="id_jeu" value="'.$_SESSION['id_jeu'].'"/>';
			echo '<input type="hidden" name="profil_ou_clan" value="profil" />';
			if(isset($_GET['page']) AND $_GET['page'] != 0)
			{
				echo '<input type="hidden" name="page" value="'.$_GET['page'].'"/>';
			}
			echo '<input type="hidden" name="accueil"/>';
			echo '</form></span>';
		}	
		$reponse = $bdd->prepare('SELECT id,commentaire, id_jeu , 
								DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
								AS date_commentaire 
								FROM commentaire 
								WHERE id_publication=:id_publication 
								ORDER BY id LIMIT 0,5')
								or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_publication' => $affichage['id']))
								or die(print_r($bdd->errorInfo()));
		$i=0;
		while ($donnees3 = $reponse->fetch()) 									// ON AFFICHE COMMENTAIRE PAR COMMENTAIRE
		{
			$i++;
			$rs = $bdd->prepare('SELECT * FROM jeu 
								WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
			$rs->execute(array('id' => $donnees3['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			$ds = $rs->fetch();
			echo '
			<div class="bloc_commentaire">';
			
			if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
			OR $affichage['lieu_publication_id_jeu'] == $_SESSION['id_jeu'] 
			OR $affichage['id_jeu'] == $_SESSION['id_jeu'])
			{
				if(isset($_GET['page']))
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						 if (sup == 0)return false;" href="accueil.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$affichage['id'].'&page='.$_GET['page'].'" title="Supprimer le commentaire">
						<div id="supprimer_commentaire_profil"></div>
					</a>';
				}
				else
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						 if (sup == 0)return false;" href="accueil.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$affichage['id'].'" title="Supprimer le commentaire">
						<div id="supprimer_commentaire_profil"></div>
					</a>';
				}
			}
			
				if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				{
					echo'<a href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'" 
							title="'.stripslashes(htmlspecialchars($ds['pseudo'])).'"
						 >';
				}
				elseif($ds['id'] == $_SESSION['id_jeu'])
					echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
				else
					echo'<span  title="Mon sous compte">';
					
				if ($ds['photo_profil'] != 0)
				{ 
					$source = getimagesize('images_utilisateurs/'.$ds['photo_profil']); // La photo est la source
					echo'<div class="centre_image40">';
					
					if ($source[0] <= 40 AND $source[1] <= 40)
						echo '<img src="images_utilisateurs/'.$ds['photo_profil'].'" />'; 
					else				
						echo'<img src="images_utilisateurs/mini_4_'.$ds['photo_profil'].'" />';
						
					echo'</div>';
				}
				else
					echo '<img  class="img_commentaire" src="images/tete2.png" alt="Planete"/>';
					
				if($ds['nom_de_compte'] != $_SESSION['nom_de_compte']
				OR $ds['id'] == $_SESSION['id_jeu'])
					echo'</a>';
				else
					echo'</span>';
					
				if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				{
					echo'<a  href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'">
							<span class="pseudo_commentaire">
								'.stripslashes(htmlspecialchars($ds['pseudo'])).'
							</span>
						</a>';
				}
				elseif($ds['id'] == $_SESSION['id_jeu'])
				{
					echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
							<span class="pseudo_commentaire">
								'.stripslashes(htmlspecialchars($ds['pseudo'])).'
							</span>
						</a>';
				}
				else
				{
					echo'<span class="pseudo_commentaire" style="color:#102c3c;" title="Mon sous compte">
							'.stripslashes(htmlspecialchars($ds['pseudo'])).'
						</span>';
				}
				
				echo'
				<br/>
				<span class="date_commentaire">
					le '.$donnees3['date_commentaire'].'
				</span>';
				$com = stripslashes(htmlspecialchars($donnees3['commentaire']));		
				if (preg_match("#http://(.+)#isU", $com))
				{
					$com = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);					
				}
				elseif (preg_match("#https://(.+)#isU", $com))
				{
					$com = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);
				}
				else
				{
					$com = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$com);
				}
				
				$com = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $com);		
				$com = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $com);	
				$com = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $com);		
				$com = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $com);
				$com = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $com);
				$com = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $com);
				$com = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $com);
				$com = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $com);
				$com = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $com);
				$com = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $com);				
				echo ' <span class="message_commentaire">'.$com.'</span>';
	
			echo '
			</div>';				
		}
		if ($i > 4)
		{
			if(isset($_GET['page']))
			{
				echo '
				<a href="commentaires.php?id_table='.$affichage['id'].'&lieu_publication='.$affichage['lieu_publication_id_jeu'].'&page_accueil='.$_GET['page'].'&lieu_page=accueil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
			else
			{
				echo '
				<a href="commentaires.php?id_table='.$affichage['id'].'&lieu_publication='.$affichage['lieu_publication_id_jeu'].'&lieu_page=accueil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
		}		
		echo'
		</div>';
	}
}
}// FIN FOREACH
}// FIN IF
elseif(empty($to_display) AND $i > 0) // TABLEAU VIDE MAIS IL A DES AMIS
{
	echo '
	<div id="demande_notification2" 
		style="background-image: url(images/fond_sombre.png);margin-top:50px;">
		Aucune publication venant de vos amis.
	</div>';
}
// GESTION AFFICHAGE PAGES
echo'<p class="nombre_page">';
$addition = 0;
foreach($tableau_amis as $element)
{
	$nbre_page = 1;
	$p = $bdd->prepare('SELECT COUNT(*) 
						AS nbre_publi 
						FROM actu 
						WHERE id_jeu=:id 
						AND actu=:actu')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id' => $element, 'actu' => 'publication'))
						or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$addition = $addition + $do['nbre_publi'];
	$p->closeCursor();	
}
	$nbr_entrees = $addition; //LE RESULTAT DE LA BOUCLE (AVEC ADDITION)

if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
$nom_page = 'accueil';
$nbr_affichage = 15;

include('pagination.php');
?>