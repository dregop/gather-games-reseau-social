<?php session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr? tout
        die('Erreur : '.$e->getMessage());
}
if (isset($_GET['id_publication']))
{
	$like = 1;
	$re_c = $bdd->prepare('SELECT id_jeu,id FROM jaime WHERE id_publication=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_publi' => $_GET['id_publication']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch())
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 0;
			$id = $donnees_c['id'];
			break;
		}
	}	
	if ($like == 0)
	{
		$req3 = $bdd->prepare('DELETE FROM jaime
							WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$req3->execute(array('id' => $id)) or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requête
		//notif
		$req3 = $bdd->prepare('DELETE FROM notifications
		WHERE id_jeu=:id_jeu AND id_publication=:id_publi AND jaime=:jaime') 
							or die(print_r($bdd->errorInfo()));
		$req3->execute(array('id_jeu' => $_SESSION['id_jeu'],
							 'id_publi' => $_GET['id_publication'],
							 'jaime' => 1)) 
							 or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requête
	}
	else
	{
		$re_c2 = $bdd->prepare('INSERT INTO jaime (id_jeu, id_publication) 
								VALUES(:id_jeu,:id_publication)')
								or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $_SESSION['id_jeu'], 
								'id_publication' => $_GET['id_publication']))
								or die(print_r($bdd->errorInfo()));
		//notif
		$re_publi = $bdd->prepare('SELECT id_jeu FROM publication WHERE id=:id_publi')
		or die(print_r($bdd->errorInfo()));
		$re_publi->execute(array('id_publi' => $_GET['id_publication']))
		or die(print_r($bdd->errorInfo()));
		$donnees_publi = $re_publi->fetch();
		if ($donnees_publi['id_jeu'] != $_SESSION['id_jeu'])
		{
			$re_notif = $bdd->prepare('INSERT INTO notifications 
			(id_jeu,id_jeu_publi,id_publication,jaime,date_notification,view) 
			VALUES(:id_jeu,:id_jeu_publi,:id_publication,:jaime,NOW(),:view)')
									or die(print_r($bdd->errorInfo()));
			$re_notif->execute(array('id_jeu' => $_SESSION['id_jeu'], 
								'id_jeu_publi' => $donnees_publi['id_jeu'],
								'id_publication' => $_GET['id_publication'],
								'jaime' => 1,
								'view' => 1))or die(print_r($bdd->errorInfo()));
		}
	}
	echo $like;
}
if (isset($_GET['id_communaute']))
{
	// il faut aussi créer la notif ici
	$like = 1;
	$re_c = $bdd->prepare('SELECT id_jeu,id FROM jaime WHERE id_communaute=:id_commu')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_commu' => $_GET['id_communaute']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch())
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 0;
			$id = $donnees_c['id'];
			break;
		}
	}	
	if ($like == 0)
	{
		$req3 = $bdd->prepare('DELETE FROM jaime
							WHERE id=:id') or die(print_r($bdd->errorInfo()));
		$req3->execute(array('id' => $id)) or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requête
	}
	else
	{
		$re_c2 = $bdd->prepare('INSERT INTO jaime (id_jeu, id_communaute) 
								VALUES(:id_jeu,:id_communaute)')
								or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $_SESSION['id_jeu'], 
								'id_communaute' => $_GET['id_communaute']))
								or die(print_r($bdd->errorInfo()));
	}
	echo $like;
}
	
if (isset($_GET['nbre_jaime']))
{
	$json['message'] = '';
	$rep = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_publication=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$rep->execute(array('id_publi' => $_GET['nbre_jaime']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_p = $rep->fetch())
	{
		$rep2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$rep2->execute(array('id_jeu' => $donnees_p['id_jeu']))
		or die(print_r($bdd->errorInfo()));
		$donnees_p2 = $rep2->fetch();
		if ($donnees_p['id_jeu'] == $_SESSION['id_jeu'])
			$json['message'] .= 'Vous, ';
		else
			$json['message'] .= '<a href="profil-i'.$donnees_p['id_jeu'].'.html" >'.$donnees_p2['pseudo'].'</a>, ';		
	}
	
	$json['message'] .= 'f';
	$json['message'] = str_replace(', f', '', $json['message']);
	if (preg_match("#Vous#", $json['message'])) 
		$json['message'] .= ' aimez ça.';
	else
		$json['message'] .= ' aiment ça.';
	echo json_encode($json);
}
if (isset($_GET['nbre_jaime_commu']))
{
	$json['message'] = '';
	$rep = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_communaute=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$rep->execute(array('id_publi' => $_GET['nbre_jaime_commu']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_p = $rep->fetch())
	{
		$rep2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$rep2->execute(array('id_jeu' => $donnees_p['id_jeu']))
		or die(print_r($bdd->errorInfo()));
		$donnees_p2 = $rep2->fetch();
		if ($donnees_p['id_jeu'] == $_SESSION['id_jeu'])
			$json['message'] .= 'Vous, ';
		else
			$json['message'] .= '<a href="profil-i'.$donnees_p['id_jeu'].'.html" >'.$donnees_p2['pseudo'].'</a>, ';		
	}
	
	$json['message'] .= 'f';
	$json['message'] = str_replace(', f', '', $json['message']);
	if (preg_match("#Vous#", $json['message'])) 
		$json['message'] .= ' aimez ça.';
	else
		$json['message'] .= ' aiment ça.';
	echo json_encode($json);
}