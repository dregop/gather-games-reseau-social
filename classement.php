<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (empty($_SESSION['pseudo']))
{
	header('Location: informations-e1.html');
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
								WHERE nom_de_compte=:ndc')
								or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
								or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');

	$reqs = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $reqs->fetch();
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />	
 </head>
   
<body>
<!--------------------- OVERLAY PROCHAINEMENT---------------------->
<div id="grand_overlay4"></div>
<div id="overlay_prochainement"> 
	<a href="profil.html">
		<img id="" src="images/prochainement.png" alt=" PROCHAINEMENT "/>
	</a>
</div>
<!--------------------- FIN OVERLAY PROCHAINEMENT---------------------->
<?php
include('menu_sans_doctype.php');
?>
<div id="corps_classement">
	
	<div id="bloc_geth">
		<p style="display:inline;font-size:1.8em;vertical-align:middle;">
<?php
echo''.$donnees_jeu['geth'].'';		
?>
		</p>
		<img id="geth_classement" src="images/geth_classement.png" alt="Geth"/>
		
		<span style="vertical-align:middle; margin-left:100px;"> 
			Vous pouvez acheter diff�rents titres et embl�mes avec vos Geths 
			sur le 
		</span> 
		<a href="g-store.html">
			<img style="vertical-align:middle;" src="images/g_store.png" alt="G-Store"/>
		</a>
		

	</div>

 <div id="bloc_gagner">
	<div id="bloc_premier">
	 <span id="premiers_pas"> Premiers pas </span>
	 <p class="sous_categorie" style="margin-top:30px;"> Premi�re connexion</p>
	 <p class="sous_categorie"> Premier sous compte</p>
	 <p class="sous_categorie"> Premier ami</p>
	 <p class="sous_categorie"> 
		Premi�re cr�ation de clan ou �tre membre d'un 
		clan pour la premi�re fois
	 </p>
	 <p class="sous_categorie"> 
		Premi�re publication sur le profil de votre 
		clan
	 </p>
	 <p class="sous_categorie"> Premi�re publication sur votre profil</p>
	 <p class="sous_categorie"> Premi�re publication sur le profil d'un ami</p>
	 <p class="sous_categorie"> 
		Premi�re publication dans les actualit�s de la communaut�
	 </p>
	 <p class="sous_categorie"> Premier commentaire</p>
	 <p class="sous_categorie"> Premier message</p>
	</div>
	
	<div id="bloc_compteur">
	 <span id="compteur"> Compteur </span>
	  <p class="sous_categorie"> 5 amis</p>
	  <p class="sous_categorie"> 10 amis</p>
	  <p class="sous_categorie"> 20 amis</p>
	  <p class="sous_categorie"> 50 amis</p>
	  <p class="sous_categorie"> 100 amis</p>
	  <p class="sous_categorie"> 200 amis</p>
	  <p class="sous_categorie"> 300 amis</p>
	  <p class="sous_categorie"> 500 amis</p>
	  <p class="sous_categorie"> 1000 amis</p>
	  <p class="sous_categorie"> 2 sous comptes</p>
	  <p class="sous_categorie"> 5 sous comptes</p>
	  <p class="sous_categorie"> 10 commentaires</p>
	  <p class="sous_categorie"> 10 publications sur son profil</p>
	  <p class="sous_categorie"> 
		10 publications sur le profil de la communaut�
	  </p>
	  <p class="sous_categorie">
		1 publication sur le profil de 10 amis diff�rents
	  </p>
	</div>
	
	<div id="bloc_journalier">
	 <span id="journalier"> Action journali�re </span>
	 <p class="sous_categorie"> Connexion journali�re</p>
	 <p class="sous_categorie"> publication journali�re sur son profil</p>
	</div>
 </div>
</div>	
<?php
include('pied_page.php'); 
?>