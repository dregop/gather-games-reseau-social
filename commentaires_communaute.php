<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}	
// LE MIEU EST DE FAIRE UNE SESSION POUR EVITER DE TOUT RENVOYER PAR URL QUAND
// LA PERSONNE COMMENTE SUR CETTE PAGE.	
if (isset($_GET['id_table']))
{ 
	$_SESSION['id_publi'] = $_GET['id_table'];
	
	if(isset($_GET['lieu_page']))
	{
		$_SESSION['lieu_page'] = $_GET['lieu_page'];
	}
	
	if(isset($_GET['page_communaute']) AND $_GET['page_communaute'] !=0)
	{
			$_SESSION['page'] = $_GET['page_communaute'];
	}
	header('Location: communaute-com.html');
}
// ON SUPPRIME LES COMMENTAIRES ////////////////////////////////////////////////
if(isset($_GET['id_publication'],$_GET['supprimer_commentaire'],
$_GET['id_commentaire']))
{
	$reqs_c = $bdd->prepare('SELECT id_jeu, date_commentaire 
							FROM commentaire WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_c->execute(array('id' => $_GET['id_commentaire']))
							or die(print_r($bdd->errorInfo()));
	$donnees_comm = $reqs_c->fetch();
	$reqs_p = $bdd->prepare('SELECT lieu_publication_id_jeu 
							FROM publication WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_p->execute(array('id' => $_GET['id_publication']))
							or die(print_r($bdd->errorInfo()));
	$donnees_publi = $reqs_p->fetch();

	if(isset($donnees_comm['id_jeu']) 
	AND $donnees_comm['id_jeu'] == $_SESSION['id_jeu'] 
	OR $donnees_publi['lieu_publication_id_jeu'] == $_SESSION['id_jeu'])
	{
		$req1 = $bdd->prepare('DELETE FROM commentaire WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id' => $_GET['id_commentaire'])) 
							or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		$req3 = $bdd->prepare('DELETE FROM notifications 
							WHERE date_notification=:date_notification 
							AND id_publication=:id_publication')
							or die(print_r($bdd->errorInfo()));
		$req3->execute(array('date_notification' => $donnees_comm['date_commentaire'], 
							'id_publication' => $_GET['id_publication'])) 
							or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requ�te
	}
	header('Location: communaute-com.html');
}
////////////////////////////////////////////////////////////////////////////////

include('menu.php');
	echo '<div id="corps_recherche">';

if (isset($_SESSION['id_publi']))// ON VERIFIE BIEN QU'IL Y AIT ID DE LA PUBLI.
{

	if(isset($_SESSION['page']))
	{
		echo'
		<a href="communaute-p'.$_SESSION['page'].'.html#'.$_SESSION['id_publi'].'">
			<div id="retour_commentaire" title="retour"></div>
		</a>';
	}
	else
	{
		echo'
		<a href="communaute.html#'.$_SESSION['id_publi'].'">
			<div id="retour_commentaire" title="retour"></div>
		</a>';
	}

		
$requete = $bdd->prepare('SELECT *, 
						DATE_FORMAT(date_message, \'%d/%m/%Y � %H:%i \') 
						AS date_message FROM communaute 
						WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$requete->execute(array('id' => $_SESSION['id_publi']))
						or die(print_r($bdd->errorInfo()));
$donnees = $requete->fetch();

	$r = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$r->execute(array('id' => $donnees['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$d = $r->fetch(); 
	echo '
	<div class="bloc_publication_communaute" id="'.$donnees['id'].'"> ';
	
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'
			<a  href="profil-i'.$d['id'].'.html">
				<p class="pseudo_publication_communaute">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</p>
			</a>';
		}
		elseif($d['id'] == $_SESSION['id_jeu'])
		{
			echo'
			<a  href="profil.html" title="Mon profil" >
				<p class="pseudo_publication_communaute">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</p>
			</a>';
		}
		else
		{
			echo'
			<p class="pseudo_publication_communaute" style="color:#102c3c;" title="Mon sous compte">
				'.stripslashes(htmlspecialchars($d['pseudo'])).'
			</p>';
		}
		
		echo'<p class="date_publication_communaute">
				le '.$donnees['date_message'].'
			</p>';
		
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'<a  href="profil-i'.$d['id'].'.html" title="'.stripslashes(htmlspecialchars($d['pseudo'])).'">';
		elseif($d['id'] == $_SESSION['id_jeu'])
			echo'<a  href="profil.html" title="Mon profil">';
		else
			echo'<span  title="Mon sous compte">';
			
		if ($d['photo_profil'] != 0)
		{
			$source = getimagesize('images_utilisateurs/'.$d['photo_profil']); 	// La photo est la source			
		   echo'
		   <div class="centre_image60">';
		   
			if ($source[0] <= 60 AND $source[1] <= 60)						
				echo'<img src="images_utilisateurs/'.$d['photo_profil'].'" alt="Photo de profil"/>';
			else
				echo'<img src="images_utilisateurs/mini_2_'.$d['photo_profil'].'" alt="Photo de profil"/>';
				
		   echo'
		   </div>';
		}
		else
			echo'<img  class="image_publication" src="images/tete3.png" alt="Photo de profil"/>';
		
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'</a>';
		elseif($d['id'] == $_SESSION['id_jeu'])
			echo'</a>';
		else
			echo'</span>';
		
		echo ' 
		<div class="info_joueur_communaute">
			<p>
				Jeu : '; 
				echo substr(stripslashes(htmlspecialchars($d['nom_jeu'])), 0, 20).'';
				if(strlen(stripslashes(htmlspecialchars($d['nom_jeu']))) > 20)
				{ 
					echo'...';
				} 
			echo'
			</p>
			
			<p>
				Plateforme : '.$d['plateforme'].'
			</p>
			
		</div>';
		echo '
		<div class="publication">';
		
		$message = nl2br(stripslashes(htmlspecialchars($donnees['message'])));
		$message = preg_replace('#&list=[a-zA-Z0-9._/\-&\?\=]+#', '',$message);
		$message = preg_replace('#&feature=[a-zA-Z0-9._/\-&\?\=]+#', '',$message);
		$message = preg_replace('#http://[a-zA-Z0-9._/\-&\?\=%;]+#i', '<a href="$0">$0</a>',$message);	
		$message = preg_replace('#https://[a-zA-Z0-9._/\-&\?\=%;]+#i', '<a href="$0">$0</a>',$message);
		$message = preg_replace('#http://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
		<object	type="application/x-shockwave-flash" width="500" height="320" data="http://www.youtube.com/v/$1&hl=fr">							
			<param name="wmode" value="transparent" />
		</object>',$message);
		$message = preg_replace('#https://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
		<object	type="application/x-shockwave-flash" width="500" height="320" data="http://www.youtube.com/v/$1&hl=fr">							
			<param name="wmode" value="transparent" />
		</object>',$message);		
		if(preg_match('#<object#', $message))
		{
			$message = preg_replace('#">#', '',$message);
		}
		$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);	
			echo'<span class="texte_publication">'.$message.'</span>';	
			
		// TEXTE + IMAGE
		if ($donnees['photo'] != 0 AND $donnees['photo'] != '')
		{
			$source = getimagesize('images_utilisateurs/'.$donnees['photo']); 	// La photo est la source
			if ($source[0] <= 900 AND $source[1] <= 600)
			{
				echo '
				<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donnees['photo'].'">';
			}
			else
			{
				echo '
				<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$donnees['photo'].'">';
			}
		
			if ($source[0] <= 200 AND $source[1] <= 200)
			{
				echo '
						<img class="img_publication" src="images_utilisateurs/'.$donnees['photo'].'" />
					</a>
				</p>'; 
			}
			else	
			{
				echo '
						<img class="img_publication" src="images_utilisateurs/mini_3_'.$donnees['photo'].'" />
					</a>
				</p>'; 
			}
		}	
		echo'
		</div>';
		
			// !!!!COMMENTAIRE!!!! //
		echo '
		<form action="communaute_post.php" method="post">';
		echo'<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />';
		echo'<input type="hidden" name="id_publication_communaute" value="'.$donnees['id'].'"/>';
		echo'<input type="hidden" name="commentaires_communaute"/>';
		if(isset($_GET['page']) AND $_GET['page'] != 0)
		{
			echo '
			 <input type="hidden" name="page" value="'.$_GET['page'].'"/>';
		}	
		echo'
		</form>';
		
		if (isset($_GET['page']) AND $_GET['page'] > 0)
		{
			$numero_page = $_GET['page'];
			$numero_page--;
			$numero_page = 20*$numero_page;
		}
		else
			$numero_page = 0;
	
		$reponse = $bdd->prepare('SELECT id, commentaire, id_jeu , 
								DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
								AS date_commentaire FROM commentaire 
								WHERE id_publication_communaute=:id_publication_communaute 
								ORDER BY id LIMIT '.$numero_page.',20')
								or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_publication_communaute' => $donnees['id']))
								or die(print_r($bdd->errorInfo()));
		$i=0;	
		while ($donnees3 = $reponse->fetch()) 									// ON AFFICHE COMMENTAIRE PAR COMMENTAIRE
		{
			$i++;
			$rs = $bdd->prepare('SELECT pseudo, photo_profil, id, nom_de_compte 
								FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
			$rs->execute(array('id' => $donnees3['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			$ds = $rs->fetch();
			echo '
			<div class="bloc_commentaire">';
			if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
			OR $donnees['id_jeu'] == $_SESSION['id_jeu'])
			{
				if(isset($_GET['page']))
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="communaute_post.php?page='.$_GET['page'].'&id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication_communaute='.$donnees['id'].'&commentaires_communaute" 
						title="Supprimer le commentaire">
						
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
				else
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="communaute_post.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication_communaute='.$donnees['id'].'&commentaires_communaute" 
						title="Supprimer le commentaire">
						
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
			}
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'<a  href="profil-i'.$ds['id'].'.html" title="'.stripslashes(htmlspecialchars($ds['pseudo'])).'">';
			elseif($ds['id'] == $_SESSION['id_jeu'])
				echo'<a  href="profil.html" title="Mon profil">';
			else
				echo'<span  title="Mon sous compte">';
				
			if ($ds['photo_profil'] != 0)
			{ 
				$source = getimagesize('images_utilisateurs/'.$ds['photo_profil']); // La photo est la source
				echo'
				<div class="centre_image40">';
				
				if ($source[0] <= 40 AND $source[1] <= 40)
					echo '<img src="images_utilisateurs/'.$ds['photo_profil'].'"/>'; 
				else				
					echo'<img src="images_utilisateurs/mini_4_'.$ds['photo_profil'].'"/>';
					
				echo'
				</div>';
			}
			else
				echo '<img  class="img_commentaire" src="images/tete2.png" alt="Planete"/>';
			
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'</a>';
			elseif($ds['id'] == $_SESSION['id_jeu'])
				echo'</a>';
			else
				echo'</span>';
	
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'
				<a  href="profil-i'.$ds['id'].'.html">
					<span class="pseudo_commentaire">
						'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a>';
			}
			elseif($ds['id'] == $_SESSION['id_jeu'])
			{
				echo'
				<a href="profil.html" title="Mon profil" >
					<span class="pseudo_commentaire">
						'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a>';
			}
			else
			{
				echo'
				<span class="pseudo_commentaire" style="color:#102c3c;" title="Mon sous compte">
					'.stripslashes(htmlspecialchars($ds['pseudo'])).'
				</span>';
			}
			
			echo'
			<br/>
			<span class="date_commentaire" style="color:#8a8989;">
				le '.$donnees3['date_commentaire'].'
			</span>';
			
			$com = stripslashes(htmlspecialchars($donnees3['commentaire']));				
			$com = preg_replace('#http://[a-zA-Z0-9._/\-&\?\=]+#i', '<a href="$0">$0</a>',$com);					
			$com = preg_replace('#https://[a-zA-Z0-9._/\-&\?\=]+#i', '<a href="$0">$0</a>',$com);				
			echo '<span class="message_commentaire">'.$com.'</span>';	
			echo '
			</div>';				
		}
		
// GESTION AFFICHAGE PAGES /////////////////////////////////////////////////////
	
$nbre_page = 1;
$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM commentaire 
					WHERE id_publication_communaute=:id_publication ')
					or die(print_r($bdd->errorInfo()));
$p->execute(array('id_publication' => $_SESSION['id_publi']))
					or die(print_r($bdd->errorInfo()));
$do = $p->fetch();
$nbr_entrees = $do['nbre_publi'];
$p->closeCursor();
		
if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
$nom_page = 'communaute-com';
$nbr_affichage = 20;

include('pagination.php');

	echo '
	</div>';
}
?>
</div>
<?php
include('pied_page.php'); 
?>
	