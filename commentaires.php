<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}	
// LE MIEU EST DE FAIRE UNE SESSION POUR EVITER DE TOUT RENVOYER PAR URL QUAND
// LA PERSONNE COMMENTE SUR CETTE PAGE.	
if (isset($_GET['id_table']))
{ 
	$_SESSION['id_publi'] = $_GET['id_table'];
	
	if(isset($_GET['lieu_publication']))
	{
		$_SESSION['id_lieu_publication'] = $_GET['lieu_publication'];
	}
	
	if(isset($_GET['lieu_page']))
	{
		$_SESSION['lieu_page'] = $_GET['lieu_page'];
		
		if(isset($_GET['page_profil']) AND $_GET['page_profil'] !=0)
		{
			$_SESSION['page'] = $_GET['page_profil'];
		}

		if(isset($_GET['id_ami']))
		{
			$_SESSION['id_ami'] = $_GET['id_ami'];
		}
		
		if(isset($_GET['pseudo_ami']))
		{
			$_SESSION['pseudo_ami'] = $_GET['pseudo_ami'];
		}
		
		if(isset($_GET['page_clan']) AND $_GET['page_clan'] !=0)
		{
			$_SESSION['page'] = $_GET['page_clan'];
		}
		
		if(isset($_GET['id_clan']))
		{
			$_SESSION['id_clan'] = $_GET['id_clan'];
		}
		
		if(isset($_GET['id_clan_get']))
		{
			$_SESSION['id_clan_get'] = $_GET['id_clan_get'];
		}
		
		if(isset($_GET['page_accueil']) AND $_GET['page_accueil'] !=0)
		{
			$_SESSION['page'] = $_GET['page_accueil'];
		}
	}
	header('Location: commentaires.html');
}
if(!isset($_SESSION['id_publi']))
	header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
	
// ON SUPPRIME LES COMMENTAIRES ////////////////////////////////////////////////
if(isset($_GET['id_publication'],$_GET['supprimer_commentaire'],
$_GET['id_commentaire']))
{
	$reqs_c = $bdd->prepare('SELECT id_jeu, date_commentaire FROM commentaire 
							WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_c->execute(array('id' => $_GET['id_commentaire']))
							or die(print_r($bdd->errorInfo()));
	$donnees_comm = $reqs_c->fetch();
	
	$reqs_p = $bdd->prepare('SELECT lieu_publication_id_jeu FROM publication 
							WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_p->execute(array('id' => $_GET['id_publication']))
							or die(print_r($bdd->errorInfo()));
	$donnees_publi = $reqs_p->fetch();

	if(isset($donnees_comm['id_jeu']) 
	AND $donnees_comm['id_jeu'] == $_SESSION['id_jeu'] 
	OR $donnees_publi['lieu_publication_id_jeu'] == $_SESSION['id_jeu'])
	{
		$req1 = $bdd->prepare('DELETE FROM commentaire WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id' => $_GET['id_commentaire'])) 
							or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		
		$req3 = $bdd->prepare('DELETE FROM notifications 
							WHERE date_notification=:date_notification 
							AND id_publication=:id_publication')
							or die(print_r($bdd->errorInfo()));
		$req3->execute(array('date_notification' => $donnees_comm['date_commentaire'], 
							'id_publication' => $_GET['id_publication']))
							or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requ�te
	}
	
	if(isset($_GET['page']))
		header('Location:commentaires-p'.$_GET['page'].'.html#'.$_GET['id_publication'].'');
	else
		header('Location:commentaires.html#'.$_GET['id_publication'].'');
}
////////////////////////////////////////////////////////////////////////////////

include('menu.php');
	echo '<div id="corps_recherche">';
   
		
if (isset($_SESSION['id_publi']))// ON VERIFIE BIEN QU'IL Y AIT ID DE LA PUBLI.
{
	// GERER TOUS LES RETOURS EN FONCTION D'OU PROVIENT LA PUBLICATION

	// CELA VIENT D'UNE PAGE PROFIL
	if(isset($_SESSION['lieu_page']) AND $_SESSION['lieu_page'] == 'profil')
	{
		if(isset($_SESSION['id_ami']))// VENANT DU PROFIL D'UN AMI
		{
			if(isset($_SESSION['page']))
			{
				echo'
				<a href="p'.$_SESSION['page'].'id'.$_SESSION['id_ami'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo_ami']))).'#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
			else
			{
				echo'
				<a href="id'.$_SESSION['id_ami'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo_ami']))).'#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
		}
		else // VENANT DE NOTRE PROFIL
		{
			if(isset($_SESSION['page']))
			{
				echo'
				<a href="p'.$_SESSION['page'].'id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
			else
			{
				echo'
				<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
		}
	}
	elseif(isset($_SESSION['lieu_page']) AND $_SESSION['lieu_page'] == 'clan')
	{
		if(isset($_SESSION['id_clan_get']))// VENANT D'UN AUTRE CLAN
		{
			if(isset($_SESSION['page']))
			{
				echo'
				<a href="team-i'.$_SESSION['id_clan'].'-p'.$_SESSION['page'].'.html#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
			else
			{
				echo'
				<a href="team-i'.$_SESSION['id_clan'].'.html#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
		}
		else // VENANT DE NOTRE CLAN
		{
			if(isset($_SESSION['page']))
			{
				echo'
				<a href="team-p'.$_SESSION['page'].'.html#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
			else
			{
				echo'
				<a href="team.html#'.$_SESSION['id_publi'].'">
					<div id="retour_commentaire" title="retour"></div>
				</a>';
			}
		}
	}
	elseif(isset($_SESSION['lieu_page']) AND $_SESSION['lieu_page'] =='accueil')
	{
		if(isset($_SESSION['page']))
		{
			echo'
			<a href="accueil-p'.$_SESSION['page'].'.html#'.$_SESSION['id_publi'].'">
				<div id="retour_commentaire" title="retour"></div>
			</a>';
		}
		else
		{
			echo'
			<a href="accueil.html#'.$_SESSION['id_publi'].'">
				<div id="retour_commentaire" title="retour"></div>
			</a>';
		}
	}

	// FIN DE TOUS LES RETOUR POSSIBLES DANS COMMENTAIRE
	
	$req = $bdd->prepare('SELECT id_jeu, message, photo, video, id, 
						DATE_FORMAT(date_publication, \'%d/%m/%Y � %Hh %imin \') 
						AS date_publication FROM publication 
						WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array( 'id' => $_SESSION['id_publi']))
						or die(print_r($bdd->errorInfo()));	
	$donnees2 = $req->fetch(); // ON AFFICHE LE MESSAGE
	
	$r = $bdd->prepare('SELECT nom_de_compte, pseudo, photo_profil,id 
						FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$r->execute(array('id' => $donnees2['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$d = $r->fetch(); 
		echo '
	<div class="bloc_publication_commentaire" id="'.$donnees2['id'].'"> ';
		
		//GERER LES LIENS SI C'EST UN SOUS COMPTE OU PAS ///////////////////////
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">';
		elseif($d['id'] == $_SESSION['id_jeu'])
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">';
		else
			echo'<span  title="Mon sous compte">';
			
		if ($d['photo_profil'] != 0)
		{
			$source = getimagesize('images_utilisateurs/'.$d['photo_profil']); 	// La photo est la source
			
			echo'<div class="centre_image60">';
			
			if ($source[0] <= 60 AND $source[1] <= 60)						
				echo'<img src="images_utilisateurs/'.$d['photo_profil'].'" alt="Photo de profil"/>';
			else
				echo'<img src="images_utilisateurs/mini_2_'.$d['photo_profil'].'" alt="Photo de profil"/>';
			
			echo'</div>';
		}
		else
			echo'<img  class="image_publication" src="images/tete3.png" alt="Photo de profil"/>';
			
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'</a>
			<a href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">
				<span class="pseudo_publication">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</span>
			</a> ';
		}
		elseif($d['id'] == $_SESSION['id_jeu'])
		{
			echo'</a>
			<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">
				<span class="pseudo_publication">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</span>
			</a> ';
		}
		else
		{
			echo'</span>
			<span class="pseudo_publication" title="Mon sous compte" style="color : #102c3c;" >
				'.stripslashes(htmlspecialchars($d['pseudo'])).'
			</span>';
		}

		echo'
		<p class="date_publication">
			le '.$donnees2['date_publication'].'
		</p>';
		
		echo '<div class="publication">';
		$message = nl2br(stripslashes(htmlspecialchars($donnees2['message'])));
		$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);	
		$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
		$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
		$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
		if (preg_match('#\[img\](.+)\[/img\]#', $message))
		{
			$message = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message);			
		}
		elseif (preg_match("#http://(.+)#isU", $message))
		{
			$message = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);					
		}
		elseif (preg_match("#https://(.+)#isU", $message))
		{
			$message = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);
		}
		else
		{
			$message = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$message);
		}
		$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
		$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
		$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
		$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
		$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
		$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
		$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
		$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
		$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
		$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);
			echo'<span class="texte_publication">'.$message.'</span>';	
			
		// TEXTE + IMAGE
		if ($donnees2['photo'] != 0 AND $donnees2['photo'] != '')
		{
			$source = getimagesize('images_utilisateurs/'.$donnees2['photo']); 	// La photo est la source
			if ($source[0] <= 900 AND $source[1] <= 600)
			{
				echo '<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donnees2['photo'].'">';
			}
			else
			{
				echo '<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$donnees2['photo'].'">';
			}
		
			if ($source[0] <= 200 AND $source[1] <= 200)
				echo '<img class="img_publication" src="images_utilisateurs/'.$donnees2['photo'].'" /></p></a>'; 
			else		
				echo '<img class="img_publication" src="images_utilisateurs/mini_3_'.$donnees2['photo'].'" /></p></a>'; 
		}
			
		if (isset($donnees2['video']) AND $donnees2['video'] != '')
		{
			$video = $donnees2['video'];
			$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
			<object	type="application/x-shockwave-flash" width="500" height="350" data="http://www.youtube.com/v/$1&hl=fr">							
				<param name="wmode" value="transparent" />
				<param name="allowFullScreen" value="true" />
			</object>',$video);
			echo $video;
		}
			
		echo'</div>';
		
// affichage j'aime 
	$nbre_jaime = 0;
	$like = 0;
	$tableau_pseudo_jaime = array();
	$tableau_id_jaime = array();
	$re_c = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_publication=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_publi' => $donnees2['id']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch()) 
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 1;
		}
		$re_c2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $donnees_c['id_jeu']))
		or die(print_r($bdd->errorInfo()));	
        $donnees_c2 = $re_c2->fetch();	
		$tableau_pseudo_jaime[$nbre_jaime] = $donnees_c2['pseudo'];
		$tableau_id_jaime[$nbre_jaime] = $donnees_c['id_jeu'];
		$nbre_jaime++; 
	}
		
	if($like == 0)
		echo'<p> <a id="'.$donnees2['id'].'"  class="like">
		<img src="images/pouce.png" title="J\'aime"/></a>';
	else
		echo'<p> <a id="'.$donnees2['id'].'"  class="like">
		<img src="images/pouce_vert.png" title="Je n\'aime plus"/></a>';

	if ($nbre_jaime > 1)
		echo ' (<span id="'.$donnees2['id'].'" class="nbre_jaime">'.$nbre_jaime.'</span>)';
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] != $_SESSION['id_jeu'])
		echo ' (<a href="profil-i'.$tableau_id_jaime[0].'.html">'.$tableau_pseudo_jaime[0].'</a>)';	
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] == $_SESSION['id_jeu'])
		echo '<span> Vous aimez</span>';		
	// fin affichage j'aime			
		
		if(isset($_SESSION['id_lieu_publication']))
		{
			$r_inconnue = $bdd->prepare('SELECT * FROM jeu WHERE id=:id_jeu')
										or die(print_r($bdd->errorInfo()));
			$r_inconnue->execute(array('id_jeu' => $_SESSION['id_lieu_publication']))
										or die(print_r($bdd->errorInfo()));
			$donnees_inconnue = $r_inconnue->fetch();
			
			$r1_ami = $bdd->prepare('SELECT * FROM amis 
									WHERE id_jeu=:id_jeu 
									AND id_jeu_ami=:id_jeu_ami')
									or die(print_r($bdd->errorInfo()));
			$r1_ami->execute(array('id_jeu' => $donnees_inconnue['id'],
									'id_jeu_ami' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo()));	
			$d1_ami = $r1_ami->fetch();
			
			$r2_ami = $bdd->prepare('SELECT * FROM amis 
									WHERE id_jeu=:id_jeu_ami 
									AND id_jeu_ami=:id_jeu')
									or die(print_r($bdd->errorInfo()));
			$r2_ami->execute(array('id_jeu' => $donnees_inconnue['id'],
									'id_jeu_ami' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo()));	
			$d2_ami = $r2_ami->fetch(); 
			
			if ($d2_ami['id_jeu'] != '' OR $d1_ami['id_jeu'] != '' 
			OR $_SESSION['id_lieu_publication'] == $_SESSION['id_jeu']) 		// CONDITION POUR AFFICHER LE BLOC POUR COMMENTER SI ON EST AMI
			{
				// COMMENTAIRE//////////////////////////////////////////////////
				echo '
				<form action="profil_post.php" method="post">
					<input type="hidden" name="commentaires"/>
					<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />
					<input type="hidden" name="id_publication" value="'.$_SESSION['id_publi'].'"/>
					<input type="hidden" name="lieu_publication" value="'.$_SESSION['id_lieu_publication'].'"/>
					<input type="hidden" name="id_jeu" value="'.$_SESSION['id_jeu'].'"/>
					<input type="hidden" name="profil_ou_clan" value="'.$_SESSION['lieu_page'].'" />';
					if(isset($_GET['page']))
					{
						echo'
						<input type="hidden" name="page" value="'.$_GET['page'].'" />';
					}
				echo'
				</form></span>';
				////////////////////////////////////////////////////////////////
			}
		}
		elseif(isset($_SESSION['id_clan']))
		{
			$r_clan = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
										or die(print_r($bdd->errorInfo()));
			$r_clan->execute(array('id_jeu' => $_SESSION['id_jeu']))
										or die(print_r($bdd->errorInfo()));
			$donnees_clan = $r_clan->fetch();
			if ($donnees_clan['id_clan'] == $_SESSION['id_clan']) 				// POUR LE CLAN
			{
				// COMMENTAIRE
				echo '
				<form action="profil_post.php" method="post">
					<input type="hidden" name="commentaires"/>
					<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />
					<input type="hidden" name="id_publication" value="'.$_SESSION['id_publi'].'"/>
					<input type="hidden" name="id_jeu" value="'.$_SESSION['id_jeu'].'"/>
					<input type="hidden" name="profil_ou_clan" value="'.$_SESSION['lieu_page'].'" />
				</form></span>';
				////////////////////////////////////////////////////////////////
			}
		}
		
		if (isset($_GET['page']) AND $_GET['page'] > 0)
		{
			$numero_page = $_GET['page'];
			$numero_page--;
			$numero_page = 	20*$numero_page;
		}
		else
			$numero_page = 0;
			
		$reponse = $bdd->prepare('SELECT id,commentaire, id_jeu , 
								DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %Hh %imin \') 
								AS date_commentaire FROM commentaire 
								WHERE id_publication=:id_publication 
								ORDER BY id LIMIT '.$numero_page.',20')
								or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_publication' => $donnees2['id']))
								or die(print_r($bdd->errorInfo()));
		$i=0;	
		while ($donnees3 = $reponse->fetch()) 									// on affiche commentaire par commentaire
		{
			$i++;
			$rs = $bdd->prepare('SELECT nom_de_compte, pseudo, photo_profil, id 
								FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
			$rs->execute(array('id' => $donnees3['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			$ds = $rs->fetch();
			echo '
			<div class="bloc_commentaire">';
			
			if(isset($_SESSION['id_lieu_publication']))							// CE QUI VIENT DE PROFIL OU ACCUEIL
			{
				if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
				OR $_SESSION['id_lieu_publication'] == $_SESSION['id_jeu'])
				{
					if(isset($_GET['page']))
					{
						echo'
						<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="commentaires.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'&page='.$_GET['page'].'" title="Supprimer le commentaire">
							
							<div id="supprimer_commentaire_profil"></div>
							
						</a>';
					}
					else
					{
						echo'
						<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="commentaires.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'" title="Supprimer le commentaire">
							
							<div id="supprimer_commentaire_profil"></div>
							
						</a>';
					}
				}
			}
			else // CE QUI VIENT DE CLAN
			{
				if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
				OR $donnees2['id_jeu'] == $_SESSION['id_jeu'])
				{
					if(isset($_GET['page']))
					{
						echo'
						<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="commentaires.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'&page='.$_GET['page'].'" title="Supprimer le commentaire">
							
							<div id="supprimer_commentaire_profil"></div>
							
						</a>';
					}
					else
					{
						echo'
						<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="commentaires.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'" title="Supprimer le commentaire">
							
							<div id="supprimer_commentaire_profil"></div>
							
						</a>';
					}
				}
			}
			
			// GERER LES LIENS SI C'EST UN SOUS COMPTE, 
			// LE SOUS COMPTE SUR LEQUEL JE SUIS CO OU UN AUTRE
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">';
			elseif($ds['id'] == $_SESSION['id_jeu'])
				echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">';
			else
				echo'<span  title="Mon sous compte">';
				
			if ($ds['photo_profil'] != 0)
			{ 
				$source = getimagesize('images_utilisateurs/'.$ds['photo_profil']); // La photo est la source
				
				echo'<div class="centre2_image40">';
				
				if ($source[0] <= 40 AND $source[1] <= 40)
					echo '<img src="images_utilisateurs/'.$ds['photo_profil'].'"/>'; 
				else				
					echo'<img src="images_utilisateurs/mini_4_'.$ds['photo_profil'].'"/>';
				
				echo'</div>';
			}
			else
			{
				echo '<img class="img_commentaire" src="images/tete2.png" alt="Planete"/>';
			}
			
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'</a>
				<a  href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'">
					<span class="pseudo_commentaire">
						'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a> ';
			}
			elseif($ds['id'] == $_SESSION['id_jeu'])
			{
				echo'</a>
				<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">
					<span class="pseudo_commentaire">
						'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a> ';
			}
			else
			{
				echo'</span>
				<span class="pseudo_commentaire" title="Mon sous compte" style="color : #102c3c;">
					'.stripslashes(htmlspecialchars($ds['pseudo'])).'
				</span>';
			}

			echo'
			<p class="date_commentaire">
				le '.$donnees3['date_commentaire'].'
			</p>';
			
			$com = stripslashes(htmlspecialchars($donnees3['commentaire']));
			
			if (preg_match("#http://(.+)#isU", $com))
			{
				$com = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);					
			}
			elseif (preg_match("#https://(.+)#isU", $com))
			{
				$com = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);
			}
			else
			{
				$com = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$com);
			}
			
			$com = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $com);		
			$com = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $com);	
			$com = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $com);		
			$com = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $com);
			$com = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $com);
			$com = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $com);
			$com = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $com);
			$com = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $com);
			$com = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $com);
			$com = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $com);
			echo'<span class="message_commentaire">'.$com.'</span>';
					
			echo '
			</div>';				
		}
		
	// GESTION AFFICHAGE PAGES /////////////////////////////////////////////////
	
	$nbre_page = 1;
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM commentaire 
						WHERE id_publication=:id_publication ')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_publication' => $donnees2['id']))
						or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_publi'];
	$p->closeCursor();
		
if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
$nom_page = 'commentaires';
$nbr_affichage = 20;

include('pagination.php');
}
?>
	</div>
	</div>
</div> 
<script> 
////////////////////////////////////// Pour le bouton j'aime
var jaime = document.getElementsByTagName('a'),
jaimeTaille = jaime.length;
		for (var i = 0 ; i < jaimeTaille ; i++) { 	
			if (jaime[i] && jaime[i].className == 'like')
			{
				jaime[i].onclick = function() { 
				var id_publi = this.id;
				var jaime2 = this;
				// On lance la requ�te ajax
				$.get('like.php?id_publication='+id_publi, function(data) { 
				if (data == 1)
					jaime2.innerHTML = '<img src="images/pouce_vert.png" title="Je n\'aime plus"/>';
				else
					jaime2.innerHTML = '<img src="images/pouce.png" title="J\'aime"/>';
					
				});				
					return false; // on bloque la redirection
				};
			}
		}
var span = document.getElementsByTagName('span'),
spanTaille = span.length;
		for (var i = 0 ; i < jaimeTaille ; i++) {
			if (span[i] && span[i].className == 'nbre_jaime')
			{
				span[i].onclick = function() {
				var href_publi = this.id;
				var jaime3 = this;
				// On lance la requ�te ajax
				$.getJSON('like.php?nbre_jaime='+href_publi, function(data) { 
					jaime3.innerHTML = data['message'];
				});				
				};
			}
		}
</script> 
<?php
include('pied_page.php'); 
?>
	