<?
/*
** Relevé des données à classer et classement
*/
$to_display = array(); // array pour stocker et trier les données

	foreach($tableau_amis as $element)
	{
	
		$requete = $bdd->prepare('SELECT id_clan, pseudo,
									photo_profil, id,
									nom_de_compte
								FROM jeu
								WHERE id=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('id_jeu' => $element))
								or die(print_r($bdd->errorInfo()));
		$donnees_p = $requete->fetch();

		$requete1 = $bdd->prepare('SELECT * ,
								DATE_FORMAT(date_actu, \'%d/%m/%Y à %H:%i \')
								AS date_affichee
								FROM actu
								WHERE id_jeu=:id_jeu
								ORDER BY date_actu
								DESC LIMIT '.$numero_page.'2')
								or die(print_r($bdd->errorInfo()));
		$requete1->execute(array('id_jeu' => $element))
								or die(print_r($bdd->errorInfo()));

		while($donnees = $requete1->fetch())
		{
			if ($donnees['actu'] == 'publication') ///////////PUBLICATION///////
			{
				$requete6 = $bdd->prepare('SELECT *
										FROM publication
										WHERE id=:id_table')
										or die(print_r($bdd->errorInfo()));
				$requete6->execute(array('id_table' => $donnees['id_table']))
										or die(print_r($bdd->errorInfo()));
				$donnees_t = $requete6->fetch();
				
				// stockage du tableau dans une cellule du tableau à trier
				// utilisation de l'ID pour classer par ordre d'enregistrement
				$to_display[$donnees_t['id']] = $donnees_t;
				
				$to_display[$donnees_t['id']]['auteur'] = $donnees_p;
			}
		}
	}

ksort($to_display); // classement selon la clé, donc selon l'id des publications

// Vérification pour voir l'array complet
echo '<pre>';
print_r($to_display);
echo '</pre>';

/*
** Affichage des publications
*/

// foreach($to_display as $)
?>