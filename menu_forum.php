<?php
if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc 
									FROM jeu WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: index.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
		
echo'
	<div id="entoure_bloc">
		<div id="sous_corps_forum">
			<div id="bloc1_forum">';
		
			$requete = $bdd->prepare('SELECT id_clan FROM jeu 
									WHERE id=:id_jeu')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo()));
			$donnees_clan = $requete->fetch();		
		
			echo'
			<img id="embleme_clanp3" src="images/embleme_clan_recherche.png" alt=" "/>
			<p id="nom_clanp3">
				'.stripslashes(htmlspecialchars($donnees2['nom_clan'])).'
			</p>';
			
			
				echo'
				<p id="infos_clanp3">
					Jeu : '; 
					echo substr(stripslashes(htmlspecialchars($donnees2['jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnees2['jeu']))) > 20)
					{ echo'...';} 
					echo'
					<br />
					Plateforme : '.$donnees2['plateforme'].'
				</p>
				<img id="bulle_forum" src="images/bulle.png" alt=" "/>
			</div>';
				
		echo'
			<div id="bloc2_forum">';
		echo'
				<div id="contient_image_clanp3">';
				if (!empty($donnees2['photo_clan']))
				{
					$source = getimagesize('images_utilisateurs/'.$donnees2['photo_clan']); // La photo est la source
					if ($source[0] <= 800 AND $source[1] <= 400) 
					{
						echo '<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donnees2['photo_clan'].'">
						<img src="images_utilisateurs/'.$donnees2['photo_clan'].'" alt="Photo de clan"/></a>'; 
					}
					else 
						echo '<a title="Afficher l\'image originale" href="images_utilisateurs/clan_2_'.$donnees2['photo_clan'].'">
						<img src="images_utilisateurs/clan_1_'.$donnees2['photo_clan'].'" alt="Photo de clan"/></a>';
				}
				else
					echo '<img id="defaut_clanp3" src="images/defaut_clan.png" alt="Photo de clan"/>';

		echo'
				</div>';
	
		echo'
				<div id="bloc_description_clanp3">';
		 $message = nl2br(stripslashes(htmlspecialchars($donnees2['information'])));
		 $message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);
				echo'
					<span id="description_clanp3"><span style="color: #4e5f69; font-size:large;">Description : 
					</span>'.$message.'</span> 
				</div>';
?>			
			</div>
	
			<div id="bloc4_p3"> <!--------------- MEMBRES ---------------->	
<?php
			$i=0;
			$requete2 = $bdd->prepare('SELECT pseudo FROM jeu 
										WHERE id_clan=:id_clan')
										or die(print_r($bdd->errorInfo()));
			$requete2->execute(array('id_clan' => $_SESSION['id_clan']))
										or die(print_r($bdd->errorInfo()));
			while($donnees_nbr = $requete2->fetch())
			{
				$i++;
			}
				
			echo'<a href="forum-membres.html">';
			
			if($i<100)
			{
				echo'
				<div id="contient_nombre">';
				
				if($i==1)
					echo'<p id="nombre_clanp3">'.$i.' membre</p>';
				else
					echo'<p id="nombre_clanp3">'.$i.' membres</p>';
			
				echo'
				</div>';
			}
			elseif($i>99)
			{
				echo'
				<div id="contient_nombre2">
					<p id="nombre_clanp3">'.$i.' membres</p>			
				</div>';
			}
			elseif($i>99999)
			{
				echo'
				<div id="contient_nombre">
					<p id="nombre_clanp3">'.$i.'</p>			
				</div>';
			}
			echo'</a>';
			$requete = $bdd->prepare('SELECT * FROM jeu WHERE id_clan=:id_clan 
									ORDER BY RAND() LIMIT 0,8')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id_clan' => $_SESSION['id_clan']))
									or die(print_r($bdd->errorInfo()));
			while($donnees_membre = $requete->fetch())
			{
				if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
					echo'<a  href="profil-i'.$donnees_membre['id'].'.html" title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
				elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
					echo'<span  title="Moi">';
				else
					echo'<span  title="Mon sous compte">';
					
				if(isset($donnees_membre['photo_profil']) AND $donnees_membre['photo_profil'] != '' AND $donnees_membre['photo_profil'] != 0)
				{
					$source = getimagesize('images_utilisateurs/'.$donnees_membre['photo_profil']); // La photo est la source
					if ($source[0] <= 60 AND $source[1] <= 60)
						echo'<img class="photo_profil_clanp3" src="images_utilisateurs/'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
					else
						echo'<img class="photo_profil_clanp3" src="images_utilisateurs/mini_2_'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
				}
				else
					echo'<img class="photo_profil_clanp3" src="images/tete3.png" alt="Photo de profil" />';

				if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
					echo'</a>';
				elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
					echo'</span>';
				else
					echo'</span>';
			}
?>

			</div> 
			
			<div id="bloc5_forum">
<?php
			if($_SESSION['statut'] == 'membre' 
			OR $_SESSION['statut'] == 'meneur')
			{
				echo'<a href="team.html">';
			}
			elseif($_SESSION['statut'] == 'visiteur')
				echo'<a href="team-i'.$_SESSION['id_clan'].'.html">';
				
					echo'<div id="contient_accueil_forum">
							<p id="retour_clanp3">
								Retour 
								<br />
								Clan 
							</p>
						</div>
					</a>';

			if ($_SESSION['photo_profil'] != 0 
			AND $_SESSION['photo_profil'] != '')
			{
				echo '<div id="centre_image40_forum">';
				$source = getimagesize('images_utilisateurs/'.$_SESSION['photo_profil']); // La photo est la source
				if ($source[0] <= 40 AND $source[1] <= 40)
					echo'<img src="images_utilisateurs/'.$_SESSION['photo_profil'].'" alt="Photo de profil"/>';
				else
					echo'<img src="images_utilisateurs/mini_4_'.$_SESSION['photo_profil'].'" alt="Photo de profil"/>';
			       echo'</div>'; 
			}
		   else 
			  echo'<img id="image40_forum" src="images/tete2.png" alt="Photo de profil"/>';
			 
			if($_SESSION['statut'] == 'meneur')
				echo'<p id="connection_forum"> Connect� en tant qu\'administrateur </p>';
			elseif($_SESSION['statut'] == 'membre')
				echo'<p id="connection_forum"> Connect� en tant que membre </p>';
			else
				echo'<p id="connection_forum"> Connect� en tant que visiteur </p>';
				
			// LIENS CHEMIN POUR RETOURNER EN ARRIERE ////////////////////////// 
			if(isset($_GET['id_categorie']) AND $_GET['id_categorie'] != '')
			{
				echo'
				<div id="chemin1_forum">
					<a href="forum.html"> 
						Forum 
					</a> 
					>
					<a href="forum-ic'.$_GET['id_categorie'].'.html"> 
						Cat�gorie 
					</a>
				</div>';
			}	
			elseif(isset($_GET['id'], $_GET['page']) AND $_GET['id'] != '' 
			AND $_GET['page'] != '' AND $_GET['id'] > 0 AND $_GET['page'] > 0)
			{
				$_SESSION['id_topic'] = $_GET['id'];
				$req_chemin1 = $bdd->prepare('SELECT titre_topic, id_categorie 
											FROM forum_topic 
											WHERE id=:id_topic')
											or die(print_r($bdd->errorInfo()));
				$req_chemin1 ->execute(array('id_topic' => $_GET['id']))
											or die(print_r($bdd->errorInfo()));
				$donnees_chemin1 = $req_chemin1	->fetch();
				echo'
				<div id="chemin1_forum">
					<a href="forum.html">
						Forum 
					</a> 
					>
					<a href="forum-ic'.$donnees_chemin1['id_categorie'].'.html"> 
						Cat�gorie 
					</a>
					>
					<a href="topic-i'.$_SESSION['id_topic'].'-p'.$_GET['page'].'.html">
						Sujet 
					</a>
				</div>';
			}				
?>
			</div>
		</div>
	</div>