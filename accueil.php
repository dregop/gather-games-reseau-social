<?php session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr? tout
        die('Erreur : '.$e->getMessage());
}

// ON SUPPRIME LES COMMENTAIRES ////////////////////////////////////////////////

if(isset($_GET['id_publication'],$_GET['supprimer_commentaire'],$_GET['id_commentaire']))
{
$reqs_c = $bdd->prepare('SELECT id_jeu, date_commentaire 
						FROM commentaire WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$reqs_c->execute(array('id' => $_GET['id_commentaire']))
						or die(print_r($bdd->errorInfo()));
$donnees_comm = $reqs_c->fetch();
$reqs_p = $bdd->prepare('SELECT lieu_publication_id_jeu 
						FROM publication WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$reqs_p->execute(array('id' => $_GET['id_publication']))
						or die(print_r($bdd->errorInfo()));
$donnees_publi = $reqs_p->fetch();

	if(isset($donnees_comm['id_jeu']) 
	AND $donnees_comm['id_jeu'] == $_SESSION['id_jeu'] 
	OR $donnees_publi['lieu_publication_id_jeu'] == $_SESSION['id_jeu'])
	{
		$req1 = $bdd->prepare('DELETE FROM commentaire WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id' => $_GET['id_commentaire'])) 
							or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		$req3 = $bdd->prepare('DELETE FROM notifications 
							WHERE date_notification=:date_notification 
							AND id_publication=:id_publication')
							or die(print_r($bdd->errorInfo()));
		$req3->execute(array('date_notification' => $donnees_comm['date_commentaire'],
							'id_publication' => $_GET['id_publication']))
							or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requ�te
	}

	if(isset($_GET['page']))
	{
		header('Location: accueil-p'.$_GET['page'].'.html#'.$_GET['id_publication'].'');
	}
	else
	{
		header('Location: accueil.html#'.$_GET['id_publication'].'');
	}
}	
////////////////////////////////////////////////////////////////////////////////
if (empty($_SESSION['pseudo']))
{
	header('Location: informations-e1.html');
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte 
									AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}

if (!isset($_SESSION['nom_de_compte']))
	header('Location: index.html');

if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');	
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body> 

<div id="overlay"></div>
<div id="contient_image_profil"></div>

<?php
include('menu_sans_doctype.php');
?>
<div id="corps_accueil">
       		
	<img id="actu" src="images/intro_actualite.png" 
							alt=" Que font vos amis sur les jeux ? "/>
	<img id="mosaique_console_accueil" 
							src="images/mosaique_console_accueil.png" alt=" "/>
<?php

$dernier24 = time() - 60*60*24;
$tableau_amis =array();
$i = 0; 

// on affiche les derni�res actualit�s des derni�res 24h des amis 
$requete = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu 
						OR id_jeu_ami=:id_jeu')
						or die(print_r($bdd->errorInfo()));
$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
while($donnees_amis = $requete->fetch()) 
{ 
	if ($donnees_amis['id_jeu'] == $_SESSION['id_jeu'])
	{
		$tableau_amis[$i] = $donnees_amis['id_jeu_ami'];
		$i++;
	}
	
	if ($donnees_amis['id_jeu_ami'] == $_SESSION['id_jeu'])
	{
		$tableau_amis[$i] = $donnees_amis['id_jeu'];
		$i++;
	}

}

if (empty($tableau_amis) AND $i==0)
	echo '<div id="sous_corps_accueil2">';	
elseif($i > 0 AND isset($tableau_amis))
{
	echo '<div id="sous_corps_accueil">';

echo'<div id="contient_menu_accueil">';
	if(isset($_GET['team']))
	{
		echo'<a href="accueil.html"><div id="publication_accueil"></div></a>';
		echo'<a href="accueil-amis.html"><div id="ami_accueil"></div></a>';
		echo'<a href="accueil-team.html"><div id="clan2_accueil"></div></a>';
	}
	elseif(isset($_GET['amis']))
	{
		echo'<a href="accueil.html"><div id="publication_accueil"></div></a>';
		echo'<a href="accueil-amis.html"><div id="ami2_accueil"></div></a>';
		echo'<a href="accueil-team.html"><div id="clan_accueil"></div></a>';
	}
	else	
	{
		echo'<a href="accueil.html"><div id="publication2_accueil"></div></a>';
		echo'<a href="accueil-amis.html"><div id="ami_accueil"></div></a>';
		echo'<a href="accueil-team.html"><div id="clan_accueil"></div></a>';
	}
echo'</div>';
}	
		
if(isset($_GET['amis']))
{
	include('accueil_ami.php');
}
elseif(isset($_GET['team']))
{
	include('accueil_clan.php');
}
else
{
	include('accueil_publication.php');
}

?>	    		 
        </div> 
		
</div>

<script>
////////////////////////////////////// Pour le bouton j'aime
var jaime = document.getElementsByTagName('a'),
jaimeTaille = jaime.length;
		for (var i = 0 ; i < jaimeTaille ; i++) { 	
			if (jaime[i] && jaime[i].className == 'like')
			{
				jaime[i].onclick = function() { 
				var id_publi = this.id;
				var jaime2 = this;
				// On lance la requ�te ajax
				$.get('like.php?id_publication='+id_publi, function(data) { 
				if (data == 1)
					jaime2.innerHTML = '<img src="images/pouce_vert.png" title="Je n\'aime plus"/>';
				else
					jaime2.innerHTML = '<img src="images/pouce.png" title="J\'aime"/>';
					
				});				
					return false; // on bloque la redirection
				};
			}
		}
var span = document.getElementsByTagName('span'),
spanTaille = span.length;
		for (var i = 0 ; i < jaimeTaille ; i++) {
			if (span[i] && span[i].className == 'nbre_jaime')
			{
				span[i].onclick = function() {
				var href_publi = this.id;
				var jaime3 = this;
				// On lance la requ�te ajax
				$.getJSON('like.php?nbre_jaime='+href_publi, function(data) { 
					jaime3.innerHTML = data['message'];
				});				
				};
			}
		}
//------------------------------------------------------------------------------
var links = document.getElementsByTagName('a'),
    linksLen = links.length;
for (var i = 0 ; i < linksLen ; i++) {
	if (links[i].title == 'Afficher l\'image originale')
	{
		links[i].onclick = function() { 
			displayImg(this); 
			return false; // on bloque la redirection
		};
	}
}

function displayImg(link) {

    var img = new Image(),
        overlay = document.getElementById('overlay');
        profil = document.getElementById('contient_image_profil');
    img.onload = function() {
        profil.innerHTML = '';
        profil.appendChild(img);
    };
    img.src = link.href;
    overlay.style.display = 'block';
    profil.style.display = 'block';
    profil.innerHTML = '<span>Chargement en cours...</span>';
}

document.getElementById('overlay').onclick = function() {
    this.style.display = 'none';
    profil.style.display = 'none';
	
};
//------------------------------------------------------------------------------
</script>
	
<?php 
include('pied_page.php');
?>