<?php
session_start(); 
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}

$reqs = $bdd->prepare('SELECT nom_de_compte FROM jeu WHERE id=:id_jeu')
					or die(print_r($bdd->errorInfo()));
$reqs->execute(array('id_jeu' => $_SESSION['id_jeu']))
					or die(print_r($bdd->errorInfo()));
$donnees_jeu = $reqs->fetch();

$reqs2 = $bdd->prepare('SELECT * FROM membres 
					WHERE nom_de_compte=:nom_de_compte')
					or die(print_r($bdd->errorInfo()));
$reqs2->execute(array('nom_de_compte' => $donnees_jeu['nom_de_compte']))
					or die(print_r($bdd->errorInfo()));
$donnees_jeu2 = $reqs2->fetch();

// MODIFICATION PAYS, VILLE, YOU TUBE
if(isset($_POST['pays'], $_POST['ville'], $_POST['youtube']) 
AND ((preg_match("#^http://#",$_POST['youtube'])) OR $_POST['youtube'] == ''))
{
	$reqa = $bdd->prepare('UPDATE membres 	
						SET pays=:pays, ville=:ville, youtube=:youtube 
						WHERE nom_de_compte = :nom_de_compte')
						or die(print_r($bdd->errorInfo()));
	$reqa->execute(array('pays' => $_POST['pays'],'ville' => $_POST['ville'],
						'youtube' => $_POST['youtube'],
						'nom_de_compte' => $donnees_jeu2['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));	
	$reqa->closeCursor(); // Termine le traitement de la requ�te
	header('Location: informations315-c-done.html');
}
elseif(isset($_POST['youtube']) 
AND !(preg_match("#^http://#",$_POST['youtube'])))
{
	header('Location: informations315-c-yt.html');
}

// MODIFICATION DU MOT DE PASSE
if (isset($_POST['reponse'], $_POST['ancien_mdp'], $_POST['nouveau_mdp'], 
$_POST['nouveau_mdp2']) 
AND $_POST['reponse'] == $donnees_jeu2['reponse'] 
AND preg_match('#^[A-Za-z0-9�&�@��]{4,25}$#',$_POST['nouveau_mdp']))
{
	$mdp_hache = sha1('qw' . $_POST['ancien_mdp']); // on hache le mdp
	
	if ($mdp_hache == $donnees_jeu2['mot_de_passe'] 
	AND $_POST['nouveau_mdp'] == $_POST['nouveau_mdp2'] 
	AND preg_match('#^[A-Za-z0-9�&�@��]{4,25}$#',$_POST['nouveau_mdp']))
	{
		$mdp_hache2 = sha1('qw' . $_POST['nouveau_mdp']); 
		
		$reqa = $bdd->prepare('UPDATE membres SET mot_de_passe=:mot_de_passe 
							WHERE nom_de_compte = :nom_de_compte')
							or die(print_r($bdd->errorInfo()));
		$reqa->execute(array('mot_de_passe' => $mdp_hache2,
							'nom_de_compte' => $donnees_jeu2['nom_de_compte']))
							or die(print_r($bdd->errorInfo()));	
		$reqa->closeCursor(); // Termine le traitement de la requ�te
		header('Location: informations315-c-done2.html');
	}
	else header('Location: informations315-c-fail.html');
	
}
elseif (isset($_POST['reponse'], $_POST['ancien_mdp'], 
$_POST['nouveau_mdp'], $_POST['nouveau_mdp2']))
{
	header('Location: informations315-c-fail.html');
}

// MODIFICATION DE L'EMAIL
if (isset($_POST['reponse'], $_POST['email2'], $_POST['email1']) 
AND $_POST['reponse'] == $donnees_jeu2['reponse'] 
AND $_POST['email1'] == $donnees_jeu2['email'] 
AND preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email2']))
{
	$reqa = $bdd->prepare('UPDATE membres SET email=:email 
						WHERE nom_de_compte = :nom_de_compte')
						or die(print_r($bdd->errorInfo()));
	$reqa->execute(array('email' => $_POST['email2'],
						'nom_de_compte' => $donnees_jeu2['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));	
	$reqa->closeCursor(); // Termine le traitement de la requ�te
	header('Location: informations315-c-true.html');
}
elseif (isset($_POST['reponse'],$_POST['email2']))
	header('Location: informations315-c-false.html');
	
	
	
	
$requte = $bdd->prepare('SELECT * FROM jeu 
						WHERE nom_de_compte=:nom_de_compte')
						or die(print_r($bdd->errorInfo()));
$requte->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));
while($donnes = $requte->fetch())
{
	if(!isset($donnes['pseudo'],$donnes['nom_jeu'],$donnes['plateforme']))
	{
	  header('Location: informations.html');
	}
}
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />	
 </head>
   
<body>  
<!--------------- BLOC QUI APPARAIT POUR CHANGER L'ADRESSE E-MAIL ------------->
<div id="grand_overlay1"></div>

<div id="overlay1_informations">
	<form action="informations_connecte.php" method="post">
		<a id="fermer2" href="#">
			<img class="fermer_informations" src="images/fermer.png" alt=" Fermer " />
		</a>
		<p id="q_overlay1">
			Question secr�te : <?php  echo $donnees_jeu2['question'];?>
		</p>
		<div id="infos_overlay1">
			<p class="sous_infos_overlay1">
				R�ponse secr�te <br />
				<input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" name="reponse" type="text"/>
			</p>
			<p class="sous_infos_overlay1">
				Adresse e-mail actuelle <br />
				<input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" name="email1" type="text"/>
			</p>
			<p class="sous_infos_overlay1">
				Nouvelle adresse e-mail <br />
				<input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" type="text" name="email2"/>
			</p>
		</div>
		<input type="submit" class="enregistrez_informations" name="Valider" value=" Enregistrer "/>
	</form>
</div>

<!--------------------- BLOC QUI APPARAIT POUR CHANGER MDP -------------------->
<div id="grand_overlay2"></div>

<div id="overlay2_informations">
	<form action="informations_connecte.php" method="post">
		<a id="fermer" href="#">
			<img class="fermer_informations" src="images/fermer.png" alt=" Fermer "/>
		</a>
		<p id="q_overlay2">
		Question secr�te : <?php  echo $donnees_jeu2['question'];?>
		</p>
		<div id="infos_overlay2">
		<p class="sous_infos_overlay2">R�ponse secr�te <br /><input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" value="" name="reponse" type="text"/></p>
			<p class="sous_infos_overlay2">
				Mot de passe actuelle <br />
				<input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" value="" name="ancien_mdp" type="password"/>
			</p>
			<p class="sous_infos_overlay2">
				Nouveau mot de passe <br />
				<input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" value="" name="nouveau_mdp" type="password"/>
			</p>
			<p class="sous_infos_overlay2">
				Retapez le nouveau mot de passe <br />
				<input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" name="nouveau_mdp2" type="password"/>
			</p>
		</div>
		<input type="submit" class="enregistrez_informations" name="Valider" value=" Enregistrer "/>
	</form>
</div>	
 
<!--------------- BLOC QUI APPARAIT POUR SUPPRIMER SOUS COMPTE ---------------->
<?php
if (isset($_POST['acces'], $_POST['reponse']) AND $_POST['acces'] == 1 
AND $_POST['reponse'] == $donnees_jeu2['reponse'])
{
	$_SESSION['question_secrete'] = 1;
}

if (!isset($_SESSION['question_secrete']))
{
?>
<div id="grand_overlay2" style="display:block;"></div>

<div id="overlay3_informations" style="display:block">
	<p id="q_secrete_overlay">
		Pour acc�der � vos informations, vous devez r�pondre � votre question secr�te
	</p>
	<form action="informations315.html" method="post">
		<p id="q_overlay3">
			question secr�te : <?php  echo $donnees_jeu2['question'];?>
		</p>
		<div id="infos_overlay2">
			<p class="sous_infos_overlay2" style="color:#102c3c;">
				R�ponse secr�te <br />
				<input style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" name="reponse" type="text"/>
			</p>
		</div>
		<input type="hidden"  name="acces" value="1"/>
		<input type="submit" class="enregistrez_informations" name="Valider" value="R�pondre"/>
	</form>
	<a href="profil.html">
		<img src="images/retour_profil2.png" alt="Retour profil" />
	</a>
</div>	
<?php
}

include('menu_sans_doctype.php'); 
?>

<div id="corps_informations_connecte">

<?php 
if (isset($_GET['c']))
{ 
	echo'
	<a href="informations315.html">
		<div class="retour" title="retour"></div>
	</a>

	<div id="informations_compte2"></div>';
	
		if (isset($_GET['done2'])) 
			echo'
			<div class="bloc_erreur_informations_co">
				<p class="bonnes_modifs">
					<img src="images/bon.png" alt=" "/>
					Mot de passe modifi�.
				</p>
			</div>';

		if (isset($_GET['fail'])) 
			echo'
			<div class="bloc_erreur_informations_co">
				<p class="erreur_modifs">
					<img src="images/attention.png" alt=" "/>
					La r�ponse secr�te ou l\'ancien mot de passe est incorrect, 
					il est aussi possible que le nouveau mot de passe ne soit 
					pas dans le bon format : lettres, chiffres, compris entre 4 
					et 25 carast�res 
				</p>
			</div>';

		if (isset($_GET['done'])) 
			echo'
			<div class="bloc_erreur_informations_co">
				<p class="bonnes_modifs">
					<img src="images/bon.png" alt=" "/>
					Donn�es modifi�es
				</p>
			</div>';

		if (isset($_GET['true'])) 
			echo'
			<div class="bloc_erreur_informations_co">
				<p class="bonnes_modifs">
					<img src="images/bon.png" alt=" "/>
					Adresse e-mail modifi�
				</p>
			</div>';

		if (isset($_GET['false'])) 
			echo'
			<div class="bloc_erreur_informations_co">
				<p class="erreur_modifs">
					<img src="images/attention.png" alt=" "/>
					La r�ponse secr�te ou l\'adresse e-mail n\'est pas correcte
				</p>
			</div>';

		if (isset($_GET['yt'])) 
			echo'
			<div class="bloc_erreur_informations_co">
				<p class="erreur_modifs">
					<img src="images/attention.png" alt=" "/>
					Le lien de votre chaine Youtube est invalide il doit 
					commencer par http:// ou https://
				</p>
			</div>';
	echo'
	<div id="bloc_compte">

		<p id="nom_compte"> 
			'.stripslashes(htmlspecialchars($donnees_jeu2['nom_de_compte'])).'
		</p>
		<p id="email_compte">
			Adresse e-mail
		</p> 
		<a href="#" id="modifier_email">
			<div class="edit" title="modifier"></div>
		</a>
		<p id="mdp_compte">
			Mot de passe
		</p> 
		<a href="#" id="modifier_mdp">
			<div class="edit" title="modifier"></div>
		</a>


		<div id="infos_compte">
			<form action="informations_connecte.php" method="post">
				<p class="sous_infos_compte">
					Pays <br />
					<input name="pays" style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" 
					value="'.stripslashes(htmlspecialchars($donnees_jeu2['pays'])).'"/>
				</p>
				<p class="sous_infos_compte">
					Ville <br />
					<input name="ville" style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" 
					value="'.stripslashes(htmlspecialchars($donnees_jeu2['ville'])).'"/>
				</p>
				<p class="sous_infos_compte">
					Chaine You Tube (liens http:// ou https://)<br />
					<input name="youtube" style="color:#102c3c;font-size:1.4em;width:340px; height:25px;" 
					value="'.stripslashes(htmlspecialchars($donnees_jeu2['youtube'])).'"/>
				</p>
		</div>
				<input type="submit" class="enregistrez_informations" name="Valider" value=" Enregistrer "/>
			</form>';		  

		echo'
	</div>';
}
elseif (isset($_GET['sc']) OR isset ($_GET['scde']) OR isset ($_GET['scd']))
{
	echo'
	<a href="informations315.html">
		<div class="retour" title="retour"></div>
	</a>';
?>
	<div id="informations_sous2"></div>
<?php
	if (isset($_GET['invalide'])) 
	{ 
		echo'
		<div class="bloc_erreur">
			<p class="erreur_modifs">
				<img src="images/attention.png" alt=" "/> 
				Image non valide, la photo de profil par d�faut du site a �t� 
				enregistr�e. 
				<br /> 
				Vous pouvez supprimer votre sous-compte et recommencer. 
				<br /> 
				(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
			</p>
		</div>';
	} 
	
	$requte = $bdd->prepare('SELECT * FROM jeu 
							WHERE nom_de_compte=:nom_de_compte')
							or die(print_r($bdd->errorInfo()));
	$requte->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))
							or die(print_r($bdd->errorInfo()));
	$i=0;
	while($donnes = $requte->fetch())
	{
		$i++;
		echo'
		<div class="sous_corps2_informations" id="'.$donnes['id'].'">
			<p class="pseudo">
				'.stripslashes(htmlspecialchars($donnes['pseudo'])).'
			</p>';
			
			if ($donnes['photo_profil'] != 0)
			{
				echo'<div class="centre_image_infos">';
				$source = getimagesize('images_utilisateurs/'.$donnes['photo_profil']); // La photo est la source
				if ($source[0] <= 200 AND $source[1] <= 200)
					echo'<img class="photo_profil" alt="Photo de profil" src="images_utilisateurs/'.$donnes['photo_profil'].'" alt="Photo de profil"/></div>';
				else
					echo'<img class="photo_profil" alt="Photo de profil" src="images_utilisateurs/mini_3_'.$donnes['photo_profil'].'" alt="Photo de profil"/></div>';
			}
			else 
			  echo'<img class="tete_enregistre" src="images/tete1.png" alt="Photo de profil"/>';
			  
			echo'
			<div class="donnees_choix">			  
				<p class="info_compte">
					Jeu : 
					<span style="color:#5a7d91">
						'.substr(stripslashes(htmlspecialchars($donnes['nom_jeu'])), 0, 25).'';
						if(strlen(stripslashes(htmlspecialchars($donnes['nom_jeu']))) > 25){ echo'...';}
					echo'
					</span>
				</p>
				<p class="info_compte">
					Plateforme : 
					<span style="color:#5a7d91">
						'.$donnes['plateforme'].'
					</span>
				</p>
				
				<form action="informations_connecte_post.php" method="post" enctype="multipart/form-data">';
				
					$requte2 = $bdd->prepare('SELECT nom_clan FROM clan 
											WHERE id=:id_clan')
											or die(print_r($bdd->errorInfo()));
					$requte2->execute(array('id_clan' => $donnes['id_clan']))
											or die(print_r($bdd->errorInfo()));
					$donnes_clan= $requte2->fetch();
					
					if(isset($donnes_clan['nom_clan']) AND $donnes_clan['nom_clan'] !='')
					{
						echo'
						<p class="info_compte">
							Clan : 
							<span style="color:#5a7d91">
								'.stripslashes(htmlspecialchars($donnes_clan['nom_clan'])).'
							</span>
						</p>';
					}
					else
					{
						echo'	
						<p class="info_compte">
							Clan : 
							<span style="color:#5a7d91">
								aucun
							</span> 
						</p>';
					}
					$message1 = nl2br(stripslashes(htmlspecialchars($donnes['description'])));
					$message1 = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message1);
					$message1 = preg_replace('#<br />#','',$message1);
					echo'
					<p class="info_compte">
						Description 
						<span style="font-size:small;">
							(250 caract�res)
						</span> 
						<br />
						<textarea name="description" style="color:#102c3c;" rows="3" cols="35" maxlength="1000">'.$message1.'</textarea>';
						
						if(isset($_GET['scd'], $_GET['id']) AND $_GET['id'] == $donnes['id']) 
						{
							echo'
							<span class="bon_description">
								<img src="images/bon.png" alt=" "/>
								Description modifi�e
							</span>';
						}	
						elseif(isset($_GET['scde'], $_GET['id']) AND $_GET['id'] == $donnes['id']) 
						{
							echo'
							<span class="erreur_description">
								<img src="images/attention.png" alt=" "/> 
								250 caract�res maximum
							</span>';
						}	
					echo'
					</p>
			</div>
					<input type="hidden" name="informations_connecte"/>
					<input type="hidden" name="id_jeu" value="'.$donnes['id'].'"/>
					<div class="changer_image">
						Image de profil
						<div class="contient_file_informations">
							<input type="file" name="photo_profil" id="photo_profil" 
							onmousedown="return false" 
							onkeydown="return false" class="inputfile_informations" 
							onchange="displayfilename(this);" />
						</div>
					</div>
					<input type="submit" class="enregistrez_informations" name="Valider" value=" Enregistrer "/>
				</form>';
			  
			if(isset($_SESSION['id_jeu']) AND $donnes['id'] != $_SESSION['id_jeu'])
			{
				echo'<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce sous-compte ?\');
					 if (sup == 0)return false;" href="informations_connecte_post.php?connecte&supprimer&id='.$donnes['id'].'">
						<div class="supprimer_a"></div>
					 </a>';
			}
			elseif(isset($_SESSION['id_jeu']) AND $donnes['id'] == $_SESSION['id_jeu'])
			{
				echo'<img id="connecte_informations" src="images/connecte_informations2.png" alt="Vous y �tes connect�"/>';
			}
			else
			{
				echo'<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce sous-compte ?\');
					 if (sup == 0)return false;" href="informations_connecte_post.php?connectesupprimer&id='.$donnes['id'].'">
						<div class="supprimer_a"></div>
					 </a>';
			}
					 
		 echo'   
		</div>';
	}
	if ($i < 5)
	{
	echo '
		<div id="fin_corps">
			<p id="texte_fin" > 
				Ajouter un sous compte 
			</p>	       
		   <a href="informations-plus.html#scc">
			<div id="plus" ></div>
		   </a>
		</div>';
	}		
}
else
{
	if(isset($_SESSION['question_secrete']))
	{
?>
		<div class="contient_liens">
			<a href="informations315-c.html">
				<div id="informations_compte"></div>
			</a>
		</div>

		<div class="contient_liens">
			<a href="informations315-sc.html">
				<div id="informations_sous"></div>
			</a>
		</div>
<?php
	}
	else
	echo'
	<div id="informations_compte"></div>
	
	<div id="informations_sous"></div>';
}
?>

</div>

<script>
var grand_overlay2 = document.getElementById('grand_overlay2');
var grand_overlay1 = document.getElementById('grand_overlay1');
var overlay2_informations = document.getElementById('overlay2_informations');
var overlay1_informations = document.getElementById('overlay1_informations');
document.getElementById('modifier_mdp').onclick = function() {
    grand_overlay2.style.display = 'block';
    overlay2_informations.style.display = 'block';
	return false; // on bloque la redirection
	};
document.getElementById('modifier_email').onclick = function() {
    grand_overlay1.style.display = 'block';
    overlay1_informations.style.display = 'block';
	return false; // on bloque la redirection
	};
document.getElementById('fermer2').onclick = function() {
    grand_overlay1.style.display = 'none';
    overlay1_informations.style.display = 'none';
	return false; // on bloque la redirection
	};
document.getElementById('fermer').onclick = function() {
    grand_overlay2.style.display = 'none';
    overlay2_informations.style.display = 'none';
	return false; // on bloque la redirection
	};
	
</script>

<?php
include('pied_page.php'); 
