<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
	die('Erreur : '.$e->getMessage());
}

//SUPPRIMER UN SOUS-COMPTE
if (isset($_GET['supprimer'],$_GET['id']))
{
	$req = $bdd->prepare('SELECT nom_de_compte FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id' => $_GET['id']))
						or die(print_r($bdd->errorInfo()));
	$do = $req->fetch();
	
	if ($do['nom_de_compte'] == $_SESSION['nom_de_compte'])
	{
		////////ON SUPPRIME LES COMMENTAIRES ASSOCIEES AU SOUS COMPTE
		$req0 = $bdd->prepare('DELETE FROM commentaire WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req0->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES PUBLICATION ASSOCIEES AU SOUS COMPTE
		$req1 = $bdd->prepare('DELETE FROM publication WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES MESSAGES ASSOCIE AU SOUS COMPTE
		$req3 = $bdd->prepare('DELETE FROM message WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req3->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES TOPICS ASSOCIE AU SOUS COMPTE
		$req4 = $bdd->prepare('DELETE FROM forum_topic WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req4->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES CATEGORIES ASSOCIE AU SOUS COMPTE
		$req5 = $bdd->prepare('DELETE FROM forum_categorie 
							WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req5->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		/////ON SUPPRIME LES PUBLICATIONS DANS COMMUNAUTE ASSOCIE AU SOUS COMPTE
		$req6 = $bdd->prepare('DELETE FROM communaute WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req6->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		///////ON SUPPRIME LES COMMENTAIRES DANS UN TOPIC ASSOCIE AU SOUS COMPTE
		$req7 = $bdd->prepare('DELETE FROM commentaire_topic 
							WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req7->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES CANDIDATURES ASSOCIE AU SOUS COMPTE
		$req8 = $bdd->prepare('DELETE FROM candidature WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req8->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES ACTUALITES ASSOCIE AU SOUS COMPTE
		$req9 = $bdd->prepare('DELETE FROM actu WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req9->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES ANNONCES DE RECRUTEMENTS SI MENEUR DE CLAN
		$req9 = $bdd->prepare('DELETE FROM recrutements WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req9->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES DEMANDES AMIS ASSOCIE AU SOUS COMPTE
		$req10 = $bdd->prepare('DELETE FROM demande_amis 	
								WHERE id_jeu_demandeur=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$req10->execute(array('id_jeu' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));
		///////ON SUPPRIME LES NOTIFICATIONS PUBLICATIONS ASSOCIE AU SOUS COMPTE
		$req11 = $bdd->prepare('DELETE FROM notifications 
								WHERE id_jeu_publi=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$req11->execute(array('id_jeu' => $_GET['id'])) 
								or die(print_r($bdd->errorInfo()));
		///////ON SUPPRIME LES NOTIFICATIONS COMMENTAIRES ASSOCIE AU SOUS COMPTE
		$req12 = $bdd->prepare('DELETE FROM notifications 
								WHERE id_jeu_com=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$req12->execute(array('id_jeu' => $_GET['id'])) 
								or die(print_r($bdd->errorInfo()));
		///////ON SUPPRIME LES ACTUS MACHIN AMI AVEC MACHIN DANS LE PREMIER SENS
		$req13 = $bdd->prepare('SELECT id FROM amis WHERE id_jeu=:id')
								or die(print_r($bdd->errorInfo()));
		$req13->execute(array('id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));
		$d13 = $req13->fetch(); 
		$req14 = $bdd->prepare('DELETE FROM actu WHERE id_table=:id_table')
								or die(print_r($bdd->errorInfo()));
		$req14->execute(array('id_table' => $d13['id'])) 
								or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES ACTUS MACHIN AMI AVEC MACHIN DANS LE SECOND SENS
		$req15 = $bdd->prepare('SELECT id FROM amis WHERE id_jeu_ami=:id')	
								or die(print_r($bdd->errorInfo()));
		$req15->execute(array('id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));
		$d15 = $req15->fetch(); 
		$req16 = $bdd->prepare('DELETE FROM actu WHERE id_table=:id_table')
								or die(print_r($bdd->errorInfo()));
		$req16->execute(array('id_table' => $d15['id'])) 
								or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LES AMIS ASSOCIEES AU SOUS COMPTE
		$req2 = $bdd->prepare('DELETE FROM amis 
								WHERE id_jeu=:id OR id_jeu_ami=:id')
								or die(print_r($bdd->errorInfo()));
		$req2->execute(array('id' => $_GET['id'])) 
		or die(print_r($bdd->errorInfo()));
		////////ON SUPPRIME LE SOUS COMPTE
		$req = $bdd->prepare('DELETE FROM jeu 
								WHERE id=:id AND nom_de_compte=:ndc')
								or die(print_r($bdd->errorInfo()));
		$req->execute(array('id' => $_GET['id'],
							'ndc' => $_SESSION['nom_de_compte'])) 
							or die(print_r($bdd->errorInfo()));
		$req = $bdd->prepare('DELETE FROM amis 
							WHERE id_jeu=:id OR id_jeu_ami=:id')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id' => $_GET['id'])) 
							or die(print_r($bdd->errorInfo()));

		// ON VERIFIE S'IL EST MEMBRE D'UN CLAN
		// ON UTILISE LE $_GET['id'] PUISQU'EN CLIQUANT SUR SUPPRIMER ON ENVOIE
		// LE GET A LA PAGE informations_connecte_post
		// C'EST PAS LA SESSION CAR QUAND ON SUPPRIME ON EST PAS CO DESSUS
		$reqs = $bdd->prepare('SELECT id FROM clan WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$reqs->execute(array('id_jeu' => $_GET['id']))
							or die(print_r($bdd->errorInfo()));
		$dodo = $reqs->fetch();
		
		if (isset($dodo['id']))
		{
			$req = $bdd->prepare('DELETE FROM clan WHERE id=:id 
								AND id_jeu=:id_jeu')
								or die(print_r($bdd->errorInfo()));
			$req->execute(array('id' => $dodo['id'],
								'id_jeu' => $_GET['id'])) 
								or die(print_r($bdd->errorInfo()));
			$reqa = $bdd->prepare('UPDATE jeu SET id_clan=0 
								WHERE id_clan=:id_clan')
								or die(print_r($bdd->errorInfo()));
			$reqa->execute(array('id_clan' => $dodo['id']))
								or die(print_r($bdd->errorInfo()));	
			$reqa->closeCursor(); // Termine le traitement de la requête
		}
		if (isset($_GET['connecte']) AND $_GET['id'] == $_SESSION['id_jeu'])
		{
		   header('Location: choix.html');
		}
		elseif (isset($_GET['connecte']))
		{
			header('Location: informations315-sc.html');
		}	
		else
		{
			header('Location: informations.html');
		}
	}
}
?>