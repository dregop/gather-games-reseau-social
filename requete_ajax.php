<?php
session_start();
header('Access-Control-Allow-Origin: *');
	//
	
	
	
	
	//----------------------//
	// Fonction du site php // 
	//----------------------//
	
	function xml2array($url, $get_attributes = 1, $priority = 'tag')
	{
		$contents = "";
		if (!function_exists('xml_parser_create'))
		{
			return array ();
		}
		$parser = xml_parser_create('');
		if (!($fp = @ fopen($url, 'rb')))
		{
			return array ();
		}
		while (!feof($fp))
		{
			$contents .= fread($fp, 8192);
		}
		fclose($fp);
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);
		if (!$xml_values)
			return; //Hmm...
		$xml_array = array ();
		$parents = array ();
		$opened_tags = array ();
		$arr = array ();
		$current = & $xml_array;
		$repeated_tag_index = array (); 
		foreach ($xml_values as $data)
		{
			unset ($attributes, $value);
			extract($data);
			$result = array ();
			$attributes_data = array ();
			if (isset ($value))
			{
				if ($priority == 'tag')
					$result = $value;
				else
					$result['value'] = $value;
			}
			if (isset ($attributes) and $get_attributes)
			{
				foreach ($attributes as $attr => $val)
				{
					if ($priority == 'tag')
						$attributes_data[$attr] = $val;
					else
						$result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}
			if ($type == "open")
			{ 
				$parent[$level -1] = & $current;
				if (!is_array($current) or (!in_array($tag, array_keys($current))))
				{
					$current[$tag] = $result;
					if ($attributes_data)
						$current[$tag . '_attr'] = $attributes_data;
					$repeated_tag_index[$tag . '_' . $level] = 1;
					$current = & $current[$tag];
				}
				else
				{
					if (isset ($current[$tag][0]))
					{
						$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
						$repeated_tag_index[$tag . '_' . $level]++;
					}
					else
					{ 
						$current[$tag] = array (
							$current[$tag],
							$result
						); 
						$repeated_tag_index[$tag . '_' . $level] = 2;
						if (isset ($current[$tag . '_attr']))
						{
							$current[$tag]['0_attr'] = $current[$tag . '_attr'];
							unset ($current[$tag . '_attr']);
						}
					}
					$last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
					$current = & $current[$tag][$last_item_index];
				}
			}
			elseif ($type == "complete")
			{
				if (!isset ($current[$tag]))
				{
					$current[$tag] = $result;
					$repeated_tag_index[$tag . '_' . $level] = 1;
					if ($priority == 'tag' and $attributes_data)
						$current[$tag . '_attr'] = $attributes_data;
				}
				else
				{
					if (isset ($current[$tag][0]) and is_array($current[$tag]))
					{
						$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
						if ($priority == 'tag' and $get_attributes and $attributes_data)
						{
							$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag . '_' . $level]++;
					}
					else
					{
						$current[$tag] = array (
							$current[$tag],
							$result
						); 
						$repeated_tag_index[$tag . '_' . $level] = 1;
						if ($priority == 'tag' and $get_attributes)
						{
							if (isset ($current[$tag . '_attr']))
							{ 
								$current[$tag]['0_attr'] = $current[$tag . '_attr'];
								unset ($current[$tag . '_attr']);
							}
							if ($attributes_data)
							{
								$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
					}
				}
			}
			elseif ($type == 'close')
			{
				$current = & $parent[$level -1];
			}
		}
		return ($xml_array);
	}
	
	//------//
	// Code //
	//------//
	
	// Variables
	$liste_jeux = array();
	$nb_jeux = 0;
	$infos_jeu = array();
	$jeu = 'Stronghold';
	$plateforme = 'PC';
	$i = 0;
	$id_infos_jeu = null;
	$requete = null;
	$reponse = array(
		'liste' => array()
	);
	$temp = null;
	
	// Connexion à la bdd
	try
	{
		include('bdd_name.php');
	}
	catch(Exception $e)
	{
		die('Erreur : '.$e->getMessage());
	}
	
	// On demande une liste de jeux
	$liste_jeux = xml2array('http://thegamesdb.net/api/GetGamesList.php?name='.urlencode($jeu));
	
	// Si il n'y a que une reponse
	if(!isset($liste_jeux['Data']['Game'][0]))
	{
		$nb_jeux = 0;
		$temp = $liste_jeux['Data']['Game'];
		unset($liste_jeux['Data']['Game']);
		$liste_jeux['Data']['Game'][0] = $temp;
	}
	else
	{
		$nb_jeux = count($liste_jeux['Data']['Game']);
		echo 'nb jeux :'.$nb_jeux.'<br/>';
	}
	
	// On parcourt la liste
	for($i=0; $i<$nb_jeux; $i++)
	{
		// Si c'est la bonne plate forme
		if($liste_jeux['Data']['Game'][$i]['Platform'] == $plateforme)
		{
			// Si c'est le jeu demandé
			if($liste_jeux['Data']['Game'][$i]['GameTitle'] == $jeu)
			{
				// On regarde s'il est déjà en BDD
				$requete = $bdd->prepare('			
					SELECT *
					FROM infos_jeu
					WHERE id_thegamesdb = :id_thegamesdb
					LIMIT 0,1
				');
				$requete->bindValue(':id_thegamesdb', $liste_jeux['Data']['Game'][$i]['id'], PDO::PARAM_INT);
				$requete->execute();
				$test_bdd = $requete->fetchAll();
				
				// Si le jeu est en BDD local
				if(count($test_bdd) == 1)
				{
					// On complète directement la reponse pour le client
					$reponse['main'] = $test_bdd[0]['jaquette'];
				}
				// Sinon le jeux doit être récupèré sur The Games DB
				else
				{
					// On demande des infos complémentaires sur ce dernier
					$infos_jeu = xml2array('http://thegamesdb.net/api/GetGame.php?id='.$liste_jeux['Data']['Game'][$i]['id']);
					
					// Création d'une nouvelle entrée de jeu
					$requete = $bdd->prepare('			
						INSERT INTO infos_jeu(
							id_thegamesdb,
							titre,
							rating,
							plateforme,
							logo,
							jaquette,
							banniere
						)
						VALUES(
							:id_thegamesdb,
							:titre,
							:rating,
							:plateforme,
							:logo,
							:jaquette,
							:banniere
						)
					');
					
					$requete->bindValue(':id_thegamesdb',	$infos_jeu['Data']['Game']['id'],														PDO::PARAM_INT);
					$requete->bindValue(':titre',			$infos_jeu['Data']['Game']['GameTitle'],												PDO::PARAM_STR);
					$requete->bindValue(':plateforme',		$infos_jeu['Data']['Game']['Platform'],													PDO::PARAM_STR);
					
					// rating
					if(isset($infos_jeu['Data']['Game']['Rating']))
					{
						$requete->bindValue(':rating', $infos_jeu['Data']['Game']['Rating'], PDO::PARAM_STR);
					}
					else
					{
						$requete->bindValue(':rating', 'none', PDO::PARAM_STR);
					}
					
					// clearlogo
					if(isset($infos_jeu['Data']['Game']['Images']['clearlogo']))
					{
						if(!is_string($infos_jeu['Data']['Game']['Images']['clearlogo']))
						{
							echo 'pd<br/>';
							$requete->bindValue(':logo', $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['clearlogo'][0], PDO::PARAM_STR);
						}
						else
						{
							echo 'puyye<br/>';
							$requete->bindValue(':logo', $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['clearlogo'], PDO::PARAM_STR);
						}
					}
					else
					{
						$requete->bindValue(':logo', 'none', PDO::PARAM_STR);
					}
					
					// boxart
					if(isset($infos_jeu['Data']['Game']['Images']['boxart']))
					{
						if(!is_string($infos_jeu['Data']['Game']['Images']['boxart']))
						{
							$requete->bindValue(':jaquette', $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['boxart'][0], PDO::PARAM_STR);
							
							// On complète la reponse pour le client
							$reponse['main'] = $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['boxart'][0];
						}
						else
						{
							$requete->bindValue(':jaquette', $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['boxart'], PDO::PARAM_STR);
							
							// On complète la reponse pour le client
							$reponse['main'] = $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['boxart'];
						}
					}
					else
					{
						$requete->bindValue(':jaquette', 'none', PDO::PARAM_STR);
						
						// On complète la reponse pour le client
						$reponse['main'] = 'none';
					}
					
					// banner
					if(isset($infos_jeu['Data']['Game']['Images']['banner']))
					{
						if(!is_string($infos_jeu['Data']['Game']['Images']['banner']))
						{
							$requete->bindValue(':banniere', $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['banner'][0], PDO::PARAM_STR);
						}
						else
						{
							$requete->bindValue(':banniere', $infos_jeu['Data']['baseImgUrl'].$infos_jeu['Data']['Game']['Images']['banner'], PDO::PARAM_STR);
						}
					}
					else
					{
						$requete->bindValue(':banniere', 'none', PDO::PARAM_STR);
					}

					$requete->execute();
					
					// On récupère l'id de la dernière entrée crée
					$id_infos_jeu = $bdd->lastInsertId();
					
					echo 'last id : '.$id_infos_jeu.'<br/>';
				}
			}
			// Sinon ce n'est pas le jeu demandé
			else
			{
				// On complète la reponse pour le client
				array_push($reponse['liste'], $liste_jeux['Data']['Game'][$i]['GameTitle']);
			}
		}
	}
	
	// Debug
	/*echo 'Reponse :<pre>';
	print_r($reponse);
	echo '</pre>';
	
	echo 'Info jeu :<pre>';
	print_r($infos_jeu);
	echo '</pre>';
	
	echo 'Liste jeux :<pre>';
	print_r($liste_jeux);
	echo '</pre>';*/
	
	echo json_encode($reponse);
?>