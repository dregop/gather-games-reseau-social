-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 24 Décembre 2013 à 18:59
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gather_games`
--

-- --------------------------------------------------------

--
-- Structure de la table `actu`
--

CREATE TABLE IF NOT EXISTS `actu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `actu` varchar(255) NOT NULL,
  `id_table` int(11) NOT NULL,
  `date_actu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=278 ;

--
-- Contenu de la table `actu`
--

INSERT INTO `actu` (`id`, `id_jeu`, `actu`, `id_table`, `date_actu`) VALUES
(6, 2, 'publication', 5, '2013-09-17 17:54:47'),
(7, 2, 'publication', 6, '2013-09-17 17:54:48'),
(9, 2, 'publication', 8, '2013-09-17 17:54:51'),
(12, 2, 'publication', 11, '2013-09-17 17:54:54'),
(13, 2, 'publication', 12, '2013-09-17 17:54:55'),
(14, 2, 'publication', 13, '2013-09-17 17:54:56'),
(15, 2, 'publication', 14, '2013-09-17 17:54:57'),
(16, 2, 'publication', 15, '2013-09-17 17:54:59'),
(17, 2, 'publication', 16, '2013-09-17 17:55:00'),
(18, 2, 'publication', 17, '2013-09-17 17:55:02'),
(19, 2, 'publication', 18, '2013-09-17 17:55:03'),
(20, 2, 'publication', 19, '2013-09-17 17:55:04'),
(21, 2, 'publication', 20, '2013-09-17 17:55:05'),
(22, 2, 'publication', 21, '2013-09-17 17:55:06'),
(23, 2, 'publication', 22, '2013-09-17 17:55:11'),
(24, 2, 'publication', 23, '2013-09-17 17:55:13'),
(25, 2, 'publication', 24, '2013-09-17 17:55:14'),
(26, 2, 'publication', 25, '2013-09-17 17:55:16'),
(27, 2, 'publication', 26, '2013-09-17 17:55:17'),
(28, 2, 'publication', 27, '2013-09-17 17:55:18'),
(29, 2, 'publication', 28, '2013-09-17 17:55:19'),
(30, 2, 'publication', 29, '2013-09-17 17:55:20'),
(31, 2, 'publication', 30, '2013-09-17 17:55:21'),
(32, 2, 'publication', 31, '2013-09-17 17:55:23'),
(33, 2, 'publication', 32, '2013-09-17 17:55:24'),
(34, 2, 'publication', 33, '2013-09-17 17:55:25'),
(35, 2, 'publication', 34, '2013-09-17 17:55:26'),
(36, 2, 'publication', 35, '2013-09-17 17:55:28'),
(37, 2, 'publication', 36, '2013-09-17 17:55:29'),
(38, 2, 'publication', 37, '2013-09-17 17:55:30'),
(39, 2, 'publication', 38, '2013-09-17 17:55:31'),
(40, 2, 'publication', 39, '2013-09-17 17:55:32'),
(41, 2, 'publication', 40, '2013-09-17 17:55:34'),
(42, 2, 'publication', 41, '2013-09-17 17:55:36'),
(43, 2, 'publication', 42, '2013-09-17 17:55:38'),
(44, 2, 'publication', 43, '2013-09-17 17:55:39'),
(45, 2, 'publication', 44, '2013-09-17 17:55:41'),
(46, 2, 'publication', 45, '2013-09-17 17:55:42'),
(47, 2, 'publication', 46, '2013-09-17 17:55:43'),
(48, 2, 'publication', 47, '2013-09-17 17:55:44'),
(49, 2, 'publication', 48, '2013-09-17 17:55:46'),
(50, 2, 'publication', 49, '2013-09-17 17:55:47'),
(51, 2, 'publication', 50, '2013-09-17 17:55:48'),
(52, 2, 'publication', 51, '2013-09-17 17:55:50'),
(53, 2, 'publication', 52, '2013-09-17 17:55:52'),
(54, 2, 'publication', 53, '2013-09-17 17:55:53'),
(55, 2, 'publication', 54, '2013-09-17 17:55:54'),
(56, 2, 'publication', 55, '2013-09-17 17:55:55'),
(57, 2, 'publication', 56, '2013-09-17 17:55:56'),
(58, 2, 'publication', 57, '2013-09-17 17:55:58'),
(59, 2, 'publication', 58, '2013-09-17 17:55:59'),
(60, 2, 'publication', 59, '2013-09-17 17:56:00'),
(61, 2, 'publication', 60, '2013-09-17 17:56:01'),
(62, 2, 'publication', 61, '2013-09-17 17:56:02'),
(79, 2, 'publication', 78, '2013-09-22 02:58:51'),
(80, 2, 'publication', 79, '2013-09-22 02:58:53'),
(81, 2, 'publication', 80, '2013-09-22 02:58:56'),
(82, 2, 'publication', 81, '2013-09-22 02:58:57'),
(83, 2, 'publication', 82, '2013-09-22 02:58:58'),
(84, 2, 'publication', 83, '2013-09-22 02:58:59'),
(85, 2, 'publication', 84, '2013-09-22 02:59:00'),
(86, 2, 'publication', 85, '2013-09-22 02:59:02'),
(87, 2, 'publication', 86, '2013-09-22 02:59:03'),
(88, 2, 'publication', 87, '2013-09-22 02:59:04'),
(89, 2, 'publication', 88, '2013-09-22 02:59:05'),
(90, 2, 'publication', 89, '2013-09-22 02:59:07'),
(91, 2, 'publication', 90, '2013-09-22 02:59:08'),
(92, 2, 'publication', 91, '2013-09-22 02:59:09'),
(93, 2, 'publication', 92, '2013-09-22 02:59:10'),
(94, 2, 'publication', 93, '2013-09-22 02:59:11'),
(95, 2, 'publication', 94, '2013-09-22 02:59:13'),
(96, 2, 'publication', 95, '2013-09-22 02:59:14'),
(97, 2, 'publication', 96, '2013-09-22 02:59:15'),
(98, 2, 'publication', 97, '2013-09-22 02:59:17'),
(99, 2, 'publication', 98, '2013-09-22 02:59:18'),
(100, 2, 'publication', 99, '2013-09-22 02:59:23'),
(101, 2, 'publication', 100, '2013-09-22 02:59:25'),
(102, 2, 'publication', 101, '2013-09-22 02:59:26'),
(103, 2, 'publication', 102, '2013-09-22 02:59:29'),
(104, 2, 'publication', 103, '2013-09-22 02:59:30'),
(105, 2, 'publication', 104, '2013-09-22 02:59:32'),
(106, 2, 'publication', 105, '2013-09-22 02:59:33'),
(107, 2, 'publication', 106, '2013-09-22 02:59:37'),
(108, 2, 'publication', 107, '2013-09-22 02:59:38'),
(109, 2, 'publication', 108, '2013-09-22 02:59:40'),
(110, 2, 'publication', 109, '2013-09-22 02:59:43'),
(111, 2, 'publication', 110, '2013-09-22 02:59:44'),
(112, 2, 'publication', 111, '2013-09-22 02:59:45'),
(113, 2, 'publication', 112, '2013-09-22 02:59:46'),
(114, 2, 'publication', 113, '2013-09-22 02:59:48'),
(115, 2, 'publication', 114, '2013-09-22 02:59:49'),
(116, 2, 'publication', 115, '2013-09-22 02:59:51'),
(117, 2, 'publication', 116, '2013-09-22 02:59:53'),
(118, 2, 'publication', 117, '2013-09-22 02:59:54'),
(227, 6, 'sous_compte', 0, '2013-09-26 01:53:31'),
(230, 2, 'sous_compte', 0, '2013-10-29 02:28:13'),
(231, 2, 'sous_compte', 0, '2013-10-29 02:28:22'),
(232, 2, 'sous_compte', 0, '2013-10-29 02:28:33'),
(233, 2, 'sous_compte', 0, '2013-10-29 02:28:43'),
(234, 2, 'creer_clan', 1, '2013-10-29 02:29:00'),
(236, 11, 'creer_clan', 3, '2013-10-29 02:30:00'),
(238, 6, 'creer_clan', 5, '2013-10-29 02:30:44'),
(241, 11, 'amis', 3, '2013-10-29 02:36:26'),
(244, 2, 'amis', 5, '2013-10-29 03:15:09'),
(245, 10, 'amis', 6, '2013-10-29 03:15:22'),
(249, 2, 'publication', 227, '2013-10-29 16:21:50'),
(250, 2, 'publication', 228, '2013-10-29 16:21:59'),
(251, 2, 'publication', 229, '2013-10-29 16:22:08'),
(252, 2, 'publication', 230, '2013-10-29 16:22:14'),
(253, 2, 'publication', 231, '2013-10-29 16:22:20'),
(254, 2, 'publication', 232, '2013-10-29 16:22:26'),
(255, 2, 'publication', 233, '2013-10-29 16:22:36'),
(256, 2, 'publication', 234, '2013-10-29 16:22:42'),
(257, 2, 'publication', 235, '2013-10-29 16:22:52'),
(258, 2, 'publication', 236, '2013-10-29 16:22:58'),
(259, 2, 'publication', 237, '2013-10-29 16:23:08'),
(260, 2, 'publication', 238, '2013-10-29 16:23:29'),
(261, 2, 'publication', 239, '2013-10-29 16:23:44'),
(262, 2, 'publication', 240, '2013-10-29 16:23:50'),
(263, 2, 'publication', 241, '2013-10-29 16:24:03'),
(264, 2, 'publication', 242, '2013-10-29 16:24:20'),
(265, 6, 'amis', 8, '2013-10-29 16:25:18'),
(268, 2, 'publication', 243, '2013-10-29 21:18:24'),
(269, 2, 'publication', 244, '2013-10-29 21:18:29'),
(270, 2, 'publication', 245, '2013-10-29 21:18:33'),
(271, 2, 'publication', 246, '2013-10-29 21:19:09'),
(272, 6, 'sous_compte', 0, '2013-12-09 22:50:23'),
(273, 6, 'sous_compte', 0, '2013-12-09 22:50:47'),
(274, 6, 'publication', 263, '2013-12-18 21:26:04'),
(275, 6, 'publication', 264, '2013-12-18 21:26:09'),
(276, 6, 'publication', 265, '2013-12-18 21:26:13'),
(277, 6, 'publication', 266, '2013-12-18 21:27:09');

-- --------------------------------------------------------

--
-- Structure de la table `amis`
--

CREATE TABLE IF NOT EXISTS `amis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `id_jeu_ami` int(11) NOT NULL,
  `date_ajout` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `amis`
--

INSERT INTO `amis` (`id`, `id_jeu`, `id_jeu_ami`, `date_ajout`) VALUES
(8, 6, 2, '2013-10-29 16:25:18');

-- --------------------------------------------------------

--
-- Structure de la table `candidature`
--

CREATE TABLE IF NOT EXISTS `candidature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `id_clan` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `changer_mdp`
--

CREATE TABLE IF NOT EXISTS `changer_mdp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_de_compte` varchar(255) NOT NULL,
  `clef` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Contenu de la table `changer_mdp`
--

INSERT INTO `changer_mdp` (`id`, `nom_de_compte`, `clef`) VALUES
(1, 'paul', 'fUvLjTgaQl2CAKdp6PEr'),
(2, 'paul', 'XzbTdpNl1QsOjc52Jeko'),
(3, 'paul', '4x9czAXWjDeUtLbNMgmV'),
(4, 'paul', 'ujL8JA0pqUwXB1mdtxRg'),
(5, 'paul', 'wznLQIZ8x70OuJvRj1BV'),
(6, 'paul', 'Zdzy2tSVqpJ4TmRw9ibg'),
(7, 'paul', 'mvNdZAE1ko7cgy56CSQi'),
(8, 'paul', 'hfGMoNeJBxYV3k4yPCX6'),
(9, 'paul', 'zCk7A3Dms8QL92tMOuPe'),
(10, 'paul', 'Pvju5Ap3oVtW6iqCl2hb'),
(11, 'paul', 'KpciDnYxMfC8dW9IaBqR'),
(12, 'paul', 'fC4Lq3pGADonJEM1SYeK'),
(13, 'paul', 'VRjO2oE9JyS7UD4Tk8xb'),
(14, 'paul', 'Y2xMFoip97B8EWc1eaDd'),
(15, 'paul', 'S5jsi0DokLb8AMvtaHBE'),
(16, 'paul', 'N6mLTgCPuwOqWvr4bydS'),
(17, 'paul', 'yranB1HE7mfRdCUbODjl'),
(18, 'paul2', 'ZJmuyBPpUFa5jOCQiWYc'),
(19, 'paul', 'yGDCf0gj1dzPFIXZpeiY'),
(20, 'paul2', 'yrBoPi8wzgblA4RV3daQ'),
(21, 'polito', 'wfnUHxNZa9GuoBcpMLA5'),
(22, 'polito', 'ZSTnzutV8D012o4i9Fk3'),
(23, 'polito', '7reOXdENjyWpmLVwGnBu'),
(24, 'polito', 'lAh0mGHIPsacVytnTBuN'),
(25, 'polito', 'lAh0mGHIPsacVytnTBuN'),
(26, 'polito', 'lAh0mGHIPsacVytnTBuN'),
(27, 'polito', 'lAh0mGHIPsacVytnTBuN'),
(28, 'paul', 'D6jpKJkFnQOgxXoY9bsG'),
(29, 'polito', 'fX7em0sgvcTjkLCD1GRQ'),
(30, 'polito', 'fX7em0sgvcTjkLCD1GRQ'),
(31, 'polito', 'fX7em0sgvcTjkLCD1GRQ'),
(32, 'paul', 'NAb23OKzusikM0p5CJ1Z'),
(33, 'paul', 'wZfMj9iLmBnoskYOGKtg'),
(34, 'paul', 'NF5zWMcBhAq2TeRfY6Dv');

-- --------------------------------------------------------

--
-- Structure de la table `clan`
--

CREATE TABLE IF NOT EXISTS `clan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `nom_clan` varchar(255) NOT NULL,
  `photo_clan` varchar(255) NOT NULL,
  `information` text NOT NULL,
  `jeu` varchar(255) NOT NULL,
  `plateforme` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `clan`
--

INSERT INTO `clan` (`id`, `id_jeu`, `nom_clan`, `photo_clan`, `information`, `jeu`, `plateforme`) VALUES
(1, 2, ' hbvun', '0', ' hbinjkl', 'bun', 'Iphone'),
(2, 10, ' gtvyuibnj', '0', 'vyubgjn', 'ctvyubgin', 'Game Gear'),
(3, 11, 'gvybuink', '0', 'vygfihokn', 'byiuoj', 'Lynx'),
(6, 7, 'buoink', '0', 'g vhykj', ' vhbyui', 'Mac OS');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_publication` int(11) NOT NULL,
  `id_publication_communaute` int(11) NOT NULL,
  `id_jeu` int(11) NOT NULL,
  `commentaire` text NOT NULL,
  `date_commentaire` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210 ;

--
-- Contenu de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `id_publication`, `id_publication_communaute`, `id_jeu`, `commentaire`, `date_commentaire`) VALUES
(6, 61, 0, 2, 'dzdazdazdaz', '2013-09-17 18:03:57'),
(7, 61, 0, 2, 'dazdzadza', '2013-09-17 18:03:59'),
(8, 61, 0, 2, 'zddazdaz', '2013-09-17 18:04:00'),
(9, 61, 0, 2, 'zaddazdaz', '2013-09-17 18:04:01'),
(10, 61, 0, 2, 'dazdza', '2013-09-17 18:04:04'),
(97, 0, 16, 2, 'yoyoyoyooyoy', '2013-10-29 04:30:06'),
(98, 0, 16, 2, 'cnuoivez', '2013-10-29 04:30:08'),
(99, 0, 16, 2, 'vezezvvez', '2013-10-29 04:30:10'),
(100, 0, 16, 2, 'vnozi,evz', '2013-10-29 04:30:13'),
(101, 0, 16, 2, 'vneoivez', '2013-10-29 04:30:14'),
(102, 0, 16, 2, 'vbeinoez', '2013-10-29 04:30:16'),
(103, 0, 16, 2, 'vbeuzijo', '2013-10-29 04:30:17'),
(104, 0, 16, 2, ' ebvzuoinvez', '2013-10-29 04:30:19'),
(105, 0, 16, 2, 'v ebziuo', '2013-10-29 04:30:21'),
(106, 0, 16, 2, 'v ebziuoi', '2013-10-29 04:30:23'),
(107, 0, 16, 2, 'bvuh,v', '2013-10-29 04:30:38'),
(108, 111, 0, 2, 'bvuheij', '2013-10-29 04:30:48'),
(109, 111, 0, 2, 'vbuhijez', '2013-10-29 04:30:51'),
(110, 111, 0, 2, 'v biyuhoj', '2013-10-29 04:30:54'),
(111, 111, 0, 2, 'bvuierz', '2013-10-29 04:30:55'),
(112, 111, 0, 2, ' bvuinho', '2013-10-29 04:30:56'),
(113, 111, 0, 2, ' vbuinve', '2013-10-29 04:31:03'),
(114, 111, 0, 2, 'bvuzinhe', '2013-10-29 04:31:06'),
(115, 111, 0, 2, 'vebzuni', '2013-10-29 04:31:07'),
(116, 111, 0, 2, 'vbeznhj', '2013-10-29 04:31:09'),
(117, 111, 0, 2, 'vbuezinhj', '2013-10-29 04:31:10'),
(118, 111, 0, 2, 'bvuez', '2013-10-29 04:31:12'),
(119, 111, 0, 2, 'bnvr', '2013-10-29 04:31:16'),
(120, 111, 0, 2, 'vez', '2013-10-29 04:31:18'),
(121, 111, 0, 2, 'vez', '2013-10-29 04:31:19'),
(122, 111, 0, 2, 'vezzev', '2013-10-29 04:31:21'),
(123, 111, 0, 2, 'vez', '2013-10-29 04:31:24'),
(124, 111, 0, 2, 'vez', '2013-10-29 04:31:25'),
(125, 111, 0, 2, 'vez', '2013-10-29 04:31:27'),
(126, 111, 0, 2, 'bhtr', '2013-10-29 04:31:28'),
(127, 111, 0, 2, 'ntr', '2013-10-29 04:31:30'),
(128, 111, 0, 2, 'caz', '2013-10-29 04:31:31'),
(129, 111, 0, 2, 'bunvezvez', '2013-10-29 04:31:39'),
(130, 111, 0, 2, 'cvvez', '2013-10-29 04:31:41'),
(132, 86, 0, 2, 'vbuijk', '2013-10-29 04:34:19'),
(133, 111, 0, 2, 'buonhiovz', '2013-10-29 04:41:09'),
(134, 111, 0, 2, 'kikooo', '2013-10-29 04:42:41'),
(135, 111, 0, 2, 'plopiki', '2013-10-29 04:42:52'),
(136, 82, 0, 2, 'buhvzne', '2013-10-29 04:43:09'),
(137, 82, 0, 2, 'buhijzeffez', '2013-10-29 04:43:12'),
(138, 82, 0, 2, 'ffezef', '2013-10-29 04:43:14'),
(139, 82, 0, 2, 'vfzeffez', '2013-10-29 04:43:15'),
(140, 82, 0, 2, 'gezgez', '2013-10-29 04:43:18'),
(141, 82, 0, 2, 'fgez', '2013-10-29 04:43:19'),
(142, 82, 0, 2, 'hbuhvivezze', '2013-10-29 04:43:25'),
(143, 0, 16, 2, 'gree', '2013-10-29 04:48:00'),
(144, 0, 16, 2, 'ferzfzezef', '2013-10-29 04:48:03'),
(145, 0, 16, 2, 'ferzfzeefz', '2013-10-29 04:48:04'),
(146, 0, 16, 2, 'vzezefezf', '2013-10-29 04:48:05'),
(147, 0, 16, 2, 'fezzef', '2013-10-29 04:48:07'),
(148, 0, 16, 2, 'trh', '2013-10-29 04:48:10'),
(149, 0, 16, 2, 'j-j-jè', '2013-10-29 04:48:12'),
(150, 0, 16, 2, 'zeezdz', '2013-10-29 04:48:13'),
(151, 0, 16, 2, 'vvbdb ', '2013-10-29 04:48:16'),
(152, 0, 16, 2, 'liuuli', '2013-10-29 04:48:18'),
(153, 0, 16, 2, 'bhtrhtr', '2013-10-29 04:48:20'),
(154, 0, 16, 2, 'azdada', '2013-10-29 04:48:22'),
(155, 111, 0, 2, 'yoyoyo c moi', '2013-10-29 04:55:42'),
(156, 0, 16, 2, 'pinoiouffff', '2013-10-29 05:04:13'),
(157, 0, 16, 2, 'mouloud', '2013-10-29 05:04:34'),
(158, 0, 16, 2, ' vbuoinvs', '2013-10-29 05:11:08'),
(159, 0, 1, 2, 'ezfezzef', '2013-10-29 05:11:46'),
(160, 0, 1, 2, 'ffezzefzef', '2013-10-29 05:11:48'),
(161, 0, 1, 2, 'fezgrere', '2013-10-29 05:11:50'),
(162, 0, 1, 2, 'bebeb', '2013-10-29 05:11:52'),
(163, 0, 1, 2, 'vgerzt', '2013-10-29 05:11:53'),
(164, 0, 1, 2, 'ntr', '2013-10-29 05:11:54'),
(165, 0, 1, 2, 'vezefzefz', '2013-10-29 21:38:42'),
(166, 0, 1, 2, 'fezfzeezf', '2013-10-29 21:38:44'),
(167, 0, 1, 2, 'ezzeez', '2013-10-29 21:38:46'),
(168, 0, 1, 2, 'fezefzezf', '2013-10-29 21:38:48'),
(169, 0, 1, 2, 'zeffezzef', '2013-10-29 21:38:49'),
(170, 0, 1, 2, 'ezfezefz', '2013-10-29 21:38:51'),
(171, 0, 1, 2, 'efvzefzefz', '2013-10-29 21:38:52'),
(172, 0, 1, 2, 'fezefzefz', '2013-10-29 21:38:54'),
(173, 0, 1, 2, 'fezfezefz', '2013-10-29 21:38:55'),
(174, 0, 1, 2, 'ezfezefzzef', '2013-10-29 21:38:57'),
(175, 0, 1, 2, 'fezfezzef', '2013-10-29 21:39:06'),
(176, 0, 1, 2, 'fezafez', '2013-10-29 21:39:09'),
(177, 0, 1, 2, 'ferght', '2013-10-29 21:39:11'),
(178, 0, 1, 2, 'gtrgteeg', '2013-10-29 21:39:13'),
(179, 0, 1, 2, 'fezef', '2013-10-29 21:39:14'),
(180, 0, 1, 2, 'bgerreer', '2013-10-29 21:39:16'),
(181, 0, 1, 2, 'gregrg', '2013-10-29 21:39:18'),
(182, 0, 1, 2, 'grerg', '2013-10-29 21:39:20'),
(183, 0, 1, 2, 'rbggreg', '2013-10-29 21:39:22'),
(184, 0, 1, 2, 'yoyoyoyoy', '2013-10-29 21:39:31'),
(185, 259, 0, 2, 'guirezfe', '2013-10-29 21:50:21'),
(186, 259, 0, 2, 'fezzefzef', '2013-10-29 21:50:24'),
(187, 259, 0, 2, 'ffefzefz', '2013-10-29 21:50:27'),
(188, 259, 0, 2, 'fzezfeezfefzzef', '2013-10-29 21:50:30'),
(189, 259, 0, 2, 'fzerzfeefzzef', '2013-10-29 21:50:32'),
(190, 259, 0, 2, 'grgr', '2013-10-29 21:50:34'),
(191, 259, 0, 2, 'greeeg', '2013-10-29 21:50:37'),
(192, 259, 0, 2, 'gvrerege', '2013-10-29 21:50:40'),
(193, 259, 0, 2, 'gergregre', '2013-10-29 21:50:43'),
(194, 259, 0, 2, 'fzfezzefezf', '2013-10-29 21:50:51'),
(195, 259, 0, 2, 'ezfezzefzefzef', '2013-10-29 21:50:56'),
(196, 259, 0, 2, 'fezzefzefzef', '2013-10-29 21:51:08'),
(197, 259, 0, 2, 'fzzeefzfez', '2013-10-29 21:51:10'),
(198, 259, 0, 2, 'fvezefzzef', '2013-10-29 21:51:13'),
(199, 259, 0, 2, 'vfefzefzefzefz', '2013-10-29 21:51:16'),
(200, 259, 0, 2, 'fzefzefzefz', '2013-10-29 21:51:18'),
(201, 259, 0, 2, 'htrhtrrth', '2013-10-29 21:51:21'),
(202, 259, 0, 2, 'nhntrrtn', '2013-10-29 21:51:24'),
(203, 259, 0, 2, 'nrntrntr', '2013-10-29 21:51:26'),
(204, 259, 0, 2, 'bhreeerh', '2013-10-29 21:51:29'),
(205, 259, 0, 2, 'nyntrn', '2013-10-29 21:51:32'),
(206, 259, 0, 2, 'nnytnntnt', '2013-10-29 21:51:36'),
(207, 259, 0, 2, 'ezgzegzefze', '2013-10-29 21:51:38'),
(208, 259, 0, 2, 'vgreree', '2013-10-29 21:51:41'),
(209, 223, 0, 2, 'yoyoyoyoyo', '2013-11-01 01:12:05');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire_topic`
--

CREATE TABLE IF NOT EXISTS `commentaire_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_topic` int(11) NOT NULL,
  `id_jeu` int(11) NOT NULL,
  `commentaire_topic` text NOT NULL,
  `date_commentaire` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `commentaire_topic`
--

INSERT INTO `commentaire_topic` (`id`, `id_topic`, `id_jeu`, `commentaire_topic`, `date_commentaire`) VALUES
(1, 10, 2, 'pouaaaoaoaooaoaloo', '2013-10-30 02:12:38'),
(2, 10, 2, 'ufzionjfzeijezf', '2013-10-30 02:12:46'),
(3, 10, 2, 'vbgyuhbj bns', '2013-10-30 02:12:51'),
(4, 10, 2, 'buihnhvsjvidvsjnsidv', '2013-10-30 02:12:56'),
(5, 10, 2, 'bhuhinj bisknjkdvs', '2013-10-30 02:13:01'),
(6, 10, 2, ' hvbhuijkn bjuisjvd', '2013-10-30 02:13:05'),
(7, 10, 2, 'bisj bikndvsj kijvds', '2013-10-30 02:13:12'),
(8, 10, 2, 'hbguihjknjbuhijok,njihko', '2013-10-30 02:13:27'),
(9, 10, 2, ' buihknj buiojk,nldvs', '2013-10-30 02:13:33'),
(10, 10, 2, '\r\nh binhkj ijkn, jk\r\n\r\n\r\ncqsbijoqck,s\r\ncqsnioqc^skp\r\n', '2013-10-30 02:13:45'),
(11, 10, 2, 'Pinoiouf batar', '2013-10-30 02:22:54'),
(12, 10, 2, 'Mousolini', '2013-10-30 02:23:07'),
(13, 10, 2, 'Roukmout le rou fichet', '2013-10-30 02:23:18');

-- --------------------------------------------------------

--
-- Structure de la table `communaute`
--

CREATE TABLE IF NOT EXISTS `communaute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `message` text NOT NULL,
  `video` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `date_message` datetime NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `communaute`
--

INSERT INTO `communaute` (`id`, `id_jeu`, `message`, `video`, `photo`, `date_message`, `time`) VALUES
(1, 2, '			', '', '', '2013-10-29 03:49:28', 1383014968),
(2, 2, '			\r\n', '', '', '2013-10-29 03:51:04', 1383015064),
(3, 2, '			bvgziuonvzvezvz\r\ncazacz\r\n\r\n\r\n\r\n\r\ncazzac', '', '', '2013-10-29 03:51:50', 1383015110),
(4, 2, 'nbvuoinvz e', '', '', '2013-10-29 03:52:03', 1383015123),
(5, 2, '			buhionj fez', '', '', '2013-10-29 03:54:50', 1383015290),
(6, 2, '			 vbuoivez', '', '', '2013-10-29 03:54:52', 1383015292),
(7, 2, '			tcfvyigub', '', '', '2013-10-29 03:54:55', 1383015295),
(8, 2, '			tcyfvgiuo', '', '', '2013-10-29 03:54:57', 1383015297),
(9, 2, '			vygèuiho', '', '', '2013-10-29 03:54:59', 1383015299),
(10, 2, '		ctfy-g_èui	', '', '', '2013-10-29 03:55:02', 1383015302),
(11, 2, '			ctfvybgiu', '', '', '2013-10-29 03:55:04', 1383015304),
(12, 2, '			xetrcyvuu', '', '', '2013-10-29 03:55:11', 1383015311),
(13, 2, '			zwretxcygvhb', '', '', '2013-10-29 03:55:14', 1383015314),
(14, 2, '			', '', '982.jpeg', '2013-10-29 03:55:26', 1383015326),
(15, 2, '			tfygubhvgvyibuh vbhjhn', '', '983.jpg', '2013-10-29 03:55:36', 1383015336),
(16, 2, '			 gvybuj', '', '', '2013-10-29 03:56:00', 1383015360),
(17, 2, '			kikrrrkiookou', '', '', '2013-10-29 03:56:12', 1383015372);

-- --------------------------------------------------------

--
-- Structure de la table `connecte`
--

CREATE TABLE IF NOT EXISTS `connecte` (
  `id_jeu` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `connecte`
--

INSERT INTO `connecte` (`id_jeu`, `timestamp`) VALUES
(6, 1387398595);

-- --------------------------------------------------------

--
-- Structure de la table `demande_amis`
--

CREATE TABLE IF NOT EXISTS `demande_amis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu_demandeur` int(11) NOT NULL,
  `id_jeu` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `forum_categorie`
--

CREATE TABLE IF NOT EXISTS `forum_categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `nom_categorie` varchar(255) NOT NULL,
  `information_categorie` varchar(255) NOT NULL,
  `bloc_categorie` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `forum_categorie`
--

INSERT INTO `forum_categorie` (`id`, `id_jeu`, `nom_categorie`, `information_categorie`, `bloc_categorie`) VALUES
(1, 2, 'ih,vokdvs', ' vybugiink,', 'general'),
(2, 2, 'kikriririr', 'uchijceez', 'general'),
(3, 2, 'yguhebzefez', 'fhuifjzfez', 'recrutements'),
(4, 2, 'buhiefzefz', 'buhinfezefz', 'jeu');

-- --------------------------------------------------------

--
-- Structure de la table `forum_topic`
--

CREATE TABLE IF NOT EXISTS `forum_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `statut` varchar(255) NOT NULL,
  `titre_topic` varchar(255) NOT NULL,
  `message_topic` text NOT NULL,
  `date_message` datetime NOT NULL,
  `view` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `forum_topic`
--

INSERT INTO `forum_topic` (`id`, `id_jeu`, `id_categorie`, `statut`, `titre_topic`, `message_topic`, `date_message`, `view`) VALUES
(1, 2, 1, '', 'buoijvivvve', 'buinvivds', '2013-10-30 01:26:37', 0),
(2, 2, 1, '', 'uihobzuevezveznjkevz', 'bghinaefzhuojefzujefz', '2013-10-30 01:26:46', 0),
(3, 2, 1, '', 'buhionlvhuive', ' buinhock,ez', '2013-10-30 01:26:55', 0),
(4, 2, 1, '', 'bhvionhjeviz', 'hbuihonezf bnfez', '2013-10-30 01:27:03', 0),
(5, 2, 1, '', 'vbghiojnuefzhjiefz', 'hbghuioznfehfzei', '2013-10-30 01:27:10', 0),
(6, 2, 1, '', 'vbujezhbyfnjefzhuhfze', 'hbvyghnjbfeziefz', '2013-10-30 01:27:18', 0),
(7, 2, 1, '', 'bgh_ijonbiufez', 'bughinoj ebfzhiunefz', '2013-10-30 01:27:26', 0),
(8, 2, 1, '', 'bghuionjhbhuijezf', 'ghf_inebfziu', '2013-10-30 01:27:41', 0),
(9, 2, 1, '', 'bg_hyiubihfze', ' vbguhinojhbuhjiezf', '2013-10-30 01:27:49', 0),
(10, 2, 1, '', 'ritoca', 'vgyhu', '2013-10-30 02:03:50', 0),
(11, 2, 3, '', 'hbik,', 'vyguhi', '2013-10-30 02:05:01', 0),
(12, 2, 3, '', 'uhhu', 'vtybu', '2013-10-30 02:05:09', 0),
(13, 2, 2, '', 'yuhijk,ce', ' vbguinhokj hbuinjk', '2013-10-30 02:34:12', 0),
(14, 2, 2, '', ' hbuionkjl nki', 'nij,okj n', '2013-10-30 02:34:51', 0);

-- --------------------------------------------------------

--
-- Structure de la table `jeu`
--

CREATE TABLE IF NOT EXISTS `jeu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_de_compte` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `nom_jeu` varchar(255) NOT NULL,
  `id_clan` int(11) NOT NULL,
  `photo_profil` varchar(11) NOT NULL,
  `description` text NOT NULL,
  `plateforme` varchar(255) NOT NULL,
  `date_modification` datetime NOT NULL,
  `geth` int(11) NOT NULL,
  `clan_view` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Contenu de la table `jeu`
--

INSERT INTO `jeu` (`id`, `nom_de_compte`, `pseudo`, `nom_jeu`, `id_clan`, `photo_profil`, `description`, `plateforme`, `date_modification`, `geth`, `clan_view`) VALUES
(2, 'paul2', 'geggrre', 'reeg', 1, '967.jpg', 'rg', 'Amiga CD32', '0000-00-00 00:00:00', 0, 0),
(3, 'paulé@à&', 'fezfezfzze', 'adzdazadzadz', 0, '0', 'zefzefzef', 'Atari 5200', '0000-00-00 00:00:00', 0, 0),
(4, 'polito', 'zdaadz', 'adzadz', 0, '0', 'dazdaz', 'Atari 7800', '0000-00-00 00:00:00', 0, 0),
(6, 'paul', 'ybviibuib', 'uh', 0, '1030.JPG', 'cefzezfezfzefze', 'GameBoy', '0000-00-00 00:00:00', 0, 0),
(10, 'paul2', 'buino', 'buinhpjo', 2, '0', 'hbuio,', 'Dreamcast', '0000-00-00 00:00:00', 0, 0),
(11, 'paul2', 'buuiu', 'vyyuihb', 3, '0', 'vhyiub', 'Amiga CD32', '0000-00-00 00:00:00', 0, 0),
(12, 'paul2', 'h bjun', ' jbinj', 1, '0', 'hubgionj', 'Dreamcast', '0000-00-00 00:00:00', 0, 0),
(13, 'paul2', 'hbyugu', 'bv_uihonn', 1, '0', 'vhygiuhn', 'Dreamcast', '0000-00-00 00:00:00', 0, 0),
(29, 'pmlo', 'ezfez', 'zeffez', 0, '1013.JPG', 'efzefzf', 'Atari 7800', '0000-00-00 00:00:00', 0, 0),
(36, 'pmlo', 'azdzaddaz', 'azdadz', 0, '', 'zaadzdza', 'Atari 5200', '0000-00-00 00:00:00', 0, 0),
(38, 'pmlo', 'zaddazza', 'zaddaz', 0, '', 'zaddzaazd', 'Atari 5200', '0000-00-00 00:00:00', 0, 0),
(39, 'paul', 'qsqcscqsc', 'scqcsc', 0, '1029.JPG', 'adzadzadzdazdazscqs', 'Dreamcast', '0000-00-00 00:00:00', 0, 0),
(40, 'paul', 'qcsqsc', 'scqqcscqs', 0, '1028.JPG', 'yoytrrrttrrttrtrfezfezs', 'Dreamcast', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_de_compte` varchar(255) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL,
  `reponse` varchar(255) NOT NULL,
  `date_inscription` datetime NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `pays` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`id`, `nom_de_compte`, `mot_de_passe`, `email`, `question`, `reponse`, `date_inscription`, `sexe`, `pays`, `ville`, `youtube`) VALUES
(1, 'paul', '647e347abb40505d6c9fe4216203a38c4838ad77', 'paul-decath@hotmail.fr', 'Quel est votre jeu préféré?', 'paul', '2013-09-17 00:34:21', 'homme', 'paul', 'paul', ''),
(2, 'paul2', '647e347abb40505d6c9fe4216203a38c4838ad77', 'paul-decath@hotmail.fr', 'Quel est le nom de jeune fille de votre mère?', 'paul', '2013-09-17 01:26:24', 'homme', 'paul', 'paul', ''),
(3, 'paulé@à&', '647e347abb40505d6c9fe4216203a38c4838ad77', 'paul-decath@hotmail.fr', 'Quel est votre jeu préféré?', 'paul', '2013-09-18 00:09:25', 'homme', 'paul', 'dfez', ''),
(4, 'polito', '647e347abb40505d6c9fe4216203a38c4838ad77', 'paul.decath@hotmail.fr', 'Quel est votre jeu préféré?', 'paulo', '2013-09-18 02:07:23', 'homme', 'paul', 'pauk', ''),
(5, 'pmlo', '647e347abb40505d6c9fe4216203a38c4838ad77', 'paul-decath@hotmail.fr', 'Quelle est votre console préférée?', 'adzdazdaz', '2013-12-09 16:57:33', 'homme', 'adzdazaz', 'ddazzadzad', '');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `message` text NOT NULL,
  `destinataire` int(11) NOT NULL,
  `date_message` datetime NOT NULL,
  `view` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu_lieu` int(11) NOT NULL,
  `id_publication` int(11) NOT NULL,
  `publication` varchar(255) NOT NULL,
  `date_notification` datetime NOT NULL,
  `commentaire` varchar(255) NOT NULL,
  `id_jeu_publi` int(11) NOT NULL,
  `id_jeu_com` int(11) NOT NULL,
  `profil_ou_clan` varchar(255) NOT NULL,
  `id_clan` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=164 ;

--
-- Contenu de la table `notifications`
--

INSERT INTO `notifications` (`id`, `id_jeu_lieu`, `id_publication`, `publication`, `date_notification`, `commentaire`, `id_jeu_publi`, `id_jeu_com`, `profil_ou_clan`, `id_clan`, `view`) VALUES
(9, 2, 61, '', '2013-09-17 18:03:57', 'dzdazdazdaz', 0, 2, 'profil', 0, 0),
(10, 2, 61, '', '2013-09-17 18:03:59', 'dazdzadza', 0, 2, 'profil', 0, 0),
(11, 2, 61, '', '2013-09-17 18:04:00', 'zddazdaz', 0, 2, 'profil', 0, 0),
(12, 2, 61, '', '2013-09-17 18:04:01', 'zaddazdaz', 0, 2, 'profil', 0, 0),
(13, 2, 61, '', '2013-09-17 18:04:04', 'dazdza', 0, 2, 'profil', 0, 0),
(100, 2, 111, '', '2013-10-29 04:30:48', 'bvuheij', 0, 2, 'profil', 0, 0),
(101, 2, 111, '', '2013-10-29 04:30:51', 'vbuhijez', 0, 2, 'profil', 0, 0),
(102, 2, 111, '', '2013-10-29 04:30:54', 'v biyuhoj', 0, 2, 'profil', 0, 0),
(103, 2, 111, '', '2013-10-29 04:30:55', 'bvuierz', 0, 2, 'profil', 0, 0),
(104, 2, 111, '', '2013-10-29 04:30:56', ' bvuinho', 0, 2, 'profil', 0, 0),
(105, 2, 111, '', '2013-10-29 04:31:03', ' vbuinve', 0, 2, 'profil', 0, 0),
(106, 2, 111, '', '2013-10-29 04:31:06', 'bvuzinhe', 0, 2, 'profil', 0, 0),
(107, 2, 111, '', '2013-10-29 04:31:07', 'vebzuni', 0, 2, 'profil', 0, 0),
(108, 2, 111, '', '2013-10-29 04:31:09', 'vbeznhj', 0, 2, 'profil', 0, 0),
(109, 2, 111, '', '2013-10-29 04:31:10', 'vbuezinhj', 0, 2, 'profil', 0, 0),
(110, 2, 111, '', '2013-10-29 04:31:12', 'bvuez', 0, 2, 'profil', 0, 0),
(111, 2, 111, '', '2013-10-29 04:31:16', 'bnvr', 0, 2, 'profil', 0, 0),
(112, 2, 111, '', '2013-10-29 04:31:18', 'vez', 0, 2, 'profil', 0, 0),
(113, 2, 111, '', '2013-10-29 04:31:19', 'vez', 0, 2, 'profil', 0, 0),
(114, 2, 111, '', '2013-10-29 04:31:21', 'vezzev', 0, 2, 'profil', 0, 0),
(115, 2, 111, '', '2013-10-29 04:31:24', 'vez', 0, 2, 'profil', 0, 0),
(116, 2, 111, '', '2013-10-29 04:31:25', 'vez', 0, 2, 'profil', 0, 0),
(117, 2, 111, '', '2013-10-29 04:31:27', 'vez', 0, 2, 'profil', 0, 0),
(118, 2, 111, '', '2013-10-29 04:31:28', 'bhtr', 0, 2, 'profil', 0, 0),
(119, 2, 111, '', '2013-10-29 04:31:30', 'ntr', 0, 2, 'profil', 0, 0),
(120, 2, 111, '', '2013-10-29 04:31:31', 'caz', 0, 2, 'profil', 0, 0),
(121, 2, 111, '', '2013-10-29 04:31:39', 'bunvezvez', 0, 2, 'profil', 0, 0),
(122, 2, 111, '', '2013-10-29 04:31:41', 'cvvez', 0, 2, 'profil', 0, 0),
(124, 2, 86, '', '2013-10-29 04:34:19', 'vbuijk', 0, 2, 'profil', 0, 0),
(125, 2, 111, '', '2013-10-29 04:41:09', 'buonhiovz', 0, 2, 'profil', 0, 0),
(126, 2, 111, '', '2013-10-29 04:42:41', 'kikooo', 0, 2, 'profil', 0, 0),
(127, 2, 111, '', '2013-10-29 04:42:52', 'plopiki', 0, 2, 'profil', 0, 0),
(128, 2, 82, '', '2013-10-29 04:43:09', 'buhvzne', 0, 2, 'profil', 0, 0),
(129, 2, 82, '', '2013-10-29 04:43:12', 'buhijzeffez', 0, 2, 'profil', 0, 0),
(130, 2, 82, '', '2013-10-29 04:43:14', 'ffezef', 0, 2, 'profil', 0, 0),
(131, 2, 82, '', '2013-10-29 04:43:15', 'vfzeffez', 0, 2, 'profil', 0, 0),
(132, 2, 82, '', '2013-10-29 04:43:18', 'gezgez', 0, 2, 'profil', 0, 0),
(133, 2, 82, '', '2013-10-29 04:43:19', 'fgez', 0, 2, 'profil', 0, 0),
(134, 2, 82, '', '2013-10-29 04:43:25', 'hbuhvivezze', 0, 2, 'profil', 0, 0),
(135, 2, 111, '', '2013-10-29 04:55:42', 'yoyoyo c moi', 0, 2, 'profil', 0, 0),
(136, 0, 0, ' bvnzevezev', '2013-10-29 21:41:06', '', 2, 0, 'clan', 1, 0),
(137, 0, 0, 'vgyhuivez', '2013-10-29 21:41:10', '', 2, 0, 'clan', 1, 0),
(138, 0, 0, 'vybunj', '2013-10-29 21:41:23', '', 2, 0, 'clan', 1, 0),
(139, 0, 0, 'gvybhujce', '2013-10-29 21:44:10', '', 2, 0, 'clan', 1, 0),
(140, 0, 0, 'vybiuvezzev', '2013-10-29 21:44:17', '', 2, 0, 'clan', 1, 0),
(141, 0, 259, '', '2013-10-29 21:50:21', 'guirezfe', 0, 2, 'clan', 1, 1),
(142, 0, 259, '', '2013-10-29 21:50:24', 'fezzefzef', 0, 2, 'clan', 1, 1),
(143, 0, 259, '', '2013-10-29 21:50:27', 'ffefzefz', 0, 2, 'clan', 1, 1),
(144, 0, 259, '', '2013-10-29 21:50:30', 'fzezfeezfefzzef', 0, 2, 'clan', 1, 1),
(145, 0, 259, '', '2013-10-29 21:50:32', 'fzerzfeefzzef', 0, 2, 'clan', 1, 1),
(146, 0, 259, '', '2013-10-29 21:50:34', 'grgr', 0, 2, 'clan', 1, 1),
(147, 0, 259, '', '2013-10-29 21:50:37', 'greeeg', 0, 2, 'clan', 1, 1),
(148, 0, 259, '', '2013-10-29 21:50:40', 'gvrerege', 0, 2, 'clan', 1, 1),
(149, 0, 259, '', '2013-10-29 21:50:43', 'gergregre', 0, 2, 'clan', 1, 1),
(150, 0, 259, '', '2013-10-29 21:51:08', 'fezzefzefzef', 0, 2, 'clan', 1, 1),
(151, 0, 259, '', '2013-10-29 21:51:10', 'fzzeefzfez', 0, 2, 'clan', 1, 1),
(152, 0, 259, '', '2013-10-29 21:51:13', 'fvezefzzef', 0, 2, 'clan', 1, 1),
(153, 0, 259, '', '2013-10-29 21:51:16', 'vfefzefzefzefz', 0, 2, 'clan', 1, 1),
(154, 0, 259, '', '2013-10-29 21:51:18', 'fzefzefzefz', 0, 2, 'clan', 1, 1),
(155, 0, 259, '', '2013-10-29 21:51:21', 'htrhtrrth', 0, 2, 'clan', 1, 1),
(156, 0, 259, '', '2013-10-29 21:51:24', 'nhntrrtn', 0, 2, 'clan', 1, 1),
(157, 0, 259, '', '2013-10-29 21:51:26', 'nrntrntr', 0, 2, 'clan', 1, 1),
(158, 0, 259, '', '2013-10-29 21:51:29', 'bhreeerh', 0, 2, 'clan', 1, 1),
(159, 0, 259, '', '2013-10-29 21:51:32', 'nyntrn', 0, 2, 'clan', 1, 1),
(160, 0, 259, '', '2013-10-29 21:51:36', 'nnytnntnt', 0, 2, 'clan', 1, 1),
(161, 0, 259, '', '2013-10-29 21:51:38', 'ezgzegzefze', 0, 2, 'clan', 1, 1),
(162, 0, 259, '', '2013-10-29 21:51:41', 'vgreree', 0, 2, 'clan', 1, 1),
(163, 1, 223, '', '2013-11-01 01:12:05', 'yoyoyoyoyo', 0, 2, 'profil', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresse_stockage` varchar(255) NOT NULL,
  `date_enregistrement` datetime NOT NULL,
  `id_jeu` int(11) NOT NULL,
  `id_clan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `photos`
--

INSERT INTO `photos` (`id`, `adresse_stockage`, `date_enregistrement`, `id_jeu`, `id_clan`) VALUES
(1, '982.jpeg', '2013-10-29 03:55:26', 2, 0),
(2, '983.jpg', '2013-10-29 03:55:36', 2, 0),
(3, '984.jpg', '2013-10-29 16:21:59', 2, 0),
(4, '985.jpeg', '2013-10-29 16:22:08', 2, 0),
(5, '986.jpg', '2013-10-29 16:22:14', 2, 0),
(6, '987.jpeg', '2013-10-29 16:22:20', 2, 0),
(7, '988.jpg', '2013-10-29 16:22:26', 2, 0),
(8, '989.jpg', '2013-10-29 16:22:36', 2, 0),
(9, '990.jpeg', '2013-10-29 16:22:42', 2, 0),
(10, '991.png', '2013-10-29 16:22:52', 2, 0),
(11, '992.png', '2013-10-29 16:22:58', 2, 0),
(12, '993.jpg', '2013-10-29 16:23:08', 2, 0),
(13, '994.jpeg', '2013-10-29 21:41:38', 0, 1),
(14, '995.jpg', '2013-10-29 21:41:44', 0, 1),
(15, '996.jpg', '2013-10-29 21:41:50', 0, 1),
(16, '997.jpg', '2013-10-29 21:41:57', 0, 1),
(17, '998.jpg', '2013-10-29 21:42:04', 0, 1),
(18, '999.jpg', '2013-10-29 21:42:11', 0, 1),
(19, '1000.jpg', '2013-10-29 21:42:18', 0, 1),
(20, '1031.jpeg', '2013-12-18 21:26:04', 6, 0);

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `message` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `date_publication` datetime NOT NULL,
  `lieu_publication` varchar(255) NOT NULL,
  `lieu_publication_id_jeu` int(11) NOT NULL,
  `profil_ou_clan` varchar(255) NOT NULL,
  `id_clan` int(11) NOT NULL,
  `video` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=267 ;

--
-- Contenu de la table `publication`
--

INSERT INTO `publication` (`id`, `id_jeu`, `message`, `photo`, `date_publication`, `lieu_publication`, `lieu_publication_id_jeu`, `profil_ou_clan`, `id_clan`, `video`) VALUES
(5, 2, 'dz', '', '2013-09-17 17:54:47', 'paul2', 2, 'profil', 0, ''),
(6, 2, 'daz', '', '2013-09-17 17:54:48', 'paul2', 2, 'profil', 0, ''),
(7, 2, 'daz', '', '2013-09-17 17:54:49', 'paul2', 2, 'profil', 0, ''),
(8, 2, 'dza', '', '2013-09-17 17:54:51', 'paul2', 2, 'profil', 0, ''),
(9, 2, 'adz', '', '2013-09-17 17:54:51', 'paul2', 2, 'profil', 0, ''),
(10, 2, 'adz', '', '2013-09-17 17:54:52', 'paul2', 2, 'profil', 0, ''),
(11, 2, 'adz', '', '2013-09-17 17:54:54', 'paul2', 2, 'profil', 0, ''),
(12, 2, 'az', '', '2013-09-17 17:54:55', 'paul2', 2, 'profil', 0, ''),
(13, 2, 'zad', '', '2013-09-17 17:54:56', 'paul2', 2, 'profil', 0, ''),
(14, 2, 'dz', '', '2013-09-17 17:54:57', 'paul2', 2, 'profil', 0, ''),
(15, 2, 'daz', '', '2013-09-17 17:54:59', 'paul2', 2, 'profil', 0, ''),
(16, 2, 'daz', '', '2013-09-17 17:55:00', 'paul2', 2, 'profil', 0, ''),
(17, 2, 'dz', '', '2013-09-17 17:55:02', 'paul2', 2, 'profil', 0, ''),
(18, 2, 'zad', '', '2013-09-17 17:55:03', 'paul2', 2, 'profil', 0, ''),
(19, 2, 'adz', '', '2013-09-17 17:55:04', 'paul2', 2, 'profil', 0, ''),
(20, 2, 'daz', '', '2013-09-17 17:55:05', 'paul2', 2, 'profil', 0, ''),
(21, 2, 'daz', '', '2013-09-17 17:55:06', 'paul2', 2, 'profil', 0, ''),
(22, 2, 'zad', '', '2013-09-17 17:55:11', 'paul2', 2, 'profil', 0, ''),
(23, 2, 'daz', '', '2013-09-17 17:55:13', 'paul2', 2, 'profil', 0, ''),
(24, 2, 'daz', '', '2013-09-17 17:55:14', 'paul2', 2, 'profil', 0, ''),
(25, 2, 'daz', '', '2013-09-17 17:55:16', 'paul2', 2, 'profil', 0, ''),
(26, 2, 'daz', '', '2013-09-17 17:55:17', 'paul2', 2, 'profil', 0, ''),
(27, 2, 'adz', '', '2013-09-17 17:55:18', 'paul2', 2, 'profil', 0, ''),
(28, 2, 'daz', '', '2013-09-17 17:55:19', 'paul2', 2, 'profil', 0, ''),
(29, 2, 'daz', '', '2013-09-17 17:55:20', 'paul2', 2, 'profil', 0, ''),
(30, 2, 'zd', '', '2013-09-17 17:55:21', 'paul2', 2, 'profil', 0, ''),
(31, 2, 'adz', '', '2013-09-17 17:55:23', 'paul2', 2, 'profil', 0, ''),
(32, 2, 'daz', '', '2013-09-17 17:55:24', 'paul2', 2, 'profil', 0, ''),
(33, 2, 'daz', '', '2013-09-17 17:55:25', 'paul2', 2, 'profil', 0, ''),
(34, 2, 'zad', '', '2013-09-17 17:55:26', 'paul2', 2, 'profil', 0, ''),
(35, 2, 'zad', '', '2013-09-17 17:55:28', 'paul2', 2, 'profil', 0, ''),
(36, 2, 'adz', '', '2013-09-17 17:55:29', 'paul2', 2, 'profil', 0, ''),
(37, 2, 'azd', '', '2013-09-17 17:55:30', 'paul2', 2, 'profil', 0, ''),
(38, 2, 'daz', '', '2013-09-17 17:55:31', 'paul2', 2, 'profil', 0, ''),
(39, 2, 'dza', '', '2013-09-17 17:55:32', 'paul2', 2, 'profil', 0, ''),
(40, 2, 'daz', '', '2013-09-17 17:55:34', 'paul2', 2, 'profil', 0, ''),
(41, 2, 'adz', '', '2013-09-17 17:55:36', 'paul2', 2, 'profil', 0, ''),
(42, 2, 'azdaz', '', '2013-09-17 17:55:38', 'paul2', 2, 'profil', 0, ''),
(43, 2, 'daz', '', '2013-09-17 17:55:39', 'paul2', 2, 'profil', 0, ''),
(44, 2, 'dza', '', '2013-09-17 17:55:41', 'paul2', 2, 'profil', 0, ''),
(45, 2, 'zd', '', '2013-09-17 17:55:42', 'paul2', 2, 'profil', 0, ''),
(46, 2, 'dz', '', '2013-09-17 17:55:43', 'paul2', 2, 'profil', 0, ''),
(47, 2, 'd', '', '2013-09-17 17:55:44', 'paul2', 2, 'profil', 0, ''),
(48, 2, 'az', '', '2013-09-17 17:55:46', 'paul2', 2, 'profil', 0, ''),
(49, 2, 'adz', '', '2013-09-17 17:55:47', 'paul2', 2, 'profil', 0, ''),
(50, 2, 'azd', '', '2013-09-17 17:55:48', 'paul2', 2, 'profil', 0, ''),
(51, 2, 'dza', '', '2013-09-17 17:55:50', 'paul2', 2, 'profil', 0, ''),
(52, 2, 'daz', '', '2013-09-17 17:55:52', 'paul2', 2, 'profil', 0, ''),
(53, 2, 'adz', '', '2013-09-17 17:55:53', 'paul2', 2, 'profil', 0, ''),
(54, 2, 'daz', '', '2013-09-17 17:55:54', 'paul2', 2, 'profil', 0, ''),
(55, 2, 'dza', '', '2013-09-17 17:55:55', 'paul2', 2, 'profil', 0, ''),
(56, 2, 'daz', '', '2013-09-17 17:55:56', 'paul2', 2, 'profil', 0, ''),
(57, 2, 'daz', '', '2013-09-17 17:55:58', 'paul2', 2, 'profil', 0, ''),
(58, 2, 'daz', '', '2013-09-17 17:55:59', 'paul2', 2, 'profil', 0, ''),
(59, 2, 'adz', '', '2013-09-17 17:56:00', 'paul2', 2, 'profil', 0, ''),
(60, 2, 'daz', '', '2013-09-17 17:56:01', 'paul2', 2, 'profil', 0, ''),
(61, 2, 'dza', '', '2013-09-17 17:56:02', 'paul2', 2, 'profil', 0, ''),
(78, 2, 'fezfezzef', '', '2013-09-22 02:58:51', 'paul2', 2, 'profil', 0, ''),
(79, 2, 'fzefez', '', '2013-09-22 02:58:53', 'paul2', 2, 'profil', 0, ''),
(80, 2, 'fezef', '', '2013-09-22 02:58:56', 'paul2', 2, 'profil', 0, ''),
(81, 2, 'efzzef', '', '2013-09-22 02:58:57', 'paul2', 2, 'profil', 0, ''),
(82, 2, 'efzfez', '', '2013-09-22 02:58:58', 'paul2', 2, 'profil', 0, ''),
(83, 2, 'efzfez', '', '2013-09-22 02:58:59', 'paul2', 2, 'profil', 0, ''),
(84, 2, 'efz', '', '2013-09-22 02:59:00', 'paul2', 2, 'profil', 0, ''),
(85, 2, 'zef', '', '2013-09-22 02:59:02', 'paul2', 2, 'profil', 0, ''),
(86, 2, 'efz', '', '2013-09-22 02:59:03', 'paul2', 2, 'profil', 0, ''),
(87, 2, 'efz', '', '2013-09-22 02:59:04', 'paul2', 2, 'profil', 0, ''),
(88, 2, 'zef', '', '2013-09-22 02:59:05', 'paul2', 2, 'profil', 0, ''),
(89, 2, 'zef', '', '2013-09-22 02:59:07', 'paul2', 2, 'profil', 0, ''),
(90, 2, 'efz', '', '2013-09-22 02:59:08', 'paul2', 2, 'profil', 0, ''),
(91, 2, 'efz', '', '2013-09-22 02:59:09', 'paul2', 2, 'profil', 0, ''),
(92, 2, 'ezf', '', '2013-09-22 02:59:10', 'paul2', 2, 'profil', 0, ''),
(93, 2, 'efz', '', '2013-09-22 02:59:11', 'paul2', 2, 'profil', 0, ''),
(94, 2, 'ez', '', '2013-09-22 02:59:13', 'paul2', 2, 'profil', 0, ''),
(95, 2, 'fez', '', '2013-09-22 02:59:14', 'paul2', 2, 'profil', 0, ''),
(96, 2, 'efz', '', '2013-09-22 02:59:15', 'paul2', 2, 'profil', 0, ''),
(97, 2, 'efz', '', '2013-09-22 02:59:17', 'paul2', 2, 'profil', 0, ''),
(98, 2, 'fez', '', '2013-09-22 02:59:18', 'paul2', 2, 'profil', 0, ''),
(99, 2, 'gr', '', '2013-09-22 02:59:23', 'paul2', 2, 'profil', 0, ''),
(100, 2, 'e', '', '2013-09-22 02:59:25', 'paul2', 2, 'profil', 0, ''),
(101, 2, 're', '', '2013-09-22 02:59:26', 'paul2', 2, 'profil', 0, ''),
(102, 2, 'rg', '', '2013-09-22 02:59:29', 'paul2', 2, 'profil', 0, ''),
(103, 2, 'g', '', '2013-09-22 02:59:30', 'paul2', 2, 'profil', 0, ''),
(104, 2, 'ge', '', '2013-09-22 02:59:32', 'paul2', 2, 'profil', 0, ''),
(105, 2, 'thr', '', '2013-09-22 02:59:33', 'paul2', 2, 'profil', 0, ''),
(106, 2, 'adz', '', '2013-09-22 02:59:37', 'paul2', 2, 'profil', 0, ''),
(107, 2, 'adz', '', '2013-09-22 02:59:38', 'paul2', 2, 'profil', 0, ''),
(108, 2, 'daz', '', '2013-09-22 02:59:40', 'paul2', 2, 'profil', 0, ''),
(109, 2, 'az', '', '2013-09-22 02:59:43', 'paul2', 2, 'profil', 0, ''),
(110, 2, 'da', '', '2013-09-22 02:59:44', 'paul2', 2, 'profil', 0, ''),
(111, 2, 'adz', '', '2013-09-22 02:59:45', 'paul2', 2, 'profil', 0, ''),
(112, 2, 'adz', '', '2013-09-22 02:59:46', 'paul2', 2, 'profil', 0, ''),
(113, 2, 'adz', '', '2013-09-22 02:59:48', 'paul2', 2, 'profil', 0, ''),
(114, 2, 'adz', '', '2013-09-22 02:59:49', 'paul2', 2, 'profil', 0, ''),
(115, 2, 'adz', '', '2013-09-22 02:59:51', 'paul2', 2, 'profil', 0, ''),
(116, 2, 'da', '', '2013-09-22 02:59:53', 'paul2', 2, 'profil', 0, ''),
(117, 2, 'zad', '', '2013-09-22 02:59:54', 'paul2', 2, 'profil', 0, ''),
(266, 6, '', '', '2013-12-18 21:27:09', 'paul', 6, 'profil', 0, 'http://www.youtube.com/watch?v=ujFR8IlaBWc'),
(265, 6, 'hybuinvre', '', '2013-12-18 21:26:13', 'paul', 6, 'profil', 0, ''),
(263, 6, '', '1031.jpeg', '2013-12-18 21:26:04', 'paul', 6, 'profil', 0, ''),
(264, 6, 'tvuiybuoinvbrsiu', '', '2013-12-18 21:26:09', 'paul', 6, 'profil', 0, ''),
(227, 2, 'uuefzzefzefzef', '', '2013-10-29 16:21:50', 'paul2', 2, 'profil', 0, ''),
(228, 2, '', '984.jpg', '2013-10-29 16:21:59', 'paul2', 2, 'profil', 0, ''),
(229, 2, '', '985.jpeg', '2013-10-29 16:22:08', 'paul2', 2, 'profil', 0, ''),
(230, 2, '', '986.jpg', '2013-10-29 16:22:14', 'paul2', 2, 'profil', 0, ''),
(231, 2, '', '987.jpeg', '2013-10-29 16:22:20', 'paul2', 2, 'profil', 0, ''),
(232, 2, '', '988.jpg', '2013-10-29 16:22:26', 'paul2', 2, 'profil', 0, ''),
(233, 2, '', '989.jpg', '2013-10-29 16:22:36', 'paul2', 2, 'profil', 0, ''),
(234, 2, '', '990.jpeg', '2013-10-29 16:22:42', 'paul2', 2, 'profil', 0, ''),
(235, 2, '', '991.png', '2013-10-29 16:22:52', 'paul2', 2, 'profil', 0, ''),
(236, 2, '', '992.png', '2013-10-29 16:22:58', 'paul2', 2, 'profil', 0, ''),
(237, 2, '', '993.jpg', '2013-10-29 16:23:08', 'paul2', 2, 'profil', 0, ''),
(238, 2, '', '', '2013-10-29 16:23:29', 'paul2', 2, 'profil', 0, 'http://www.youtube.com/watch?v=Ys_XKiykA44'),
(239, 2, '', '', '2013-10-29 16:23:44', 'paul2', 2, 'profil', 0, 'http://www.youtube.com/watch?v=OUkkaqSNduU'),
(240, 2, '', '', '2013-10-29 16:23:50', 'paul2', 2, 'profil', 0, 'http://www.youtube.com/watch?v=OUkkaqSNduU'),
(241, 2, '', '', '2013-10-29 16:24:03', 'paul2', 2, 'profil', 0, 'http://www.youtube.com/watch?v=hqpGw-NXBG4'),
(242, 2, '', '', '2013-10-29 16:24:20', 'paul2', 2, 'profil', 0, 'https://www.youtube.com/watch?v=SgxQJRh2-fs'),
(243, 2, '', '', '2013-10-29 21:18:24', 'paul', 1, 'profil', 0, 'http://www.youtube.com/watch?v=mfFQuhWaA_k'),
(244, 2, '', '', '2013-10-29 21:18:29', 'paul', 1, 'profil', 0, 'http://www.youtube.com/watch?v=mfFQuhWaA_k'),
(245, 2, '', '', '2013-10-29 21:18:33', 'paul', 1, 'profil', 0, 'http://www.youtube.com/watch?v=mfFQuhWaA_k'),
(246, 2, '', '', '2013-10-29 21:19:09', 'paul2', 2, 'profil', 0, 'http://www.youtube.com/watch?v=mfFQuhWaA_k'),
(247, 2, ' bvnzevezev', '', '2013-10-29 21:41:06', '', 0, 'clan', 1, ''),
(248, 2, 'vgyhuivez', '', '2013-10-29 21:41:10', '', 0, 'clan', 1, ''),
(249, 2, 'vybunj', '', '2013-10-29 21:41:23', '', 0, 'clan', 1, 'http://www.youtube.com/watch?v=mfFQuhWaA_k'),
(250, 2, '', '', '2013-10-29 21:41:28', '', 0, 'clan', 1, 'http://www.youtube.com/watch?v=mfFQuhWaA_k'),
(251, 2, '', '994.jpeg', '2013-10-29 21:41:38', '', 0, 'clan', 1, ''),
(252, 2, '', '995.jpg', '2013-10-29 21:41:44', '', 0, 'clan', 1, ''),
(253, 2, '', '996.jpg', '2013-10-29 21:41:50', '', 0, 'clan', 1, ''),
(254, 2, '', '997.jpg', '2013-10-29 21:41:57', '', 0, 'clan', 1, ''),
(255, 2, '', '998.jpg', '2013-10-29 21:42:04', '', 0, 'clan', 1, ''),
(256, 2, '', '999.jpg', '2013-10-29 21:42:11', '', 0, 'clan', 1, ''),
(257, 2, '', '1000.jpg', '2013-10-29 21:42:18', '', 0, 'clan', 1, ''),
(258, 2, '', '', '2013-10-29 21:42:23', '', 0, 'clan', 1, 'http://www.youtube.com/watch?v=mfFQuhWaA_k'),
(259, 2, '', '', '2013-10-29 21:44:01', '', 0, 'clan', 1, 'http://www.youtube.com/watch?v=bRbL9-FqBFw'),
(260, 2, '', '', '2013-10-29 21:44:06', '', 0, 'clan', 1, 'http://www.youtube.com/watch?v=bRbL9-FqBFw'),
(261, 2, 'gvybhujce', '', '2013-10-29 21:44:10', '', 0, 'clan', 1, ''),
(262, 2, 'vybiuvezzev', '', '2013-10-29 21:44:17', '', 0, 'clan', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `recrutements`
--

CREATE TABLE IF NOT EXISTS `recrutements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_clan` int(11) NOT NULL,
  `id_jeu` int(11) NOT NULL,
  `annonce` text NOT NULL,
  `date_annonce` datetime NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `recrutements`
--

INSERT INTO `recrutements` (`id`, `id_clan`, `id_jeu`, `annonce`, `date_annonce`, `time`) VALUES
(1, 1, 2, 'vbinov,zj invze', '2013-10-29 05:12:23', 1383019943),
(2, 1, 2, 'jnij,ol', '2013-10-29 05:20:34', 1383020434),
(3, 1, 2, 'hbjin\r\n', '2013-10-29 05:20:38', 1383020438),
(4, 1, 2, 'rctfygu', '2013-10-29 05:20:41', 1383020441),
(5, 1, 2, 'vbhy', '2013-10-29 05:20:44', 1383020444),
(6, 1, 2, ' hj', '2013-10-29 05:20:47', 1383020447),
(7, 1, 2, ' hbkj', '2013-10-29 05:20:50', 1383020450),
(8, 1, 2, ' hbu', '2013-10-29 05:20:54', 1383020454),
(9, 1, 2, 'bhkjn ', '2013-10-29 05:20:58', 1383020458),
(10, 1, 2, ' vhjbn', '2013-10-29 05:21:01', 1383020461),
(11, 1, 2, ' hjbh', '2013-10-29 05:21:05', 1383020465),
(12, 1, 2, 'h jvbj', '2013-10-29 05:21:09', 1383020469),
(13, 1, 2, ' hvyb', '2013-10-29 05:21:12', 1383020472),
(14, 1, 2, 'gvhbjk', '2013-10-29 05:21:15', 1383020475),
(15, 1, 2, 'gvhbk', '2013-10-29 05:21:18', 1383020478),
(16, 1, 2, ' gjvh', '2013-10-29 05:21:21', 1383020481),
(17, 1, 2, ' gyvihb', '2013-10-29 05:21:24', 1383020484),
(18, 1, 2, 'vbjk', '2013-10-29 05:21:28', 1383020488),
(19, 1, 2, 'vhbb', '2013-10-29 05:21:31', 1383020491),
(20, 1, 2, 'vhjb', '2013-10-29 05:21:34', 1383020494),
(21, 1, 2, 'vhjbb', '2013-10-29 05:21:38', 1383020498),
(22, 1, 2, ' gjvhbk', '2013-10-29 05:21:41', 1383020501),
(23, 1, 2, 'hjbbjk', '2013-10-29 05:21:44', 1383020504),
(24, 1, 2, 'gj hbbkj', '2013-10-29 05:21:47', 1383020507),
(25, 1, 2, 'bbhkjjb', '2013-10-29 05:21:51', 1383020511),
(26, 1, 2, ' hbkjbkj', '2013-10-29 05:21:54', 1383020514),
(27, 1, 2, 'hbbjbk', '2013-10-29 05:21:58', 1383020518);

-- --------------------------------------------------------

--
-- Structure de la table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jeu` int(11) NOT NULL,
  `adresse_stockage` varchar(255) NOT NULL,
  `date_enregistrement` datetime NOT NULL,
  `id_clan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `videos`
--

INSERT INTO `videos` (`id`, `id_jeu`, `adresse_stockage`, `date_enregistrement`, `id_clan`) VALUES
(1, 1, 'https://www.youtube.com/watch?v=h1l4pDXbkic', '2013-09-18 03:47:33', 0),
(2, 2, 'http://www.youtube.com/watch?v=Ys_XKiykA44', '2013-10-29 16:23:29', 0),
(3, 2, 'http://www.youtube.com/watch?v=OUkkaqSNduU', '2013-10-29 16:23:44', 0),
(4, 2, 'http://www.youtube.com/watch?v=OUkkaqSNduU', '2013-10-29 16:23:50', 0),
(5, 2, 'http://www.youtube.com/watch?v=hqpGw-NXBG4', '2013-10-29 16:24:03', 0),
(6, 2, 'https://www.youtube.com/watch?v=SgxQJRh2-fs', '2013-10-29 16:24:19', 0),
(7, 2, 'http://www.youtube.com/watch?v=mfFQuhWaA_k', '2013-10-29 21:18:24', 0),
(8, 2, 'http://www.youtube.com/watch?v=mfFQuhWaA_k', '2013-10-29 21:18:29', 0),
(9, 2, 'http://www.youtube.com/watch?v=mfFQuhWaA_k', '2013-10-29 21:18:32', 0),
(10, 2, 'http://www.youtube.com/watch?v=mfFQuhWaA_k', '2013-10-29 21:19:09', 0),
(11, 0, 'http://www.youtube.com/watch?v=mfFQuhWaA_k', '2013-10-29 21:41:23', 1),
(12, 0, 'http://www.youtube.com/watch?v=mfFQuhWaA_k', '2013-10-29 21:41:28', 1),
(13, 0, 'http://www.youtube.com/watch?v=mfFQuhWaA_k', '2013-10-29 21:42:23', 1),
(14, 0, 'http://www.youtube.com/watch?v=bRbL9-FqBFw', '2013-10-29 21:44:01', 1),
(15, 0, 'http://www.youtube.com/watch?v=bRbL9-FqBFw', '2013-10-29 21:44:06', 1),
(16, 6, 'http://www.youtube.com/watch?v=ujFR8IlaBWc', '2013-12-18 21:27:09', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
