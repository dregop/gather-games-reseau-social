<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}

// ON ENREGISTRE LE COMMENTAIRE DANS LA BDD + REDIRECTION
if (isset($_POST['commentaire']) AND isset($_POST['id_publication_communaute']) 
AND $_POST['commentaire'] != '')
{
	// on insert le message ds la bdd
	$req = $bdd->prepare('INSERT INTO commentaire(id_publication_communaute,
						id_jeu,commentaire,date_commentaire) 
						VALUES(:id_publication_communaute,:id_jeu,:commentaire,
						NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_publication_communaute' => $_POST['id_publication_communaute'],
						'id_jeu' => $_SESSION['id_jeu'],
						'commentaire' => $_POST['commentaire'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	if(isset($_POST['commentaires_communaute']))
	{
		if(isset($_POST['page']))
			header('Location: communaute-com-p'.$_POST['page'].'.html#'.$_POST['id_publication_communaute'].'');
		else
			header('Location: communaute-com.html#'.$_POST['id_publication_communaute'].'');
	}
	elseif(!isset($_POST['commentaires_communaute']))
	{
		if(isset($_POST['page']))
			header('Location: communaute-p'.$_POST['page'].'.html#'.$_POST['id_publication_communaute'].'');
		else
			header('Location: communaute.html#'.$_POST['id_publication_communaute'].'');
	}

}
elseif(isset($_POST['commentaire'],$_POST['id_publication_communaute']) 
AND $_POST['commentaire'] == '' AND !isset($_POST['commentaires_communaute']))
{
	header('Location: communaute.html#'.$_POST['id_publication_communaute'].'');
}
elseif(isset($_POST['commentaire'],$_POST['id_publication_communaute'],
$_POST['commentaires_communaute']) AND $_POST['commentaire'] == '')
{
	header('Location: communaute-com.html#'.$_POST['id_publication_communaute'].'');
}

// ON SUPPRIME LE COMMENTAIRE DANS COMMUNAUTE ET COMMENTAIRES_COMMUNAUTE
if(isset($_GET['id_publication_communaute'],$_GET['supprimer_commentaire'],
$_GET['id_commentaire']))
{
	$reqs_c = $bdd->prepare('SELECT id_jeu, date_commentaire 
							FROM commentaire WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_c->execute(array('id' => $_GET['id_commentaire']))
							or die(print_r($bdd->errorInfo()));
	$donnees_comm = $reqs_c->fetch();
	$reqs_communaute = $bdd->prepare('SELECT id_jeu
							FROM communaute WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_communaute->execute(array('id' => $_GET['id_publication_communaute']))
							or die(print_r($bdd->errorInfo()));
	$donnees_communaute = $reqs_communaute->fetch();

	if(isset($donnees_comm['id_jeu']) 
	AND $donnees_comm['id_jeu'] == $_SESSION['id_jeu'] 
	OR $donnees_communaute['id_jeu'] == $_SESSION['id_jeu'])
	{
		$req1 = $bdd->prepare('DELETE FROM commentaire WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id' => $_GET['id_commentaire'])) 
							or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		$req3 = $bdd->prepare('DELETE FROM notifications 
							WHERE date_notification=:date_notification 
							AND id_publication=:id_publication')
							or die(print_r($bdd->errorInfo()));
		$req3->execute(array('date_notification' => $donnees_comm['date_commentaire'],
							'id_publication' => $_GET['id_publication'])) 
							or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requ�te
	}
	if(isset($_GET['commentaires_communaute']))
	{
		if(isset($_GET['page']))
			header('Location: communaute-com-p'.$_GET['page'].'.html#'.$_GET['id_publication_communaute'].'');
		else
			header('Location: communaute-com.html#'.$_GET['id_publication_communaute'].'');
	}
	elseif(!isset($_GET['commentaires_communaute']))
	{
		if(isset($_GET['page']))
			header('Location: communaute-p'.$_GET['page'].'.html#'.$_GET['id_publication_communaute'].'');
		else
			header('Location: communaute.html#'.$_GET['id_publication_communaute'].'');
	}	
}
?>