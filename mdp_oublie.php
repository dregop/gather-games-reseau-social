<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
function genererCode($n=20)
{
    $caracteres = array_merge(range(0,9),range('a','z'),range('A','Z'));
    shuffle($caracteres);
    return substr(implode('',$caracteres),0,$n);
}
	
if(isset($_POST['nom_de_compte']))
{ 
	$repoo = $bdd->prepare('SELECT nom_de_compte FROM membres 
							WHERE nom_de_compte =:nom_de_compte')
							or die(print_r($bdd->errorInfo()));
	$repoo->execute(array('nom_de_compte' => $_POST['nom_de_compte']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $repoo->fetch();
	if($_POST['nom_de_compte'] == $donnees['nom_de_compte'] 
	AND $_POST['nom_de_compte'] != '')
	{
		$_SESSION['nom_de_compte'] = stripslashes(htmlspecialchars($_POST['nom_de_compte']));
		header('Location: reponse-secrete-'.stripslashes(htmlspecialchars($donnees['nom_de_compte'])).'.html');
	}
	else
	{
		$erreur_ndc = 'e';
		header('Location: mot-de-passe-'.$erreur_ndc.'.html');
	}
}

include('menu_index.php');
 ?>   
	<div class="corps">
		<form action="mdp_oublie.php" method="post">
			<p id="mdp_oublie2"> Mot de passe oubli� ? 
				<span id="mdp_oublie3"> 
					pas de panique, suivez les instructions 
				</span>
			</p>
			<div id="renseignement">
			<p class="texte_mdp_oublie"> Nom de compte <br /> 
				<input class="input2" type="text" name="nom_de_compte" style="width:340px; height:25px;"/>
			</p>
			<input type="submit" value="Validez" class="validez"/>
		</form>
<?php

		if(isset($_GET['erreur_ndc']) AND $_GET['erreur_ndc'] == 'e')
		{
			echo'
			<p class="erreur_mdp">
				<img src="images/attention.png" alt="erreur"> 
				Ce nom de compte n\'existe pas. Veuillez r�essayer
			</p>';
		}	
	
	echo'</div>
	</div>';
							
include('pied_page_index.php'); ?>	