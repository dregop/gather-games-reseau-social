<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
	$re = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$donnees = $re->fetch();
	$re2 = $bdd->prepare('SELECT * FROM clan WHERE id=:id_clan 
						AND id_jeu=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$re2->execute(array('id_clan' => $donnees['id_clan'], 
						'id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$donnees2 = $re2->fetch();
	
/////////////////////////// POUR POSTER UN NOUVEAU SUJET ///////////////////////

if (isset($_POST['titre_topic'],$_POST['message_topic'], 
$_SESSION['id_categorie']) AND $_POST['message_topic'] != ''
AND $_POST['titre_topic'] != ''  AND strlen($_POST['titre_topic']) < 41 )
{
	$req = $bdd->prepare('INSERT INTO forum_topic(id_jeu, id_categorie, 
						titre_topic, message_topic, date_message) 
						VALUES(:id_jeu, :id_categorie, :titre_topic,
						:message_topic, NOW())')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'id_categorie' => $_SESSION['id_categorie'],
						'titre_topic' => $_POST['titre_topic'],
						'message_topic' => $_POST['message_topic']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te
	
	header('Location: forum-ic'.$_SESSION['id_categorie'].'.html');	
}
elseif(isset($_POST['titre_topic'],$_POST['message_topic'], 
$_SESSION['id_categorie']) AND strlen($_POST['titre_topic']) > 41 )
{
	header('Location: forum-ic'.$_SESSION['id_categorie'].'-sujet.html');
}

// POUR AJOUTER UNE CATEGORIE //////////////////////////////////////////////////

if (isset($_POST['ajout_categorie'], $_POST['information_categorie']) 
AND $_POST['ajout_categorie'] != '' AND  $_POST['information_categorie'] != ''
AND strlen($_POST['information_categorie'])<101 
AND strlen($_POST['ajout_categorie'])<41)
{
	if(isset($_POST['bloc_categorie1']))
	{
		$req = $bdd->prepare('INSERT INTO forum_categorie(id_jeu, nom_categorie, 
							information_categorie, bloc_categorie) 
							VALUES(:id_jeu, :nom_categorie, 
							:information_categorie, :bloc_categorie)')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'nom_categorie' => $_POST['ajout_categorie'],
							'information_categorie' => $_POST['information_categorie'],
							'bloc_categorie' => $_POST['bloc_categorie1']))
							or die(print_r($bdd->errorInfo()));	
		$req->closeCursor(); // Termine le traitement de la requ�te
		header('Location: forum.html');
	}
	elseif(isset($_POST['bloc_categorie2']))
	{
		$req = $bdd->prepare('INSERT INTO forum_categorie(id_jeu, nom_categorie, 
							information_categorie, bloc_categorie) 
							VALUES(:id_jeu, :nom_categorie, 
							:information_categorie, :bloc_categorie)')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'nom_categorie' => $_POST['ajout_categorie'],
							'information_categorie' => $_POST['information_categorie'],
							'bloc_categorie' => $_POST['bloc_categorie2']))
							or die(print_r($bdd->errorInfo()));	
		$req->closeCursor(); // Termine le traitement de la requ�te
		header('Location: forum.html');
	}
	elseif(isset($_POST['bloc_categorie3']))
	{
		$req = $bdd->prepare('INSERT INTO forum_categorie(id_jeu, nom_categorie, 
							information_categorie, bloc_categorie) 
							VALUES(:id_jeu, :nom_categorie, 
							:information_categorie, :bloc_categorie)')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'nom_categorie' => $_POST['ajout_categorie'],
							'information_categorie' => $_POST['information_categorie'],
							'bloc_categorie' => $_POST['bloc_categorie3']))
							or die(print_r($bdd->errorInfo()));	
		$req->closeCursor(); // Termine le traitement de la requ�te
		header('Location: forum.html');
	}
	elseif(isset($_POST['bloc_categorie4']))
	{
		$req = $bdd->prepare('INSERT INTO forum_categorie(id_jeu, nom_categorie, 
							information_categorie, bloc_categorie) 
							VALUES(:id_jeu, :nom_categorie, 
							:information_categorie, :bloc_categorie)')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'nom_categorie' => $_POST['ajout_categorie'],
							'information_categorie' => $_POST['information_categorie'],
							'bloc_categorie' => $_POST['bloc_categorie4']))
							or die(print_r($bdd->errorInfo()));	
		$req->closeCursor(); // Termine le traitement de la requ�te
		header('Location: forum.html');
	}
}
elseif (isset($_POST['ajout_categorie'], $_POST['information_categorie'],
$_SESSION['id_categorie']) AND strlen($_POST['ajout_categorie']) > 40 
OR strlen($_POST['information_categorie']) > 100 OR $_POST['message_topic'] == '' 
OR $_POST['titre_topic'] == '')
{
	header('Location: forum-fail.html');
}

if (!isset($_SESSION['id_categorie']))
{
	header('Location: forum.html');
}
if (!isset($_SESSION['statut']))
{
	header('Location: team.html'); 
}
?>