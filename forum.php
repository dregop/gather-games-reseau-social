<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
    die('Erreur : '.$e->getMessage());	
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
	header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
////////////////////// POUR AJOUTER UNE NOUVELLE CATEGORIE /////////////////////


if (isset($_GET['supprimer_sujet'],$_GET['id_sujet']))
{
	$req = $bdd->prepare('DELETE FROM forum_topic WHERE id=:id_sujet')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_sujet' => $_GET['id_sujet'])) 
						or die(print_r($bdd->errorInfo()));
	header('Location: forum-ic'.$_SESSION['id_categorie'].'.html');
}

if (isset($_GET['bloquer_sujet'],$_GET['id_sujet']))
{
	$req = $bdd->prepare('UPDATE forum_topic SET statut=:statut WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id' => $_GET['id_sujet'],'statut' => 'bloquer'))
						or die(print_r($bdd->errorInfo()));
	header('Location: forum-ic'.$_SESSION['id_categorie'].'.html');
}

if (isset($_GET['debloquer_sujet'],$_GET['id_sujet']))
{
	$req = $bdd->prepare('UPDATE forum_topic SET statut=:statut WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id' => $_GET['id_sujet'],'statut' => '')) 
						or die(print_r($bdd->errorInfo()));
	header('Location: forum-ic'.$_SESSION['id_categorie'].'.html');
}
if (isset($_GET['supprimer_categorie'],$_GET['id_categorie']))
{
		$req = $bdd->prepare('DELETE FROM forum_topic 
							WHERE id_categorie=:id_categorie')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_categorie' => $_GET['id_categorie'])) 
							or die(print_r($bdd->errorInfo()));
		$req = $bdd->prepare('DELETE FROM forum_categorie WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id' => $_GET['id_categorie'])) 
							or die(print_r($bdd->errorInfo()));
		header('Location: forum.html');
}

if (!isset($_SESSION['statut']))
{
	header('Location: team.html'); 
}

?>
<!DOCTYPE html> 
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body> 
<div id="overlay"></div>
<div id="contient_image_profil"></div>
<?php
// OVERLAY MEMBRES
if(isset($_GET['membres']))
{
?>
<div id="grand_overlay4"></div>
<div id="overlay4_clan">
	<a href="forum.html">
		<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
	</a>
	<p id="titre_overlay_membre">Voici les membres de la team</p>
	<div id="sous_overlay4">
<?php
	
	if (isset($_GET['p']) AND $_GET['p'] > 0)
	{
		$numero_page = $_GET['p'];
		$numero_page--;
		$numero_page = 30*$numero_page;
	}
	else
		$numero_page = 0;
	$requete = $bdd->prepare('SELECT * FROM jeu WHERE id_clan=:id_clan 
							ORDER by pseudo ASC 
							LIMIT '.$numero_page.',30')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_clan' => $_SESSION['id_clan']))
							or die(print_r($bdd->errorInfo()));
	while($donnees_membre = $requete->fetch())
	{
		echo'
		<div class="membre_forum">';
		
		if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'<a  href="profil-i'.$donnees_membre['id'].'.html" title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
		elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
			echo'<span  title="Moi">';
		else
			echo'<span  title="Mon sous compte">';
		//////////////////////////////////////////////////////////////////////// ON L'ENTORUE AVEC LES BONNES CONDITIONS POUR LES LIENS
		echo'
		<span class="pseudo_overlay4">
			'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'
		</span>';
		////////////////////////////////////////////////////////////////////////
		if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'</a>';
		else
			echo'</span>';
			
		echo'
		<div class="centreimg_overlay">';
		// POUR LES LIENS SI SOUS COMPTE OU PAS
		if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'<a  href="profil-i'.$donnees_membre['id'].'.html" title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
		elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
			echo'<span  title="Moi">';
		else
			echo'<span  title="Mon sous compte">';
			
		if(isset($donnees_membre['photo_profil']) 
		AND $donnees_membre['photo_profil'] != '' 
		AND $donnees_membre['photo_profil'] != 0)
		{  
		  $source = getimagesize('images_utilisateurs/'.$donnees_membre['photo_profil']); // La photo est la source
			if ($source[0] <= 60 AND $source[1] <= 60)
				echo '<img src="images_utilisateurs/'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
			else
				echo '<img src="images_utilisateurs/mini_2_'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
		}
		else
			echo '<img src="images/tete3.png" alt="Photo de profil" />';
			
		//////POUR LES LIENS SI SOUS COMPTE OU PAS
		if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'</a>';
		else
			echo'</span>';
			
		echo'</div>';
		
		if(strlen($donnees_membre['nom_jeu']) > 15)
		{
			echo'
			<p class="information_membre" style="font-size:x-small;">'; 
			echo 
				substr(stripslashes(htmlspecialchars($donnees_membre['nom_jeu'])), 0, 20).'';
				if(strlen(stripslashes(htmlspecialchars($donnees_membre['nom_jeu']))) > 20)
				{ echo'...';} 
			echo'
			</p>';
		}
		else
		{
			echo'
			<p class="information_membre">
				'.stripslashes(htmlspecialchars($donnees_membre['nom_jeu'])).'
			</p>';
		}
		echo'
		<p class="information_membre">
			'.$donnees_membre['plateforme'].'
		</p>
		</div>';	
	}
	
	echo'
	</div>';
	
	$nbre_page = 1;
	
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM jeu 
						WHERE id_clan=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_clan' => $_SESSION['id_clan']))
						or die(print_r($bdd->errorInfo()));
	
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_publi'];
	$p->closeCursor();
	
	if (isset ($_GET['p']))
	$current_page = $_GET['p'];
	else
		$current_page = 1;
		
	$nom_page = 'forum';
	$page_suite = 'membres';
	$nbr_affichage = 30;

	include('pagination_p.php');
?>			
		
</div>
<?php
}
?>

<!----------------------- OVERLAY NOUVELLE CATEGORIE !------------------------->
</div>
<div id="grand_overlay1"></div>

<div id="bloc_categorie1">
	<a id="fermer1" href="#">
		<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
	</a>
	<p id="titre_overlay_message">Nouvelle cat�gorie </p>
	<form action="forum_post.php" method="post">
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:20px;">
			Nom de la cat�gorie 
			<span style="font-size:small;">
				(40 caract�res max)
			</span>
			<input style="width:340px; height:25px;color:#102c3c;font-weight:bolder;" type="text" name="ajout_categorie" />
			<br /> <br />
			Information de la cat�gorie  
			<span style="font-size:small;">
				(100 caract�res max)
			</span> 
			<textarea id="input_information_categorie" rows="" cols="" name="information_categorie" placeholder="Indiquez � quoi sert cette cat�gorie"></textarea>
			<br />
		</p>	
			<input type="submit" id="envoyer_profil" name="Valider" value="Ajouter"/>
			<input type="hidden" name="bloc_categorie1" value="general" />
	</form>
</div>	

<div id="bloc_categorie2">
	<a id="fermer2" href="#">
		<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
	</a>
	<p id="titre_overlay_message">
		Nouvelle cat�gorie 
	</p>
	<form action="forum_post.php" method="post">
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;">
			Nom de la cat�gorie 
			<span style="font-size:small;">
				(40 caract�res max)
			</span>
			<input style="width:340px; height:25px;color:#102c3c;font-weight:bolder;" type="text" name="ajout_categorie" />
			<br /> <br />
			Information de la cat�gorie 
			<span style="font-size:small;">
				(100 caract�res max)</span> 
			<textarea id="input_information_categorie" rows="" cols="" name="information_categorie" placeholder="Indiquez � quoi sert cette cat�gorie"></textarea><br />
		</p>	
			<input type="submit" id="envoyer_profil" name="Valider" value="Ajouter"/>
			<input type="hidden" name="bloc_categorie2" value="recrutements" />
	</form>
</div>

<div id="bloc_categorie3">
	<a id="fermer3" href="#"><img id="fermer_message" src="images/fermer.png" alt=" Fermer " /></a>
	<p id="titre_overlay_message">Nouvelle cat�gorie </p>
	<form action="forum_post.php" method="post">
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;">
			Nom de la cat�gorie 
			<span style="font-size:small;">
				(40 caract�res max)
			</span>
			<input style="width:340px; height:25px;color:#102c3c;font-weight:bolder;" type="text" name="ajout_categorie" /><br /> <br />
			Information de la cat�gorie
			<span style="font-size:small;">
				(100 caract�res max)
			</span>
			<textarea id="input_information_categorie" rows="" cols="" name="information_categorie" placeholder="Indiquez � quoi sert cette cat�gorie"></textarea><br />
		</p>	
			<input type="submit" id="envoyer_profil" name="Valider" value="Ajouter"/>
			<input type="hidden" name="bloc_categorie3" value="jeu" />
	</form>
</div>

<div id="bloc_categorie4">
	<a id="fermer4" href="#"><img id="fermer_message" src="images/fermer.png" alt=" Fermer " /></a>
	<p id="titre_overlay_message">Nouvelle cat�gorie </p>
	<form action="forum_post.php" method="post">
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;">
			Nom de la cat�gorie 
			<span style="font-size:small;">
				(40 caract�res max)
			</span>
			<input style="width:340px; height:25px;color:#102c3c;font-weight:bolder;" type="text" name="ajout_categorie" /><br /> <br />
			Information de la cat�gorie 
			<span style="font-size:small;">
				(100 caract�res max)
			</span>
			<textarea id="input_information_categorie" rows="" cols="" name="information_categorie" placeholder="Indiquez � quoi sert cette cat�gorie"></textarea><br />
		</p>	
			<input type="submit" id="envoyer_profil" name="Valider" value="Ajouter"/>
			<input type="hidden" name="bloc_categorie4" value="autres" />
	</form>
</div>

<!--------------------------- OVERLAY NOUVEAU SUJET !--------------------------->
<div id="overlay_sujet">
	<a id="fermer" href="#">
		<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
	</a>
	<div id="debut_overlay_sujet">
<?php  
			echo '<div class="image_sujet" style="margin-top:20px;">';
			if ($_SESSION['photo_profil'] != 0 AND $_SESSION['photo_profil'] != '')
			{
				$source = getimagesize('images_utilisateurs/'.$_SESSION['photo_profil']); // La photo est la source
				if ($source[0] <= 60 AND $source[1] <= 60)
					echo'<img src="images_utilisateurs/'.$_SESSION['photo_profil'].'" alt="Photo de profil"/>';
				else
					echo'<img src="images_utilisateurs/mini_2_'.$_SESSION['photo_profil'].'" alt="Photo de profil"/>';
			       
			}
			else 
			  echo '<img src="images/tete3.png" alt="Photo de profil"/>';
			echo'</div>';
			echo'
			<span id="pseudo_overlay_sujet">
				'.stripslashes(htmlspecialchars($_SESSION['pseudo'])).'
			</span>';
?>
		<p id="nouveau_overlay_sujet">
			Nouveau sujet
		</p> 
	</div>
	<form method="post" action="forum_post.php">
		<div id="contient_message_topic">
			Nom du sujet 
			<span style="font-size:small;">
				(40 caract�res max)
			</span>
			<input style="width:500px; height:25px;color:#102c3c;font-weight:bolder;" type="text" name="titre_topic" />
			<br /> <br />
			Votre message 
			<span style="font-size:small;">
				(n'oubliez pas les formules de politesse)
			</span>
			<br />
			<br />
			<script type="text/javascript" src="bbcode.js"></script>
			<a href="#">
				<img src="images/bbcode/gras.gif" alt="gif" title="Gras"
				onclick="javascript:bbcode('[g]', '[/g]'); return(false)"/>
			</a>
			<a href="#">
				<img src="images/bbcode/italique.gif" alt="gif" title="Italique" 
				onclick="javascript:bbcode('[i]', '[/i]'); return(false)"/>
			</a>
			<a href="#">
				<img src="images/bbcode/souligner.gif" alt="gif" title="Souligner"
				onclick="javascript:bbcode('[s]', '[/s]'); return(false)"/>
			</a>
			<a href="#">
				<img src="images/bbcode/image.gif" alt="gif" title="Image" 
				onclick="javascript:bbcode('[img]', '[/img]'); return(false)"/>
			</a>
			<a href="#">
				<img src="images/bbcode/lien.gif" alt="gif" title="Lien" 
				onclick="javascript:bbcode('[a]', '[/a]'); return(false)"/>
			</a>
			<textarea id="input_nouveau_sujet" rows="" cols="" name="message_topic" placeholder="�crivez votre message"></textarea><br />	
		</div>
		<input type="submit" id="envoyer_profil" name="Valider" value="Ajouter"/>	
	</form>
</div>
<?php 
include('menu_sans_doctype.php');

	$re2 = $bdd->prepare('SELECT * FROM clan WHERE id=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$re2->execute(array( 'id_clan' => $_SESSION['id_clan']))
						or die(print_r($bdd->errorInfo()));
	$donnees2 = $re2->fetch();
	
echo '
<div id="corps_forum">';
////////////////////////////////// 1 ER BLOCK DU FORUM (NOM,IMAGE,MEMBRE etc...)
include('menu_forum.php');
?>
<!-------------------------- ON PASSE AU BLOC FORUM !! ------------------------>

<!----------------------- DEBUT PAGE AVEC LES TOPICS -------------------------->
<?php
if (isset($_GET['id_categorie']) AND !isset($_GET['supprimer_categorie']))
{
	$_SESSION['id_categorie'] = $_GET['id_categorie'];
	
	$rezs = $bdd->prepare('SELECT nom_categorie FROM forum_categorie 
							WHERE id=:id_categorie')
							or die(print_r($bdd->errorInfo()));
	$rezs->execute(array('id_categorie' => $_SESSION['id_categorie']))
							or die(print_r($bdd->errorInfo()));
	$donneeszs = $rezs->fetch();
	
	if (isset($_GET['sujet'])) // ERREUR SUJET
	{	
		echo'
		<div id="bloc_erreur_topic">
			<p class="erreur_modifs">
				<img src="images/attention.png" alt=" "/> 
				Attention, le titre du sujet doit contenir 40 carat�res maximum.
			</p>
		</div>';
	}

	if($_SESSION['statut'] == 'meneur' OR $_SESSION['statut'] == 'membre')
	{
		echo'
		<a id="nouveau_sujet_overlay" href="#">
			<div id="nouveau_sujet"></div>
		</a>';
		$pas_repeter=1;
	}
			
	$r_recrutements = $bdd->prepare('SELECT id FROM forum_categorie 
									WHERE bloc_categorie=:bloc_categorie')
									or die(print_r($bdd->errorInfo()));
	$r_recrutements->execute(array('bloc_categorie' => 'recrutements'))
									or die(print_r($bdd->errorInfo()));
									
	while ($donnees_recrutement = $r_recrutements->fetch()) 					// on affiche message par message
	{
		if($_SESSION['statut'] == 'visiteur' 
		AND $donnees_recrutement['id'] == $_GET['id_categorie'] 
		AND !isset($pas_repeter))
		{
			echo'
			<a id="nouveau_sujet_overlay" href="#">
				<div id="nouveau_sujet"></div>
			</a>';
		}
	}
 echo '
	<div id="bloc_sujets">
		<p id="nom_categorie">
			Cat�gorie : 
			'.stripslashes(htmlspecialchars($donneeszs['nom_categorie'])).'
		</p>
		<p id="reponse">R�ponse(s)</p>
		<p class="dernier_message">Dernier message</p>';
		
	if (isset($_GET['p']) AND $_GET['p'] > 0)
	{
		$numero_page = $_GET['p'];
		$numero_page--;
		$numero_page = 20*$numero_page;
	}
	else
		$numero_page = 0;
		
$re4 = $bdd->prepare('SELECT f.* FROM forum_topic f
					WHERE id_categorie=:id_categorie 
					ORDER BY date_message DESC')
					or die(print_r($bdd->errorInfo()));
$re4->execute(array('id_categorie' => $_SESSION['id_categorie']))
					or die(print_r($bdd->errorInfo()));
$i = 0;
while ($donnees4 = $re4->fetch()) // on affiche message par message
{
	echo'
	<div class="contient_bloquer_supprimer">';
	
	$re5 = $bdd->prepare('SELECT pseudo, photo_profil, id FROM jeu 
						WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$re5->execute(array('id_jeu' => $donnees4['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$donnees5 = $re5->fetch();
	
	if ($_SESSION['statut'] == 'meneur')
	{
		echo '<a href="forum.php?supprimer_sujet='.$_GET['id_categorie'].'&id_sujet='.$donnees4['id'].'" title="Supprimer" 
				 onclick ="var sup=confirm(\'?tes vous sur de vouloir supprimer ce sujet ?\');if (sup == 0)return false;">
					<div class="supprimer_sujet"></div>
			  </a>';
	}
	elseif($donnees4['id_jeu'] == $_SESSION['id_jeu'] AND $donnees4['statut'] != 'bloquer' )
	{
		echo'<a href="forum.php?supprimer_sujet='.$_GET['id_categorie'].'&id_sujet='.$donnees4['id'].'" title="Supprimer" 
			onclick ="var sup=confirm(\'?tes vous sur de vouloir supprimer ce sujet ?\');if (sup == 0)return false;">
					<div class="supprimer_sujet" style="margin-top:45px;"></div>
			</a>';
	}
	
	if ($_SESSION['statut'] == 'membre' OR $_SESSION['statut'] == 'visiteur' )
	{
		if($donnees4['id_jeu'] == $_SESSION['id_jeu'] AND $donnees4['statut'] == 'bloquer')
		{
			echo'<a href="forum.php?supprimer_sujet='.$_GET['id_categorie'].'&id_sujet='.$donnees4['id'].'" title="Supprimer" 
				onclick ="var sup=confirm(\'?tes vous sur de vouloir supprimer ce sujet ?\');if (sup == 0)return false;">
						<div class="supprimer_sujet"></div>
				</a>';
			echo '<div class="sujet_bloquer" style="margin-top:10px;"></div>';	  
		}
	}
	
	if ($_SESSION['statut'] == 'meneur')
	{
		if ($donnees4['statut'] == 'bloquer')
		{
			echo'<a href="forum.php?debloquer_sujet='.$_GET['id_categorie'].'&id_sujet='.$donnees4['id'].'" title="D?bloquer" 
				onclick ="var sup=confirm(\'?tes vous sur de vouloir d?bloquer ce sujet ?\');if (sup == 0)return false;">
					<div class="bloquer_sujet"></div>
				</a>';
		}	
	}
	
	if ($_SESSION['statut'] == 'membre' OR $_SESSION['statut'] == 'visiteur' )
	{
		if ($donnees4['statut'] == 'bloquer' AND $donnees4['id_jeu'] != $_SESSION['id_jeu'])
			echo '<div class="sujet_bloquer" style="margin-top:45px;"></div>';
	}
	if ($_SESSION['statut'] == 'meneur')
	{
		if ($donnees4['statut'] != 'bloquer')
		{
			echo'
				<a href="forum.php?bloquer_sujet='.$_GET['id_categorie'].'&id_sujet='.$donnees4['id'].'" title="Bloquer" 
				onclick ="var sup=confirm(\'?tes vous sur de vouloir bloquer ce sujet ?\');if (sup == 0)return false;">
					<div class="avant_bloquer_sujet"></div>
				</a>';
		}
	}

	
	echo'
	</div>
	<a href="topic-i'.$donnees4['id'].'-p1.html">
		<div class="contient_renseignement_sujet">
	
			<p class="pseudo_sujet">
				Pseudo : '.stripslashes(htmlspecialchars($donnees5['pseudo'])).'
			</p>
			<div class="contient_titre_sujet">
				'.$donnees4['titre_topic'].'
			</div>';
				if ($donnees5['photo_profil'] != 0)
				{
					$source = getimagesize('images_utilisateurs/'.$donnees5['photo_profil']); // La photo est la source
					echo'<div class="image_sujet">';
					if ($source[0] <= 60 AND $source[1] <= 60)						
						echo'<img src="images_utilisateurs/'.$donnees5['photo_profil'].'" alt="Photo de profil"/>';
					else
						echo'<img src="images_utilisateurs/mini_2_'.$donnees5['photo_profil'].'" alt="Photo de profil"/>';
					echo'</div>';
				}
				else
					echo'<img  style="float:left;margin-top:-5px;margin-left:25px;" src="images/tete3.png" alt="Photo de profil"/>';
		echo'
		</div>
	</a>';
	
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_commentaire 
						FROM commentaire_topic WHERE id_topic=:id')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id' =>$donnees4['id']))
						or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();	
	
	$p2 = $bdd->prepare('SELECT *, 	
						DATE_FORMAT(date_commentaire, \'%d/%m/%Y ? %H:%i \') 
						AS date_commentaire FROM commentaire_topic 
						WHERE id_topic=:id ORDER BY date_commentaire DESC')
						or die(print_r($bdd->errorInfo()));
	$p2->execute(array('id' =>$donnees4['id']))
						or die(print_r($bdd->errorInfo()));
	$do2 = $p2->fetch();
	
	$p3 = $bdd->prepare('SELECT * FROM jeu WHERE id=:id ')
						or die(print_r($bdd->errorInfo()));
	$p3->execute(array('id' =>$do2['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$do3 = $p3->fetch();
	
	if ($do2['commentaire_topic'])
	{
		echo '
		<p class="nbr_reponse">
			'.$do['nbre_commentaire'].'
		</p>
		<p class="dernier_message_sujet">
			<span class="dernier_message2" style="font-size:small;">
				Par : '.stripslashes(htmlspecialchars($do3['pseudo'])).' 
				le '.$do2['date_commentaire'].'
			</span>
		</p>';
	}
	else
	{
		echo '
		<p class="nbr_reponse">0</p>
		<p class="dernier_message_sujet">Aucun</p>';
	}
	
	$i++;
}
if($i==0)
{
	$r_recrutements2 = $bdd->prepare('SELECT id FROM forum_categorie 
									WHERE bloc_categorie=:bloc_categorie')
									or die(print_r($bdd->errorInfo()));
	$r_recrutements2->execute(array('bloc_categorie' => 'recrutements'))
									or die(print_r($bdd->errorInfo()));
	$donnees_recrutement2 = $r_recrutements2->fetch(); 							// on affiche message par message
	
		if($_SESSION['statut'] == 'meneur' OR $_SESSION['statut'] == 'membre')
			echo'<img id="pas_sujet" src="images/pas_sujet.png" alt=" Il n\'y a aucune sujet de publi�. Vous pouvez en ajouter un ci-dessus. " />';
		elseif($_SESSION['statut'] == 'visiteur' AND $_GET['id_categorie'] == $donnees_recrutement2['id'])
			echo'<img id="pas_sujet" src="images/pas_sujet.png" alt=" Il n\'y a aucune sujet de publi�. Vous pouvez en ajouter un ci-dessus. " />';
		elseif ($_GET['id_categorie'] != $donnees_recrutement2['id'] AND $_SESSION['statut'] == 'visiteur' )
			echo'<img id="pas_sujet" src="images/pas_sujet_visiteur.png" alt=" En tant que visiteur, vous pouvez ajouter un sujet seulement dans la cat�gorie Recrutements" />';
}

	$nbre_page = 1;
	
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM forum_topic 
						WHERE id_categorie=:id_categorie')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_categorie' => $_GET['id_categorie']))
						or die(print_r($bdd->errorInfo()));
	
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_publi'];
	$p->closeCursor();
	
	if (isset ($_GET['page']))
		$current_page = $_GET['page'];
	else
		$current_page = 1;
		
	$nom_page = 'forum';
	$nbr_affichage = 20;

echo'<p class="nombre_page">';

if (isset($nbr_entrees) AND $nbr_entrees != 0)
{
	$nbr_page = ceil($nbr_entrees / $nbr_affichage); 							//diviser puis arrondir au sup�rieur
	
	if ($current_page >= 1 AND $current_page <= $nbr_page)
	{
		if ($nbr_page > 1)
		{
			echo'
				<span style="color:#102c3c;">';

				echo'
				<a href="'.$nom_page.'-ic'.$_SESSION['id_categorie'].'-p1.html">';
		
			if ($current_page - 3 > 1)
			{
				echo'
						<span class="numero_page1">1</span>
					</a>';
				echo '...';
			}
			else
			{
				if((isset($_GET['page']) AND $_GET['page'] ==1) 
				OR !isset($_GET['page']))
				{
					echo'
						<span class="numero_page2">1</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">1</span>
					</a>';
				}
			}
			
			echo '
				</span>';
			
			$count = -2;
			
			while ($count <= 2)
			{
				$affich_page = $current_page + $count ;
				
				if ($affich_page > 1
				AND $affich_page < $nbr_page)
				{
					echo'
					<a href="'.$nom_page.'-ic'.$_SESSION['id_categorie'].'-p'.$affich_page.'.html">';
			
				
					if(isset($_GET['page']) AND $_GET['page'] == $affich_page)
					{
						echo'
							<span class="numero_page2">'.$affich_page.'</span>
						</a>';
					}
					else
					{
						echo'
							<span class="numero_page">'.$affich_page.'</span>
						</a>';
					}
				}
				$count ++;
			}
			
			echo'
				<span style="color:#102c3c;">';
			if ($current_page + 3 < $nbr_page)
				echo '...';
			echo'
			<a href="'.$nom_page.'-ic'.$_SESSION['id_categorie'].'p'.$nbr_page.'.html">';
			
			if ($current_page + 3 < $nbr_page)
			{
				echo'
						<span class="numero_page1">'.$nbr_page.'</span>
					</a>';
			}
			else
			{
				if(isset($_GET['page']) AND $_GET['page'] == $nbr_page)
				{
					echo'
						<span class="numero_page2">'.$nbr_page.'</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">'.$nbr_page.'</span>
					</a>';
				}
			}
				
				echo'
				</span>';
		}
	}
	else
	{
		echo 'Erreur de num�ro de page.'; // ERREUR
	}
}
echo'</p>';
// FIN GESTION AFFICHAGE PAGES
		echo'
		<div id="corps_invisible2"></div>
	</div>';
?>
<script>
var overlay_sujet = document.getElementById('overlay_sujet');
var nouveau_sujet_overlay = document.getElementById('nouveau_sujet_overlay');
//NOUVEAU SUJET
	nouveau_sujet_overlay.onclick =  function()
	{
		grand_overlay1.style.display = 'block';
		overlay_sujet.style.display = 'block';
        return false; // on bloque la redirection
	};
	document.getElementById('fermer').onclick = function() {
		grand_overlay1.style.display = 'none';
		overlay_sujet.style.display = 'none';
		return false; // on bloque la redirection
	};

	var links = document.getElementsByTagName('a'),
		linksLen = links.length;
	for (var i = 0 ; i < linksLen ; i++) {
		if (links[i].title == 'Afficher l\'image originale')
		{
			links[i].onclick = function() { 
				displayImg(this); 
				return false; // on bloque la redirection
			};
		}
	}
function displayImg(link) {

    var img = new Image(),
        overlay = document.getElementById('overlay');
        profil = document.getElementById('contient_image_profil');
    img.onload = function() {
        profil.innerHTML = '';
        profil.appendChild(img);
    };
    img.src = link.href;
    overlay.style.display = 'block';
    profil.style.display = 'block';
    profil.innerHTML = '<span>Chargement en cours...</span>';
}

document.getElementById('overlay').onclick = function() {
    this.style.display = 'none';
    profil.style.display = 'none';
	
};
</script>
<?php
}// FIN PAGE AVEC LES TOPICS ///////////////////////////////////////////////////
else
{
	if (isset($_GET['fail'])) 
	{
		echo'
		<div id="bloc_erreur_forum">
			<p class="erreur_modifs">
				<img src="images/attention.png" alt=" "/> 
				Attention, le titre de la categorie doit contenir 40 carat�res 
				maximum quant � l\'information, elle doit contenir 100 
				caract�res maximum.
			</p>
		</div>';
	}
	
	if (isset($_GET['fail']))
	{
		echo'
	<div id="general2_forum">';
	}
	else
		echo'
	<div id="general_forum">';
?>
		<p class="titre_categorie_forum">G�n�ral</p>
		<p class="sujets_forum">Sujets</p>
		<p class="messages_forum">Messages</p>
		<p class="dernier_commentaire">Dernier commentaire</p>
<?php
		
		$requete1_categorie = $bdd->prepare('SELECT * FROM forum_categorie 
											WHERE id_jeu=:id_jeu')
											or die(print_r($bdd->errorInfo()));
		$requete1_categorie->execute(array('id_jeu' => $donnees2['id_jeu']))
											or die(print_r($bdd->errorInfo()));
		$i = 0;
		while($donnees1_categorie = $requete1_categorie->fetch())
		{
			if(isset($donnees1_categorie['bloc_categorie']) 
			AND $donnees1_categorie['bloc_categorie'] == 'general')
			{
				if($_SESSION['statut'] == 'meneur')
				{
					echo'<a href="forum.php?supprimer_categorie&id_categorie='.$donnees1_categorie['id'].'" title="Supprimer" 
						 onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette cat�gorie ?\');if (sup == 0)return false;">
							<div class="contient_supprimer_categorie">
								<div class="supprimer_categorie"></div>
							</div>
						  </a>';
				}
				echo'
				<p></p>
				<a style="color:white;" href="forum-ic'.$donnees1_categorie['id'].'.html">';
				if($_SESSION['statut'] == 'meneur')
				{
					echo'
					<p class="sous_titre_categorie">';
				}
				else
				{
					echo'
					<p class="sous_titre2_categorie">';
				}
				
				echo'
						<span style="border-bottom:1px dashed #4e5f69; color:#4e5f69;">
							'.stripslashes(htmlspecialchars($donnees1_categorie['nom_categorie'])).'
						</span> 
						<span class="information_categorie">
							'.stripslashes(htmlspecialchars($donnees1_categorie['information_categorie'])).'
						</span>
					</p>
				</a>';
				
				$nbre_total_com = 0;
				$p = $bdd->prepare('SELECT COUNT(*) AS nbre_sujet 
									FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p->execute(array('id_categorie' =>$donnees1_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do = $p->fetch();	
			
				$p4 = $bdd->prepare('SELECT c.date_commentaire date_commentaire 
									FROM commentaire_topic c
									INNER JOIN forum_topic ft
									ON ft.id = c.id_topic
									WHERE ft.id_categorie = :id
									ORDER BY date_commentaire DESC')
									or die(print_r($bdd->errorInfo()));
				$p4->execute(array('id' =>$donnees1_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do4 = $p4->fetch();
				
				$p5 = $bdd->prepare('SELECT id_jeu, 
									DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
									AS date_commentaire FROM commentaire_topic 
									WHERE date_commentaire=:d')
									or die(print_r($bdd->errorInfo()));
				$p5->execute(array('d' => $do4['date_commentaire']))
									or die(print_r($bdd->errorInfo()));
				$do5 = $p5->fetch();
				
				$p55 = $bdd->prepare('SELECT * FROM jeu WHERE id=:d')
									or die(print_r($bdd->errorInfo()));
				$p55->execute(array('d' => $do5['id_jeu']))
									or die(print_r($bdd->errorInfo()));
				$do55 = $p55->fetch();
					
					
				$p2 = $bdd->prepare('SELECT id FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p2->execute(array('id_categorie' =>$donnees1_categorie['id']))
									or die(print_r($bdd->errorInfo()));
									
				while($do2 = $p2->fetch())
				{
					$p3 = $bdd->prepare('SELECT COUNT(*) AS nbre_com 
										FROM commentaire_topic 
										WHERE id_topic=:id_topic')
										or die(print_r($bdd->errorInfo()));
					$p3->execute(array('id_topic' =>$do2['id']))
										or die(print_r($bdd->errorInfo()));
					$do3 = $p3->fetch();	
					$nbre_total_com = $nbre_total_com + $do3['nbre_com'];
				}
				$nbre_total_com = $nbre_total_com + $do['nbre_sujet'];
				echo'
					<p class="nbr_sujets">'.$do['nbre_sujet'].'</p>';
				if (($nbre_total_com - 1) > 0)
				{
					echo'
					<p class="nbr_messages">
						'.$nbre_total_com.'
					</p>';
				}
				else
				{
					echo'
					<p class="nbr_messages">
						0
					</p>';
				}
					

				if(isset($do55['pseudo'], $do5['date_commentaire']))
				{
					echo'
					<div class="contient_dernier_message2">
						<p class="dernier_message2" style="font-size:small;">
							Par : 
							'.stripslashes(htmlspecialchars($do55['pseudo'])).' 
							le '.$do5['date_commentaire'].'
						</p>
					</div>';
				}
				else
				{
					echo'
					<p class="dernier_message3" style="font-size:small;float:left">
						Aucun
					</p>';
				}
				
			$i++;
			}
		}
		if ($_SESSION['statut'] == 'meneur' AND $i>0)
		{
			echo'
			<a id="nouvelle_categorie1" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';
		}
		elseif ($_SESSION['statut'] == 'meneur' AND $i==0)
		{
			echo'
			<p>
				<img class="pas_categorie_meneur" src="images/pas_categorie_meneur.png" alt="Il n\'y a aucune cat�gorie. Vous pouvez en ajouter une." />
			</p>';
			echo'
			<a id="nouvelle_categorie1" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';	
		}
		elseif ($_SESSION['statut'] != 'meneur' AND $i==0)
		{
			echo'
			<img class="pas_categorie" src="images/pas_categorie.png" alt="Il n\'y a aucune cat�gorie, seul le meneur peut en ajouter. Contactez le." />
			<div class="corps_invisible_forum"></div>';
		}
		else
		{
			echo'
			<div class="corps_invisible_forum"></div>';
		}
?>
	</div>
	
	<div id="recrutements_forum">
		<p class="titre_categorie_forum">Recrutements</p>
		<p class="sujets_forum">Sujets</p>
		<p class="messages_forum">Messages</p>
		<p class="dernier_commentaire"> Dernier commentaire </p>
<?php	
		$requete2_categorie = $bdd->prepare('SELECT * FROM forum_categorie 
											WHERE id_jeu=:id_jeu')
											or die(print_r($bdd->errorInfo()));
		$requete2_categorie->execute(array('id_jeu' => $donnees2['id_jeu']))
											or die(print_r($bdd->errorInfo()));
		$i = 0;
		while($donnees2_categorie = $requete2_categorie->fetch())
		{
			if(isset($donnees2_categorie['bloc_categorie']) 
			AND $donnees2_categorie['bloc_categorie'] == 'recrutements')
			{
				if($_SESSION['statut'] == 'meneur')
				{
					echo'<a href="forum.php?supprimer_categorie&id_categorie='.$donnees2_categorie['id'].'" title="Supprimer" 
						 onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette cat�gorie ?\');if (sup == 0)return false;">
							<div class="contient_supprimer_categorie">
								<div class="supprimer_categorie"></div>
							</div>
						 </a>';
				}
				echo'
				
				<a style="color:white;" href="forum-ic'.$donnees2_categorie['id'].'.html">';
				if($_SESSION['statut'] == 'meneur')
				{
					echo'
					<p class="sous_titre_categorie">';
				}
				else
				{
					echo'
					<p class="sous_titre2_categorie">';
				}
				
				echo'
						<span style="border-bottom:1px dashed #4e5f69; color:#4e5f69;">
							'.stripslashes(htmlspecialchars($donnees2_categorie['nom_categorie'])).'
						</span> 
						<span class="information_categorie">
							'.stripslashes(htmlspecialchars($donnees2_categorie['information_categorie'])).'
						</span>
					</p>
				</a>';
				
				$nbre_total_com = 0;
				$p = $bdd->prepare('SELECT COUNT(*) AS nbre_sujet 
									FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p->execute(array('id_categorie' =>$donnees2_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do = $p->fetch();	
				
				$p4 = $bdd->prepare('SELECT c.date_commentaire date_commentaire 
									FROM commentaire_topic c
									INNER JOIN forum_topic ft
									ON ft.id = c.id_topic
									WHERE ft.id_categorie = :id
									ORDER BY date_commentaire DESC')
									or die(print_r($bdd->errorInfo()));
				$p4->execute(array('id' =>$donnees2_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do4 = $p4->fetch();
				
				$p5 = $bdd->prepare('SELECT id_jeu, 	
									DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
									AS date_commentaire 
									FROM commentaire_topic 
									WHERE date_commentaire=:d')
									or die(print_r($bdd->errorInfo()));
				$p5->execute(array('d' => $do4['date_commentaire']))
									or die(print_r($bdd->errorInfo()));
				$do5 = $p5->fetch();
				
				$p55 = $bdd->prepare('SELECT * FROM jeu WHERE id=:d')
									or die(print_r($bdd->errorInfo()));
				$p55->execute(array('d' => $do5['id_jeu']))
									or die(print_r($bdd->errorInfo()));
				$do55 = $p55->fetch();
					
				$p2 = $bdd->prepare('SELECT id FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p2->execute(array('id_categorie' =>$donnees2_categorie['id']))
									or die(print_r($bdd->errorInfo()));
									
				while($do2 = $p2->fetch())
				{
					$p3 = $bdd->prepare('SELECT COUNT(*) AS nbre_com 
										FROM commentaire_topic 
										WHERE id_topic=:id_topic')
										or die(print_r($bdd->errorInfo()));
					$p3->execute(array('id_topic' =>$do2['id']))
										or die(print_r($bdd->errorInfo()));
					$do3 = $p3->fetch();	
					$nbre_total_com = $nbre_total_com + $do3['nbre_com'];
				}
				$nbre_total_com = $nbre_total_com + $do['nbre_sujet'];
				echo'
						<p class="nbr_sujets">'.$do['nbre_sujet'].'</p>';
						
				if (($nbre_total_com - 1) > 0)
					echo'<p class="nbr_messages">'.$nbre_total_com.'</p>';
				else
					echo '<p class="nbr_messages">0</p>';
				
				if(isset($do55['pseudo'], $do5['date_commentaire']))
				{
					echo'
					<div class="contient_dernier_message2">
						<p class="dernier_message2" style="font-size:small;">
							Par : 
							'.stripslashes(htmlspecialchars($do55['pseudo'])).' 
							le '.$do5['date_commentaire'].'
						</p>
					</div>';
				}
				else
				{
					echo'
					<p class="dernier_message3" style="font-size:small;float:left">
						Aucun
					</p>';
				}
				
			$i++;
			}
		}
		if ($_SESSION['statut'] == 'meneur' AND $i>0)
		{
			echo'
			<a id="nouvelle_categorie2" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';
		}
		elseif ($_SESSION['statut'] == 'meneur' AND $i==0)
		{
			echo'
			<p>
				<img class="pas_categorie_meneur" src="images/pas_categorie_meneur.png" alt="Il n\'y a aucune cat�gorie. Vous pouvez en ajouter une." />
			</p>';
			echo'
			<a id="nouvelle_categorie2" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';	
		}
		elseif ($_SESSION['statut'] != 'meneur' AND $i==0)
		{
			echo'
			<img class="pas_categorie" src="images/pas_categorie.png" alt="Il n\'y a aucune cat�gorie, seul le meneur peut en ajouter. Contactez le." />
			<div class="corps_invisible_forum"></div>';
		}
		else
		{
			echo'
			<div class="corps_invisible_forum"></div>';
		}		
?>
	</div>
	
	<div id="jeu_forum">
		<p class="titre_categorie_forum">
<?php
		$re2 = $bdd->prepare('SELECT * FROM clan WHERE id=:id_clan')
							or die(print_r($bdd->errorInfo()));
		$re2->execute(array('id_clan' => $_SESSION['id_clan']))
							or die(print_r($bdd->errorInfo()));
		$donnees2 = $re2->fetch();
		echo''.$donnees2['jeu'].''
?>
		</p>
		<p class="sujets_forum">Sujets</p>
		<p class="messages_forum">Messages</p>
		<p class="dernier_commentaire"> Dernier commentaire </p>
<?php
		
		$requete3_categorie = $bdd->prepare('SELECT * FROM forum_categorie 
											WHERE id_jeu=:id_jeu')
											or die(print_r($bdd->errorInfo()));
		$requete3_categorie->execute(array('id_jeu' => $donnees2['id_jeu']))
											or die(print_r($bdd->errorInfo()));
		$i = 0;
		while($donnees3_categorie = $requete3_categorie->fetch())
		{
			if(isset($donnees3_categorie['bloc_categorie']) 
			AND $donnees3_categorie['bloc_categorie'] == 'jeu')
			{
				if($_SESSION['statut'] == 'meneur')
				{
					echo'<a href="forum.php?supprimer_categorie&id_categorie='.$donnees3_categorie['id'].'" title="Supprimer" 
						 onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette cat�gorie ?\');if (sup == 0)return false;">
							<div class="contient_supprimer_categorie">
								<div class="supprimer_categorie"></div>
							</div>
						  </a>';
				}
				echo'
				
				<a style="color:white;" href="forum-ic'.$donnees3_categorie['id'].'.html">';
				if($_SESSION['statut'] == 'meneur')
				{
					echo'
					<p class="sous_titre_categorie">';
				}
				else
				{
					echo'
					<p class="sous_titre2_categorie">';
				}
				
				echo'
						<span style="border-bottom:1px dashed #4e5f69; color:#4e5f69;">
							'.stripslashes(htmlspecialchars($donnees3_categorie['nom_categorie'])).'
						</span> 
						<span class="information_categorie">
							'.stripslashes(htmlspecialchars($donnees3_categorie['information_categorie'])).'
						</span>
					</p>
				</a>';
				$nbre_total_com = 0;
				$p = $bdd->prepare('SELECT COUNT(*) AS nbre_sujet 
									FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p->execute(array('id_categorie' =>$donnees3_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do = $p->fetch();	
				
				$p4 = $bdd->prepare('SELECT c.date_commentaire date_commentaire 
									FROM commentaire_topic c
									INNER JOIN forum_topic ft
									ON ft.id = c.id_topic
									WHERE ft.id_categorie = :id
									ORDER BY date_commentaire DESC')
									or die(print_r($bdd->errorInfo()));
				$p4->execute(array('id' =>$donnees3_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do4 = $p4->fetch();
				
				$p5 = $bdd->prepare('SELECT id_jeu, 
									DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
									AS date_commentaire FROM commentaire_topic
									WHERE date_commentaire=:d')
									or die(print_r($bdd->errorInfo()));
				$p5->execute(array('d' => $do4['date_commentaire']))
									or die(print_r($bdd->errorInfo()));
				$do5 = $p5->fetch();
				
				$p55 = $bdd->prepare('SELECT * FROM jeu WHERE id=:d')
									or die(print_r($bdd->errorInfo()));
				$p55->execute(array('d' => $do5['id_jeu']))
									or die(print_r($bdd->errorInfo()));
				$do55 = $p55->fetch();
					
				$p2 = $bdd->prepare('SELECT id FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p2->execute(array('id_categorie' =>$donnees3_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				while($do2 = $p2->fetch())
				{
					$p3 = $bdd->prepare('SELECT COUNT(*) AS nbre_com 
										FROM commentaire_topic 
										WHERE id_topic=:id_topic')
										or die(print_r($bdd->errorInfo()));
					$p3->execute(array('id_topic' =>$do2['id']))
										or die(print_r($bdd->errorInfo()));
					$do3 = $p3->fetch();	
					$nbre_total_com = $nbre_total_com + $do3['nbre_com'];
				}
				
				$nbre_total_com = $nbre_total_com + $do['nbre_sujet'];
				echo'
						<p class="nbr_sujets">'.$do['nbre_sujet'].'</p>';
						
				if (($nbre_total_com - 1) > 0)
					echo'<p class="nbr_messages">'.$nbre_total_com.'</p>';
				else
					echo '<p class="nbr_messages">0</p>';
					
				if(isset($do55['pseudo'], $do5['date_commentaire']))
				{
					echo'
					<div class="contient_dernier_message2">
						<p class="dernier_message2" style="font-size:small;">
							Par : 
							'.stripslashes(htmlspecialchars($do55['pseudo'])).' 
							le '.$do5['date_commentaire'].'
						</p>
					</div>';
				}
				else
				{
					echo'
					<p class="dernier_message3" style="font-size:small;float:left">
						Aucun
					</p>';
				}
				
			$i++;
			}
		}
		if ($_SESSION['statut'] == 'meneur' AND $i>0)
		{
			echo'
			<a id="nouvelle_categorie3" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';
		}
		elseif ($_SESSION['statut'] == 'meneur' AND $i==0)
		{
			echo'
			<p>
				<img class="pas_categorie_meneur" src="images/pas_categorie_meneur.png" alt="Il n\'y a aucune cat�gorie. Vous pouvez en ajouter une." />
			</p>';
			echo'
			<a id="nouvelle_categorie3" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';	
		}
		elseif ($_SESSION['statut'] != 'meneur' AND $i==0)
		{
			echo'
			<img class="pas_categorie" src="images/pas_categorie.png" alt="Il n\'y a aucune cat�gorie, seul le meneur peut en ajouter. Contactez le." />
			<div class="corps_invisible_forum"></div>';
		}
		else
		{
			echo'
			<div class="corps_invisible_forum"></div>';
		}		
?>
	</div>
	
	<div id="autres_forum">
		<p class="titre_categorie_forum">Autres</p>
		<p class="sujets_forum">Sujets</p>
		<p class="messages_forum">Messages</p>
		<p class="dernier_commentaire"> Dernier commentaire </p>
<?php
		
		$requete4_categorie = $bdd->prepare('SELECT * FROM forum_categorie 
											WHERE id_jeu=:id_jeu')
											or die(print_r($bdd->errorInfo()));
		$requete4_categorie->execute(array('id_jeu' => $donnees2['id_jeu']))
											or die(print_r($bdd->errorInfo()));
		$i = 0;
		while($donnees4_categorie = $requete4_categorie->fetch())
		{
			if(isset($donnees4_categorie['bloc_categorie']) 
			AND $donnees4_categorie['bloc_categorie'] == 'autres')
			{
				if($_SESSION['statut'] == 'meneur')
				{
					echo'<a href="forum.php?supprimer_categorie&id_categorie='.$donnees4_categorie['id'].'" title="Supprimer" 
						 onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette cat�gorie ?\');if (sup == 0)return false;">
							<div class="contient_supprimer_categorie">
								<div class="supprimer_categorie"></div>
							</div>
						  </a>';
				}
				echo'
				
				<a style="color:white;" href="forum-ic'.$donnees4_categorie['id'].'.html">';
				if($_SESSION['statut'] == 'meneur')
				{
					echo'
					<p class="sous_titre_categorie">';
				}
				else
				{
					echo'
					<p class="sous_titre2_categorie">';
				}
				
				echo'
						<span style="border-bottom:1px dashed #4e5f69; color:#4e5f69;">
							'.stripslashes(htmlspecialchars($donnees4_categorie['nom_categorie'])).'
						</span> 
						<span class="information_categorie">
							'.stripslashes(htmlspecialchars($donnees4_categorie['information_categorie'])).'
						</span>
					</p>
				</a>';
				
				$nbre_total_com = 0;
				$p = $bdd->prepare('SELECT COUNT(*) AS nbre_sujet 
									FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p->execute(array('id_categorie' =>$donnees4_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do = $p->fetch();	
				
				$p4 = $bdd->prepare('SELECT c.date_commentaire date_commentaire 
									FROM commentaire_topic c
									INNER JOIN forum_topic ft
									ON ft.id = c.id_topic
									WHERE ft.id_categorie = :id
									ORDER BY date_commentaire DESC')
									or die(print_r($bdd->errorInfo()));
				$p4->execute(array('id' =>$donnees4_categorie['id']))
									or die(print_r($bdd->errorInfo()));
				$do4 = $p4->fetch();
				
				$p5 = $bdd->prepare('SELECT id_jeu, 
									DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
									AS date_commentaire FROM commentaire_topic 
									WHERE date_commentaire=:d')
									or die(print_r($bdd->errorInfo()));
				$p5->execute(array('d' => $do4['date_commentaire']))
									or die(print_r($bdd->errorInfo()));
				$do5 = $p5->fetch();
				
				$p55 = $bdd->prepare('SELECT * FROM jeu WHERE id=:d')
									or die(print_r($bdd->errorInfo()));
				$p55->execute(array('d' => $do5['id_jeu']))
									or die(print_r($bdd->errorInfo()));
				$do55 = $p55->fetch();
					
				$p2 = $bdd->prepare('SELECT id FROM forum_topic 
									WHERE id_categorie=:id_categorie')
									or die(print_r($bdd->errorInfo()));
				$p2->execute(array('id_categorie' =>$donnees4_categorie['id']))
									or die(print_r($bdd->errorInfo()));
									
				while($do2 = $p2->fetch())
				{
					$p3 = $bdd->prepare('SELECT COUNT(*) AS nbre_com 
										FROM commentaire_topic 
										WHERE id_topic=:id_topic')
										or die(print_r($bdd->errorInfo()));
					$p3->execute(array('id_topic' =>$do2['id']))
										or die(print_r($bdd->errorInfo()));
					$do3 = $p3->fetch();	
					$nbre_total_com = $nbre_total_com + $do3['nbre_com'];
				}
				$nbre_total_com = $nbre_total_com + $do['nbre_sujet'];
				echo'
						<p class="nbr_sujets">'.$do['nbre_sujet'].'</p>';
				if (($nbre_total_com - 1) > 0)
					echo'<p class="nbr_messages">'.$nbre_total_com.'</p>';
				else
					echo '<p class="nbr_messages">0</p>';

					
				if(isset($do55['pseudo'], $do5['date_commentaire']))
				{
					echo'
					<div class="contient_dernier_message2">
						<p class="dernier_message2" style="font-size:small;">
							Par : 
							'.stripslashes(htmlspecialchars($do55['pseudo'])).' 
							le '.$do5['date_commentaire'].'
						</p>
					</div>';
				}
				else
				{
					echo'
					<p class="dernier_message3" style="font-size:small;float:left">
						Aucun
					</p>';
				}
				
			$i++;
			}
		}
		if ($_SESSION['statut'] == 'meneur' AND $i>0)
		{
			echo'
			<a id="nouvelle_categorie4" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';
		}
		elseif ($_SESSION['statut'] == 'meneur' AND $i==0)
		{
			echo'
			<p>
				<img class="pas_categorie_meneur" src="images/pas_categorie_meneur.png" alt="Il n\'y a aucune cat�gorie. Vous pouvez en ajouter une." />
			</p>';
			echo'
			<a id="nouvelle_categorie4" href="#">
				<div class="nouvelle_categorie" ></div>
			</a>
			<div class="corps_invisible_forum"></div>';	
		}
		elseif ($_SESSION['statut'] != 'meneur' AND $i==0)
		{
			echo'
			<img class="pas_categorie" src="images/pas_categorie.png" alt="Il n\'y a aucune cat�gorie, seul le meneur peut en ajouter. Contactez le." />
			<div class="corps_invisible_forum"></div>';
		}	
		else
		{
			echo'
			<div class="corps_invisible_forum"></div>';
		}
?>
	</div>
<?php
}
?>	
</div>
 
<script>
var membre = document.getElementById('membre');
var nouvelle_categorie1 = document.getElementById('nouvelle_categorie1');
var nouvelle_categorie2 = document.getElementById('nouvelle_categorie2');
var nouvelle_categorie3 = document.getElementById('nouvelle_categorie3');
var nouvelle_categorie4 = document.getElementById('nouvelle_categorie4');
var bloc_categorie1 = document.getElementById('bloc_categorie1');
var bloc_categorie2 = document.getElementById('bloc_categorie2');
var bloc_categorie3 = document.getElementById('bloc_categorie3');
var bloc_categorie4 = document.getElementById('bloc_categorie4');
var grand_overlay1 = document.getElementById('grand_overlay1');
var overlay_clan = document.getElementById('overlay_clan');

//CATEGORIE 1
	nouvelle_categorie1.onclick =  function()
	{
		grand_overlay1.style.display = 'block';
		bloc_categorie1.style.display = 'block';
        return false; // on bloque la redirection
	};
	document.getElementById('fermer1').onclick = function() {
		grand_overlay1.style.display = 'none';
		bloc_categorie1.style.display = 'none';
		return false; // on bloque la redirection
	};
	
//CATEGORIE 2
	nouvelle_categorie2.onclick =  function()
	{
		grand_overlay1.style.display = 'block';
		bloc_categorie2.style.display = 'block';
        return false; // on bloque la redirection
	};
	document.getElementById('fermer2').onclick = function() {
		grand_overlay1.style.display = 'none';
		bloc_categorie2.style.display = 'none';
		return false; // on bloque la redirection
	};
	
//CATEGORIE 3
	nouvelle_categorie3.onclick =  function()
	{
		grand_overlay1.style.display = 'block';
		bloc_categorie3.style.display = 'block';
        return false; // on bloque la redirection
	};
	document.getElementById('fermer3').onclick = function() {
		grand_overlay1.style.display = 'none';
		bloc_categorie3.style.display = 'none';
		return false; // on bloque la redirection
	};	
	
//CATEGORIE 4	
	nouvelle_categorie4.onclick =  function()
	{
		grand_overlay1.style.display = 'block';
		bloc_categorie4.style.display = 'block';
        return false; // on bloque la redirection
	};
	document.getElementById('fermer4').onclick = function() {
		grand_overlay1.style.display = 'none';
		bloc_categorie4.style.display = 'none';
		return false; // on bloque la redirection
	};

	var links = document.getElementsByTagName('a'),
		linksLen = links.length;
	for (var i = 0 ; i < linksLen ; i++) {
		if (links[i].title == 'Afficher l\'image originale')
		{
			links[i].onclick = function() { 
				displayImg(this); 
				return false; // on bloque la redirection
			};
		}
	}
function displayImg(link) {

    var img = new Image(),
        overlay = document.getElementById('overlay');
        profil = document.getElementById('contient_image_profil');
    img.onload = function() {
        profil.innerHTML = '';
        profil.appendChild(img);
    };
    img.src = link.href;
    overlay.style.display = 'block';
    profil.style.display = 'block';
    profil.innerHTML = '<span>Chargement en cours...</span>';
}
document.getElementById('overlay').onclick = function() {
    this.style.display = 'none';
    profil.style.display = 'none';
	
};
</script>

<?php
include('pied_page.php');
?>