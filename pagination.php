<?php
////////////////////////////////////////////////////////////////////////////////
// PAGINATION MARCHE POUR TOUT /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

echo'<p class="nombre_page">';

if (isset($nbr_entrees) AND $nbr_entrees != 0)
{
	$nbr_page = ceil($nbr_entrees / $nbr_affichage); 							//diviser puis arrondir au supérieur
	
	if ($current_page >= 1 AND $current_page <= $nbr_page)
	{
		if ($nbr_page > 1)
		{
			echo'
				<span style="color:#102c3c;">';
				if(isset($_GET['id']))
				{
					echo'
					<a href="'.$nom_page.'-i'.$_GET['id'].'-p1.html">';
				}
				elseif(isset($page_suite))
				{
					echo'
					<a href="'.$nom_page.'-'.$page_suite.'-p1.html">';
				}
				else
				{
					echo'
					<a href="'.$nom_page.'-p1.html">';
				}
		
			if ($current_page - 3 > 1)
			{
				echo'
						<span class="numero_page1">1</span>
					</a>';
				echo '...';
			}
			else
			{
				if((isset($_GET['page']) AND $_GET['page'] ==1) 
				OR !isset($_GET['page']))
				{
					echo'
						<span class="numero_page2">1</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">1</span>
					</a>';
				}
			}
			
			echo '
				</span>';
			
			$count = -2;
			
			while ($count <= 2)
			{
				$affich_page = $current_page + $count ;
				
				if ($affich_page > 1
				AND $affich_page < $nbr_page)
				{
					if(isset($_GET['id']))
					{
						echo'
						<a href="'.$nom_page.'-i'.$_GET['id'].'-p'.$affich_page.'.html">';
					}
					elseif(isset($page_suite))
					{
						echo'
						<a href="'.$nom_page.'-'.$page_suite.'-p'.$affich_page.'.html">';
					}
					else
					{
						echo'
						<a href="'.$nom_page.'-p'.$affich_page.'.html">';
					}
					
					if(isset($_GET['page']) AND $_GET['page'] == $affich_page)
					{
						echo'
							<span class="numero_page2">'.$affich_page.'</span>
						</a>';
					}
					else
					{
						echo'
							<span class="numero_page">'.$affich_page.'</span>
						</a>';
					}
				}
				$count ++;
			}
			
			echo'
				<span style="color:#102c3c;">';
			if ($current_page + 3 < $nbr_page)
				echo '...';
			if(isset($_GET['id']))
			{
				echo'
				<a href="'.$nom_page.'-i'.$_GET['id'].'-p'.$nbr_page.'.html">';
			}
			elseif(isset($page_suite))
			{
				echo'	
				<a href="'.$nom_page.'-'.$page_suite.'-p'.$nbr_page.'.html">';
			}
			else
			{
				echo'	
				<a href="'.$nom_page.'-p'.$nbr_page.'.html">';
			}
			
			if ($current_page + 3 < $nbr_page)
			{
				echo'
						<span class="numero_page1">'.$nbr_page.'</span>
					</a>';
			}
			else
			{
				if(isset($_GET['page']) AND $_GET['page'] == $nbr_page)
				{
					echo'
						<span class="numero_page2">'.$nbr_page.'</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">'.$nbr_page.'</span>
					</a>';
				}
			}
				
				echo'
				</span>';
		}
	}
	else
	{
		echo 'Erreur de numéro de page.'; // ERREUR
	}
}
echo'</p>';
// FIN GESTION AFFICHAGE PAGES
?>