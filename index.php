<?php
session_start();
$menu = 0;
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
// ON SUPPRIME LES SESSIONS PROVENANT DE CHANGER MDP
unset($_SESSION['email']);
unset($_SESSION['code']);

if (isset($_COOKIE['nom_de_compte']) AND !empty($_COOKIE['nom_de_compte']))
{
	$_SESSION['nom_de_compte'] = $_COOKIE['nom_de_compte'];
	header('Location: choix.html');
}
if (isset($_SESSION['id_jeu']))
{
	header('Location: profil.html');
}
include('menu_index.php'); 
	
		// ouverture du fichier compteur pour compt� le nbre de visites
		$monfichier = fopen('compteur_visiteur.txt', 'r+');				 
		$compteur = fgets($monfichier); 
		$compteur++; 
		fseek($monfichier, 0); 
		fputs($monfichier, $compteur); 
		fclose($monfichier);
?>
    <div class="corps">
	
		<img class="intro" src="images/intro.png" alt="Partagez vos exp�riences, fondez votre clan, �largissez votre cercle d�amis et faites-vous connaitre !" />

        <div id="corps_droite">    
			<img class="anonyme" src="images/anonyme.png" alt=" "/>     
		</div>     

        <p id="inscription1">
			<span id="inscription1_2">
				Inscription 
			</span>
			<span id="inscription1_3">
				<img class="rejoignez" src="images/rejoignez.png" alt="Rejoignez la G-Team"/>
			</span>
		</p>
		
        <div id="corps_gauche">
        
<?php  
	echo'
	<form action="index_post.php" method="post">
		<p class="texte_inscription">
			Nom de compte <br />';
			
			if(isset($_GET['nom_de_compte']))
			{
			echo'
			<input class="input2" type="text" name="nom_de_compte"  value="'.$_GET['nom_de_compte'].'" style="width:340px; height:25px;"/>';
			}
			else
			{
			echo'
			<input class="input2" type="text" name="nom_de_compte"  value="" style="width:340px; height:25px;"/>';
			}
		echo'
		</p>
		<p class="texte_inscription">
			Mot de passe <br /> 
			<input class="input2" value="" type="password" name="mot_de_passe" style="width:340px; height:25px;"/>
		</p>
		<p class="texte_inscription">
			Retapez le mot de passe <br /> 
			<input class="input2" type="password" name="mot_de_passe2" style="width:340px; height:25px;"/>
		</p>
		<p class="texte_inscription">
			Adresse e-mail <br />';			
			
			if(isset($_GET['email']))
			{
			echo'
			<input class="input2" type="text" name="email" value="'.$_GET['email'].'" style="width:340px; height:25px;"/>';
			}
			else
			{
			echo'
			<input class="input2" type="text" name="email" value="" style="width:340px; height:25px;"/>';
			}
		echo'
		</p>
		<p id="accepter">
			<input type="checkbox" name="case" />
			<label for="case">
			J\'ai lu et j\'accepte les 
			<a href="charte.html" id="conditions" >
				conditions g�n�rales d\'utilisation
			</a>
			</label>
		</p>
		<input type="submit" id="publier" name="Continuez" value="Continuez" class="continuez"/>
	</form>';
	
	if (isset($_GET['erreur']) AND $_GET['erreur'] == 'incomplet')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" /> 
			Veuillez compl�ter enti�rement l\'inscription
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'e1')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" /> 
			Ce nom de compte existe d�j�
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'e2')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" /> 
			Le nom de compte n\'est pas conforme
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'e3')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" /> 
			Votre nom de compte doit �tre compris entre 4 et 12 caract�res
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'e4')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" />
			Votre nom de compte doit �tre compris entre 4 et 12 caract�res
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'email')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" />
			L\'adresse e-mail n\'est pas conforme
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'mdp2')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" /> 
			Les deux mots de passe ne sont pas identiques
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'mdp')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" />
			Le mot de passe n\'est pas conforme
		</p>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'case')
	{
		echo'
		<p class="erreur_inscription">
			<img src="images/attention.png" alt="erreur" /> 
			Veuillez accepter les conditions g�n�rales d\'utilisation
		</p>';
	}
	
echo'
        </div>';

	if( isset($_GET['erreur']))
	{
	echo'
	<div id="mise_jour_erreur">
		<p> 
			<span id="conseil">
				Une mise � jour de votre navigateur est conseill�e :
			</span>
			<span id="navigateur">
				<img id="jour" " src="images/jour.png" alt="logo" />
				<a href="http://www.mozilla.org/fr/firefox/fx/" class="navigateur2">Mozilla Firefox </a>
				- <a href="https://www.google.com/intl/fr/chrome/browser/?platform=win&hl=fr" class="navigateur2">Google Chrome </a> 
				- <a href="http://www.opera.com/" class="navigateur2">Opera </a> 
				- <a href="http://www.microsoft.com/france/windows/internet-explorer/telecharger-ie9.aspx" class="navigateur2">Internet Explorer </a>
			</span>
		</p>
	</div>';
	}
	else
	{
	 echo'
	 <div id="mise_jour">
		<p> 
			<span id="conseil">
				Une mise � jour de votre navigateur est conseill�e :
			</span>
			<span id="navigateur">
				<img id="jour" " src="images/jour.png" alt="logo"/><a href="http://www.mozilla.org/fr/firefox/fx/" class="navigateur2">Mozilla Firefox </a> 
				- <a href="https://www.google.com/intl/fr/chrome/browser/?platform=win&hl=fr" class="navigateur2">Google Chrome </a> 
				- <a href="http://www.opera.com/" class="navigateur2">Opera </a> 
				- <a href="http://www.microsoft.com/france/windows/internet-explorer/telecharger-ie9.aspx" class="navigateur2">Internet Explorer </a>
			</span>
		</p>
	</div>';
	}
?>		
		<div id="bas_informations_index">
			<div id="creez_clan_index"></div>
			<div id="communaute_index"></div>
			<div id="partagez_index"></div>
		</div>
		
		<img id="fin_index" src="images/fin_index.png" alt="Cr�ez aussi plusieurs sous comptes pour diff�rencier les jeux auxquels vous jouez !" />
		
	</div>
	
<script>	
//------------------------------------------------------------------------------
var publie = document.getElementById('publier');
var publie2 = document.getElementById('publier2');
if (publie)
{
	publie.onclick = function() {
	publie.style.display = 'none';
	};
}
if (publie2)
{
	publie2.onclick = function() {
	publie2.style.display = 'none';
	};
}
</script>
		
<?php
include('pied_page_index.php'); 
?>  
