<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (isset($_POST['message'],$_POST['Date'],$_POST['Month'],$_POST['Year'],$_POST['Century']) 
AND $_POST['message'] != '' AND $_POST['Date'] >= 15 AND $_POST['Year'] >= 0
AND $_POST['Month'] >= 0 AND $_POST['Year'] <= 99 AND $_POST['Month'] <= 31
AND $_POST['Century'] >= 15 AND $_POST['Century'] <= 2020)
{
header('location: chemin.php');
}
?>
<SCRIPT LANGUAGE="JavaScript">
/*
SCRIPT TROUVE SUR L'EDITEUR JAVASCRIPT
http://www.editeurjavascript.com
*/
/*******************************************************
* CALENDRIER GREGORIEN PERPETUEL v1.0 *
* par SKAMP (skamp@befrance.com) (09/09/2000) *
********************************************************
* Ce script permet de choisir un mois et une annee en *
* particulier, afin d'afficher dynamiquement le *
* calendrier correspondant. Par defaut c'est celui du *
* mois courant qui s'affiche. Note : la 1ere semaine *
* de l'annee commence le 1er lundi. *
* *
* Le code suivant s'inspire de celui de Jean-Michel *
* Berthier (berth@cybercable.fr, *
* perso.cybercable.fr/berth/jstips/calendrier.htm). *
* *
* MODIFICATIONS NECESSAIRES POUR PORTAGE VERS D'AUTRES *
* NAVIGATEURS : N'A ETE TESTE QUE SOUS MICROSOFT *
* INTERNET EXPLORER 5.00.2614.3500 *
*******************************************************/
var HTMLCode = "";
var DaysList = new Array("Jour_Vide", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim");
var MonthsList = new Array("Mois_Vide", "Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre");
var MonthLength = new Array("Mois_longueur_vide",31,29,31,30,31,30,31,31,30,31,30,31);

var QueryDate = 0; /* Jour demande (date)*/
var QueryMonth = 0; /* Mois demande*/
var QueryYear = 0; /* Annee demandee*/
var QueryDay = 0; /* Jour de la semaine du jour demande, inconnu*/
var FirstDay = 0; /* Jour de la semaine du 1er jour du mois*/
var WeekRef = 0; /* Numerotation des semaines*/
var WeekOne = 0; /* Numerotation des semaines*/

var Today = new Date();
var TodaysYear = Today.getYear();
var TodaysMonth = Today.getMonth() + 1;
var TodaysDate = Today.getDate();
var TodaysDay = Today.getDay() + 1;
if (TodaysYear < 2000) { TodaysYear += 1900; }

/* On commence par verifier les donnees fournies par l'utilisateur*/
function CheckData(i)
{
	
	QueryDate = Today.getDate();
	QueryMonth = Today.getMonth() + i;
	QueryYear = Today.getYear() + 1900;
	MonthLength[2] = CheckLeap(QueryYear);

	DisplaySchedule(); 
}
/* Teste une annee pour determiner si elle est bissextile ou pas*/
function CheckLeap(yy)
{
if ((yy % 100 != 0 && yy % 4 == 0) || (yy % 400 == 0)) { return 29; }
else { return 28; }
}

/* Renvoie le numero de la semaine correspondant a la date requise*/
function DefWeekNum(dd)
{
numd = 0;
numw = 0;
for (n=1; n<QueryMonth; n++)
{
numd += MonthLength[n];
}
numd = numd + dd - (9 - DefDateDay(QueryYear,1,1));
numw = Math.floor(numd / 7) + 1;

if (DefDateDay(QueryYear,1,1) == 1) { numw++; }
return numw;
}

/* Renvoie le numero du jour de la semaine correspondant a la date requise */
function DefDateDay(yy,mm,dd)
{
return Math.floor((Date2Days(yy,mm,dd)-2) % 7) + 1;
}

/* Transforme la date en nb de jours theoriques */
function Date2Days(yy,mm,dd)
{
if (mm > 2)
{
var bis = Math.floor(yy/4) - Math.floor(yy/100) + Math.floor(yy/400);
var zy = Math.floor(yy * 365 + bis);
var zm = (mm-1) * 31 - Math.floor(mm * 0.4 + 2.3);
return (zy + zm + dd);
}
else
{
var bis = Math.floor((yy-1)/4) - Math.floor((yy-1)/100) + Math.floor((yy-1)/400);
var zy = Math.floor(yy * 365 + bis);
return (zy + (mm-1) * 31 + dd);
}
}

/* Produit le code HTML qui formera le calendrier */
function DisplaySchedule()
{
HTMLCode = "<table cellspacing=0 cellpadding=3 border=3 bordercolor=#000033>";
QueryDay = DefDateDay(QueryYear,QueryMonth,QueryDate);
WeekRef = DefWeekNum(QueryDate);
WeekOne = DefWeekNum(1);
HTMLCode += "<tr align=center><td colspan=8 class=TITRE><b>" + MonthsList[QueryMonth] + " " + QueryYear + "</b></td></tr><tr align=center>";

for (s=1; s<8; s++)
{
if (QueryDay == s && QueryMonth == (Today.getMonth() + 1)) { HTMLCode += "<td><b><font color=#ff0000>" + DaysList[s] + "</font></b></td>"; }
else { HTMLCode += "<td><b>" + DaysList[s] + "</b></td>"; }
}

HTMLCode += "<td><b><font color=#888888>Sem</font></b></td></tr>";
a = 0;

for (i=(1-DefDateDay(QueryYear,QueryMonth,1)); i<MonthLength[QueryMonth]; i++)
{
HTMLCode += "<tr align=center>";
for (j=1; j<8; j++)
{
if ((i+j) <= 0) { HTMLCode += "<td>&nbsp;</td>"; }
else if ((i+j) == QueryDate && QueryMonth == (Today.getMonth() + 1)) 
{ HTMLCode += "<td><b><font color=#ff0000>" + (i+j) + "</font></b></td>"; }
else if ((i+j) > MonthLength[QueryMonth]) { HTMLCode += "<td>&nbsp;</td>"; }
else { HTMLCode += "<td>" + (i+j) + "</td>"; }
}

if ((WeekOne+a) == WeekRef) { HTMLCode += "<td><b><font color=#00aa00>" + WeekRef + "</font></b></td>"; }
else { HTMLCode += "<td><font color=#888888>" + (WeekOne+a) + "</font></td>"; }
HTMLCode += "</tr>";
a++;
i = i + 6;
}

Calendrier.innerHTML = HTMLCode + "</table>";
}
</SCRIPT>
<STYLE type="text/css">
<!--
SELECT, INPUT, TABLE { font-family : Verdana; font-size : 10px; color : #000033; }
.TITRE { font-family : Verdana; font-size : 12px; color : #000033; }
-->
</STYLE> 
Ajouter un �v�nement : <br />
<form name="Cal" action="chemin.php" method="post">
<textarea name="message" >
</textarea><br />
S�lectionner une date : <br />
<script language="JavaScript1.2" type="text/javascript">
/* AFFICHE LES 4 MENUS DEROULANTS PERMETTANT DE
SELECTIONNER LE JOUR, LE MOIS ET L'ANNEE
**************************************************/
DateText = "<select name=\"Date\">"
for (d=1; d<32; d++)
{
DateText += "<option";
if (d == TodaysDate) { DateText += " SELECTED"; }
DateText += ">";
if (d < 10) { DateText += "0"; }
DateText += d + "</option>";
}
DateText += "</select>";
/*************************************************/
MonthText = "<select name=\"Month\">"
for (m=1; m<13; m++)
{
MonthText += "<option";
if (m == TodaysMonth) { MonthText += " SELECTED"; }
MonthText += ">";
MonthText += MonthsList[m] + "</option>";
}
MonthText += "</select>";
/*************************************************/
CenturyText = "<select name=\"Century\">"
for (c=20; c<25; c++)
{
CenturyText += "<option";
if (c == Math.floor(TodaysYear / 100)) { CenturyText += " SELECTED"; }
CenturyText += ">" + c + "</option>";
}
CenturyText += "</select>";
/*************************************************/
YearText = "<select name=\"Year\">";
for (y=0; y<100; y++)
{
YearText += "<option";
if (y == (TodaysYear - Math.floor(TodaysYear / 100) * 100)) { YearText += " SELECTED"; }
YearText += ">";
if (y < 10) { YearText += "0"; }
YearText += y + "</option>";
}
YearText += "</select>";
/*************************************************/
document.write(DateText + MonthText + CenturyText + YearText);
</script><br />
<input type="submit" name="envoyer" value="Valider" />
</form>
<button onclick="CheckData(2)">Plus</button><br />
<button onclick="CheckData(0)">Moins</button>
<div id="Calendrier"></div> 
<script language="JavaScript1.2" type="text/javascript">CheckData(1)</script>