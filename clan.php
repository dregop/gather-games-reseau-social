<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
// ON DETRUIT LES SESSIONS PROVENANT DE COMMENTAIRES
unset($_SESSION['id_clan']);
unset($_SESSION['id_publi']);
unset($_SESSION['id_clan_get']);
unset($_SESSION['lieu_page']);
unset($_SESSION['page']);

// ON DETRUIT LES SESSIONS VENANT DE VIDEOS ET IMAGES
unset($_SESSION['id_jeu_images']);
unset($_SESSION['id_jeu_videos']);

unset($_SESSION['id_clan_images']);
unset($_SESSION['id_clan_videos']);

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
	header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
if (empty($_SESSION['pseudo']))
{
	header('Location: informations-e1.html');
}
if (isset($_GET['id']) AND $_GET['id'] > 0)
{
	$reqs = $bdd->prepare('SELECT * FROM clan WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id' => $_GET['id']))
						or die(print_r($bdd->errorInfo()));
	$donnees = $reqs->fetch();
	if ($donnees['id_jeu'] == $_SESSION['id_jeu'])
	{
		header('Location: team.html');
	}
	$res = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$res->execute(array('id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$do = $res->fetch();
	$res2 = $bdd->prepare('SELECT nom_clan FROM clan WHERE id=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$res2->execute(array('id_clan' => $do['id_clan']))
						or die(print_r($bdd->errorInfo()));
	$do2 = $res2->fetch();		
	if ($do2['nom_clan'] != '' AND $do2['nom_clan'] != 0)
	{
		$_SESSION['statut'] = 'visiteur2';
	}
	else
	{
		$_SESSION['statut'] = 'visiteur';
	}
}
elseif(isset($_POST['id']))
{
	$reqs = $bdd->prepare('SELECT * FROM clan WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id' => $_POST['id']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $reqs->fetch();
	$_SESSION['statut'] = 'visiteur';
}
else
{
	$requete = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $requete->fetch();
	$reqs = $bdd->prepare('SELECT * FROM clan WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id' => $donnees_jeu['id_clan']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $reqs->fetch();
	$_SESSION['statut'] = 'membre';
}

if ($donnees['id_jeu'] == $_SESSION['id_jeu'])
{
	$_SESSION['statut'] = 'meneur';
}

// ON SUPPRIME LA PUBLICATION //////////////////////////////////////////////////

if(isset($_GET['id_publication'],$_GET['supprimer_publication']))
{
	$reqs_p = $bdd->prepare('SELECT id_jeu, lieu_publication_id_jeu FROM publication 
							WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_p->execute(array('id' => $_GET['id_publication']))
							or die(print_r($bdd->errorInfo()));
	$donnees_publi = $reqs_p->fetch();
	if(isset($donnees_publi['id_jeu'],$donnees_publi['lieu_publication_id_jeu']) 
	AND $donnees_publi['id_jeu'] == $_SESSION['id_jeu'] 
	OR $_SESSION['statut'] == 'meneur')
	{
		$req1 = $bdd->prepare('DELETE FROM publication WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		
		$req2 = $bdd->prepare('DELETE FROM actu WHERE id_table=:id')
								or die(print_r($bdd->errorInfo()));
		$req2->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
								
		$req3 = $bdd->prepare('DELETE FROM notifications
								WHERE id_publication=:id')
								or die(print_r($bdd->errorInfo()));
		$req3->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requ�te
		
		$req4 = $bdd->prepare('DELETE FROM commentaire 
								WHERE id_publication=:id')
								or die(print_r($bdd->errorInfo()));
		$req4->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
		$req4->closeCursor(); // Termine le traitement de la requ�te
	}
	if(isset($_GET['page']))
		header('Location: team-p'.$_GET['page'].'.html');
	else
		header('Location: team.html');
}
			
// ON SUPPRIME LES COMMENTAIRES ////////////////////////////////////////////////
if(isset($_GET['id_publication'],$_GET['supprimer_commentaire'],$_GET['id_commentaire']))
{
	$reqs_c = $bdd->prepare('SELECT id_jeu, date_commentaire FROM commentaire 
							WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_c->execute(array('id' => $_GET['id_commentaire']))
							or die(print_r($bdd->errorInfo()));
	$donnees_comm = $reqs_c->fetch();
	$reqs_p = $bdd->prepare('SELECT id_jeu FROM publication WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs_p->execute(array('id' => $_GET['id_publication']))
							or die(print_r($bdd->errorInfo()));
	$donnees_publi = $reqs_p->fetch();

	if(isset($donnees_comm['id_jeu']) 
	AND $donnees_comm['id_jeu'] == $_SESSION['id_jeu']  
	OR $_SESSION['statut'] == 'meneur' 
	OR $donnees_publi['id_jeu'] == $_SESSION['id_jeu'])
	{
		$req1 = $bdd->prepare('DELETE FROM commentaire WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id' => $_GET['id_commentaire'])) 
								or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		$req3 = $bdd->prepare('DELETE FROM notifications 
								WHERE date_notification=:date_notification 
								AND id_publication=:id_publication')
								or die(print_r($bdd->errorInfo()));
		$req3->execute(array('date_notification' => $donnees_comm['date_commentaire'], 
								'id_publication' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requ�te
	}
	if(isset($_GET['page']))
		header('Location: team-p'.$_GET['page'].'.html#'.$_GET['id_publication'].'');
	else
		header('Location: team.html#'.$_GET['id_publication'].'');
}
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body> 

<div id="overlay"></div>
<div id="contient_image_profil"></div>
<?php // OVERLAY VIDEO
if (isset($_GET['watch_video']))
{		 
	echo'<div id="grand_overlay_videos">';
	if (isset($_GET['id']) AND $_GET['id'] > 0)
	{
		echo'
		<a href="team-i'.$_GET['id'].'.html">
			<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
		</a>';
	}
	else
	{
		echo'
		<a href="team.html">
			<img id="fermer_message" src="images/quitter_video.png" alt=" Fermer "/>
		</a>';
	}
	
	echo'</div>';
	echo'<div id="overlay2_videos" style="display:block">';
	echo'
	<object type="application/x-shockwave-flash" width="700" height="450" data="http://www.youtube.com/v/'.stripslashes(htmlspecialchars($_GET['watch_video'])).'&hl=fr">							
		<param name="wmode" value="transparent" />
		<param name="allowFullScreen" value="true" />
	</object> ';
	echo '</div>';
}// FIN OVERLAY VIDEO

// OVERLAY SUPPRIMER MEMBRE ////////////////////////////////////////////////////
if(isset($_GET['sup']) 
AND $donnees['id_jeu'] == $_SESSION['id_jeu'])
{
?>
	<div id="grand_overlay4"></div>
	<div id="overlay4_clan">
		<a href="team.html">
			<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
		</a>
		<p id="titre_overlay_membre">
			Supprimer un membre du Clan
		</p>
		<div id="sous_overlay4">
	<?php
			if (isset($_GET['p']) AND $_GET['p'] > 0)
			{
				$numero_page = $_GET['p'];
				$numero_page--;
				$numero_page = 30*$numero_page;
			}
			else
				$numero_page = 0;
				
			$requete = $bdd->prepare('SELECT * FROM jeu WHERE id_clan=:id_clan 
									ORDER by pseudo ASC
									LIMIT '.$numero_page.',30')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id_clan' => $donnees['id']))
									or die(print_r($bdd->errorInfo()));
									
			while($donnees_membre = $requete->fetch())
			{
				echo'
				<div class="supprimer_meneur">
					<a href="id'.$donnees_membre['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_membre['pseudo']))).'">
						<span class="pseudo_overlay4">
							'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'
						</span>
					</a>';	
					echo'
					<div class="centreimg_overlay">';
					echo'
						<a  href=id'.$donnees_membre['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_membre['pseudo']))).'" 
						title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
						
					if(isset($donnees_membre['photo_profil']) AND $donnees_membre['photo_profil'] != '' AND $donnees_membre['photo_profil'] != 0)
					{  
						$source = getimagesize('images_utilisateurs/'.$donnees_membre['photo_profil']); // La photo est la source
						if ($source[0] <= 60 AND $source[1] <= 60)
							echo'<img src="images_utilisateurs/'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
						else
							echo'<img src="images_utilisateurs/mini_2_'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
					}
					else
					{
						echo'<img src="images/tete3.png" alt="Photo de profil" />';
					}
					
					echo'
						</a>
					</div>';
					if($_SESSION['id_jeu'] == $donnees_membre['id'])
					{
						echo'<img id="meneur_supprimer_membre"src="images/meneur_clanp3.png" alt="Meneur" />';
					}
					
					if($_SESSION['id_jeu'] != $donnees_membre['id'])
					{
						echo'
						<form action="clan_post.php" method="post">
							<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce membre?\');
							if (sup == 0)return false;">
								<input type="submit" class="supprimer_clanp3" name="Valider" value="Supprimer"/>
							</a>';
					}
				echo'
				</div>';
				
				//ON ENVOIT L'ID DU JOUEUR
				echo'	<input type="hidden" name="supprimer_membre"/>
						<input type="hidden" name="id_jeu" value="'.$donnees_membre['id'].'"/>
					</form>';
				
			}
			
		echo'
		</div>';
			
		$nbre_page = 1;
		
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM jeu 
							WHERE id_clan=:id_clan')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('id_clan' => $donnees['id']))
							or die(print_r($bdd->errorInfo()));
		
		$do = $p->fetch();
		$nbr_entrees = $do['nbre_publi'];
		$p->closeCursor();
		
		if (isset ($_GET['p']))
		$current_page = $_GET['p'];
		else
			$current_page = 1;
			
		$nom_page = 'team';
		$suite_page = 'sup';
		$nbr_affichage = 30;

		include('pagination_p.php');
			
?>			
	</div>
<?php
}


// OVERLAY SUPPRIMER LA TEAM ///////////////////////////////////////////////////
if(isset($_GET['nommer']))
{
?>
	<div id="grand_overlay4"></div>
	<div id="overlay4_clan">
		<a href="team.html">
			<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
		</a>
		<p id="titre_overlay_membre">
			Pour quitter le Clan vous devez nommer un nouveau meneur
		</p>
		<div id="sous_overlay_supprimer_clan">
<?php			
			if (isset($_GET['p']) AND $_GET['p'] > 0)
			{
				$numero_page = $_GET['p'];
				$numero_page--;
				$numero_page = 30*$numero_page;
			}
			else
				$numero_page = 0;
			
			$requete = $bdd->prepare('SELECT * FROM jeu 
									WHERE id_clan=:id_clan 
									ORDER by pseudo ASC 
									LIMIT '.$numero_page.',30')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id_clan' => $donnees['id']))
									or die(print_r($bdd->errorInfo()));
			while($donnees_membre = $requete->fetch())
			{	
			echo'
			<div class="nommer_meneur">
				<a href="id'.$donnees_membre['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_membre['pseudo']))).'">
					<span class="pseudo_overlay4">
						'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'
					</span>
				</a>';	
				echo'
				<div class="centreimg_overlay">';
				echo'<a  href="id'.$donnees_membre['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_membre['pseudo']))).'" 
					title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
					
				if(isset($donnees_membre['photo_profil']) AND $donnees_membre['photo_profil'] != '' AND $donnees_membre['photo_profil'] != 0)
				{
					$source = getimagesize('images_utilisateurs/'.$donnees_membre['photo_profil']); // La photo est la source
				if ($source[0] <= 60 AND $source[1] <= 60)
					echo'<img src="images_utilisateurs/'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
				else
					echo'<img src="images_utilisateurs/mini_2_'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
				}
				else
				{
					echo'<img src="images/tete3.png" alt="Photo de profil" />';
				}
				echo'
					</a>
				</div>';

				if($_SESSION['id_jeu'] != $donnees_membre['id'])
				{
					echo'
					<form action="clan_post.php" method="post">
						<a onclick ="var sup=confirm(\'�tes vous sur de vouloir nommer '.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).' meneur du Clan?\');
						if (sup == 0)return false;">
							<input type="submit" class="nommer_clanp3" name="Valider" value="Nommer"/>
						</a>';
				}
			echo'
			</div>';
			
			//ON ENVOIT L'ID DU CLAN ET L'ID DU JOUEUR
					echo'
						<input type="hidden" name="nommer"/>
						<input type="hidden" name="id_jeu" value="'.$donnees_membre['id'].'"/> 
						<input type="hidden" name="id_clan" value="'.$donnees['id'].'"/>
					</form>';
			}
			}
			echo'
		</div>';
			
		$nbre_page = 1;
		
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM jeu 
							WHERE id_clan=:id_clan')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('id_clan' => $donnees['id']))
							or die(print_r($bdd->errorInfo()));
		
		$do = $p->fetch();
		$nbr_entrees = $do['nbre_publi'];
		$p->closeCursor();
		
		if (isset ($_GET['p']))
		$current_page = $_GET['p'];
		else
			$current_page = 1;
			
		$nom_page = 'clan';
		$suite_page = 'supprimer';
		$nbr_affichage = 30;

		include('pagination_p.php');
?>			
	</div>
<?php

// OVERLAY MEMBRE
if(isset($_GET['membres']))
{
?>
	<div id="grand_overlay4"></div>
	<div id="overlay4_clan">
<?php
		if(isset($_GET['id'], $_GET['membres']))
			echo'
			<a href="team-i'.$_GET['id'].'.html">';
	
		if(empty($_GET['id']))
			echo'
			<a href="team.html">';
?>
				<img id="fermer_message" src="images/fermer.png" alt=" Fermer "/>
			</a>
		<p id="titre_overlay_membre">
			Voici les membres du Clan
		</p>
		<div id="sous_overlay4">
<?php			
			if (isset($_GET['p']) AND $_GET['p'] > 0)
			{
				$numero_page = $_GET['p'];
				$numero_page--;
				$numero_page = 30*$numero_page;
			}
			else
				$numero_page = 0;
				
			$requete = $bdd->prepare('SELECT * FROM jeu 
									WHERE id_clan=:id_clan 
									ORDER by pseudo ASC 
									LIMIT '.$numero_page.',30')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id_clan' => $donnees['id']))
									or die(print_r($bdd->errorInfo()));
									
			while($donnees_membre = $requete->fetch())
			{
				echo'
				<div class="membre_forum">';
				
				if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
				{
					echo'<a  href="id'.$donnees_membre['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_membre['pseudo']))).'" 
						title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
				}
				elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
					echo'<span  title="Moi">';
				else
					echo'<span  title="Mon sous compte">';
				// ON L'ENTORUE AVEC LES BONNES CONDITIONS POUR LES LIENS //////
				echo'		<span class="pseudo_overlay4">
								'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'
							</span>';
				////////////////////////////////////////////////////////////////
				if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
					echo'</a>';
				else
					echo'</span>';
					
				echo'<div class="centreimg_overlay">';
				// POUR LES LIENS SI C'EST SON SOUS COMPTE OU PAS
				if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
				{
					echo'<a  href="id'.$donnees_membre['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_membre['pseudo']))).'" 
					title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
				}
				elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
					echo'<span  title="Moi">';
				else
					echo'<span  title="Mon sous compte">';
					
				if(isset($donnees_membre['photo_profil']) AND $donnees_membre['photo_profil'] != '' AND $donnees_membre['photo_profil'] != 0)
				{  
					$source = getimagesize('images_utilisateurs/'.$donnees_membre['photo_profil']); // La photo est la source
					if ($source[0] <= 60 AND $source[1] <= 60)
						echo'<img src="images_utilisateurs/'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
					else
						echo'<img src="images_utilisateurs/mini_2_'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
				}
				else
					echo'<img src="images/tete3.png" alt="Photo de profil" />';
					
				// POUR LES LIENS SI C'EST SON SOUS COMPTE OU PAS
				if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
					echo'</a>';
				else
					echo'</span>';
					
				echo'</div>';
				if(strlen($donnees_membre['nom_jeu']) > 15)
				{
					echo'
					<p class="information_membre" style="font-size:x-small;">'; 
						echo 
						substr(stripslashes(htmlspecialchars($donnees_membre['nom_jeu'])), 0, 20).'';
						if(strlen(stripslashes(htmlspecialchars($donnees_membre['nom_jeu']))) > 20)
						{ echo'...';} 
					echo'
					</p>';
				}
				else
				{
					echo'
					<p class="information_membre">
						'.stripslashes(htmlspecialchars($donnees_membre['nom_jeu'])).'
					</p>';
				}
				echo'
				<p class="information_membre">
					'.$donnees_membre['plateforme'].'
				</p>
				</div>';	
			}
		
		echo'
		</div>';
		
		$nbre_page = 1;
		
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM jeu 
							WHERE id_clan=:id_clan')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('id_clan' => $donnees['id']))
							or die(print_r($bdd->errorInfo()));
		
		$do = $p->fetch();
		$nbr_entrees = $do['nbre_publi'];
		$p->closeCursor();
		
		if (isset ($_GET['p']))
		$current_page = $_GET['p'];
		else
			$current_page = 1;
			
		$nom_page = 'clan';
		$suite_page = 'membres';
		$nbr_affichage = 30;

		include('pagination_p.php');
?>				
		
	</div>
<?php
}
?>
<!----------------------- OVERLAY MODIFIER DESCRIPTION !----------------------->
<!--[if gte IE 6]>
	<style type="text/css">
		#modifier_description_clan
		{
			height:160px;
		}
	</style>
<![endif]-->
<div id="grand_overlay1"></div>
<div id="overlay1_clan">
	<a id="fermer4" href="#">
		<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
	</a>
	<p id="titre_overlay_membre">
		Modifier la description de votre Clan 
	</p>
	<form action="clan_post.php" method="post">
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;">Description
			<textarea id="modifier_description_clan" name="modifier_info_clan"><?php echo $donnees['information'];?></textarea>
			<br />
			<input type="submit" id="publier5" class="enregistrez_informations" name="Valider" value=" Enregistrer "/>
		</p>
		<input type="hidden" name="changer_description"/>
	</form>
</div>
<?php
if(isset($_GET['id'])) // CHOPPER TOUT DU MENEUR POUR LES CANDIDATURES
{
	$reqs = $bdd->prepare('SELECT id_jeu FROM clan WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id' => $_GET['id']))
							or die(print_r($bdd->errorInfo()));
	$id_meneur = $reqs->fetch();
	$reqs->closeCursor(); // Termine le traitement de la requ�te
	
	$reqs_meneur = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
	$reqs_meneur->execute(array('id' => $id_meneur['id_jeu']))
								or die(print_r($bdd->errorInfo()));
	$donnees_meneur = $reqs_meneur->fetch();
	$reqs_meneur->closeCursor(); // Termine le traitement de la requ�te
}
else
{
	$requete = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
								or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $requete->fetch();
	$requete->closeCursor(); // Termine le traitement de la requ�te
	
	$reqs = $bdd->prepare('SELECT id_jeu FROM clan WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id' => $donnees_jeu['id_clan']))
							or die(print_r($bdd->errorInfo()));
	$id_meneur = $reqs->fetch();
	$reqs->closeCursor(); // Termine le traitement de la requ�te
	
	$reqs_meneur = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
	$reqs_meneur->execute(array('id' => $id_meneur['id_jeu']))
								or die(print_r($bdd->errorInfo()));
	$donnees_meneur2 = $reqs_meneur->fetch();
	$reqs_meneur->closeCursor(); // Termine le traitement de la requ�te
}

?>
<!--------------------------- OVERLAY CANDIDATURE !---------------------------->
<!--[if gte IE 6]>
	<style type="text/css">
		#message_candidature_overlay
		{
			height:185px;
			width:395px;
		}
	</style>
<![endif]-->
<div id="grand_overlay2"></div> 
<div id="overlay2_clan">
	<a id="fermer2" href="#">
		<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
	</a>
	<p id="titre_overlay_membre">
		Devenir membre 
	</p>
	<form action="clan_post.php" method="post">
<?php
if(isset($donnees_meneur['pseudo']) AND $donnees_meneur['pseudo'] != '')
{
	if(strlen($donnees_meneur['pseudo']) < 10)
	{
		echo'
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;">Message de candidature au meneur : 
			<a href="id'.$donnees_meneur['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_meneur['pseudo']))).'" 
			title="'.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'">';
				echo
				' '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'
			</a>';
	}
	else
	{
		echo'
		<p style="text-align:left; color:#4e5f69; margin-left:25px; 
		margin-top:10px;">
			Message de candidature au meneur :
			<br />
			<a  href="id'.$donnees_meneur['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_meneur['pseudo']))).'" 
			title="'.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'">';
				echo' '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'
			</a>';
	}
}
else
{
	echo'
		<p style="text-align:left; color:#4e5f69; margin-left:25px; 
		margin-top:30px;">
			Message de candidature';
}
echo '
			<textarea id="message_candidature_overlay" rows="" cols="" name="candidature"></textarea>
			<br />
		</p>
		<input type="submit" id="publier" class="envoyer_profil" name="Valider" value="Envoyer"/>
		<input type="hidden" name="id_clan" value="'.$donnees['id'].'"/>
	</form>
</div>';
?>

<!------------------------ OVERLAY MODIFIER IMAGE CLAN !----------------------->
<div id="grand_overlay3"></div>
<div id="overlay3_clan">
	<a id="fermer3" href="#">
		<img id="fermer_photo_clanp3" src="images/fermer.png" alt=" Fermer " />
	</a>
<?php
echo'
	<div id="contient2_image_clanp3">';	
	if (!empty($donnees['photo_clan']))
	{
		$source = getimagesize('images_utilisateurs/'.$donnees['photo_clan']); 	// La photo est la source
			if ($source[0] <= 400 AND $source[1] <= 200)
			{
				echo'<img src="images_utilisateurs/'.$donnees['photo_clan'].'" alt="Photo de clan"/>'; 
			}
			else 
				echo'<img src="images_utilisateurs/clan_1_'.$donnees['photo_clan'].'" alt="Photo de clan"/>';
				
	}
	else
		echo'<img id="defaut_clanp3" src="images/defaut_clan.png" alt="Photo de clan"/>';
?>
		<form id="formulaire" action="clan_post.php" method="post" enctype="multipart/form-data">
			<input type="hidden" name="changer_image" />
			<input type="file" id="input" name="photo_clan" style="margin-top:15px;" />
			<input type="submit" id="publier3" class="enregistrez_clanp3" name="Valider" value=" Enregistrer "/>
		</form>
	</div>
</div>
<?php 

include('menu_sans_doctype.php');
 		
$_SESSION['id_clan'] = $donnees['id'];
// CREER CLAN = CLAN PAGE 2 
if (isset($_GET['creer']) AND !isset($donnees['id']))
{
?>
	<div id="corps_clanp2" >

		<img id="creez_clanp2" src="images/creez_clanp2.png" alt=" Cr�ez votre page clan rapidement " />
			
		<div id="sous_corps_clanp2">
			<img id="defaut_clan" src="images/defaut_clan.png" alt=" Image clan par d�faut " />
			<form action="clan_post.php" method="post" enctype="multipart/form-data">
				<p class="selectionner_image"> 
					S�lectionner une image pour votre team
					<br /> 
					<input type="file" class="parcourir" name="photo_clan" value="Parcourir" />
				</p>
				<p class="info_clan"> 
					*Nom de la team 
					<br /> 
					<input class="input3" type="text" name="nom_clan" style="width:340px; height:25px;"/>
				</p>
				<p class="info_clan"> 
					*Jeu 
					<br /> 
					<input class="input3" type="text" id="recherche_jeu" name="jeu" style="width:340px; height:25px;"/>
				</p>
				<div id="results" style="display:none;"></div>
				<p class="info_clan">
					<label for="plateforme">
						*Plateforme 
						<br />
					</label>
					<select class="input1" name="plateforme" id="plateforme" style="width:345px; height:32px;" >
					   <option value="S�lectionnez">S�lectionnez</option>
					   <option value="S�lectionnez">S�lectionnez</option>
					   <option value="3DO">3DO</option>
					   <option value="Amiga CD32">Amiga CD32</option>
					   <option value="Atari 2600">Atari 2600</option>
					   <option value="Atari 5200">Atari 5200</option>
					   <option value="Atari 7800">Atari 7800</option>
					   <option value="Dreamcast">Dreamcast</option>
					   <option value="Famicom Disk">Famicom Disk</option>
					   <option value="GameBoy">GameBoy</option>
					   <option value="GameBoy Advance">GameBoy Advance</option>
					   <option value="GameBoy SP">GameBoy SP</option>
					   <option value="GameCube">GameCube</option>
					   <option value="Game Gear">Game Gear</option>
					   <option value="Iphone">Iphone</option>
					   <option value="Jaguar">Jaguar</option>
					   <option value="Lynx">Lynx</option>
					   <option value="Mac OS">Mac OS</option>
					   <option value="Master System">Master System</option>
					   <option value="Mega CD">Mega CD</option>
					   <option value="Megadrive">Megadrive</option>
					   <option value="NeoGeo">NeoGeo</option>
					   <option value="NeoGeo CD">NeoGeo CD</option>
					   <option value="NES">NES</option>
					   <option value="Nintendo 64">Nintendo 64</option>
					   <option value="Nintendo DS" >Nintendo DS</option>
					   <option value="Nintendo 3DS">Nintendo 3DS</option>
					   <option value="PC">PC</option>
					   <option value="Playstation">Playstation</option>
					   <option value="Playstation 2">Playstation 2</option>
					   <option value="Playstation 3">Playstation 3</option>
					   <option value="Playstation 4">Playstation 4</option>			   
					   <option value="PSP">PSP</option>
					   <option value="PSP GO">PSP GO</option>
					   <option value="PSP Vita">PSP Vita</option>
					   <option value="Saturn">Saturn</option>
					   <option value="Super NES" >Super NES</option>
					   <option value="Wii" >Wii</option>
					   <option value="Wii U" >Wii U</option>
					   <option value="WonderSwan" >WonderSwan</option>
					   <option value="Xbox" >Xbox</option>
					   <option value="Xbox 360" >Xbox 360</option>
					   <option value="Xbox One" >Xbox One</option>
					</select>
				</p>
				<p class="info_clan">
					*Description de la team (250 caract�res maximum) 
					<br />
					<textarea id="input4" name="information" rows="" cols="" maxlength="1000" ></textarea>
				</p>
				<input type="submit" id="publier2" class="enregistrez_informations" name="Valider" value=" Enregistrer "/>
			</form>

		</div>
<?php
	if (isset($_GET['fail']))
	{
		echo'
		<p class="bloc_erreur">
			<img src="images/attention.png" alt=" "/> 
			Veuillez remplir tous les champs tout en faisant attention � 
			votre nom de team qui doit �tre compris entre 2 et 
			20 caract�res. 
			La description doit contenir 250 caract�res maximum.
		</p>';
	}
	echo'
	</div>'; ?>
<script>
// LE CODE QUI SUIT CONCERNE LA BDD JEU
var results = document.getElementById('results');
var recherche = document.getElementById('recherche_jeu');
var mot_recherche, reponse;
var selection = -1;
recherche.addEventListener('keyup', function (event) 
{ 
var xdr = new XMLHttpRequest();  

	mot_recherche = recherche.value;
	xdr.open('GET', 'requete_ajax.php?name=' + mot_recherche);
	xdr.onreadystatechange = function(e) // On g�re ici une requ�te asynchrone
	{ 
        if (xdr.readyState == 4 && xdr.status == 200) // Si le fichier est charg� sans erreur
		{ 
var parser = new DOMParser();
var xmlDoc = parser.parseFromString(xdr.responseText, "application/xml");
            reponse = xmlDoc.getElementsByTagName('GameTitle');
			results.innerHTML = '';
			var taille_reponse = reponse.length;
			for (var i=0; i < taille_reponse; i++)
			{
				div = document.createElement('div');
				div.innerHTML = reponse[i].textContent;
				results.appendChild(div);
			}
			results.style.display = 'block';
        }
		else if(xdr.readyState == 4 && xdr.status != 200)
		{
			alert('Une erreur est survenue !\n\nCode :' + xdr.status + '\nTexte : ' + xdr.statusText);
		}
    }
	xdr.send(null);
},false);
results.addEventListener('click', function (event)
{
	texte = event.target.textContent;
	recherche.value = texte;
	results.style.display = 'none';
},false);

//------------------------------------------------------------------------------
</script>'
<?php 
// FIN CREER CLAN = CLAN PAGE 2 
}
elseif ((isset($donnees_jeu['id_clan']) AND $donnees_jeu['id_clan'] != '' 
AND $donnees_jeu['id_clan'] != 0) 
OR isset($_GET['id']) OR isset($_POST['id']))
{
	// IL A DEJA CREE UN CLAN = PAGE 3 LES OVERLAY SONT AU DESSUS DU DOCTYPE

 	if (isset($_GET['id']) AND $_GET['id'] > 0)
	{
		echo'<input type="hidden" name="id" value="'.$_GET['id'].'"/>';
	}
?>
<div id="corps_clanp3">
<?php

	if(isset($_GET['ed']))
	{
	   echo'
		<p class="bloc_erreur">
			<img src="images/attention.png" alt="erreur"> 
			La description ne doit pas d�passer les 250 caract�res. 
		</p>';
	}
	 
echo '
	<div id="sous_corps_clanp3">

		<div id="bloc1_p3">';
		$requete = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
		$donnees_clan = $requete->fetch();
		
		if(isset($_GET['ec'])) //erreur candidature
		{
			echo'<a href="#" id="rejoindre">
					<div id="devenir_membre2"></div>
				</a>';
		}
		elseif ($_SESSION['statut'] == 'meneur')
		{	
			echo'<img class="haut_droit_clanp3"src="images/meneur_clanp3.png" alt="Meneur" />';
		}
		elseif ($_SESSION['statut'] == 'membre')
		{
			echo'<img class="haut_droit_clanp3"src="images/membre_clanp3.png" alt="Membre" />';
		}
		elseif (isset($donnees_clan['id_clan']) AND $donnees_clan['id_clan'] == 0) //VISITEUR SANS GUILDE
		{
			echo'<a href="#" id="rejoindre"><div id="devenir_membre"></div></a>';
		}
		else
			echo'';
			
	if(isset($_GET['id']))
	{	
		echo'<div id="centre_image60_clanp3">';
			if ($donnees_meneur['photo_profil'] != 0)
			{
				echo'<a  href="id'.$donnees_meneur['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_meneur['pseudo']))).'">';
				$source = getimagesize('images_utilisateurs/'.$donnees_meneur['photo_profil']); // La photo est la source
				
				if ($source[0] <= 60 AND $source[1] <= 60)	
				{
					echo'
						<img title="Meneur : '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'" 
						src="images_utilisateurs/'.$donnees_meneur['photo_profil'].'" alt="Photo de profil"/>
					</a>';
				}
				else
				{
					echo'
						<img title="Meneur : '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'" 
						src="images_utilisateurs/mini_2_'.$donnees_meneur['photo_profil'].'" alt="Photo de profil"/>
					</a>';
				}
			}
			else
			{
				echo '
				<a  href="id'.$donnees_meneur['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_meneur['pseudo']))).'">
					<img  title="Meneur : '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'" 
					class="image_publication" src="images/tete3.png" alt="Photo de profil"/>
				</a>';
			}
		echo'</div>';
	}
	else
	{
		echo'<div id="centre_image60_clanp3">';
		if ($donnees_meneur2['photo_profil'] != 0)
		{
			echo '<a  href="id'.$donnees_meneur2['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_meneur2['pseudo']))).'">';
			$source = getimagesize('images_utilisateurs/'.$donnees_meneur2['photo_profil']); // La photo est la source
			
			if ($source[0] <= 60 AND $source[1] <= 60)	
			{		
				echo'
					<img title="Meneur : '.stripslashes(htmlspecialchars($donnees_meneur2['pseudo'])).'" 
					src="images_utilisateurs/'.$donnees_meneur2['photo_profil'].'" alt="Photo de profil"/>
				</a>';
			}
			else
			{
				echo'
					<img title="Meneur : '.stripslashes(htmlspecialchars($donnees_meneur2['pseudo'])).'" 
					src="images_utilisateurs/mini_2_'.$donnees_meneur2['photo_profil'].'" alt="Photo de profil"/>
				</a>';
			}
		}
		else
		{
			echo '
			<a  href="id'.$donnees_meneur2['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_meneur2['pseudo']))).'">
				<img  title="Meneur : '.stripslashes(htmlspecialchars($donnees_meneur2['pseudo'])).'" 
				class="image_publication" src="images/tete3.png" alt="Photo de profil"/>
			</a>';
		}
		echo'</div>';
	}		
			
		echo'	<img id="embleme_clanp3" src="images/embleme_clan_recherche.png" alt=" "/>
				<p id="nom_clanp3">
					'.stripslashes(htmlspecialchars($donnees['nom_clan'])).'
				</p>	
				<p id="infos_clanp3">
					Jeu : '; 
					echo substr(stripslashes(htmlspecialchars($donnees['jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnees['jeu']))) > 20)
					{ echo'...';} 
					echo'
					<br />
					Plateforme : '.$donnees['plateforme'].'
				</p>
			</div>';
					
			echo'
			<div id="bloc2_p3">';
		echo'
		<div id="contient_image_clanp3">';
		if (!empty($donnees['photo_clan']))
		{
			$source = getimagesize('images_utilisateurs/'.$donnees['photo_clan']); // La photo est la source
			if ($source[0] <= 400 AND $source[1] <= 200) 
			{
				echo'
				<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donnees['photo_clan'].'">
					<img src="images_utilisateurs/'.$donnees['photo_clan'].'" alt="Photo de clan"/>
				</a>'; 
			}
			else 
				echo'
				<a title="Afficher l\'image originale" href="images_utilisateurs/clan_2_'.$donnees['photo_clan'].'">
					<img src="images_utilisateurs/clan_1_'.$donnees['photo_clan'].'" alt="Photo de clan"/>
				</a>';
		}
		else
			echo'<img id="defaut_clanp3" src="images/defaut_clan.png" alt="Photo de clan"/>';
			
	// MODIFIER IMAGE CLAN	////////////////////////////////////////////////////	
	if ($_SESSION['statut'] == 'meneur')
	{	
		echo'
		<a href="#" id="changer_image_clanp3">
			<div id="edit2_clanp3" title="modifier"></div>
		</a>';
	}
//------------------------------------------------------------------------------
	echo'
	</div>';
		echo'<div id="bloc_description_clanp3">';
		$message = nl2br(stripslashes(htmlspecialchars($donnees['information'])));
		$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);
		if (preg_match("#http://(.+)#isU", $message))
		{
			$message = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);					
		}
		elseif (preg_match("#https://(.+)#isU", $message))
		{
			$message = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);
		}
		else
		{
			$message = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$message);
		}
		echo'
		<span id="description_clanp3">
			<span style="color: #4e5f69; font-size:large;">
				Description : 
			</span>
			'.$message.'
		</span>'; 

		echo'
			</div>';
		if ($_SESSION['statut'] == 'meneur')
		{	
			echo'
			<a href="#" style="display:none" id="changer_info_guilde">
				<div id="edit_clanp3" title="modifier"></div>
			</a>';
		}
?>			
		</div>
	
		<div id="bloc3_p3">		
			<a href="forum.html">
				<div id="forum_clanp3"></div>
			</a>
		</div>
	
		<div id="bloc4_p3"> <!-- MEMBRES -->		
<?php		
			$i=0;
			$requete2 = $bdd->prepare('SELECT pseudo FROM jeu 
										WHERE id_clan=:id_clan')
										or die(print_r($bdd->errorInfo()));
			$requete2->execute(array('id_clan' => $donnees['id']))	
										or die(print_r($bdd->errorInfo()));
			while($donnees_nbr = $requete2->fetch())
			{
				$i++;
			}
			
			if($_SESSION['statut'] == 'meneur' AND $i > 1)
			{
				echo'
				<a	href="team-sup.html" title="Supprimer un membre">
					<div id="supprimer_membre_clanp3"></div>
				</a>';
			}	
			
			if(isset($_GET['id']))
				echo'
				<a href="team-i'.$_GET['id'].'-membres.html">';
			else
				echo'
				<a href="team-membres.html">';
				
			if($i<100)
			{
				echo'
				<div id="contient_nombre">';
				if($i==1)
					echo'<p id="nombre_clanp3">'.$i.' membre</p>';
				else
					echo'<p id="nombre_clanp3">'.$i.' membres</p>';
			
				echo'
				</div>';
			}
			elseif($i>99)
			{
				echo'<div id="contient_nombre2">
						<p id="nombre_clanp3">'.$i.' membres</p>			
					</div>';
			}
			elseif($i>99999)
			{
				echo'<div id="contient_nombre">
						<p id="nombre_clanp3">'.$i.'</p>			
					</div>';
			}
			echo'</a>';
	
		$requete = $bdd->prepare('SELECT * FROM jeu 
								WHERE id_clan=:id_clan 
								ORDER BY RAND() LIMIT 0,8')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('id_clan' => $donnees['id']))
								or die(print_r($bdd->errorInfo()));
		while($donnees_membre = $requete->fetch())
		{
		
			if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'<a  href="id'.$donnees_membre['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_membre['pseudo']))).'" 
					title="'.stripslashes(htmlspecialchars($donnees_membre['pseudo'])).'">';
			}
			elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
				echo'<span  title="Moi">';
			else
				echo'<span  title="Mon sous compte">';
				
			if(isset($donnees_membre['photo_profil']) AND $donnees_membre['photo_profil'] != '' AND $donnees_membre['photo_profil'] != 0)
			{
				$source = getimagesize('images_utilisateurs/'.$donnees_membre['photo_profil']); // La photo est la source
				if ($source[0] <= 60 AND $source[1] <= 60)
					echo'<img class="photo_profil_clanp3" src="images_utilisateurs/'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
				else
					echo'<img class="photo_profil_clanp3" src="images_utilisateurs/mini_2_'.$donnees_membre['photo_profil'].'" alt="Photo de profil" />';
			}
			else
				echo'<img class="photo_profil_clanp3" src="images/tete3.png" alt="Photo de profil" />';
				
			if($donnees_membre['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'</a>';
			elseif($donnees_membre['id'] == $_SESSION['id_jeu'])
				echo'</span>';
			else
				echo'</span>';
		}
		
?>
		</div>
		
	</div>
<?php
	//MESSAGE D'ERREUR IMAGE INVALIDE

	if(isset($_GET['erreur_image']) AND $_GET['erreur_image'] == 0)
	{
		echo'
		<p class="bloc_erreur">
			<img src="images/attention.png" alt="erreur"> Image non valide.
			(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
		</p>';
	}
	if(isset($_GET['erreur_image']) AND $_GET['erreur_image'] == 1)
	{
		echo'
		<p class="bloc_erreur">
			<img src="images/attention.png" alt="erreur"> Image non valide.
			(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
		</p>';
	} 
	
	// SAVOIR LE NOMBRE DE PUBLICATION POUR METTRE EN CORPS EN FONCTION DU NOMBRE //
	$req = $bdd->prepare('SELECT COUNT(*) AS nbre FROM publication 
						WHERE id_clan=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $donnees['id']))
						or die(print_r($bdd->errorInfo()));
	$do = $req->fetch();
		
	if ($_SESSION['statut'] == 'meneur' OR $_SESSION['statut'] == 'membre')
	{	
	?>
		<!--[if gte IE 6]>
			<style type="text/css">
				.champs_publication
				{
					width:597px;
					height:74px;
				}
				.champs_you_tube
				{
					width:597px;
				}
			</style>
		<![endif]-->
		
		<div class="bbcode_css">
			<script type="text/javascript" src="bbcode.js"></script>
			<a href="#">
				<img src="images/bbcode/gras.gif" alt="gif" title="Gras"
				onclick="javascript:bbcode('[g]', '[/g]'); return(false)"/>
			</a>
			<a href="#">
				<img src="images/bbcode/italique.gif" alt="gif" title="Italique" 
				onclick="javascript:bbcode('[i]', '[/i]'); return(false)"/>
			</a>
			<a href="#">
				<img src="images/bbcode/souligner.gif" alt="gif" title="Souligner"
				onclick="javascript:bbcode('[s]', '[/s]'); return(false)"/>
			</a>
			<a href="#">
				<img src="images/bbcode/image.gif" alt="gif" title="Image" 
				onclick="javascript:bbcode('[img]', '[/img]'); return(false)"/>
			</a>
			<!-- fin bbcode -->	
		</div>
		<div id="debut_publication"> 
			<form id="formulaire2" action="profil_post.php" method="post" enctype="multipart/form-data">
				<div id="contient_champs_publication">		
					<textarea type="text" id="publication" class="champs_publication" 
						name="publication" placeholder=" Publiez sur le profil de votre clan " 
						style="color:#102c3c; font-weight:bolder;" ></textarea>
					
					<textarea type="text" id="you_tube" class="champs_you_tube" 
						name="you_tube" placeholder=" Inserez votre lien You Tube" 
						style="font-size:small; font-weight:bolder;" ></textarea>
					
					<div id="contient_publier">
						<input type="submit" id="publier4" name="publiez" value="Publiez" class="publier"/>
					</div>
				</div>
				<!-- VIDEOS! -->
				<div id="conteneurfile">
					<input type="file" name="videos" onmousedown="return false" 
						onkeydown="return false" class="inputfile" 
						onchange="displayfilename(this);" />   
				</div>
				<!-- PHOTOS -->
				<div id="conteneurfile2">
					<input type="file" name="photos" onmousedown="return false" 
						onkeydown="return false" class="inputfile" 
						onchange="displayfilename(this);" />   
				</div>	
				<div id="there"></div>
		</div>	
				<input type="hidden" name="id_clan" value="<?php echo$donnees['id']; ?>"/>
				<input type="hidden" name="profil_ou_clan" value="clan"/>
	<?php      
				if (isset($_GET['id']))
					echo '<input type="hidden" name="id" value="'.$_GET['id'].'"/>';
	 
		echo'</form>';		
	}
	else
	{
		if($do['nbre'] != 0)
			echo'<img id="pas_membre_clanp3" src="images/pas_membre.png" alt="Devenez membre de ce Clan et publiez sur son profil."/>';
	}
?>			
	<div id="fond_profil_right"></div>		
    <div id="sous_corps2_clanp3">
<?php
	if($_SESSION['statut'] != 'membre')
	{
		echo'
		<div id="bloc_chercher_creer">';
		
		$req_candidature = $bdd->prepare('SELECT * FROM candidature 
										WHERE id_clan=:id_clan')
										or die(print_r($bdd->errorInfo()));
		$req_candidature->execute(array('id_clan' => $donnees_clan['id_clan']))
										or die(print_r($bdd->errorInfo()));
		$i=0;
		while ($donnees_candidature = $req_candidature->fetch())
		{
			$i++;
		}
		if($_SESSION['statut'] == 'meneur' AND $i>0)
		{
			echo'
			<a href="admission.html">
				<div id="candidature2_clanp3"></div>
			</a>';
		}
		
		$requete = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
		$donnees_clan = $requete->fetch();
		if ($_SESSION['statut'] != 'meneur')
		{
			if(isset($donnees_clan['id_clan']) AND $donnees_clan['id_clan'] == 0)
			{
				echo'
				<a href="recherche.html?nom&recherche=">
					<div id="chercher_clanp3" title=" Chercher un Clan "></div>
				</a>			
				<a href="team-creer.html">
					<div id="creer_clanp3" title=" Cr�er un Clan "></div>
				</a>';
			}
			else
			{
				echo'
				<a href="recherche.html?nom&recherche=">
					<div id="chercher_clanp3_grand"></div>
				</a>';
			}
		}
		elseif($_SESSION['statut'] == 'meneur' AND $i==0)
		{
			echo'
			<a href="admission.html">
				<div id="candidature_clanp3"></div>
			</a>';
		}
		echo'
		</div>';
	}
?>			
	<!---------------------------- BLOC IMAGES !----------------------------> 
<?php
	$req_boucle = $bdd->prepare('SELECT adresse_stockage FROM photos 
								WHERE id_clan=:id_clan 
								ORDER BY date_enregistrement DESC LIMIT 0,9 ')
								or die(print_r($bdd->errorInfo()));
	$req_boucle->execute(array('id_clan' => $donnees['id']))
								or die(print_r($bdd->errorInfo()));
	$i=0;
	while ($donneees2 = $req_boucle->fetch()) // on affiche message par message
	{
		$i++;
	}
	if($i>0)
		echo'
	<div id="bloc_images2">';
	else
      	echo'
	<div id="bloc_images">';  

		echo'
		<div id="images_profil">
			<span class="nbr_profil" style="margin-left:193px;">
				( '.$i.' )
			</span>
		</div>';
		
			
		$req = $bdd->prepare('SELECT adresse_stockage FROM photos 
							WHERE id_clan=:id_clan ORDER BY date_enregistrement 
							DESC LIMIT 0,9 ')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_clan' => $donnees['id']))
							or die(print_r($bdd->errorInfo()));
							
		while ($donneees2 = $req->fetch()) // on affiche message par message
		{
			$source = getimagesize('images_utilisateurs/'.$donneees2['adresse_stockage']); // La photo est la source
			if ($source[0] <= 900 AND $source[1] <= 600)
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donneees2['adresse_stockage'].'">';
			else
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$donneees2['adresse_stockage'].'">';
?>
	<!--[if gte IE 6]>
		<style type="text/css">
			.centre_image_bloc_images
			{
				width:0px;
				height:0px;
				margin-left:50px;
				margin-top:60px;
			}
			.centre_image_bloc_images img
			{
				margin-top:-90px;
				margin-left:-30px;
			}
		</style>
	<![endif]-->
<?php				
			echo'<div class="centre_image_bloc_images">';
			if ($source[0] <= 60 AND $source[1] <= 60)
			{
				echo'
					<img src="images_utilisateurs/'.$donnees2['adresse_stockage'].'" />
				</a>'; 
			}
			else
			{
				echo'
					<img src="images_utilisateurs/mini_2_'.$donneees2['adresse_stockage'].'"  alt="Planete"/>
				</a>';
			}
			echo'</div>';
		}
		if($i==0)
			echo'<img src="images/pas_image.png"  alt=" Aucune image "/>';
			
		if($i>0)
		{
			echo'
			<a href="images-it'.$donnees['id'].'.html">
				<div class="afficher_tout" title="Afficher toutes les images" ></div>
			</a>';
		}
?>
	  
    </div>
   
	<!---------------------------- BLOC VIDEOS !----------------------------> 
<?php
	$req55 = $bdd->prepare('SELECT adresse_stockage FROM videos
							WHERE id_clan=:id_clan ORDER BY id DESC LIMIT 0,6')
							or die(print_r($bdd->errorInfo()));
	$req55->execute(array('id_clan' => $donnees['id']))
							or die(print_r($bdd->errorInfo()));
	$v = 0;
	while ($donnees55 = $req55->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
	{
		$v++;
	}
	if($v>0)
		echo'
	<div id="bloc_videos2">';
	else
		echo'
	<div id="bloc_videos">';

		echo'
		<div id="videos_profil">
			<span class="nbr_profil" style="margin-left:193px;">
				( '.$v.' )
			</span>
		</div>';
			
		$req55 = $bdd->prepare('SELECT adresse_stockage FROM videos
		WHERE id_clan=:id_clan ORDER BY id DESC LIMIT 0,6')or die(print_r($bdd->errorInfo()));
		$req55->execute(array('id_clan' => $donnees['id']))or die(print_r($bdd->errorInfo()));
		$v = 0;
		while ($donnees55 = $req55->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
		{
			$video = stripslashes(htmlspecialchars($donnees55['adresse_stockage']));
			if (isset($_GET['id']))
			{
				$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i',
				'<a title="Voir la video"  href="clan.php?watch_video=$1&id='.$_GET['id'].'">
					<p class="petite_videos"> 
						<img style="width:75px;height:55px;" src="http://i1.ytimg.com/vi/$1/default.jpg"  alt="video youtube"/>
					</p>
				</a>',$video);	
				$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
			}
			else
			{
				$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i',
				'<a title="Voir la video"  href="clan.php?watch_video=$1">
					<p class="petite_videos"> 
						<img style="width:75px;height:55px;" src="http://i1.ytimg.com/vi/$1/default.jpg"  alt="video youtube"/>
					</p>
				</a>',$video);	
				$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
			}
			
			echo $video;	
			$v++;	
		 }
		if($v==0)
			echo'<img src="images/pas_video.png"  alt=" Aucune image "/>';
			
		
		if($v>0)
		{
			echo'
			<a href="videos-it'.$donnees['id'].'.html">
				<div class="afficher_tout" title="Afficher toutes les vid�os" style="margin-top:28px;" >
				</div>
			</a>';
		}
	 ?>
    </div> 
		
<?php
	
	if($_SESSION['statut'] == 'membre' OR $_SESSION['statut'] == 'meneur')
	{
		$i=0;
		$requete2 = $bdd->prepare('SELECT pseudo FROM jeu 
									WHERE id_clan=:id_clan')
									or die(print_r($bdd->errorInfo()));
		$requete2->execute(array('id_clan' => $donnees['id']))
									or die(print_r($bdd->errorInfo()));
		while($donnees_nbr = $requete2->fetch())
		{
			$i++;
		}
		
		if($_SESSION['statut'] == 'membre')
		{
			echo'
			<a onclick ="var sup=confirm(\'�tes vous sur de vouloir quitter le Clan?\');
		    if (sup == 0)return false;" href="clan_post.php?quitter&id_clan='.$donnees_clan['id_clan'].'">
				<div id="quitter_clanp3"></div>
			</a>';
		}
		elseif($_SESSION['statut'] == 'meneur' AND $i>1)
		{
			echo'
			<a href="team-nommer.html">
				<div id="quitter_clanp3"></div>
			</a>';
		}
		elseif($_SESSION['statut'] == 'meneur' AND $i==1)
		{
			echo'
			<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer le Clan?\');
		    if (sup == 0)return false;" href="clan_post.php?supprimer&id_clan='.$donnees_clan['id_clan'].'">
				<div id="supprimer_clanp3"></div>
			</a>';
		}
	}
?>
	</div>
<?php

	if (isset($do['nbre']) AND $do['nbre'] == 0 
	AND ($_SESSION['statut'] == 'meneur' 
	OR $_SESSION['statut'] == 'membre'))
	{
		echo'
		<div id="corps_publication2_clanp3" >
			<img src="images/aucune_publication.png" alt=" Il n\'y a aucune publication sur cette page. "/>';
	}
	elseif (isset($donnees_clan['id_clan']) AND $donnees_clan['id_clan'] == 0 
	AND isset($do['nbre']) AND $do['nbre'] ==0)
	{
		echo'
		<div id="corps_publication3_clanp3" >
			<img src="images/pas_membre_publication.png" alt=" Il n\'y a aucune publication sur cette page. Vous pouvez cr�er ou chercher un autre Clan. "/>';
	}	
	elseif (isset($donnees_clan['id_clan']) AND $donnees_clan['id_clan'] != 0 
	AND isset($do['nbre']) AND $do['nbre'] ==0)
	{
		echo'
		<div id="corps_publication3_clanp3" >
			<img src="images/pas_publication_1clan.png" alt=" Il n\'y a aucune publication sur cette page. Vous pouvez cr�er ou chercher un autre Clan. "/>';
	}
	elseif ($_SESSION['statut'] != 'membre' AND $_SESSION['statut'] != 'meneur' 
	AND isset($do['nbre']) AND $do['nbre'] !=0 )
	{
		echo'<div id="corps_publication4_clanp3">';
	}
	else
	{
		echo'<div id="corps_publication_clanp3">';
	}

	if (isset($_GET['page']) AND $_GET['page'] > 0)
	{
		$numero_page = $_GET['page'];
		$numero_page--;
		$numero_page = 15*$numero_page;
	}
	else
	$numero_page = 0;
		
	$req = $bdd->prepare('SELECT *, 
						DATE_FORMAT(date_publication, \'%d/%m/%Y � %Hh %imin \') 
						AS date_publication FROM publication 
						WHERE id_clan=:id_clan 
						ORDER BY id DESC LIMIT '.$numero_page.',15')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $donnees['id']))
						or die(print_r($bdd->errorInfo()));
		
	while ($donnees2 = $req->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
	{
	$r = $bdd->prepare('SELECT nom_de_compte, pseudo, photo_profil,id 
						FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$r->execute(array('id' => $donnees2['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$d = $r->fetch(); 
		echo '
	<div class="bloc_publication_commentaire" id="'.$donnees2['id'].'"> ';
	
		if($_SESSION['statut']=='meneur' 
		OR $donnees2['id_jeu'] == $_SESSION['id_jeu'])
		{
			if(isset($_GET['page']))
			{
				echo'
				<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette publication ?\');
				if (sup == 0)return false;" href="clan.php?id_publication='.$donnees2['id'].'&supprimer_publication&page='.$_GET['page'].'" title="Supprimer la publication">
					<div id="supprimer_membre_clanp3"></div>
				</a>';
			}
			else
			{
				echo'
				<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette publication ?\');
				if (sup == 0)return false;" href="clan.php?id_publication='.$donnees2['id'].'&supprimer_publication" title="Supprimer la publication">
					<div id="supprimer_membre_clanp3"></div>
				</a>';
			}
		}
	
		/*
		GERER LES LIENS SI C'EST UN SOUS COMPTE, LE SOUS COMPTE SUR LEQUEL 
		JE SUIS CO OU UN AUTRE 
		*/
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">';
		elseif($d['id'] == $_SESSION['id_jeu'])
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">';
		else
			echo'<span  title="Mon sous compte">';
			
		if ($d['photo_profil'] != 0)
		{
			$source = getimagesize('images_utilisateurs/'.$d['photo_profil']); // La photo est la source
			
			echo'<div class="centre_image60">';
			if ($source[0] <= 60 AND $source[1] <= 60)						
				echo'<img src="images_utilisateurs/'.$d['photo_profil'].'" alt="Photo de profil"/>';
			else
				echo'<img src="images_utilisateurs/mini_2_'.$d['photo_profil'].'" alt="Photo de profil"/>';
			echo'</div>';
		}
		else
			echo'<img  class="image_publication" src="images/tete3.png" alt="Photo de profil"/>';
			
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'</a>
			<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">
				<span class="pseudo_publication">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</span>
			</a> ';
		}
		elseif($d['id'] == $_SESSION['id_jeu'])
		{
			echo'</a>
			<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">
				<span class="pseudo_publication">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</span>
			</a> ';
		}
		else
		{
			echo'</span>
			<span class="pseudo_publication" title="Mon sous compte" style="color : #102c3c;" >
				'.stripslashes(htmlspecialchars($d['pseudo'])).'
			</span>';
		}

		echo'
		<p class="date_publication">
			le '.$donnees2['date_publication'].'
		</p>';
		
		echo'
		<div class="publication">';
		$message = nl2br(stripslashes(htmlspecialchars($donnees2['message'])));
		$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);	
		$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
		$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
		$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
		if (preg_match('#\[img\](.+)\[/img\]#', $message))
		{
			$message = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message);			
		}
		elseif (preg_match("#http://(.+)#isU", $message))
		{
			$message = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);					
		}
		elseif (preg_match("#https://(.+)#isU", $message))
		{
			$message = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);
		}
		else
		{
			$message = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$message);
		}
		$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
		$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
		$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
		$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
		$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
		$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
		$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
		$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
		$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
		$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);
		echo'<span class="texte_publication">'.$message.'</span>';	
			
			// TEXTE + IMAGE
			if ($donnees2['photo'] != 0 AND $donnees2['photo'] != '')
			{
				$source = getimagesize('images_utilisateurs/'.$donnees2['photo']); // La photo est la source
				if ($source[0] <= 900 AND $source[1] <= 600)
				{
					echo'<p class="img_bloc">';
					echo'	<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donnees2['photo'].'">';
				}
				else
				{
					echo'<p class="img_bloc">';
					echo'	<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$donnees2['photo'].'">';
				}
			
				if ($source[0] <= 300 AND $source[1] <= 300)
				{
					echo'
								<img class="img_publication" src="images_utilisateurs/'.$donnees2['photo'].'" />
							</a>
						</p>';
				}
				else
				{
					echo'
								<img class="img_publication" src="images_utilisateurs/mini_1_'.$donnees2['photo'].'" />
							</a>
						</p>'; 
				}
			}
			
			if (isset($donnees2['video']) AND $donnees2['video'] != '')
			{
				$video = ($donnees2['video']);
				$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
				<object	type="application/x-shockwave-flash" width="500" height="350" data="http://www.youtube.com/v/$1&hl=fr">							
					<param name="wmode" value="transparent" />
					<param name="allowFullScreen" value="true" />
				</object>',$video);
				$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
				echo $video;
			}
			
		echo'</div>';
			
	// affichage j'aime 
	$nbre_jaime = 0;
	$like = 0;
	$tableau_pseudo_jaime = array();
	$tableau_id_jaime = array();
	$re_c = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_publication=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_publi' => $donnees2['id']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch()) 
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 1;
		}
		$re_c2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $donnees_c['id_jeu']))
		or die(print_r($bdd->errorInfo()));	
        $donnees_c2 = $re_c2->fetch();	
		$tableau_pseudo_jaime[$nbre_jaime] = $donnees_c2['pseudo'];
		$tableau_id_jaime[$nbre_jaime] = $donnees_c['id_jeu'];
		$nbre_jaime++; 
	}
		
	if($like == 0)
		echo'<p> <a id="'.$donnees2['id'].'"  class="like">
		<img src="images/pouce.png" title="J\'aime"/></a>';
	else
		echo'<p> <a id="'.$donnees2['id'].'"  class="like">
		<img src="images/pouce_vert.png" title="Je n\'aime plus"/></a>';

	if ($nbre_jaime > 1)
		echo ' (<span id="'.$donnees2['id'].'" class="nbre_jaime">'.$nbre_jaime.'</span>)';
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] != $_SESSION['id_jeu'])
		echo ' (<a href="profil-i'.$tableau_id_jaime[0].'.html">'.$tableau_pseudo_jaime[0].'</a>)';	
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] == $_SESSION['id_jeu'])
		echo '<span> Vous aimez</span>';		
	// fin affichage j'aime					
			
		// COMMENTAIRES	
		if ($_SESSION['statut'] == 'meneur' OR $_SESSION['statut'] == 'membre')
		{			
			echo'<form action="profil_post.php" method="post">';
			echo'<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />';
			echo'<input type="hidden" name="id_publication" value="'.$donnees2['id'].'"/>';
			echo'<input type="hidden" name="profil_ou_clan" value="clan"/>';	
			echo'<input type="hidden" name="id_jeu" value="'.$d['id'].'"/>';
			echo'<input type="hidden" name="id_clan" value="'.$donnees['id'].'"/>';
			if(isset($_GET['page']) AND $_GET['page'] != 0)
			{
				echo '<input type="hidden" name="page" value="'.$_GET['page'].'"/>';
			}
			echo'</form>';
		}
		$reponse = $bdd->prepare('SELECT id, commentaire, id_jeu , 
								DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %Hh %imin \') 
								AS date_commentaire FROM commentaire 
								WHERE id_publication=:id_publication 
								ORDER BY id LIMIT 0,5')
								or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_publication' => $donnees2['id']))
								or die(print_r($bdd->errorInfo()));
		$i=0;	
		while ($donnees3 = $reponse->fetch()) // on affiche commentaire par commentaire
		{
			$i++;
			$rs = $bdd->prepare('SELECT nom_de_compte, pseudo, photo_profil, id 
								FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
			$rs->execute(array('id' => $donnees3['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			$ds = $rs->fetch();
			
			$reqs_p = $bdd->prepare('SELECT id_jeu FROM publication 
									WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
			$reqs_p->execute(array('id' => $donnees2['id']))
									or die(print_r($bdd->errorInfo()));
			$donnees_publi = $reqs_p->fetch();
			
			echo '
			<div class="bloc_commentaire">';
			
			if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
			OR $_SESSION['statut'] == 'meneur' 
			OR $donnees_publi['id_jeu'] == $_SESSION['id_jeu'])
			{
				if(isset($_GET['page']))
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
					if (sup == 0)return false;" href="clan.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'&page='.$_GET['page'].'" title="Supprimer le commentaire">
						<div id="supprimer_commentaire_profil"></div>
					</a>';
				}
				else
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
					if (sup == 0)return false;" href="clan.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'" title="Supprimer le commentaire">
						<div id="supprimer_commentaire_profil"></div>
					</a>';
				}
			}
				
			/* 
				GERER LES LIENS SI C'EST UN SOUS COMPTE, LE SOUS COMPTE SUR 
				LEQUEL JE SUIS CO OU UN AUTRE
			*/
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">';
			elseif($ds['id'] == $_SESSION['id_jeu'])
				echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">';
			else
				echo'<span  title="Mon sous compte">';
				
			if ($ds['photo_profil'] != 0)
			{ 
				$source = getimagesize('images_utilisateurs/'.$ds['photo_profil']); // La photo est la source
				echo'<div class="centre2_image40">';
				if ($source[0] <= 40 AND $source[1] <= 40)
					echo'<img src="images_utilisateurs/'.$ds['photo_profil'].'" />'; 
				else				
					echo'<img src="images_utilisateurs/mini_4_'.$ds['photo_profil'].'"  />';
				echo'</div>';
			}
			else
			{
				echo'<img  class="img_commentaire" src="images/tete2.png" alt="Planete"/>';
			}
			
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'</a>
				<a href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'">
					<span class="pseudo_commentaire">
					'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a> ';
			}
			elseif($ds['id'] == $_SESSION['id_jeu'])
			{
				echo'</a>
				<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">
					<span class="pseudo_commentaire">
						'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a> ';
			}
			else
			{
				echo'</span>
				<span class="pseudo_commentaire" title="Mon sous compte" style="color : #102c3c;">
					'.stripslashes(htmlspecialchars($ds['pseudo'])).'
				</span>';
			}

			echo'
			<p class="date_commentaire">
				le '.$donnees3['date_commentaire'].'
			</p>';
			
			$com = stripslashes(htmlspecialchars($donnees3['commentaire']));			
			if (preg_match("#http://(.+)#isU", $com))
			{
				$com = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);					
			}
			elseif (preg_match("#https://(.+)#isU", $com))
			{
				$com = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);
			}
			else
			{
				$com = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$com);
			}
			
			$com = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $com);		
			$com = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $com);	
			$com = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $com);		
			$com = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $com);
			$com = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $com);
			$com = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $com);
			$com = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $com);
			$com = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $com);
			$com = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $com);
			$com = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $com);				
			echo'<span class="message_commentaire">'.$com.'</span>';
					
			echo '
			</div>';				
		}
		if ($i > 4)
		{
			if(isset($_GET['id'])) // SUR UN AUTRE CLAN !
			{
				if(isset($_GET['page']))
				{
					echo '
					<a href="commentaires.php?id_table='.$donnees2['id'].'&id_clan_get='.$_GET['id'].'&id_clan='.$donnees['id'].'&page_clan='.$_GET['page'].'&lieu_page=clan"> 
						<div class="tous_commentaires"></div>
					</a>';
				}
				else
				{
					echo '
					<a href="commentaires.php?id_table='.$donnees2['id'].'&id_clan_get='.$_GET['id'].'&id_clan='.$donnees['id'].'&lieu_page=clan"> 
						<div class="tous_commentaires"></div>
					</a>';
				}
			}
			else // SUR SON CLAN
			{
				if(isset($_GET['page']))
				{
					echo '
					<a href="commentaires.php?id_table='.$donnees2['id'].'&id_clan='.$donnees['id'].'&page_clan='.$_GET['page'].'&lieu_page=clan"> 
						<div class="tous_commentaires"></div>
					</a>';
				}
				else
				{
					echo '
					<a href="commentaires.php?id_table='.$donnees2['id'].'&id_clan='.$donnees['id'].'&lieu_page=clan"> 
						<div class="tous_commentaires"></div>
					</a>';
				}			
			}
		}
			
	echo '
	</div>';

	}
	// GESTION AFFICHAGE PAGES

	if(isset($_GET['id']))
	{
		$nbre_page = 1;
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi 
							FROM publication WHERE id_clan=:id_clan')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('id_clan' =>$_GET['id']))
							or die(print_r($bdd->errorInfo()));
		$do = $p->fetch();
		$nbr_entrees = $do['nbre_publi'];
		$p->closeCursor();
	}
	else
	{
		$nbre_page = 1;
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM publication 
							WHERE id_clan=:id_clan')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('id_clan' =>$donnees['id']))
							or die(print_r($bdd->errorInfo()));
		$do = $p->fetch();
		$nbr_entrees = $do['nbre_publi'];
		$p->closeCursor();
	}

	if (isset ($_GET['page']))
		$current_page = $_GET['page'];
	else
		$current_page = 1;
		
	// DEFINIR LES VARIABLES AVANT LE INCLUDE///////////////////////////////////	
	$nom_page = 'team';
	$nbr_affichage = 15;

	include('pagination.php');
?>
	</div>
	<!-- 
		NECESSAIRE POUR GARDER LE PIED DE PAGE EN BAS A CAUSE DU 
		FLOAT: RIGHT et FLOAT: LEFT
	-->
	<div id="corps_invisible"></div> 
</div>
<script>
////////////////////////////////////// Pour le bouton j'aime
var jaime = document.getElementsByTagName('a'),
jaimeTaille = jaime.length;
		for (var i = 0 ; i < jaimeTaille ; i++) { 	
			if (jaime[i] && jaime[i].className == 'like')
			{
				jaime[i].onclick = function() { 
				var id_publi = this.id;
				var jaime2 = this;
				// On lance la requ�te ajax
				$.get('like.php?id_publication='+id_publi, function(data) { 
				if (data == 1)
					jaime2.innerHTML = '<img src="images/pouce_vert.png" title="Je n\'aime plus"/>';
				else
					jaime2.innerHTML = '<img src="images/pouce.png" title="J\'aime"/>';
					
				});				
					return false; // on bloque la redirection
				};
			}
		}
var span = document.getElementsByTagName('span'),
spanTaille = span.length;
		for (var i = 0 ; i < jaimeTaille ; i++) {
			if (span[i] && span[i].className == 'nbre_jaime')
			{
				span[i].onclick = function() {
				var href_publi = this.id;
				var jaime3 = this;
				// On lance la requ�te ajax
				$.getJSON('like.php?nbre_jaime='+href_publi, function(data) { 
					jaime3.innerHTML = data['message'];
				});				
				};
			}
		}
//------------------------------------------------------------------------------
var publie = document.getElementById('publier');
var publie2 = document.getElementById('publier2');
var publie3 = document.getElementById('publier3');
var publie4 = document.getElementById('publier4');
var publie5 = document.getElementById('publier5');
if (publie)
{
	publie.onclick = function() {
	publie.style.display = 'none';
	};
}
if (publie2)
{
	publie2.onclick = function() {
	publie2.style.display = 'none';
};
}
if (publie3)
{
	publie3.onclick = function() {
	publie3.style.display = 'none';
	};
}
if (publie4)
{
	publie4.onclick = function() {
	publie4.style.display = 'none';
	};
}
if (publie5)
{
	publie5.onclick = function() {
	publie5.style.display = 'none';
	};
}
var publier = document.getElementById('publier');
	publier.onclick = function() {
    publier.style.display = 'none';
	};
	function displayfilename(input)
	{
		filename = input.value;
		filename = filename.substring(filename.lastIndexOf('\\')+1); // Windows path
		filename = filename.substring(filename.lastIndexOf('/')+1); // Linux path
		document.getElementById('there').innerHTML = filename;
	}
		
	var links = document.getElementsByTagName('a'),
		linksLen = links.length;
	for (var i = 0 ; i < linksLen ; i++) {
		if (links[i].title == 'Afficher l\'image originale')
		{
			links[i].onclick = function() { 
				displayImg(this); 
				return false; // on bloque la redirection
			};
		}
	}
function displayImg(link) {

    var img = new Image(),
        overlay = document.getElementById('overlay');
        profil = document.getElementById('contient_image_profil');
    img.onload = function() {
        profil.innerHTML = '';
        profil.appendChild(img);
    };
    img.src = link.href;
    overlay.style.display = 'block';
    profil.style.display = 'block';
    profil.innerHTML = '<span>Chargement en cours...</span>';
}

document.getElementById('overlay').onclick = function() {
    this.style.display = 'none';
    profil.style.display = 'none';
	
};
	
var info = document.getElementById('description_clanp3');
var changer_info_guilde = document.getElementById('changer_info_guilde');
var rejoindre = document.getElementById('rejoindre');
if (changer_info_guilde)
{
		info.addEventListener('mouseover', function () 
		{
		changer_info_guilde.style.display = 'block';
		},false);
		info.addEventListener('mouseout', function () 
		{
		changer_info_guilde.style.display = 'none';
		},false);
		changer_info_guilde.addEventListener('mouseover', function () 
		{
		changer_info_guilde.style.display = 'block';
		},false);
		
var overlay1_clan = document.getElementById('overlay1_clan');
var grand_overlay1 = document.getElementById('grand_overlay1');
	changer_info_guilde.onclick =  function()
	{
	overlay1_clan.style.display = 'block';
	grand_overlay1.style.display = 'block';
        return false; // on bloque la redirection
	};
document.getElementById('fermer4').onclick = function() {
    overlay1_clan.style.display = 'none';
	grand_overlay1.style.display = 'none';
	return false; // on bloque la redirection
	};
}

if (rejoindre)
{
var overlay2_clan = document.getElementById('overlay2_clan');
var grand_overlay2 = document.getElementById('grand_overlay2');
	rejoindre.onclick =  function()
	{
	overlay2_clan.style.display = 'block';
	grand_overlay2.style.display = 'block';
        return false; // on bloque la redirection
	};
document.getElementById('fermer2').onclick = function() {
    overlay2_clan.style.display = 'none';
	grand_overlay2.style.display = 'none';
	return false; // on bloque la redirection
	};
}
var formulaire = document.getElementById('formulaire');
var contient_image_clanp3 = document.getElementById('contient_image_clanp3');
var changer_image_clanp3 = document.getElementById('changer_image_clanp3');
var overlay3_clan = document.getElementById('overlay3_clan');
var grand_overlay3 = document.getElementById('grand_overlay3');
if (changer_image_clanp3)
{
		contient_image_clanp3.addEventListener('mouseover', function () 
		{
		changer_image_clanp3.style.display = 'block';
		},false);
		contient_image_clanp3.addEventListener('mouseout', function () 
		{
		changer_image_clanp3.style.display = 'none';
		},false);
		
		changer_image_clanp3.addEventListener('click', function () 
		{

			overlay3_clan.style.display = 'block';
			grand_overlay3.style.display = 'block';  
		},false);
	document.getElementById('fermer3').onclick = function() {
	overlay3_clan.style.display = 'none';
	grand_overlay3.style.display = 'none';
	return false; // on bloque la redirection
	};
}	

</script>
<?php
}
else // CREER OU CHERCHER UN CLAN = CLAN PAGE 1 ////////////////////////////////
{
?>
	<div id="corps_clanp1">
	
		<div class="contient_liens">
			<a href="team-creer.html">
				<div id="p1creer_clan"></div>
			</a>
		</div>

	    <div class="contient_liens">
			<a href="recherche.html?nom&recherche=">
				<div id="p1chercher_clan"></div>
			</a>
		</div>
		
	</div>
<?php
}
include('pied_page.php');
?>