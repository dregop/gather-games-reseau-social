var destinataire = document.getElementById('destinataire');
var resultats = document.getElementById('resultats');
var mot_destinataire, reponse;
var selection = -1;
destinataire.addEventListener('keyup', function (event) 
{
	mot_destinataire = destinataire.value;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'ajax.php?destinataire=' + mot_destinataire);
	xhr.onreadystatechange = function(e) // On g�re ici une requ�te asynchrone
	{ 
		resultats.innerHTML = '';
        if (xhr.readyState == 4 && xhr.status == 200) // Si le fichier est charg� sans erreur
		{ 
            reponse = JSON.parse(xhr.responseText);
			if (reponse != 'nul')
			{
				for (var id in reponse)
				{
					div = document.createElement('div');
						var img = document.createElement('img');
						img.src = 'images_utilisateurs/mini_2_' + reponse[id] + '.jpg';
						div.appendChild(img);
						div.innerHTML += id;
					resultats.appendChild(div);
				}
			}				
        }
		else if(xhr.readyState == 4 && xhr.status != 200)
		{
			alert('Une erreur est survenue !\n\nCode :' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
    }
	xhr.send(null);
},false);
resultats.addEventListener('click', function (event)
{
	texte = event.target.textContent;
	destinataire.value = texte;
},false);
document.addEventListener('click', function (event)
{
	resultats.innerHTML = '';
},false);
