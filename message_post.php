<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
include('fonctions.php');

if (isset($_POST['destinataire']) AND isset($_POST['message']) 
AND $_POST['message'] != '' AND verifAmi($_POST['destinataire']))
{
	// on insert le message dans la bdd
	$req = $bdd->prepare('INSERT INTO message(id_jeu,destinataire,message,
						date_message,view) 
						VALUES(:id_jeu,:destinataire,:message,NOW(),:view)') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'destinataire' => $_POST['destinataire'],
						'message' => $_POST['message'],
						'view' => 1)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	header('Location: profil-i'.$_POST['destinataire'].'-send.html');
}
elseif (isset($_POST['destinataire2']) AND isset($_POST['message2']) 
AND $_POST['message2'] != '' AND verifAmi($_POST['destinataire2']))
{
	// on insert le message dans la bdd
	$req = $bdd->prepare('INSERT INTO message(id_jeu,destinataire,message,
						date_message,view) 
						VALUES(:id_jeu,:destinataire,:message,NOW(),:view)')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'destinataire' => $_POST['destinataire2'],
						'message' => $_POST['message2'],
						'view' => 1)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	header('Location: message-send.html');
}
elseif(isset($_POST['message']) AND $_POST['message'] == '') //OBLIGE DE FAIRE COMME CA SINON BUG AVEC L'AUTRE PAGE MESSAGE
{
	header('Location: profil-i'.$_POST['destinataire'].'-erreur.html');
}
elseif(isset($_POST['message2']) AND $_POST['message2'] == '')
{
	header('Location: message-erreur.html');
}

