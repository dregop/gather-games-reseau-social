<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (isset($_GET['ami_id_jeu']))
{
	// on met un message de demande d'amiti�
	$req = $bdd->prepare('INSERT INTO demande_amis(id_jeu_demandeur, id_jeu,view) 
						VALUES(:id_jeu_demandeur,:id_jeu,:view)') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu_demandeur' => $_SESSION['id_jeu'],
						'id_jeu' => $_GET['ami_id_jeu'],
						'view' => 1)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	header('Location: profil-i'.$_GET['ami_id_jeu'].'.html');
}
include('menu.php');
?>
    <div id="corps_recherche">
		<img id="intro_recherche" src="images/intro_recherche.png" 
			alt="Recherche. Trouvez le profil de vos amis, de vos joueurs 
			pr�f�r�s mais aussi le clan de vos r�ves !" />		
	  
	  <div id="sous_corps_recherche">
<?php
		if(!isset($_GET['gamers']) AND !isset($_GET['nom']) AND !isset($_GET['jeu']))
		{
			if (isset($_POST['recherche']))
				$_GET['recherche'] = $_POST['recherche'];
				
				echo'
				<a href="recherche.html?recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="pseudo2_recherche"></div>
				</a>';
				echo'
				<a href="recherche.html?gamers&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="joueur_jeu_recherche"></div>
				</a> ';
				echo'
				<a href="recherche.html?nom&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="nom_recherche"></div>
				</a>';
				echo'
				<a href="recherche.html?jeu&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="clan_jeu_recherche"></div>
				</a>';
		}
		else
		{
			echo'
			<a href="recherche.html?recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
				<div id="pseudo_recherche"></div>
			</a>';
			
			if(isset($_GET['gamers']))
			{
				echo'
				<a href="recherche.html?gamers&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="joueur_jeu2_recherche"></div>
				</a> ';
			}
			else
			{
				echo'
				<a href="recherche.html?gamers&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="joueur_jeu_recherche"></div>
				</a> ';
			}
			if(isset($_GET['nom']))	
			{
				echo'
				<a href="recherche.html?nom&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="nom2_recherche"></div>
				</a>';
			}
			else
			{
				echo'
				<a href="recherche.html?nom&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="nom_recherche"></div>
				</a>';
			}
			if(isset($_GET['jeu']))
			{			
				echo'
				<a href="recherche.html?jeu&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="clan_jeu2_recherche"></div>
				</a>';
			}
			else
				echo'
				<a href="recherche.html?jeu&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'">
					<div id="clan_jeu_recherche"></div>
				</a>';
		}
?>
	  </div>	
<?php
if (isset($_POST['recherche']))
	$recherche = $_POST['recherche'];
elseif (isset( $_GET['recherche']))
	$recherche = $_GET['recherche'];
else
	$recherche = '';

	
if (!isset($_GET['gamers']) AND !isset($_GET['nom']) 
AND !isset($_GET['jeu']))
{
	if (isset($_GET['page']))
	{
		$numero_page = $_GET['page'];
		$numero_page--;
		$numero_page = 20*$numero_page;
	}
	else
		$numero_page = 0;
			
	$resu = $bdd->prepare('SELECT COUNT(*) AS resultats FROM jeu
									WHERE pseudo LIKE :recherche')
						or die(print_r($bdd->errorInfo()));
	$resu->execute(array('recherche' => $recherche.'%'))
						or die(print_r($bdd->errorInfo()));		
	$resultat = $resu->fetch();		
	
	$repo = $bdd->prepare('SELECT * FROM jeu WHERE pseudo LIKE :recherche 
						ORDER BY pseudo LIMIT '.$numero_page.',20')
						or die(print_r($bdd->errorInfo()));
	$repo->execute(array('recherche' => $recherche.'%'))
						or die(print_r($bdd->errorInfo()));
?>
	<div id="sous_corps2_recherche">
		<h2>Recherche les joueurs par pseudo :</h2>
<?php
		while ($donnees = $repo->fetch()) // on affiche message par message
		{
			// ON AFFICHE LE PSEUDO
			if ($donnees['id'] == $_SESSION['id_jeu'] || 
			$donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
			{
				echo'
				<div class="bloc_affichage_recherche2">';
					echo'
					<span class="pseudo_recherche2">
						'.stripslashes(htmlspecialchars($donnees['pseudo'])).'
					</span>';
			}
			else
			{
				echo'
				<div class="bloc_affichage_recherche">';
					echo'
					<a href="profil-i'.$donnees['id'].'.html">
						<span class="pseudo_recherche">
							'.stripslashes(htmlspecialchars($donnees['pseudo'])).'
						</span>
					</a>';
			}
			
			// SAVOIR SI IL EST AMI, SI C'EST SON SOUS COMPTE, SI EN ATTENTE ETC
			
			echo'
			<div class="bloc_ajouter_recherche">';
			
			$re = $bdd->query('SELECT * FROM amis')or die(print_r($bdd->errorInfo())); 
			while ($d = $re->fetch())
			{
				$nul =0;
				if ($d['id_jeu'] == $donnees['id'])
				{
					if ($_SESSION['id_jeu'] == $d['id_jeu_ami'])
					{
						echo '<p class="ami_recherche">Ami(e)</p>';
						$nul = 1;
						break;
					}
				}
				if ($d['id_jeu_ami'] == $donnees['id'])
				{
					if ($_SESSION['id_jeu'] == $d['id_jeu'])
					{
						echo '<p class="ami_recherche">Ami(e)</p>';
						$nul = 1;
						break;
					}
				}
			
			}
			$attente=1;
			$rep = $bdd->prepare('SELECT id_jeu FROM demande_amis 
								WHERE id_jeu_demandeur=:id_jeu')
								or die(print_r($bdd->errorInfo()));
			$rep->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			while ($donnees_d = $rep->fetch())
			{	
				if ($donnees_d['id_jeu'] == $donnees['id'])
				{
					echo '<p class="attente_recherche">En attente</p>';
					$attente = 0;
					break;
				}
			}
			$repz = $bdd->prepare('SELECT id_jeu_demandeur FROM demande_amis 
									WHERE id_jeu=:id_jeu')or die(print_r($bdd->errorInfo()));
			$repz->execute(array('id_jeu' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo())); // je s�lectionne les gens �
			while ($donnees_z = $repz->fetch())
			{
				if ($donnees_z['id_jeu_demandeur'] == $donnees['id'])
				{
					echo'<p class="attente_recherche">En attente</p>';
					
					$attente = 0;
					break;
				}
			}
			if ($donnees['id'] == $_SESSION['id_jeu'] || 
			$donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
			{
				echo '<p class="mon_recherche">Mon sous compte</p>';
			}
			elseif (isset($attente) AND $attente == 1)
			{
				if (isset($nul) AND $nul == 1)
					echo '';
				else
				{
					echo'
					<a href="recherche.php?ami_id_jeu='.$donnees['id'].'">
						<div class="ajouter_recherche" title=" Ajouter comme ami(e) " /></div>
					</a>';
				}
			}
			$re->closeCursor(); // Termine le traitement de la requ�te
			
			echo'
			</div>';
				
			//ON AFFICHE LES PHOTO DE PROFIL
			if (isset($donnees['photo_profil']) 
			AND $donnees['photo_profil'] != '' AND $donnees['photo_profil'] != 0
			AND $donnees['id'] == $_SESSION['id_jeu'] || 
			$donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
			{   
				$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); // La photo est la source
				echo'<div class="centre_image_bloc_recherche">';
				if ($source[0] <= 60 AND $source[1] <= 60)
				{
					echo'
					<a href="profil-i'.$donnees['id'].'.html">
						<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />
					</a>';
				}
				else
				{
					echo'
					<a href="profil-i'.$donnees['id'].'.html">
						<img src="images_utilisateurs/mini_2_'.$donnees['photo_profil'].'" alt="Photo de profil" />
					</a>';
				}
				echo'</div>';
			}
			elseif (empty($donnees['photo_profil'])
			AND $donnees['id'] == $_SESSION['id_jeu'] || 
			$donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
			{   
				echo'<img class="photo_profil_recherche" src="images/tete3.png" alt="Photo de profil" />';
			}
			elseif(isset($donnees['photo_profil']) 
			AND $donnees['photo_profil'] != '' 
			AND $donnees['photo_profil'] != 0)
			{	
				$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); // La photo est la source
				echo'<div class="centre_image_bloc_recherche">';
					if ($source[0] <= 60 AND $source[1] <= 60)
					{
						echo'
						<a href="profil-i'.$donnees['id'].'.html">
						<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />
						</a>';
					}
					else
					{
						echo'
						<a href="profil-i'.$donnees['id'].'.html">
						<img src="images_utilisateurs/mini_2_'.$donnees['photo_profil'].'" alt="Photo de profil" />
						</a>';
					}
				echo'</div>';
			}
			elseif (empty($donnees['photo_profil']))
			{   
				echo'
				<a href="profil-i'.$donnees['id'].'.html">
					<img class="photo_profil_recherche" src="images/tete3.png" alt="Photo de profil"/>
				</a>';
			}
				
			echo'
			<p class="info_joueur_recherche">';
				
			if(strlen($donnees['nom_jeu']) > 14)
			{
				echo'
				Jeu :
				<span style="font-size:0.7em;">
					'.substr(stripslashes(htmlspecialchars($donnees['nom_jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnees['nom_jeu']))) > 20)
					{ echo'...';}
				echo'
				</span>
				<br />';
			}
			else
			{
				echo'
				Jeu : 
				'.substr(stripslashes(htmlspecialchars($donnees['nom_jeu'])), 0, 20).'';
				if(strlen(stripslashes(htmlspecialchars($donnees['nom_jeu']))) > 20)
				{ echo'...';}
				echo'
				<br />';
			}	
			
			echo'Platerforme : '.$donnees['plateforme'].'
			
			</p>';
							
			echo'</div>';
			
		}
		
	if ($resultat == 0)
	{
		echo'
		<p class="aucun_resultat" >
			Aucun joueur n\'a �t� trouv�.<br /> Essayez une autre recherche.
		</p>';
	}
	else
	{
		echo'
		<p class="resultat">
			'.$resultat['resultats'].' r�sultat';if($resultat>1){echo's';}
		echo'
		<p>';
	}
	echo'</div>';
	$resu->closeCursor();
}
elseif(isset($_GET['gamers']))
{
	if (isset($_GET['page']))
	{
		$numero_page = $_GET['page'];
		$numero_page--;
		$numero_page = 20*$numero_page;
	}
	else
		$numero_page = 0;
?>
<?php
	$resu = $bdd->prepare('SELECT COUNT(*) AS resultats FROM jeu
									WHERE nom_jeu LIKE :recherche')
						or die(print_r($bdd->errorInfo()));
	$resu->execute(array('recherche' => $recherche.'%'))
						or die(print_r($bdd->errorInfo()));		
	$resultat = $resu->fetch();
	
		$repo = $bdd->prepare('SELECT * FROM jeu WHERE nom_jeu LIKE :recherche 
							ORDER BY nom_jeu LIMIT '.$numero_page.',20')
							or die(print_r($bdd->errorInfo()));
		$repo->execute(array('recherche' => $recherche.'%'))
							or die(print_r($bdd->errorInfo()));
?>
	
	<div id="sous_corps2_recherche">
		<h2>Recherche les joueurs par jeu :</h2>
<?php
		while ($donnees = $repo->fetch()) // on affiche message par message
		{
			// ON AFFICHE LE PSEUDO
			if ($donnees['id'] == $_SESSION['id_jeu'] || 
			$donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
			{
				echo'
				<div class="bloc_affichage_recherche2">';
					echo'
					<span class="pseudo_recherche2">
						'.stripslashes(htmlspecialchars($donnees['pseudo'])).'
					</span>';
			}
			else
			{
				echo'
				<div class="bloc_affichage_recherche">';
					echo'
					<a href="profil-i'.$donnees['id'].'.html">
						<span class="pseudo_recherche">
							'.stripslashes(htmlspecialchars($donnees['pseudo'])).'
						</span>
					</a>';
			}
			// SAVOIR SI IL EST AMI, SI C'EST SON SOUS COMPTE, SI EN ATTENTE ETC	
			echo'
			<div class="bloc_ajouter_recherche">';
			
			$re = $bdd->query('SELECT * FROM amis')or die(print_r($bdd->errorInfo())); 
			while ($d = $re->fetch())
			{
				$nul =0;
				if ($d['id_jeu'] == $donnees['id'])
				{
					if ($_SESSION['id_jeu'] == $d['id_jeu_ami'])
					{
						echo '<p class="ami_recherche">Ami(e)</p>';
						$nul = 1;
						break;
					}
				}
				if ($d['id_jeu_ami'] == $donnees['id'])
				{
					if ($_SESSION['id_jeu'] == $d['id_jeu'])
					{
						echo '<p class="ami_recherche">Ami(e)</p>';
						$nul = 1;
						break;
					}
				}
			
			}
			$attente=1;
			$rep = $bdd->prepare('SELECT id_jeu FROM demande_amis 
								WHERE id_jeu_demandeur=:id_jeu')
								or die(print_r($bdd->errorInfo()));
			$rep->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			while ($donnees_d = $rep->fetch())
			{	
				if ($donnees_d['id_jeu'] == $donnees['id'])
				{
					echo '<p class="attente_recherche">En attente</p>';
					$attente = 0;
					break;
				}
			}
			$repz = $bdd->prepare('SELECT id_jeu_demandeur 
									FROM demande_amis WHERE id_jeu=:id_jeu')
									or die(print_r($bdd->errorInfo()));
			$repz->execute(array('id_jeu' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo()));
			while ($donnees_z = $repz->fetch())
			{
				if ($donnees_z['id_jeu_demandeur'] == $donnees['id'])
				{
					echo '<p class="attente_recherche">En attente</p>';
					$attente = 0;
					break;
				}
			}
			if ($donnees['id'] == $_SESSION['id_jeu'] || 
			$donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
			{
				echo '<p class="mon_recherche">Mon sous compte</p>';
			}
			elseif (isset($attente) AND $attente == 1)
			{
				if (isset($nul) AND $nul == 1)
					echo '';
				else
				{
					echo'
					<a href="recherche.php?ami_id_jeu='.$donnees['id'].'">
						<div class="ajouter_recherche" title=" Ajouter comme ami(e) "/></div>
					</a>';
				}
			}
			$re->closeCursor(); // Termine le traitement de la requ�te
		
			echo'
			</div>';
				
			//ON AFFICHE LES PHOTO DE PROFIL
			    if (isset($donnees['photo_profil']) 
				AND $donnees['photo_profil'] != '' AND $donnees['photo_profil'] != 0
				AND $donnees['id'] == $_SESSION['id_jeu'] ||
				$donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
				{   
					$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); // La photo est la source
					echo'<div class="centre_image_bloc_recherche">';
					if ($source[0] <= 60 AND $source[1] <= 60)
					{
						echo'
						<a href="profil-i'.$donnees['id'].'.html">
							<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />
						</a>';
					}
					else
					{
						echo'
						<a href="profil-i'.$donnees['id'].'.html">
							<img src="images_utilisateurs/mini_2_'.$donnees['photo_profil'].'" alt="Photo de profil" />
						</a>';
					}
					echo'</div>';
				}
				elseif (empty($donnees['photo_profil'])
				AND $donnees['id'] == $_SESSION['id_jeu'] 
				OR $donnees['nom_de_compte'] == $_SESSION['nom_de_compte'])
				{   
					echo'<img class="photo_profil_recherche" src="images/tete3.png" alt="Photo de profil" />';
				}
				elseif(isset($donnees['photo_profil']) 
				AND $donnees['photo_profil'] != '' 
				AND $donnees['photo_profil'] != 0)
				{	
					$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); // La photo est la source
					echo'<div class="centre_image_bloc_recherche">';
						if ($source[0] <= 60 AND $source[1] <= 60)
						{
							echo'
							<a href="profil-i'.$donnees['id'].'.html">
								<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />
							</a>';
						}
						else
						{
							echo'
							<a href="profil-i'.$donnees['id'].'.html">
								<img src="images_utilisateurs/mini_2_'.$donnees['photo_profil'].'" alt="Photo de profil" />
							</a>';
						}
					echo'</div>';
				}
				elseif (empty($donnees['photo_profil']))
				{   
					echo'
					<a href="profil-i'.$donnees['id'].'.html">
						<img class="photo_profil_recherche" src="images/tete3.png" alt="Photo de profil" />
					</a>';
				}
				
				echo'
				<p class="info_joueur_recherche">';
	
				if(strlen($donnees['nom_jeu']) > 14)
				{
					echo'
					Jeu : 
					<span style="font-size:0.7em;">
						'.substr(stripslashes(htmlspecialchars($donnees['nom_jeu'])), 0, 20).'';
						if(strlen(stripslashes(htmlspecialchars($donnees['nom_jeu']))) > 20)
						{ echo'...';}
					echo'
					</span>
					<br />';
				}
				else
				{
					echo'
					Jeu : 
					'.substr(stripslashes(htmlspecialchars($donnees['nom_jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnees['nom_jeu']))) > 20)
					{ echo'...';}
					echo'
					<br />';
				}	
						
				echo'Platerforme : '.$donnees['plateforme'].'
				</p>';
			echo'</div>';
		}	
	if ( $resultat == 0)
	{
		echo'
		<p class="aucun_resultat" >
			Aucun joueur n\'a �t� trouv�.<br /> Essayez une autre recherche.
		</p>';
	}
	else
	{
		echo'
		<p class="resultat">
			'.$resultat['resultats'].' r�sultat';if($resultat>1){echo's';}
		echo'
		<p>';
	}
	$resu->closeCursor();
?>
	</div>

<?php
}
elseif(isset($_GET['nom']))	
{
	if (isset($_GET['page']))
	{
		$numero_page = $_GET['page'];
		$numero_page--;
		$numero_page = 20*$numero_page;
	}
	else
		$numero_page = 0;
// ------------------------------- CLAN ----------------------------------------
/*
	CHOPPER L'ID CLAN DU GAS CONNECTE POUR NE PAS RENVOYER PAR L'URL l'ID DU CLAN 
	SI IL EN FAIT PARTI
*/
		$reqs3 = $bdd->prepare('SELECT id_clan FROM jeu 
								WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$reqs3->execute(array('id' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
		$session_idclan = $reqs3->fetch();	
?>
	 <div id="sous_corps3_recherche">
		<h2>Recherche les Clans par nom : </h2>	
<?php
	$resu = $bdd->prepare('SELECT COUNT(*) AS resultats FROM clan
									WHERE nom_clan LIKE :recherche')
						or die(print_r($bdd->errorInfo()));
	$resu->execute(array('recherche' => $recherche.'%'))
						or die(print_r($bdd->errorInfo()));		
	$resultat = $resu->fetch();
	
		$repo = $bdd->prepare('SELECT * FROM clan 
								WHERE nom_clan 
								LIKE :recherche 
								ORDER BY nom_clan 
								LIMIT '.$numero_page.',20')
								or die(print_r($bdd->errorInfo()));
		$repo->execute(array('recherche' => $recherche.'%'))
								or die(print_r($bdd->errorInfo()));
		while ($donnees = $repo->fetch()) // on affiche message par message
		{
			echo'<div class="bloc_affichage2_recherche">';
			
			if($session_idclan['id_clan'] != $donnees['id'])
			{
				echo'
				<a href="team-i'.$donnees['id'].'.html">
					<span class="nom_clan_recherche">
						<img class="embleme_clan_recherche" src="images/embleme_clan_recherche.png" alt=" "/>
						<span class="nom2_clan_recherche">
							'.stripslashes(htmlspecialchars($donnees['nom_clan'])).'
						</span>
					</span>
				</a>';
			}
			else
			{
				echo'
				<a href="team.html">
					<span class="nom_clan_recherche">
						<img class="embleme_clan_recherche" src="images/embleme_clan_recherche.png" alt=" "/>
						<span class="nom2_clan_recherche">
							'.stripslashes(htmlspecialchars($donnees['nom_clan'])).'
						</span>
					</span>
				</a>';
			}
			if (isset($donnees['photo_clan']) AND $donnees['photo_clan'] != '' 
			AND $donnees['photo_clan'] != 0)
			{	
				echo'
				<div class="centre_image2_bloc_recherche">';
				$source = getimagesize('images_utilisateurs/'.$donnees['photo_clan']); // La photo est la source
				if ($source[0] <= 120 AND $source[1] <= 60)
				{
					if($session_idclan['id_clan'] != $donnees['id']) 			// SAVOIR SI ON ENVOIT L'ID CLAN PAR URL OU PAS
						echo'<a href="team-i'.$donnees['id'].'.html">';
					else
						echo'<a href="team.html">';
						
					echo'<img src="images_utilisateurs/'.$donnees['photo_clan'].'" alt="Photo de Clan" />
							</a>';
				}
				else
				{
					if($session_idclan['id_clan'] != $donnees['id'])
						echo'<a href="team-i'.$donnees['id'].'.html">';
					else
						echo'<a href="team.html">';
						
					echo'<img src="images_utilisateurs/clan_3_'.$donnees['photo_clan'].'" alt="Photo de Clan" />
							</a>';
				}
				echo'</div>';
			}
			else
			{
				if($session_idclan['id_clan'] != $donnees['id'])
					echo'<a href="team-i'.$donnees['id'].'.html">';
				else
					echo'<a href="team.html">';
					
				echo'<img class="defaut_clan2" src="images/defaut_clan2.png" alt="Photo de Clan"/>
						</a>';
			}
			echo'<p class="info_clan_recherche">';
				if(strlen($donnees['jeu']) > 15)
				{
					echo'
					Jeu : 
					<span style="font-size:0.7em;">
						'.substr(stripslashes(htmlspecialchars($donnees['jeu'])), 0, 20).'';
						if(strlen(stripslashes(htmlspecialchars($donnees['jeu']))) > 20)
						{ echo'...';}
					echo'
					</span>
					<br />';
				}
				else
				{
					echo'
					Jeu : 
					'.substr(stripslashes(htmlspecialchars($donnees['jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnees['jeu']))) > 20)
					{ echo'...';}
					echo'
					<br />';
				}		
					echo'Platerforme : '.$donnees['plateforme'].'
				 </p>';
			echo'</div>';
		}
	if ( $resultat == 0)
	{
		echo'
		<p class="aucun_resultat" >
			Aucun Clan n\'a �t� trouv�.<br /> Essayez une autre recherche.
		</p>';
	}
	else
	{
		echo'
		<p class="resultat">
			'.$resultat['resultats'].' r�sultat';if($resultat>1){echo's';}
		echo'
		<p>';
	}
	$resu->closeCursor();
}
elseif(isset($_GET['jeu']))	
{
	if (isset($_GET['page']))
	{
		$numero_page = $_GET['page'];
		$numero_page--;
		$numero_page = 20*$numero_page;
	}
else
$numero_page = 0;
// ------------------------------- CLAN ----------------------------------------
	$resu = $bdd->prepare('SELECT COUNT(*) AS resultats FROM clan
									WHERE jeu LIKE :recherche')
						or die(print_r($bdd->errorInfo()));
	$resu->execute(array('recherche' => $recherche.'%'))
						or die(print_r($bdd->errorInfo()));		
	$resultat = $resu->fetch();
	
$repo = $bdd->prepare('SELECT * FROM clan WHERE jeu LIKE :recherche 
					ORDER BY jeu LIMIT '.$numero_page.',20')
					or die(print_r($bdd->errorInfo()));
$repo->execute(array('recherche' => $recherche.'%'))
					or die(print_r($bdd->errorInfo()));
?>
	<div id="sous_corps3_recherche">
		<h2>Recherche les Clans par jeu : </h2>	
<?php
/*
		CHOPPER L'ID CLAN DU GAS CONNECTE POUR NE PAS RENVOYER PAR L'URL l'ID 
		DU CLAN SI IL EN FAIT PARTI 
*/
		$reqs3 = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$reqs3->execute(array('id' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
		$session_idclan = $reqs3->fetch();
		while ($donnees = $repo->fetch()) // on affiche message par message
		{
			echo'<div class="bloc_affichage2_recherche">';
			
			if($session_idclan['id_clan'] != $donnees['id'])
			{
				echo'
				<a href="team-i'.$donnees['id'].'.html">
					<span class="nom_clan_recherche">
						<img class="embleme_clan_recherche" src="images/embleme_clan_recherche.png" alt=" "/>
						<span class="nom2_clan_recherche">
							'.stripslashes(htmlspecialchars($donnees['nom_clan'])).'
						</span>
					</span>
				</a>';
			}
			else
			{
				echo'
				<a href="team.html">
					<span class="nom_clan_recherche">
						<img class="embleme_clan_recherche" src="images/embleme_clan_recherche.png" alt=" "/>
						<span class="nom2_clan_recherche">
							'.stripslashes(htmlspecialchars($donnees['nom_clan'])).'
						</span>
					</span>
				</a>';
			}
			
			if (isset($donnees['photo_clan']) AND $donnees['photo_clan'] != '' 
			AND $donnees['photo_clan'] != 0)
			{	
				echo'<div class="centre_image2_bloc_recherche">';
						$source = getimagesize('images_utilisateurs/'.$donnees['photo_clan']); // La photo est la source
						if ($source[0] <= 120 AND $source[1] <= 60)
						{
							if($session_idclan['id_clan'] != $donnees['id']) 	// SAVOIR SI ON ENVOIT L'ID CLAN PAR URL OU PAS
								echo'<a href="team-i'.$donnees['id'].'.html">';
							else
								echo'<a href="team.html">';
								
							echo'<img src="images_utilisateurs/'.$donnees['photo_clan'].'" alt="Photo de Clan" />
									</a>';
						}
						else
						{
							if($session_idclan['id_clan'] != $donnees['id'])
								echo'<a href="team-i'.$donnees['id'].'.html">';
							else
								echo'<a href="team.html">';
								
							echo'<img src="images_utilisateurs/clan_3_'.$donnees['photo_clan'].'" alt="Photo de Clan" />
									</a>';
						}
				echo'</div>';
			}
			else
			{
				if($session_idclan['id_clan'] != $donnees['id'])
					echo'<a href="team-i'.$donnees['id'].'.html">';
				else
					echo'<a href="team.html">';
				echo'<img class="defaut_clan2" src="images/defaut_clan2.png" alt="Photo de Clan"/>
						</a>';
			}
			echo'<p class="info_clan_recherche">';
				if(strlen($donnees['jeu']) > 15)
				{
					echo'
					Jeu : 
					<span style="font-size:0.7em;">
						'.substr(stripslashes(htmlspecialchars($donnees['jeu'])), 0, 20).'';
						if(strlen(stripslashes(htmlspecialchars($donnees['jeu']))) > 20)
						{ echo'...';}
					echo'
					</span>
					<br />';
				}
				else
				{
					echo'
					Jeu : 
					'.substr(stripslashes(htmlspecialchars($donnees['jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnees['jeu']))) > 20)
					{ echo'...';}
					
					echo'
					<br />';
				}		
					echo'Platerforme : '.$donnees['plateforme'].'
				 </p>';
			echo'</div>';
		}
	if ( $resultat == 0)
	{
		echo'
		<p class="aucun_resultat" >
			Aucun Clan n\'a �t� trouv�.
			<br /> 
			Essayez une autre recherche.
		</p>';
	}
	else
	{
		echo'
		<p class="resultat">
			'.$resultat['resultats'].' r�sultat';
			if($resultat>1)
			{echo's';}
		echo'
		<p>';
	}
	$resu->closeCursor();
}
// GESTION AFFICHAGE PAGES
echo'<p class="nombre_page">';

	$nbre_page = 1;
	if(isset($_GET['gamers']))
	{
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_message FROM jeu 
							WHERE nom_jeu LIKE :recherche')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('recherche' => $recherche.'%'))
							or die(print_r($bdd->errorInfo()));
	}
	elseif(isset($_GET['nom']))	
	{
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_message FROM clan 
							WHERE nom_clan LIKE :recherche')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('recherche' => $recherche.'%'))
							or die(print_r($bdd->errorInfo()));
	}
	elseif(isset($_GET['jeu']))
	{
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_message FROM clan 
							WHERE jeu LIKE :recherche')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('recherche' => $recherche.'%'))
							or die(print_r($bdd->errorInfo()));
	}
	else
	{
		$p = $bdd->prepare('SELECT COUNT(*) AS nbre_message FROM jeu 
							WHERE pseudo LIKE :recherche')
							or die(print_r($bdd->errorInfo()));
		$p->execute(array('recherche' => $recherche.'%'))
							or die(print_r($bdd->errorInfo()));
	}
	
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_message'];
	$p->closeCursor();

if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
echo'<p class="nombre_page">';
	
if (isset($nbr_entrees) AND $nbr_entrees != 0)
{
	$nbr_page = ceil($nbr_entrees / 20); //diviser puis arrondir au sup�rieur
	
	if ($current_page >= 1 AND $current_page <= $nbr_page)
	{
		if ($nbr_page > 1)
		{
			echo'
				<span style="color:#102c3c;">';
			if(isset($_GET['gamers']))
				echo'<a href="recherche.html?gamers&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page=1">';
			elseif(isset($_GET['nom']))	
				echo'<a href="recherche.html?nom&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page=1">';
			elseif(isset($_GET['jeu']))
				echo'<a href="recherche.html?jeu&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page=1">';
			else
				echo'<a href="recherche.html?recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page=1">';
				
			if ($current_page - 3 > 1)
			{
				echo'
						<span class="numero_page1">1</span>
					</a>';
				echo '...';
			}
			else
			{
				if((isset($_GET['page']) AND $_GET['page'] ==1) 
				OR !isset($_GET['page']))
				{
					echo'
						<span class="numero_page2">1</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">1</span>
					</a>';
				}
			}	
			echo '
				</span>';
			
			$count = -2;
			
			while ($count <= 2)
			{
				$affich_page = $current_page + $count ;
				
				if ($affich_page > 1
				AND $affich_page < $nbr_page)
				{
					if(isset($_GET['gamers']))
						echo '<a href="recherche.html?gamers&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$affich_page.'">';
					elseif(isset($_GET['nom']))	
						echo '<a href="recherche.html?nom&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$affich_page.'">';
					elseif(isset($_GET['jeu']))
						echo '<a href="recherche.html?jeu&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$affich_page.'">';
					else
						echo '<a href="recherche.html?recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$affich_page.'">';
					
					if(isset($_GET['page']) AND $_GET['page'] == $affich_page)
					{
						echo'
							<span class="numero_page2">'.$affich_page.'</span>
						</a>';
					}
					else
					{
						echo'
							<span class="numero_page">'.$affich_page.'</span>
						</a>';
					}
				}
				$count ++;
			}
			
			echo'
				<span style="color:#102c3c;">';
			if ($current_page + 3 < $nbr_page)
				echo '...';
			
			if(isset($_GET['gamers']))
				echo'<a href="recherche.html?gamers&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$nbr_page.'">';
			elseif(isset($_GET['nom']))	
				echo'<a href="recherche.html?nom&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$nbr_page.'">';
			elseif(isset($_GET['jeu']))
				echo'<a href="recherche.html?jeu&recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$nbr_page.'">';
			else
				echo'<a href="recherche.html?recherche='.stripslashes(htmlspecialchars($_GET['recherche'])).'&page='.$nbr_page.'">';
			
			if ($current_page + 3 < $nbr_page)
			{
				echo'
						<span class="numero_page1">'.$nbr_page.'</span>
					</a>';
			}
			else
			{
				if(isset($_GET['page']) AND $_GET['page'] == $nbr_page)
				{
					echo'
						<span class="numero_page2">'.$nbr_page.'</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">'.$nbr_page.'</span>
					</a>';
				}
			}
				
			echo'
				</span>';
		}
	}
	else
	{
		echo 'Erreur de num�ro de page.'; // ERREUR
	}
}
echo'</p>';
// FIN GESTION AFFICHAGE PAGES	

?>      
	</div>  
	<!--
	NECESSAIRE POUR GARDER LE PIED DE PAGE EN BAS A CAUSE 
	DE FLOAT: RIGHT et FLOAT: LEFT 
	-->
	<div id="corps_invisible2"></div>   
	<div id="corps_invisible"></div> 	  
	     
</div>
<?php include('pied_page.php'); ?>