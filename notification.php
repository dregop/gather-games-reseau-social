<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (isset($_GET['v'],$_GET['demande']))
{
	$req1 = $bdd->prepare('UPDATE demande_amis SET view=0 WHERE id_jeu=:id')
							or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$req1->closeCursor(); // Termine le traitement de la requ�te 	
}

if (isset($_GET['v']) AND !isset($_GET['demande']))
{
	//ON S'OCCUPE DES NOTIFICATIONS !!! => UN AMI PUBLIE SUR SON MUR  ou 
	// COMMENTE UNE PUBLICATION SUR SON MUR
	
	$req1 = $bdd->prepare('UPDATE notifications SET view=0 
							WHERE id_jeu_lieu=:id')
							or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$req1->closeCursor(); // Termine le traitement de la requ�te 

	//ON S'OCCUPE DES NOTIFICATIONS !!! => TOUS LES COMMENTAIRES ASSOCIE A SA 
	// PUBLICATION
	$req4_notif = $bdd->prepare('SELECT id FROM publication 
								WHERE id_jeu =:id_jeu ')
								or die(print_r($bdd->errorInfo()));
	$req4_notif->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
	while ($do4_notif = $req4_notif->fetch())
	{
		$req5 = $bdd->prepare('UPDATE notifications SET view=0 
								WHERE id_publication =:id_publication ')
								or die(print_r($bdd->errorInfo()));
		$req5->execute(array('id_publication' => $do4_notif['id']))
								or die(print_r($bdd->errorInfo()));	
		$req5->closeCursor(); // Termine le traitement de la requ�te 
	}
	
	//ON S'OCCUPE DES NOTIFICATIONS !!! => UNE PUBLICATION SUR LE CLAN
	$req3 = $bdd->prepare('UPDATE jeu SET clan_view=0 WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$req3->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$req3->closeCursor(); // Termine le traitement de la requ�te 	
}

if (isset($_GET['choix'], $_GET['demandeur']) AND $_GET['choix'] == 1)
{
	// on insert l'ami dans la bdd
	$req = $bdd->prepare('INSERT INTO amis(id_jeu,id_jeu_ami,date_ajout) 
						VALUES(:id_jeu,:id_jeu_ami,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'id_jeu_ami' => $_GET['demandeur']))
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
		$rep = $bdd->prepare('SELECT id FROM amis WHERE id_jeu=:id_jeu 
							AND id_jeu_ami=:id_jeu_ami')
							or die(print_r($bdd->errorInfo()));
		$rep->execute(array('id_jeu' => $_SESSION['id_jeu'], 
							'id_jeu_ami' => $_GET['demandeur']))
							or die(print_r($bdd->errorInfo()));
		$ok = $rep->fetch();
	// On insert dans actualit�s
	$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,id_table,date_actu) 
						VALUES(:id_jeu,:actu,:id_table,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'id_table' => $ok['id'],
						'actu' => 'amis')) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	
	// supprimer la demande d'ami
	$req = $bdd->prepare('DELETE FROM demande_amis 
						WHERE id_jeu_demandeur=:id_jeu_d AND id_jeu=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu_d' => $_GET['demandeur'],
						'id_jeu' => $_SESSION['id_jeu'])) 
						or die(print_r($bdd->errorInfo()));
	$req = $bdd->prepare('DELETE FROM demande_amis
						WHERE id_jeu_demandeur=:id_jeu AND id_jeu=:id_jeu_d')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu_d' => $_GET['demandeur'],
						'id_jeu' => $_SESSION['id_jeu'])) 
						or die(print_r($bdd->errorInfo()));
	header('Location: notification-demande.html');
}
elseif (isset($_GET['choix'], $_GET['demandeur']) AND $_GET['choix'] == 0)
{
	// supprimer la demande d'ami
	$req = $bdd->prepare('DELETE FROM demande_amis 
						WHERE id_jeu_demandeur=:id_jeu_d AND id_jeu=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu_d' => $_GET['demandeur'],
						'id_jeu' => $_SESSION['id_jeu'])) 
						or die(print_r($bdd->errorInfo()));
	$req = $bdd->prepare('DELETE FROM demande_amis
						WHERE id_jeu_demandeur=:id_jeu AND id_jeu=:id_jeu_d')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu_d' => $_GET['demandeur'],
						'id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	header('Location: notification-demande.html');
}

include('menu.php');
?>

<div id="corps_notification">
	<img id="g_news" src="images/g_news.png" alt="Gamers News"/> 
	<p id="intro_notification">  
		Cette page regroupe vos actualit�s. Vous pouvez y voir vos derniers 
		messages, vos dernieres demande d'ajout ami et enfin vos notifications 
		pour profiter au mieux de Gather Games.
	<p>
	<div id="sous_corps_notification">
<?php 
		//ON RECUPERE LES VIEW DANS MESSAGE  
		$req1_notif = $bdd->prepare('SELECT view FROM message 
									WHERE destinataire=:destinataire 
									AND view=1')
									or die(print_r($bdd->errorInfo()));
		$req1_notif->execute(array('destinataire' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo()));
		$n1=0;
		while ($do1_notif = $req1_notif->fetch())
		{
			$n1++;
		}
		//ON RECUPERE LES VIEW DANS DEMANDE AMI
		$req2_notif = $bdd->prepare('SELECT view FROM demande_amis 
									WHERE id_jeu=:id_jeu AND view=1')
									or die(print_r($bdd->errorInfo()));
		$req2_notif->execute(array('id_jeu' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo()));
		$n2=0;
		while ($do2_notif = $req2_notif->fetch())
		{
			$n2++;
		}

		//NOTIFICATION
		if(isset($_GET['demande']))
		{
			echo'
			<a href="notification.html">
				<div id="notif_notification"></div>
			</a>';
		}
		else
		{
			echo'
			<a href="notification.html">
				<div id="notif2_notification"></div>
			</a>';
		}
		//MESSAGE
		if($n1 > 0 )
		{
			echo'
			<a href="message-v.html">
				<div id="message3_notification"></div>
			</a>';
		}
		else
		{
			echo'
			<a href="message.html">
				<div id="message_notification"></div>
			</a>';
		}
		//DEMANDE AMI	
		if(isset($_GET['demande']) AND $n2 == 0 )
		{
			echo'
			<a href="notification-demande.html">
				<div id="ajouter2_notification"></div>
			</a>';
		}
		elseif($n2>0)
		{
			echo'
			<a href="notification-demande-v.html">
				<div id="ajouter3_notification"></div>
			</a>';
		}
		else
		{
			echo'
			<a href="notification-demande.html">
				<div id="ajouter_notification"></div>
			</a>';
		}
?>
	</div>
<?php
	

	if (isset($_GET['demande']))
	{	
		$i =0;
		$rep = $bdd->prepare('SELECT id_jeu_demandeur,id FROM demande_amis 
							WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$rep->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
		while ($donnees = $rep->fetch())
		{
			$i++;
			$rep2 = $bdd->prepare('SELECT id_jeu, id FROM demande_amis 
									WHERE id_jeu=:id 
									AND id_jeu_demandeur=:id_jeu')
									or die(print_r($bdd->errorInfo()));
			$rep2->execute(array('id_jeu' => $_SESSION['id_jeu'], 
									'id' => $donnees['id_jeu_demandeur']))
									or die(print_r($bdd->errorInfo()));
			$donnees2 = $rep2->fetch();
			$rep1 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
			$rep1->execute(array('id' => $donnees['id_jeu_demandeur']))
									or die(print_r($bdd->errorInfo()));	
			$donnees3 = $rep1->fetch();
			if (isset($donnees2['id_jeu']) 
			AND $donnees2['id_jeu'] == $donnees['id_jeu_demandeur'] 
			AND $donnees['id'] < $donnees2['id'])
			{
				echo '
				<div id="demande_notification2">
					La demande est d�j� en cours pour le sous compte 
					'.$donnees3['pseudo'].'.
				</div>';
			}
			else
			{
				echo '
				<div class="demande_notification">';
				$req = $bdd->prepare('SELECT * FROM jeu WHERE id=:id_jeu')
									or die(print_r($bdd->errorInfo()));
				$req->execute(array('id_jeu' => $donnees['id_jeu_demandeur']))
									or die(print_r($bdd->errorInfo()));
				$donnee = $req->fetch();
				
				echo'<div class="centre_demande_notification">';
				
				if ($donnee['photo_profil'] != '' AND $donnee['photo_profil'] != 0)
				{		
					$source = getimagesize('images_utilisateurs/'.$donnee['photo_profil']); // La photo est la source
					if ($source[0] <= 200 AND $source[1] <= 200)
					{
						echo'
						<a  href="id'.$donnee['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnee['pseudo']))).'">
							<img  src="images_utilisateurs/'.$donnee['photo_profil'].'" alt="Photo de profil" />';
					}
					else
					{
						echo'
						<a  href=id'.$donnee['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnee['pseudo']))).'">
							<img src="images_utilisateurs/mini_3_'.$donnee['photo_profil'].'" alt="Photo de profil" />';
					}
					echo'</a>';
				}
				else 
				{
					echo'
					<a  href="id'.$donnee['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnee['pseudo']))).'">
						<img src="images/tete1.png" alt="Photo de profil"/>
					</a>';
				}
				echo'</div>';
				echo'
					<p class="accepter_refuser">
						<a  href="id'.$donnee['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnee['pseudo']))).'">';
						if(strlen(stripslashes(htmlspecialchars($donnee['pseudo']))) > 15)
							echo'
							<span style="color:#102c3c; font-size:small;">
								'.stripslashes(htmlspecialchars($donnee['pseudo'])).'
							</span>
							<br />
						</a> 
							vous a ajout� � ses amis. 
							<br />';
						if(strlen(stripslashes(htmlspecialchars($donnee['pseudo']))) < 15)
							echo'
							<span style="color:#102c3c; font-size:large;">
								'.stripslashes(htmlspecialchars($donnee['pseudo'])).'
							</span>
							<br />
						</a> 
							vous a ajout� � ses amis. 
							<br />';
						
						echo'
						<a href="notification.php?choix=1&demandeur='.$donnees['id_jeu_demandeur'].'">
							Accepter
						</a> 
						ou
						<a href="notification.php?choix=0&demandeur='.$donnees['id_jeu_demandeur'].'">
							Refuser
						</a>
					</p>
				</div>';
			}
		}
		
		if ($i == 0)
		{
			echo'
			<div id="demande_notification2">
				Aucune demande.
			</div>';
		}
	echo'
	</div>';
	}
	else
	{
		echo'
			<p id="vos_notifs">
				Voici vos notifications 
				<a href="notification-tout.html">
					<span style="font-size:small;">
						(Tout afficher)
					</span>
				</a>
			</p>';
			
		echo'<div class="bordure_notifs"></div>';
		
		$notification = 0;
		$i = 0;
		// ON RECUPERE SON L'ID DE SON CLAN
		$r_clan = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$r_clan->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
		$d_clan = $r_clan->fetch();	
		
		// ON RECUPERE TOUS LES ID DE PUBLICATION DU GAS !
		$r_publication = $bdd->prepare('SELECT id FROM publication WHERE id_jeu=:id_jeu ')
							or die(print_r($bdd->errorInfo()));
		$r_publication->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
		
		while($d_publication = $r_publication->fetch()) 
		{ 
			$tableau_publication[$i]= $d_publication['id'];
			$i++;
		}	
		
		$morceau_requete = "";
		$morceau_requete2 = "";
		$morceau_requete3 = "";
		
		if(isset($tableau_publication)) // POUR CEUX QUI ONT PUBLIE ET QUI ONT DES NOTIFS
		{
			foreach($tableau_publication as $element)
			{
				$zero=0;
				$morceau_requete .= " OR id_publication = ".$element." AND id_jeu_com != ".$_SESSION['id_jeu']." " ;
				$morceau_requete2 .= " AND id_publication = ".$zero."  AND id_jeu_publi != ".$_SESSION['id_jeu']." " ; // QQL PUBLIE SUR TA TEAM
				$morceau_requete3 .= " AND id_publication = ".$element." AND id_jeu_com != ".$_SESSION['id_jeu']." " ; 
				// COMMENTAIRE DE TA PUBLI SUR TEAM
			}
			if(isset($_GET['tout']))
			{
				$r_tout = $bdd->prepare('SELECT * ,
										DATE_FORMAT(date_notification, \'%d/%m/%Y � %Hh %imin \') 
										AS date_notif
										FROM notifications 
										WHERE (id_jeu_lieu=:id_jeu_lieu 
										AND id_jeu_com != :id_jeu_com)
										OR (id_clan=:id_clan 
										AND profil_ou_clan =:profil_ou_clan '.$morceau_requete2.')
										OR (id_clan=:id_clan 
										AND profil_ou_clan =:profil_ou_clan '.$morceau_requete3.')
										 '.$morceau_requete.'
										ORDER BY date_notification DESC LIMIT 0,200')
										or die(print_r($bdd->errorInfo()));
				$r_tout->execute(array('id_jeu_lieu' => $_SESSION['id_jeu'],
										'id_jeu_com' => $_SESSION['id_jeu'],
										'id_clan' => $d_clan['id_clan'],
										'profil_ou_clan' => 'clan'))
										or die(print_r($bdd->errorInfo()));
			}
			else
			{
			// $morceau_requete .= " OR id_publication = ".$element." AND id_jeu_com != ".$_SESSION['id_jeu']." " ;
			// $morceau_requete2 .= " AND id_jeu_publi != ".$_SESSION['id_jeu']." AND id_jeu_com != ".$_SESSION['id_jeu']." " ;
				$r_tout = $bdd->prepare('SELECT * ,
										DATE_FORMAT(date_notification, \'%d/%m/%Y � %Hh %imin \') 
										AS date_notif
										FROM notifications 
										WHERE (id_jeu_lieu=:id_jeu_lieu 
										AND id_jeu_com != :id_jeu_com)
										OR (id_clan=:id_clan 
										AND profil_ou_clan =:profil_ou_clan '.$morceau_requete2.')
										OR (id_clan=:id_clan 
										AND profil_ou_clan =:profil_ou_clan '.$morceau_requete3.')
										 '.$morceau_requete.'
										ORDER BY date_notification DESC LIMIT 0,20')
										or die(print_r($bdd->errorInfo()));
				$r_tout->execute(array('id_jeu_lieu' => $_SESSION['id_jeu'],
										'id_jeu_com' => $_SESSION['id_jeu'],
										'id_clan' => $d_clan['id_clan'],
										'profil_ou_clan' => 'clan'))
										or die(print_r($bdd->errorInfo()));
			}
		}
		else 
		{ 
			/* 
				POUR CEUX QUI N'ONT PAS FAIT DE PUBLICATION MAIS QUI ONT QUAND 
				MEME DES NOTIFS
				SI IL N'A PAS PUBLIE UNE SEUL PUBLICATION, PERSONNE PEU 
				COMMENTER SINON CA PRODUIT LE TABLEAU ET CA VA EN HAUT DANS 
				L'AUTRE CONDITION !
			*/
			
			$zero=0;
			$morceau_requete2 .= " AND id_publication = ".$zero." AND id_jeu_com = ".$zero." AND id_jeu_publi != ".$_SESSION['id_jeu']." " ; // QQL PUBLIE SUR TA TEAM
			// ON MONTRE BIEN QUE id_jeu_com = 0 ca montre que c'est une publication et pas commentaire.
			if(isset($_GET['tout']))
			{
				$r_tout = $bdd->prepare('SELECT * ,
										DATE_FORMAT(date_notification, \'%d/%m/%Y � %Hh %imin \') 
										AS date_notif
										FROM notifications 
										WHERE (id_jeu_lieu=:id_jeu_lieu 
										AND id_jeu_com != :id_jeu_com)
										OR (id_clan=:id_clan 
										AND profil_ou_clan =:profil_ou_clan '.$morceau_requete2.')
										OR (id_jeu_publi=:id_jeu_lieu AND jaime=1)
										ORDER BY date_notification DESC LIMIT 0,200')
										or die(print_r($bdd->errorInfo()));
				$r_tout->execute(array('id_jeu_lieu' => $_SESSION['id_jeu'],
										'id_jeu_com' => $_SESSION['id_jeu'],
										'id_clan' => $d_clan['id_clan'],
										'profil_ou_clan' => 'clan'))
										or die(print_r($bdd->errorInfo()));
			}
			else
			{
				$r_tout = $bdd->prepare('SELECT * ,
										DATE_FORMAT(date_notification, \'%d/%m/%Y � %Hh %imin \') 
										AS date_notif
										FROM notifications 
										WHERE (id_jeu_lieu=:id_jeu_lieu 
										AND id_jeu_com != :id_jeu_com)
										OR (id_clan=:id_clan 
										AND profil_ou_clan =:profil_ou_clan '.$morceau_requete2.')
										OR (id_jeu_publi=:id_jeu_lieu AND jaime=1)
										ORDER BY date_notification DESC LIMIT 0,20')
										or die(print_r($bdd->errorInfo()));
				$r_tout->execute(array('id_jeu_lieu' => $_SESSION['id_jeu'],
										'id_jeu_com' => $_SESSION['id_jeu'],
										'id_clan' => $d_clan['id_clan'],
										'profil_ou_clan' => 'clan'))
										or die(print_r($bdd->errorInfo()));
			}
		}
		while($d_tout = $r_tout->fetch()) 
		{
			if ($d_tout['jaime'] == 1 AND $d_tout['id_jeu_publi'] == $_SESSION['id_jeu'])
			{
				$re_publi = $bdd->prepare('SELECT message,id FROM publication WHERE id=:id_publi')
				or die(print_r($bdd->errorInfo()));
				$re_publi->execute(array('id_publi' => $d_tout['id_publication']))
				or die(print_r($bdd->errorInfo()));
				$donnees_publi = $re_publi->fetch();
				
				echo '<img class="publication_notifs" 
				src="images/publication_notifs.png" alt="Publication" />';
				echo'
				<div class="publication_profil">
					<p class="date_notifs">
						'.$d_tout['date_notif'].'
					</p>						
					';	
				$r_autre = $bdd->prepare('SELECT pseudo, photo_profil 
											FROM jeu WHERE id=:id_jeu')
											or die(print_r($bdd->errorInfo()));
				$r_autre->execute(array('id_jeu' => $d_tout['id_jeu']))
											or die(print_r($bdd->errorInfo()));
				$d_autre = $r_autre->fetch();						
				if(isset($d_autre['photo_profil']) AND $d_autre['photo_profil'] != '' 
				AND $d_autre['photo_profil'] != 0)
				{
					$source = getimagesize('images_utilisateurs/'.$d_autre['photo_profil']); // La photo est la source
					if ($source[0] <= 60 AND $source[1] <= 60)
						echo '<img src="images_utilisateurs/'.$d_autre['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_2_'.$d_autre['photo_profil'].'" alt="Photo de profil" />';
				}
				else
					echo'<img src="images/tete3.png" alt="Photo de profil" />';

					$message1 = stripslashes(htmlspecialchars($donnees_publi['message']));
					
				$message1 = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message1);	
				$message1 = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message1);
				$message1 = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message1);
				$message1 = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message1);
				$message1 = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message1);
				if (preg_match("#\[a\]http://(.+)\[/a\]#isU", $message1)
				OR preg_match("#\[a\]https://(.+)\[/a\]#isU", $message1) )
				{
					$message1 = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="$1" target="_blank">$1</a>', $message1);
				}
				else
				{
					$message1 = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="http://$1" target="_blank">$1</a>', $message1);
				}
				$message1 = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message1);		
				$message1 = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message1);	
				$message1 = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message1);		
				$message1 = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message1);
				$message1 = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message1);
				$message1 = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message1);
				$message1 = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message1);
				$message1 = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message1);
				$message1 = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message1);
				$message1 = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message1);
				
				echo '<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'#'.$donnees_publi['id'].'">
						<p class="pseudo_notifs">
							'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
							a aim� votre publication : " '.$message1.' "
					    </p></a>';
				echo '</div>';
			}
			
			$verif = $bdd->prepare('SELECT id FROM publication 
			WHERE message=:message AND
			date_publication=:date_publication AND id_jeu=:id_jeu')
			or die(print_r($bdd->errorInfo()));
			$verif->execute(array('message' => $d_tout['publication'],
			'date_publication' => $d_tout['date_notification'],
			'id_jeu' => $d_tout['id_jeu_publi']))
			or die(print_r($bdd->errorInfo()));
			$do_verif = $verif->fetch();
			//si la publie a �t� supprim�e on affiche pas
			if (isset($do_verif['id']) OR (isset($d_tout['id_jeu_com']) 
			AND $d_tout['id_jeu_com'] != 0))
			{
						
			$r_autre = $bdd->prepare('SELECT pseudo, photo_profil 
									FROM jeu WHERE id=:id_jeu_publi
									OR id=:id_jeu_com')
									or die(print_r($bdd->errorInfo()));
			$r_autre->execute(array('id_jeu_publi' => $d_tout['id_jeu_publi'],
									'id_jeu_com' => $d_tout['id_jeu_com']))
									or die(print_r($bdd->errorInfo()));
			$d_autre = $r_autre->fetch();	
			
			if(isset($d_tout['id_jeu_publi']) AND $d_tout['id_jeu_publi'] != 0)
			{
				echo '
				<img class="publication_notifs" src="images/publication_notifs.png" alt="Publication" />';
			}
			else
			{
				echo '
				<img class="commentaire_notifs" src="images/commentaire_notifs_ami.png" alt="Commentaire" />';
			}
			
			echo'
			<div class="publication_profil">
				<p class="date_notifs">
					'.$d_tout['date_notif'].'
				</p>
				
				<div class="centre_image_notifs">';			
				if(isset($d_autre['photo_profil']) AND $d_autre['photo_profil'] != '' 
				AND $d_autre['photo_profil'] != 0)
				{
					$source = getimagesize('images_utilisateurs/'.$d_autre['photo_profil']); // La photo est la source
					if ($source[0] <= 60 AND $source[1] <= 60)
						echo '<img src="images_utilisateurs/'.$d_autre['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_2_'.$d_autre['photo_profil'].'" alt="Photo de profil" />';
				}
				else
					echo'<img src="images/tete3.png" alt="Photo de profil" />';
					
				if(isset($d_tout['id_jeu_publi']) AND $d_tout['id_jeu_publi'] != 0)
				{	
					$message1 = stripslashes(htmlspecialchars($d_tout['publication']));
				}
				else
				{
					$message1 = stripslashes(htmlspecialchars($d_tout['commentaire']));
				}
					
				$message1 = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message1);	
				$message1 = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message1);
				$message1 = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message1);
				$message1 = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message1);
				$message1 = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message1);
				if (preg_match("#\[a\]http://(.+)\[/a\]#isU", $message1)
				OR preg_match("#\[a\]https://(.+)\[/a\]#isU", $message1) )
				{
					$message1 = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="$1" target="_blank">$1</a>', $message1);
				}
				else
				{
					$message1 = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="http://$1" target="_blank">$1</a>', $message1);
				}
				$message1 = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message1);		
				$message1 = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message1);	
				$message1 = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message1);		
				$message1 = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message1);
				$message1 = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message1);
				$message1 = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message1);
				$message1 = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message1);
				$message1 = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message1);
				$message1 = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message1);
				$message1 = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message1);
	
				
				echo'
				</div>';
					
					if(isset($d_tout['id_jeu_publi'], $d_tout['profil_ou_clan']) 
					AND $d_tout['id_jeu_publi'] != 0 
					AND $d_tout['profil_ou_clan'] =='profil')
					{
						$rep1 = $bdd->prepare('SELECT id FROM publication 
											WHERE message=:message AND
											date_publication=:date_publication
											AND id_jeu=:id_jeu')
											or die(print_r($bdd->errorInfo()));
						$rep1->execute(array('message' => $d_tout['publication'],
											'date_publication' => $d_tout['date_notification'],
											'id_jeu' => $d_tout['id_jeu_publi']))
											or die(print_r($bdd->errorInfo()));
						$donnees1 = $rep1->fetch();	
						
						if(!isset($donnees1['id']))
						{
							echo' 
							<p class="pas_publication">
								<img src="images/attention.png" alt="erreur" /> 
								Publication supprim�e
							</p> 
						<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">
							<p class="pseudo_notifs">
							'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
							a publi� sur votre profil : " '.$message1.' "';
						}
						else
						{
							echo'
						<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'#'.$donnees1['id'].'">
							<p class="pseudo_notifs">
							'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
							a publi� sur votre profil : " '.$message1.' "';
						}
					}
					elseif(isset($d_tout['id_jeu_publi'], $d_tout['profil_ou_clan'],
					$d_tout['id_clan']) AND $d_tout['id_jeu_publi'] != 0 
					AND $d_tout['profil_ou_clan'] =='clan'
					AND $d_tout['id_clan'] != 0)
					{
						$rep1 = $bdd->prepare('SELECT id FROM publication 
											WHERE message=:message AND
											date_publication=:date_publication
											AND id_jeu=:id_jeu')
											or die(print_r($bdd->errorInfo()));
						$rep1->execute(array('message' => $d_tout['publication'],
											'date_publication' => $d_tout['date_notification'],
											'id_jeu' => $d_tout['id_jeu_publi']))
											or die(print_r($bdd->errorInfo()));
						$donnees1 = $rep1->fetch();	
						
						if(!isset($donnees1['id']))
						{
							echo'
							<p class="pas_publication">
								<img src="images/attention.png" alt="erreur" /> 
								Publication supprim�e
							</p> 
						<a href="team.html">
							<p class="pseudo_notifs">
							'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
							a publi� sur le profil de la Team : " '.$message1.' "';
						}
						else	
						{
							echo'
						<a href="team.html#'.$donnees1['id'].'">
							<p class="pseudo_notifs">
							'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
							a publi� sur le profil de la Team : " '.$message1.' "';
						}
					}
					elseif(isset($d_tout['id_jeu_com'], $d_tout['profil_ou_clan'],
					$d_tout['id_clan'],$d_tout['id_publication']) AND $d_tout['id_jeu_com'] != 0
					AND $d_tout['profil_ou_clan'] =='clan'
					AND $d_tout['id_clan'] != 0
					AND $d_tout['id_publication'] != 0)
					{
						echo'
					<a href="team.html#'.$d_tout['id_publication'].'">
						<p class="pseudo_notifs">
						'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
						a comment� votre publication sur le profil de la Team :
						" '.$message1.' "';
					}
					elseif(isset($d_tout['id_publication'], $d_tout['profil_ou_clan'],
					$d_tout['id_jeu_lieu'])
					AND $d_tout['profil_ou_clan'] =='profil'
					AND $d_tout['id_jeu_lieu'] == $_SESSION['id_jeu'])
					{
						echo'
						<p class="lieu_notifs">
							Sur votre profil 
						</p>
					<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'#'.$d_tout['id_publication'].'">
						<p class="pseudo_notifs">
						'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
						a comment� une publication par :
						" '.$message1.' "';
					}
					elseif(isset($d_tout['id_publication'], $d_tout['profil_ou_clan'],
					$d_tout['id_jeu_lieu'])
					AND $d_tout['profil_ou_clan'] =='profil'
					AND $d_tout['id_jeu_lieu'] != $_SESSION['id_jeu'])
					{
						$r_pseudo = $bdd->prepare('SELECT pseudo FROM jeu
										WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
						$r_pseudo->execute(array('id' => $d_tout['id_jeu_lieu']))
											or die(print_r($bdd->errorInfo()));
						$d_pseudo = $r_pseudo->fetch();	
						
						echo'
						<p class="lieu_notifs">
							Sur le profil d\'un ami
						</p>
					<a href="id'.$d_tout['id_jeu_lieu'].'-'.urlencode(stripslashes(htmlspecialchars($d_pseudo['pseudo']))).'#'.$d_tout['id_publication'].'">
						<p class="pseudo_notifs">
						'.stripslashes(htmlspecialchars($d_autre['pseudo'])).' 
						a comment� votre publication par :
						" '.$message1.' "';
					}	
					echo'
					</p>
				</a>
			</div>';
			$notification++;
			}
		}
		
		if($notification == 0)
		{
			echo '
			<div id="demande_notification2">
				Vous n\'avez aucune notification.
			</div>';
		}
	
}
?>	
    </div>
	
<?php include('pied_page.php'); ?>