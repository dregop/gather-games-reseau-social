<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
// ON DETRUIT LES SESSIONS PROVENANT DE COMMENTAIRE
unset($_SESSION['id_publi']);
unset($_SESSION['id_lieu_publication']);
unset($_SESSION['lieu_page']);
unset($_SESSION['id_ami']);
unset($_SESSION['page']);


// ON DETRUIT LES SESSIONS VENANT DE VIDEOS ET IMAGES
unset($_SESSION['id_jeu_images']);
unset($_SESSION['id_jeu_videos']);

unset($_SESSION['id_clan_images']);
unset($_SESSION['id_clan_videos']);


if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}	
if (isset($_GET['pseudo']) AND isset($_GET['id_jeu']) 
AND isset($_GET['photo']))
{ 
	$reqs = $bdd->prepare('SELECT nom_de_compte FROM jeu 
						WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id_jeu' => $_GET['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $reqs->fetch();
	if ($donnees_jeu['nom_de_compte'] == $_SESSION['nom_de_compte'])
	{
//MESSAGE DE PREMIERE CONNEXION	
		$req_n = $bdd->prepare('SELECT nouveau FROM membres 
										WHERE nom_de_compte=:ndc')
										or die(print_r($bdd->errorInfo()));
		$req_n->execute(array('ndc' => $_SESSION['nom_de_compte']))
										or die(print_r($bdd->errorInfo()));
		$donnees_n = $req_n->fetch();
		if ($donnees_n['nouveau'] == 1)
		{
			// on insert le message dans la bdd
			$req = $bdd->prepare('INSERT INTO message(id_jeu,destinataire,message,
								date_message,view) 
								VALUES(:id_jeu,:destinataire,:message,NOW(),:view)') 
								or die(print_r($bdd->errorInfo()));
			$message='
			Salut '.$_GET['pseudo'].' ! 
			<br /> <br />
			L\'�quipe de Gather Games te remercie pour ton inscription.
			Si tu as la moindre question les administrateurs seront l� pour y 
			r�pondre.
			<br />
			N\'h�site pas � partager le site, puisque plus la communaut� sera 
			grande, plus il y aura de partages et de rencontres.
			<br /><br />
			� toi de jouer !';
			$req->execute(array('id_jeu' => 0,
								'destinataire' => $_GET['id_jeu'],
								'message' => $message,
								'view' => 1)) 
								or die(print_r($bdd->errorInfo()));
			$req->closeCursor(); // Termine le traitement de la requ�te		
			
			$req = $bdd->prepare('UPDATE membres SET nouveau=0 WHERE nom_de_compte=:ndc') 
								or die(print_r($bdd->errorInfo()));
			$req->execute(array('ndc' => $_SESSION['nom_de_compte'])) 
			or die(print_r($bdd->errorInfo()));
			$req->closeCursor(); // Termine le traitement de la requ�te
		}
// FIN MESSAGE
		$_SESSION['pseudo'] = $_GET['pseudo'];
		$_SESSION['id_jeu'] = $_GET['id_jeu'];
		$_SESSION['photo_profil'] = $_GET['photo'];
		header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
	}
	else header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
}	
if (!isset($_SESSION['nom_de_compte']))
{
	header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
if (isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu']) 		// UN AUTRE PROFIL != DE TON ID
{
	$reqs = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id' => $_GET['id']))
							or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $reqs->fetch();
	if (!$donnees_jeu['id'])
	{
		header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
	}
	if ($donnees_jeu['nom_de_compte'] == $_SESSION['nom_de_compte'])
	{
		header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
	}
}
else
{
	$reqs = $bdd->prepare('SELECT * FROM jeu WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $reqs->fetch();
}

$reqs = $bdd->prepare('SELECT * FROM membres 
						WHERE nom_de_compte=:nom_de_compte')
						or die(print_r($bdd->errorInfo()));
$reqs->execute(array('nom_de_compte' => $donnees_jeu['nom_de_compte'],))
						or die(print_r($bdd->errorInfo()));
$donnees = $reqs->fetch();

// ON SUPPRIME L'AMI ! /////////////////////////////////////////////////////////
if(isset($_GET['id_ami'],$_GET['supprimer']))
{
	$req = $bdd->prepare('DELETE FROM amis WHERE id_jeu_ami=:id 
						AND id_jeu=:id2')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id' => $_GET['id_ami'], 'id2' => $_SESSION['id_jeu'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	
	$req2 = $bdd->prepare('DELETE FROM amis WHERE id_jeu=:id 
						AND id_jeu_ami=:id2')
						or die(print_r($bdd->errorInfo()));
	$req2->execute(array('id' => $_GET['id_ami'], 'id2' => $_SESSION['id_jeu'])) 
						or die(print_r($bdd->errorInfo()));
	$req2->closeCursor(); // Termine le traitement de la requ�te
	
	header('Location: amis-id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');   // TON PROFIL
}

// ON SUPPRIME LA PUBLICATION //////////////////////////////////////////////////
if(isset($_GET['id_publication'],$_GET['supprimer_publication']))
{
$reqs_p = $bdd->prepare('SELECT id_jeu, lieu_publication_id_jeu 
						FROM publication WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$reqs_p->execute(array('id' => $_GET['id_publication']))
						or die(print_r($bdd->errorInfo()));
$donnees_publi = $reqs_p->fetch();

	if(isset($donnees_publi['id_jeu'],$donnees_publi['lieu_publication_id_jeu'])
	AND $donnees_publi['id_jeu'] == $_SESSION['id_jeu'] 
	OR $donnees_publi['lieu_publication_id_jeu'] == $_SESSION['id_jeu'])
	{
		$req1 = $bdd->prepare('DELETE FROM publication WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$req1->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		
		$req2 = $bdd->prepare('DELETE FROM actu WHERE id_table=:id')
								or die(print_r($bdd->errorInfo()));
		$req2->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));					
								
		$req3 = $bdd->prepare('DELETE FROM notifications 
								WHERE id_publication=:id')
								or die(print_r($bdd->errorInfo()));
		$req3->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
		$req3->closeCursor(); // Termine le traitement de la requ�te
		
		$req4 = $bdd->prepare('DELETE FROM commentaire 
								WHERE id_publication=:id')
								or die(print_r($bdd->errorInfo()));
		$req4->execute(array('id' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
		$req4->closeCursor(); // Termine le traitement de la requ�te
	}	
	if(isset($_GET['id_ami']) AND $_GET['id_ami'] != $_SESSION['id_jeu'])
	{
		if(isset($_GET['page']))
		{
			header('Location: p'.$_GET['page'].'-id'.$_GET['id_ami'].'-'.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'');
		}
		else
		{
			header('Location: id'.$_GET['id_ami'].'-'.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'');
		}
	}
	else
	{
		if(isset($_GET['page']))
		{
			header('Location: p'.$_GET['page'].'-id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
		}
		else
		{
			header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
		}
	}
}

//ON SUPPRIME LES COMMENTAIRES /////////////////////////////////////////////////

	if(isset($_GET['id_publication'],$_GET['supprimer_commentaire'],
	$_GET['id_commentaire']))
	{
		$reqs_c = $bdd->prepare('SELECT id_jeu, date_commentaire 
								FROM commentaire WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$reqs_c->execute(array('id' => $_GET['id_commentaire']))
								or die(print_r($bdd->errorInfo()));
		$donnees_comm = $reqs_c->fetch();
		$reqs_p = $bdd->prepare('SELECT lieu_publication_id_jeu,
								id_jeu
								FROM publication WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$reqs_p->execute(array('id' => $_GET['id_publication']))
								or die(print_r($bdd->errorInfo()));
		$donnees_publi = $reqs_p->fetch();
	
		if(isset($donnees_comm['id_jeu']) 
		AND $donnees_comm['id_jeu'] == $_SESSION['id_jeu'] 
		OR $donnees_publi['lieu_publication_id_jeu'] == $_SESSION['id_jeu']
		OR $donnees_publi['id_jeu'] == $_SESSION['id_jeu'])
		{
			$req1 = $bdd->prepare('DELETE FROM commentaire WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
			$req1->execute(array('id' => $_GET['id_commentaire'])) 
									or die(print_r($bdd->errorInfo()));
			$req1->closeCursor(); // Termine le traitement de la requ�te
			$req3 = $bdd->prepare('DELETE FROM notifications 
									WHERE date_notification=:date_notification 
									AND id_publication=:id_publication')
									or die(print_r($bdd->errorInfo()));
			$req3->execute(array('date_notification' => $donnees_comm['date_commentaire'],
								'id_publication' => $_GET['id_publication'])) 
								or die(print_r($bdd->errorInfo()));
			$req3->closeCursor(); // Termine le traitement de la requ�te
		}
		if(isset($_GET['id_ami']) AND $_GET['id_ami'] != $_SESSION['id_jeu'])   // PROFIL D'UN AMI
		{
			if(isset($_GET['page']))
			{
				header('Location: p'.$_GET['page'].'-id'.$_GET['id_ami'].'-'.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'#'.$_GET['id_publication'].'');
			}
			else
			{
				header('Location: '.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'-i'.$_GET['id_ami'].'#'.$_GET['id_publication'].'');
				header('Location: id'.$_GET['id_ami'].'-'.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'#'.$_GET['id_publication'].'');

			}
		}
		else
		{
			if(isset($_GET['page']))// TON PROFIL
			{
				header('Location: p'.$_GET['page'].'-id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'#'.$_GET['id_publication'].'');
			}
			else
			{
				header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'#'.$_GET['id_publication'].'');
			}
		}
	}
	
////////////////////////////////////////////////////////////////////////////////	
	if(isset($_GET['amis']))
	{
		$r1 = $bdd->prepare('SELECT id_jeu_ami FROM amis 
							WHERE id_jeu=:id_jeu 
							ORDER BY date_ajout DESC LIMIT 0,9')
							or die(print_r($bdd->errorInfo()));
		$r1->execute(array('id_jeu' => $donnees_jeu['id']))
							or die(print_r($bdd->errorInfo()));	
		$a1=0;
		while ($d1 = $r1->fetch())
		{
			$a1++;
		}
		
		$r3 = $bdd->prepare('SELECT id_jeu FROM amis 
							WHERE id_jeu_ami=:id_jeu_ami 
							ORDER BY date_ajout DESC LIMIT 0,9')
							or die(print_r($bdd->errorInfo()));
		$r3->execute(array( 'id_jeu_ami' => $donnees_jeu['id']))
							or die(print_r($bdd->errorInfo()));
		$a2=0;
		while($d3 = $r3->fetch())
		{
			$a2++;
		}
		if($a2 == 0 AND $a1 == 0)
			header('Location: id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'');
	}
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body>
<!--------------------- OVERLAY AMIS!---------------------->
<?php
if(isset($_GET['amis']))
{
?>
<div id="grand_overlay4"></div>
<div id="overlay4_clan">
<?php
if(isset($_GET['id'], $_GET['amis']) AND $_GET['id'] != $_SESSION['id_jeu'])
{
	echo'<a href="id'.$_GET['id'].'-'.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'">';
}
else
{
	echo'<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">';
}
	
?>

	<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
	</a>
	<p id="titre_overlay_membre">Les amis</p>
	<div id="sous_overlay4">
<?php

$tableau_amis =array();
$i = 0; 

if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
{
	$requete = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu 
							OR id_jeu_ami=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_GET['id']))
							or die(print_r($bdd->errorInfo()));
}
else
{
	$requete = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu 
							OR id_jeu_ami=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
}
while($donnees_amis = $requete->fetch()) 
{ 
	if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu']) 				// QUAND ON EST SUR LE PROFIL D'UN AUTRE //
	{
		if ($donnees_amis['id_jeu'] == $_GET['id'])
		{
			$tableau_amis[$i] = $donnees_amis['id_jeu_ami'];
			
			foreach($tableau_amis as $id_joueur)
			{
				$r_pseudo = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
				$r_pseudo->execute(array('id' => $id_joueur))
										or die(print_r($bdd->errorInfo()));
				while($d_pseudo = $r_pseudo->fetch()) 
				{ 
					//POUR POUVOIR CLASSER DANS L'ORDRE ALPHABETIQUE ///////////
					// ON MET LE PSEUDO EN PREMIER /////////////////////////////
					$to_display[$d_pseudo['pseudo']] = $d_pseudo;
					$to_display[$d_pseudo['pseudo']]['id'] = $id_joueur;
				}
			}
			$i++;
		}
		
		if ($donnees_amis['id_jeu_ami'] == $_GET['id'])
		{
			$tableau_amis[$i] = $donnees_amis['id_jeu'];
			
			foreach($tableau_amis as $id_joueur)
			{
				$r_pseudo = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
				$r_pseudo->execute(array('id' => $id_joueur))
										or die(print_r($bdd->errorInfo()));
				while($d_pseudo = $r_pseudo->fetch()) 
				{ 
					//POUR POUVOIR CLASSER DANS L'ORDRE ALPHABETIQUE ///////////
					// ON MET LE PSEUDO EN PREMIER /////////////////////////////
					$to_display[$d_pseudo['pseudo']] = $d_pseudo;
					$to_display[$d_pseudo['pseudo']]['id'] = $id_joueur;
				}
			}
			
			$i++;
		}
	}
	else
	{
		if ($donnees_amis['id_jeu'] == $_SESSION['id_jeu'])
		{
			$tableau_amis[$i] = $donnees_amis['id_jeu_ami'];
			
			foreach($tableau_amis as $id_joueur)
			{
				$r_pseudo = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
				$r_pseudo->execute(array('id' => $id_joueur))
										or die(print_r($bdd->errorInfo()));
				while($d_pseudo = $r_pseudo->fetch()) 
				{ 
					//POUR POUVOIR CLASSER DANS L'ORDRE ALPHABETIQUE ///////////
					// ON MET LE PSEUDO EN PREMIER /////////////////////////////
					$to_display[$d_pseudo['pseudo']] = $d_pseudo;
					$to_display[$d_pseudo['pseudo']]['id'] = $id_joueur;
				}
			}
			$i++;
		}
		
		if ($donnees_amis['id_jeu_ami'] == $_SESSION['id_jeu'])
		{
			$tableau_amis[$i] = $donnees_amis['id_jeu'];
			
			foreach($tableau_amis as $id_joueur)
			{
				$r_pseudo = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
				$r_pseudo->execute(array('id' => $id_joueur))
										or die(print_r($bdd->errorInfo()));
				while($d_pseudo = $r_pseudo->fetch()) 
				{ 
					//POUR POUVOIR CLASSER DANS L'ORDRE ALPHABETIQUE ///////////
					// ON MET LE PSEUDO EN PREMIER /////////////////////////////
					$to_display[$d_pseudo['pseudo']] = $d_pseudo;
					$to_display[$d_pseudo['pseudo']]['id'] = $id_joueur;
				}
			}
			
			$i++;
		}
	}

}

ksort($to_display);// classement

	
if(isset($_GET['p']) AND $_GET['p'] > 0)
	$fu = $_GET['p']-1;
else
	$fu = 0;

$to_display = array_slice ($to_display, $fu*20, 20, true);

foreach($to_display as $element)
{					
	$r2 = $bdd->prepare('SELECT nom_de_compte, photo_profil,pseudo,id, 
						nom_jeu, plateforme FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$r2->execute(array('id' => $element['id']))
						or die(print_r($bdd->errorInfo()));
	$d2 = $r2->fetch();	
echo'
	<div class="membre_forum">';
	if($d2['nom_de_compte'] != $_SESSION['nom_de_compte'])
		echo'<a  href="id'.$d2['id'].'-'.urlencode(stripslashes(htmlspecialchars($d2['pseudo']))).'" title="'.stripslashes(htmlspecialchars($d2['pseudo'])).'">';
	elseif($d2['id'] == $_SESSION['id_jeu'])
		echo'<span  title="Moi">';
	else
		echo'<span  title="Mon sous compte">';
	// ON L'ENTOURE AVEC LES BONNES CONDITIONS POUR LES LIENS //////////////////
	echo'
	<span class="pseudo_overlay4">
		'.stripslashes(htmlspecialchars($d2['pseudo'])).'
	</span>';
	////////////////////////////////////////////////////////////////////////////
	if($d2['nom_de_compte'] != $_SESSION['nom_de_compte'])
		echo'</a>';
	else
		echo'</span>';
		
	if(isset($_GET['id'],$_GET['amis']) AND $_GET['id'] == $_SESSION['id_jeu'])
	{
		echo '
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cet ami ?\');
		  if (sup == 0)return false;" href="profil.php?supprimer&id_ami='.$d2['id'].'&pseudo='.$d2['pseudo'].'">
			<div class="supprimer"> </div>
		</a>';
	}	
	echo'
	<div class="centreimg_overlay">';
	// POUR LES LIENS SI SOUS COMPTE OU PAS ////////////////////////////////////
	if($d2['nom_de_compte'] != $_SESSION['nom_de_compte'])
		echo'<a  href="id'.$d2['id'].'-'.urlencode(stripslashes(htmlspecialchars($d2['pseudo']))).'" title="'.stripslashes(htmlspecialchars($d2['pseudo'])).'">';
	elseif($d2['id'] == $_SESSION['id_jeu'])
		echo'<span  title="Moi">';
	else
		echo'<span  title="Mon sous compte">';
		
	if(isset($d2['photo_profil']) AND $d2['photo_profil'] != '' AND $d2['photo_profil'] != 0)
	{  
		$source = getimagesize('images_utilisateurs/'.$d2['photo_profil']); 	// La photo est la source
		if ($source[0] <= 60 AND $source[1] <= 60)
			echo '<img src="images_utilisateurs/'.$d2['photo_profil'].'" alt="Photo de profil" />';
		else
			echo '<img src="images_utilisateurs/mini_2_'.$d2['photo_profil'].'" alt="Photo de profil" />';
	}
	else
		echo '<img src="images/tete3.png" alt="Photo de profil" />';
	// POUR LES LIENS SI SOUS COMPTE OU PAS ////////////////////////////////////
	if($d2['nom_de_compte'] != $_SESSION['nom_de_compte'])
		echo'</a>';
	else
		echo'</span>';	
	echo'
	</div>';
	if(strlen($d2['nom_jeu']) > 15)
	{
		echo'
		<p class="information_membre" style="font-size:x-small;">'; 
		echo substr(stripslashes(htmlspecialchars($d2['nom_jeu'])), 0, 20).'';
		if(strlen(stripslashes(htmlspecialchars($d2['nom_jeu']))) > 20)
		{echo'...';} 
		echo'
		</p>';
	}
	else
	{
		echo'
		<p class="information_membre">
			'.stripslashes(htmlspecialchars($d2['nom_jeu'])).'
		</p>';
	}
	echo'
	<p class="information_membre">'.$d2['plateforme'].'</p>
	</div>';
}

?>					
	</div>
<?php

// GESTION AFFICHAGE PAGES /////////////////////////////////////////////////////

	$nbre_page = 1;
	$p1 = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM amis 
						WHERE id_jeu_ami=:id_jeu_ami')
						or die(print_r($bdd->errorInfo()));
	$p1->execute(array( 'id_jeu_ami' => $donnees_jeu['id']))
						or die(print_r($bdd->errorInfo()));
	$do1 = $p1->fetch();
	$p1->closeCursor();
	
	$p2 = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM amis 
					WHERE id_jeu=:id_jeu')
					or die(print_r($bdd->errorInfo()));
	$p2->execute(array('id_jeu' => $donnees_jeu['id']))
					or die(print_r($bdd->errorInfo()));	
	
	$do2 = $p2->fetch();
	$p2->closeCursor();
	$nbr_entrees = ($do1['nbre_publi']+$do2['nbre_publi']);

if (isset ($_GET['p']))
	$current_page = $_GET['p'];
else
	$current_page = 1;
	
$r_pseudo= $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu ')
						or die(print_r($bdd->errorInfo()));
$r_pseudo->execute(array('id_jeu' => $_GET['id']))
						or die(print_r($bdd->errorInfo()));
$d_pseudo = $r_pseudo->fetch();

$nom = ''.$d_pseudo['pseudo'].''; // PSEUDO ou NOM de la TEAM
$suite_page = 'amis';
$nbr_affichage = 20;

include('pagination_pseudo_overlay.php');
?>
</div><!------------------ FIN OVERLAY AMIS!------------------->	
<?php
}	
if (isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])	
{
?>
	<div id="grand_overlay1"></div><!-------------- BLOC MESSAGE --------------> 
	<div id="overlay_envoyer_message">
		<a id="fermer" href="#">
			<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
		</a>
		<p id="titre_overlay_message">Envoyer un message</p>
		<form action="message_post.php" method="post">
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;"> 
			destinataire : 
			<?php
			echo''.stripslashes(htmlspecialchars($donnees_jeu['pseudo'])).''
			?>
			<textarea id="input_envoyer_message" rows="" cols="" name="message" style="color:#102c3c;font-size:1.2em;font-weight:bolder;"></textarea><br />
			<input type="hidden" name="destinataire" value="<?php echo$donnees_jeu['id']; ?>"/>
			<input type="hidden" name="pseudo" value="<?php echo$donnees_jeu['pseudo']; ?>"/>
			<input type="submit" id="envoyer_profil" name="Valider" value="Envoyer"/>
		</p>
		</form>
	</div><!----------------------- FIN BLOC MESSAGE !-------------------------> 	
 <?php 
} 
	else echo '<div id="fermer"></div>';
    echo'<div id="overlay"></div>
	     <div id="contient_image_profil"></div>'; // SEPARER LES DEUX BLOCS POUR EVITER L'INCOMPREHENSION DU NAVIGATEUR 
if (isset($_GET['watch_video']))
{		 
	echo'<div id="grand_overlay_videos">';
	if (isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
	{
		echo '
		<a href="id'.$_GET['id'].'-'.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'">
			<img id="fermer_message" src="images/quitter_video.png" alt="Fermer"/>
		</a>';
	}
	else
	{
		echo'
		<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">
			<img id="fermer_message" src="images/quitter_video.png" alt=" Fermer " />
		</a>';
	}
	echo'</div>';
	echo'<div id="overlay2_videos" style="display:block">
			<object type="application/x-shockwave-flash" width="700" height="450" data="http://www.youtube.com/v/'.stripslashes(htmlspecialchars($_GET['watch_video'])).'&hl=fr">							
				<param name="wmode" value="transparent" />
				<param name="allowFullScreen" value="true" />
			</object> ';
	echo'</div>';
}					

include('menu_sans_doctype.php');
echo'
<div id="corps_profil">';
    
	echo'
   <div id="sous_corps1_profil">
	<div id="infos_droite">';
	
	// POUR SAVOIR SI ADMIN AVEC ID OU SANS ID DANS L'URL
	if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
	{	
		$r_joueur = $bdd->prepare('SELECT nom_de_compte FROM jeu
								WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$r_joueur->execute(array('id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));
		$d_joueur = $r_joueur->fetch();
		$r_admin = $bdd->prepare('SELECT admin FROM membres 
								WHERE nom_de_compte=:nom_de_compte')
								or die(print_r($bdd->errorInfo()));
		$r_admin->execute(array('nom_de_compte' => $d_joueur['nom_de_compte']))
								or die(print_r($bdd->errorInfo()));
		$d_admin = $r_admin->fetch();
	}
	else
	{
		$r_admin = $bdd->prepare('SELECT admin FROM membres 
								WHERE nom_de_compte=:nom_de_compte')
								or die(print_r($bdd->errorInfo()));
		$r_admin->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))
								or die(print_r($bdd->errorInfo()));
		$d_admin = $r_admin->fetch();
	}
	
	if($d_admin['admin'] == 'oui')
	{
		echo'
		<img src="images/titre_admin.png" alt=" ">';
	}
	else
	{
		echo'
		<span title="Prochainement"><img src="images/titre.png" alt=" "></span>';
	}
		echo'
		<div id="info_profil">   

		<p id="jeu_profil"> 
			Jeu :<br /><span class="fond1_info_profil"> '; 
			echo substr(stripslashes(htmlspecialchars($donnees_jeu['nom_jeu'])), 0, 20).'';
			if(strlen(stripslashes(htmlspecialchars($donnees_jeu['nom_jeu']))) > 19)
			{ echo'...';}
			echo'</span>
		</p>';

	echo'
		<p>
			<span style="font-size:small; font-weight:bolder;">
				Plateforme :
			</span> 
			<br />
			<span class="fond2_info_profil">
				'.$donnees_jeu['plateforme'].'
			</span>
		</p>
		
		<p>
			<span style="font-size:small; font-weight:bolder;">Pays :</span> 
			<br />
			<span class="fond2_info_profil">'; 
				echo substr(stripslashes(htmlspecialchars($donnees['pays'])), 0, 20).'';
				if(strlen(stripslashes(htmlspecialchars($donnees['pays']))) > 20){ echo'...';} echo'
			</span>
		</p>
		
		<p>
			<span style="font-size:small; font-weight:bolder;">Ville :</span> 
			<br />
			<span class="fond2_info_profil">'; 
				echo substr(stripslashes(htmlspecialchars($donnees['ville'])), 0, 20).'';
				if(strlen(stripslashes(htmlspecialchars($donnees['ville']))) > 20){ echo'...';} echo'
			</span>
		</p>
		
		<p>
			<span style="font-size:small; font-weight:bolder;">Sexe :</span> 
			<br />
			<span class="fond2_info_profil">'.$donnees['sexe'].'</span>
		</p>';
		
	   echo'	
		</div>
	</div>';
	echo '
	<div id="bloc_pseudo_profil">';	
	
	if(isset($donnees['site_web']) AND $donnees['site_web'] != '')
	{
		echo'	
		<p id="liens_chaine">
			<a href="'.stripslashes(htmlspecialchars($donnees['site_web'])).'" target="_blank">
				<img id="liens_chaine_image" src="images/site_web.png" 
				alt="Site Web" title="Site Web">
			</a>
		</p>';
	}
	if(isset($donnees['daylimotion']) AND $donnees['daylimotion'] != '')
	{
		echo'	
		<p id="liens_chaine">
			<a href="'.stripslashes(htmlspecialchars($donnees['daylimotion'])).'" target="_blank">
				<img id="liens_chaine_image" src="images/daylimotion.png" 
				alt="Daylimotion" title="Chaine Daylimotion">
			</a>
		</p>';
	}
	if(isset($donnees['youtube']) AND $donnees['youtube'] != '')
	{
		echo'	
		<p id="liens_chaine">
			<a href="'.stripslashes(htmlspecialchars($donnees['youtube'])).'" target="_blank">
				<img id="liens_chaine_image" src="images/you_tube1.png" 
				alt="YouTube" title="Chaine YouTube">
			</a>
		</p>';
	}
	if(isset($donnees['twitch']) AND $donnees['twitch'] != '')
	{
		echo'	
		<p id="liens_chaine">
			<a href="'.stripslashes(htmlspecialchars($donnees['twitch'])).'" target="_blank">
				<img id="liens_chaine_image" src="images/twitch.png" 
				alt="Twitch" title="Chaine Twitch">
			</a>
		</p>';
	}
	
	echo '
	<span id="pseudo_profil">
		'.stripslashes(htmlspecialchars($donnees_jeu['pseudo'])).'
	</span>	
	</div>';	
	
	if ($donnees_jeu['photo_profil'] != 0)
	{
		$source = getimagesize('images_utilisateurs/'.$donnees_jeu['photo_profil']); // La photo est la source
		if ($source[0] <= 900 AND $source[1] <= 600)
			echo '<a href="images_utilisateurs/'.$donnees_jeu['photo_profil'].'" title="Afficher l\'image originale" >';
		else
			echo '<a href="images_utilisateurs/grand_'.$donnees_jeu['photo_profil'].'" title="Afficher l\'image originale" >';
		echo'<div class="centre_image200">';
		
		if ($source[0] <= 200 AND $source[1] <= 200)
			echo'<img class="photo_profil"  src="images_utilisateurs/'.$donnees_jeu['photo_profil'].'" alt="Photo de profil" />';
		else
			echo'<img class="photo_profil"  src="images_utilisateurs/mini_3_'.$donnees_jeu['photo_profil'].'" alt="Photo de profil" />';
		echo'</div></a>';
	}
	else 
	{
		echo'
		<div class="photo_profil2">
			<img src="images/tete1.png" alt="Photo de profil"/>
		</div>';
	}

	// CLAN
	echo'<div id="bloc_clan">';
	if (isset($donnees_jeu['id_clan']) AND $donnees_jeu['id_clan'] != '' 
	AND $donnees_jeu['id_clan'] != 0)
	{ 
		// On prend le nom Clan de mon compte et des autres quand je vais sur 
		// leur profil
		$reqs2 = $bdd->prepare('SELECT nom_clan FROM clan WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$reqs2->execute(array('id' => $donnees_jeu['id_clan']))
								or die(print_r($bdd->errorInfo()));
		$donnees2 = $reqs2->fetch();
		
		// Prendre notre id_clan pour le comparer � celui de nos amis //////////
		$reqs3 = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$reqs3->execute(array('id' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
		$session_idclan = $reqs3->fetch();
		
		if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'] 
		AND $donnees_jeu['id_clan'] != $session_idclan['id_clan'])
		{
			echo'
			<span id="clan_profil">
				<img id="embleme_clan" src="images/clan_profil.png" alt=" "/>
				<a href="team-i'.$donnees_jeu['id_clan'].'.html">
					'.stripslashes(htmlspecialchars($donnees2['nom_clan'])).'
				</a>
			</span>';
		}
		// SI ON EST SUR LE PROFIL D'UN AMI ET QU'IL EST DANS LA MEME TEAM /////
		elseif(isset($_GET['id'])  AND $_GET['id'] != $_SESSION['id_jeu'] 		
		AND $donnees_jeu['id_clan'] == $session_idclan['id_clan'])
		{
			echo'
			<span id="clan_profil">
				<img id="embleme_clan" src="images/clan_profil.png" alt=" "/>
				<a href="team.html">
				'.stripslashes(htmlspecialchars($donnees2['nom_clan'])).'
				</a>
			</span>';
		}
		else
		{
			echo'
			<span id="clan_profil">
				<img id="embleme_clan" src="images/clan_profil.png" alt=" "/>
				<a href="team.html">
					'.stripslashes(htmlspecialchars($donnees2['nom_clan'])).'
				</a>
			</span>';
		}
		
		$requt1 = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$requt1->execute(array('id_jeu' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));
		$donnees_clan1 = $requt1->fetch();
		$requt2 = $bdd->prepare('SELECT * FROM clan WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$requt2->execute(array('id' => $donnees_clan1['id_clan']))
								or die(print_r($bdd->errorInfo()));
		$donnees_clan2 = $requt2->fetch();
		$_SESSION['statut'] = 'membre';

		// L'ID est dans tous les URL 
		if($donnees_clan2['id_jeu'] == $_GET['id'])
		{
			$_SESSION['statut'] = 'meneur';
		}
		
		if ($_SESSION['statut'] == 'meneur' 
		AND $_SESSION['statut'] != 'membre')
		{	
			echo'<img id="meneur_profil"src="images/meneur_profil.png" alt="Meneur" />';
		}
	}
	else
	{
		echo'
		<span id="clan_profil">
			<img id="embleme_clan" src="images/clan_profil.png" alt=" "/> 
			Aucune Team
		</span>';
	}
    echo'</div>         
			  
			  
	<div id="geth_profil">
		<a href="classement.html">
			<img id="geth2_profil" src="images/geth2.png" alt=" "/> 
			'.$donnees_jeu['geth'].'
		</a>	
	</div>';	
		
    if($d_admin['admin'] == 'oui')
	{
		echo'
		<div id="embleme_profil">
			<span title="Prochainement">
				<img class="embleme1" src="images/embleme1_admin.png" alt="Embleme"/>
			</span>
			<span title="Prochainement">
				<img class="embleme1" src="images/embleme2_admin.png" alt="Embleme"/>
			</span>
			<span title="Prochainement">
				<img class="embleme1" src="images/embleme3_admin.png" alt="Embleme"/>
			</span>
			<span title="Prochainement">
				<img class="embleme1" src="images/embleme4_admin.png" alt="Embleme"/>
			</span> 
		</div>';
	}
	else
	{
		$resu = $bdd->query('SELECT nom_de_compte FROM membres LIMIT 0,102') 
		or die(print_r($bdd->errorInfo()));	
		while($resultat = $resu->fetch())
		{
			if ($resultat['nom_de_compte'] == $donnees_jeu['nom_de_compte'])
				$embleme_bienvenue=1;
		}
		echo'<div id="embleme_profil">
			<span title="Novice">
				<img class="embleme1" src="images/embleme2.png" alt="Embleme"/>
			</span>';			
		if (isset($embleme_bienvenue) AND $embleme_bienvenue == 1)
		{
			echo '<span title="Embl�me de bienvenue">
				<img class="embleme1" src="images/embleme_bienvenue.png" alt="Embleme"/>
			</span>';
		}
		else
		{
			echo '<span title="Prochainement">
				<img class="embleme1" src="images/embleme1.png" alt="Embleme"/>
			</span>';
		}
			echo '<span title="Prochainement">
				<img class="embleme1" src="images/embleme1.png" alt="Embleme"/>
			</span>
			<span title="Prochainement">
				<img class="embleme1" src="images/embleme1.png" alt="Embleme"/>
			</span> 
		</div>';
	}
	
$i=0; // SAVOIR SI ON AFFICHE OU PAS EN FONCTION DU NOMBRE DE SOUS COMPTE 
$reqsa = $bdd->prepare('SELECT * FROM jeu 
						WHERE nom_de_compte=:nom_de_compte')
						or die(print_r($bdd->errorInfo()));
$reqsa->execute(array('nom_de_compte' => $donnees['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));
while($donnees_bloc = $reqsa->fetch())
{
	if ($i > 0)
	{
		echo'
		<div id="sous_compte_profil">
		Sous compte(s) li�s :';

		$reqs = $bdd->prepare('SELECT * FROM jeu 
								WHERE nom_de_compte=:nom_de_compte')
								or die(print_r($bdd->errorInfo()));
		$reqs->execute(array('nom_de_compte' => $donnees['nom_de_compte']))
								or die(print_r($bdd->errorInfo()));
		while ($donnees_sous = $reqs->fetch())
		{ 
			if ($donnees_sous['id'] != $donnees_jeu['id'])
			{
				if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
					echo'<a href="id'.$donnees_sous['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_sous['pseudo']))).'" title="'.stripslashes(htmlspecialchars($donnees_sous['pseudo'])).'">';
				else
					echo'<span title="Mon sous compte">';
					
				if(isset($donnees_sous['photo_profil']) 
				AND $donnees_sous['photo_profil'] != '' 
				AND $donnees_sous['photo_profil'] != 0)
				{
					$source = getimagesize('images_utilisateurs/'.$donnees_sous['photo_profil']); // La photo est la source
					if ($source[0] <= 60 AND $source[1] <= 60)
						echo '<img class="photo_profil_sous" src="images_utilisateurs/'.$donnees_sous['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img class="photo_profil_sous" src="images_utilisateurs/mini_2_'.$donnees_sous['photo_profil'].'" alt="Photo de profil" />';
				}
				else
					echo '<img class="photo_profil_sous" src="images/tete3.png" alt="Photo de profil" />';
							
				if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
					echo'</a>';
				else
					echo'</span>';	
			}
		}

		echo'
		</div>';
	break;
	}
	$i++;
}
	
	echo'
	</div>';
	
if(isset($_GET['erreur_image']) AND $_GET['erreur_image'] == 0)
{
	echo'
	<p class="bloc_erreur">
		<img src="images/attention.png" alt="erreur"> 
		Image non valide.
		(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
	</p>';
}
if(isset($_GET['erreur_image']) AND $_GET['erreur_image'] == 1)
{
	echo'
	<p class="bloc_erreur">
		<img src="images/attention.png" alt="erreur"> 
		Image non valide.
		(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
	</p>';
} 
	
if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu']) 					// SUR LE PROFIL D'UN AUTRE
{
	$requete = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu 
							AND id_jeu_ami=:id_jeu_ami')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_GET['id'], 
							'id_jeu_ami' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$donnees_amis = $requete->fetch();	
	
	$requete2 = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu_ami 
							AND id_jeu_ami=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$requete2->execute(array('id_jeu' => $_GET['id'], 
							'id_jeu_ami' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$donnees_amis2 = $requete2->fetch(); 

if ($donnees_amis['id_jeu'] != '' OR $donnees_amis2['id_jeu'] != '') 		// CONDITION POUR AFFICHER LE PROFIL SI ON EST AMI
{
?>
<!--[if gte IE 6]>
	<style type="text/css">
		.champs_publication
		{
			height:75px;
		}
	</style>
<![endif]-->
  <!-- PUBLICATION ! -->
	<div class="bbcode_css">
		<script type="text/javascript" src="bbcode.js"></script>
		<a href="#">
			<img src="images/bbcode/gras.gif" alt="gif" title="Gras"
			onclick="javascript:bbcode('[g]', '[/g]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/italique.gif" alt="gif" title="Italique" 
			onclick="javascript:bbcode('[i]', '[/i]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/souligner.gif" alt="gif" title="Souligner"
			onclick="javascript:bbcode('[s]', '[/s]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/image.gif" alt="gif" title="Image" 
			onclick="javascript:bbcode('[img]', '[/img]'); return(false)"/>
		</a>
		<!-- fin bbcode -->	
	</div>
	<div id="debut_publication"> 
	<form id="formulaire" action="profil_post.php" method="post" enctype="multipart/form-data">
		<div id="contient_champs_publication">
			<textarea type="text" id="publication" class="champs_publication" name="publication" placeholder=" Publiez sur cette page, partagez vos exp�riences... " style="font-weight:bolder;" ></textarea>
			<textarea type="text" id="you_tube" class="champs_you_tube" name="you_tube" title="Inserez votre lien You Tube" placeholder=" Inserez votre lien You Tube" style="font-weight:bolder;" ></textarea>
			<div id="contient_publier">
				<input type="submit" name="publiez" value="Publiez" class="publier"/>
			</div>
		</div>
		
		<!-- VIDEOS! -->
		<span title="Prochainement ! Vous pouvez publiez vos vid�os You Tube.">
			<div id="conteneurfile">
			<!-- <input type="file" name="videos" onmousedown="return false" 
				onkeydown="return false" class="inputfile" 
				onchange="displayfilename(this);" /> -->  
			</div>
		</span>
		<!-- PHOTOS -->
		<div id="conteneurfile2">
			<input type="file" name="photos" onmousedown="return false" 
			onkeydown="return false" class="inputfile" 
			onchange="displayfilename(this);" />   
		</div>	
		<div id="there"></div>
	</div>	
	<input type="hidden" name="lieu_publication" value="<?php echo''.$donnees['nom_de_compte'].''; ?>" />
	<input type="hidden" name="lieu_publication_id_jeu" value="<?php echo $donnees_jeu['id']; ?>" />
	<input type="hidden" name="pseudo" value="<?php echo $_GET['pseudo']; ?>" />
	<input type="hidden" name="id_ami" value="<?php echo $_GET['id']; ?>" />
	<input type="hidden" name="profil_ou_clan" value="profil" />
<?php      	
echo'</form>		

<div id="fond_profil_right"></div>';
	
}
else
{
	echo' 
	<div id="profil_pas_ami">
		<img id="pas_ami_profil" src="images/pas_ami_profil.png" alt="Devenez ami avec ce joueur et publiez sur son profil !" />
	</div>';
}
	
	
}
else
{
?>
<!--[if gte IE 6]>
	<style type="text/css">
		.champs_publication
		{
			width:597px;
			height:74px;
		}
		.champs_you_tube
		{
			width:597px;
		}
	</style>
<![endif]-->
<!-- PUBLICATION ! -->
	<div class="bbcode_css">
		<script type="text/javascript" src="bbcode.js"></script>
		<a href="#">
			<img src="images/bbcode/gras.gif" alt="gif" title="Gras"
			onclick="javascript:bbcode('[g]', '[/g]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/italique.gif" alt="gif" title="Italique" 
			onclick="javascript:bbcode('[i]', '[/i]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/souligner.gif" alt="gif" title="Souligner"
			onclick="javascript:bbcode('[s]', '[/s]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/image.gif" alt="gif" title="Image" 
			onclick="javascript:bbcode('[img]', '[/img]'); return(false)"/>
		</a>
		<!-- fin bbcode -->	
	</div>
	<div id="debut_publication"> 
	<form id="formulaire" action="profil_post.php" method="post" enctype="multipart/form-data">
		<div id="contient_champs_publication">
			<textarea type="text" id="publication" cols="" rows="" class="champs_publication" name="publication" placeholder=" Publiez sur cette page, partagez vos exp�riences... " style="font-weight:bolder;" ></textarea>
			<textarea type="text" id="you_tube" cols="" rows="" class="champs_you_tube" name="you_tube" title="Inserez votre lien You Tube" placeholder=" Inserez votre lien You Tube" style="font-size:small; font-weight:bolder;" ></textarea>
			<div id="contient_publier">
				<input type="submit" id="publier" name="publiez" value="Publiez" class="publier"/>
			</div>
		</div>
		
		<!-- VIDEOS! -->
		<span title="Prochainement ! Vous pouvez publiez vos vid�os You Tube.">
			<div id="conteneurfile">
			<!-- <input type="file" name="videos" onmousedown="return false" 
				onkeydown="return false" class="inputfile" 
				onchange="displayfilename(this);" /> -->  
			</div>
		</span>
		<!-- PHOTOS -->
		<div id="conteneurfile2">
			<input type="file" name="photos" onmousedown="return false" 
			onkeydown="return false" class="inputfile" 
			onchange="displayfilename(this);" />   
		</div>	
		<div id="there"></div>
	</div>	
	<input type="hidden" name="lieu_publication" value="<?php echo''.$donnees['nom_de_compte'].''; ?>" />
	<input type="hidden" name="lieu_publication_id_jeu" value="<?php echo$donnees_jeu['id']; ?>" />
	<input type="hidden" name="pseudo" value="<?php echo $_GET['pseudo']; ?>" />
	<input type="hidden" name="profil_ou_clan" value="profil" />
<?php  
if (isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
	echo'<input type="hidden" name="id" value="'.$_GET['id'].'"/>';
	 		

echo'</form>		
<div id="fond_profil_right"></div>';
}
?>
  
<div id="sous_corps2_profil">
  
<?php
if(isset($donnees['twitch']) AND $donnees['twitch'] != '' 
AND strstr($donnees['twitch'], 'www.twitch.tv') 
OR strstr($donnees['twitch'], 'fr.twitch.tv'))
{

	echo'
	<div id="bloc_stream">
		<img style="border-bottom:2px solid #102c3c;" src="images/twitch_video.png" alt="Twitch TV" />';

		function getStream ($lien) 
		{
			if (strstr($lien, 'www.twitch.tv')) 
			{
				$lien = str_replace('http://www.twitch.tv/', '', $lien) ;
				if (!empty($lien))
				{
				 return '<object type="application/x-shockwave-flash" height="225" width="300" id="live_embed_player_flash"
				data="http://www.twitch.tv/widgets/live_embed_player.swf?channel='.$lien.'" bgcolor="#000000">
				<param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" />
				<param name="allowNetworking" value="all" /><param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" />
				<param name="flashvars" value="hostname=www.twitch.tv&channel='.$lien.'&auto_play=true&start_volume=25" />
				</object>' ;
				}
				else
					return 'Impossible d\'afficher un lecteur' ;
			}
			elseif (strstr($lien, 'fr.twitch.tv')) 
			{
				$lien = str_replace('http://fr.twitch.tv/', '', $lien) ;
				if (!empty($lien))
				{
				 return '<object type="application/x-shockwave-flash" height="225" width="300" id="live_embed_player_flash"
				data="http://www.twitch.tv/widgets/live_embed_player.swf?channel='.$lien.'" bgcolor="#000000">
				<param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" />
				<param name="allowNetworking" value="all" /><param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" />
				<param name="flashvars" value="hostname=www.twitch.tv&channel='.$lien.'&auto_play=true&start_volume=25" />
				</object>' ;
				}
				else
					return 'Impossible d\'afficher un lecteur' ;
			}
			else
				return 'Impossible d\'afficher un lecteur : '.$lien ;
		}

	$lien = $donnees['twitch']; 
	echo'<a href="'.$lien.'" target="_blank">'.getStream($lien).'</a>

	</div>';

}
// BLOC AJOUTER AMI, MESSAGE, CHERCHER AMI /////////////////////////////////////

// conditions pour afficher le bouton "ajouter ami" et message	
if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
{
	
	echo'
	<div id="message_ajouter_profil">';
		
		
	$requete = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu 
							AND id_jeu_ami=:id_jeu_ami')
							or die(print_r($bdd->errorInfo()));			
	$requete->execute(array('id_jeu' => $_GET['id'], 
							'id_jeu_ami' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));			
	$donnees_amis = $requete->fetch();		
			
	$requete2 = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu_ami 
							AND id_jeu_ami=:id_jeu')
							or die(print_r($bdd->errorInfo()));		
	$requete2->execute(array('id_jeu' => $_GET['id'], 
							'id_jeu_ami' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));		
	$donnees_amis2 = $requete2->fetch();
			
	$requete3 = $bdd->prepare('SELECT * FROM demande_amis 
							WHERE id_jeu_demandeur=:id_jeu_ami')
							or die(print_r($bdd->errorInfo()));	
	$requete3->execute(array('id_jeu_ami' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	while($donnees_amis3 = $requete3->fetch())	
	{			
		if ($donnees_amis3['id_jeu'] == $_GET['id'])				
		{						
			echo'
			<div id="en_attente_profil">
				<p id="en_attente_profil2">En attente</p>
			</div>';			
			$i = 0;
		}
			
	}
		
	if ($donnees_amis['id_jeu'] == '' AND $donnees_amis2['id_jeu'] == '' 
	AND isset($i) AND $i !=0)
	{			
		echo'
		<a href="recherche.php?ami_id_jeu='.$_GET['id'].'">
			<div id="ajouter_profil" title=" Ajouter comme ami(e) "></div>
		</a>';
	}		
	elseif (isset($i) AND $i !=0)
	{			
		echo'
		<div id="en_attente_profil">
			<p id="en_attente_profil2">Ami(e)</p>
		</div>';
	}		
	if(isset($_GET['send']))
	{			
		echo'
		<a href="" id="envoyer_message">
			<div id="message_profil_bon" title=" Envoyer un message " ></div>
		</a>';
	}		
	elseif(isset($_GET['erreur']))
	{			
		echo'
		<a href="" id="envoyer_message">
			<div id="message_profil_erreur" title=" Envoyer un message " ></div>
		</a>';
	}		
	else
	{			
		echo'
		<a href="" id="envoyer_message">
			<div id="message_profil" title=" Envoyer un message " ></div>
		</a>';
	}	
		
	echo'
		</div>'; 
	
}	
else	
{  
	echo'
	<a href="recherche.html?recherche=">
		<div id="chercher_joueur_profil"></div>
	</a>';	
}
// FIN BLOC AJOUTER AMI , MESSAGE , CHERCHER AMI ///////////////////////////////

if(isset($donnees_jeu['description']) AND $donnees_jeu['description'] != '' )
{
?>  
	<div id="bloc_description">
		<img src="images/description_profil.png" alt=" Description " />
<?php
		$message1 = nl2br(stripslashes(htmlspecialchars($donnees_jeu['description'])));
		$message1 = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message1);
		if (preg_match("#http://(.+)#isU", $message1))
		{
			$message1 = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message1);					
		}
		elseif (preg_match("#https://(.+)#isU", $message1))
		{
			$message1 = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message1);
		}
		else
		{
			$message1 = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$message1);
		}
		echo '<p id="description_profil">'.$message1.'</p>
	</div>';

}

	// BLOC AMIS ///////////////////////////////////////////////////////////////
	$i=0;
	$requete = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu 
						OR id_jeu_ami=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $donnees_jeu['id']))
							or die(print_r($bdd->errorInfo()));
	while($donnees_amis = $requete->fetch()) 
	{ 
		if ($donnees_amis['id_jeu'] == $donnees_jeu['id'])
		{
			$tableau_amis[$i] = $donnees_amis['id_jeu_ami'];
			$i++;
		}
		
		if ($donnees_amis['id_jeu_ami'] == $donnees_jeu['id'])
		{
			$tableau_amis[$i] = $donnees_amis['id_jeu'];
			$i++;
		}

	}
	
	if($i > 0)
		echo'<div id="bloc_amis2">';
	else
		echo'<div id="bloc_amis">';
	
	echo'
		<div id="amis_profil">
			<span class="nbr_profil">
				( '.$i.' )
			</span>
		</div>';
if(isset($tableau_amis))
{	
	shuffle($tableau_amis); // classement
	$fu = 0;
	$tableau_amis = array_slice ($tableau_amis, $fu*8, 8, true);
	
	foreach($tableau_amis as $element)
	{
		$requetes = $bdd->prepare('SELECT photo_profil,pseudo,id FROM jeu 
								WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$requetes->execute(array('id' => $element))
								or die(print_r($bdd->errorInfo()));
		$donn_ami = $requetes->fetch();
		
		echo'<div class="centre_image_bloc_amis">';
		
		if ($donn_ami['photo_profil'] != 0 AND $donn_ami['photo_profil'] != '')
		{	
			echo '
			<a href="id'.$donn_ami['id'].'-'.urlencode(stripslashes(htmlspecialchars($donn_ami['pseudo']))).'" title="'.stripslashes(htmlspecialchars($donn_ami['pseudo'])).'">';
			$source = getimagesize('images_utilisateurs/'.$donn_ami['photo_profil']); // La photo est la source
			if ($source[0] <= 60 AND $source[1] <= 60)
			{			
				echo'
				<img src="images_utilisateurs/'.$donn_ami['photo_profil'].'" alt="Photo de profil" />
				<br/>
			</a>';
			}
			else
			{
				echo'
				<img src="images_utilisateurs/mini_2_'.$donn_ami['photo_profil'].'" alt="Photo de profil" />
				<br/>
			</a>';
			}
		}
		else
		{	
			echo'
			<a href="id'.$donn_ami['id'].'-'.urlencode(stripslashes(htmlspecialchars($donn_ami['pseudo']))).'" title="'.stripslashes(htmlspecialchars($donn_ami['pseudo'])).'">
				<img src="images/tete3.png" alt="Photo de profil" />
				<br/>
			</a>';			
		}
		
		echo'</div>';
	}
}
	
	if($i == 0)
		echo'<img  src="images/pas_ami.png" alt=" Aucun ami ajout� " />';
		
	if($i > 0)
	{
		if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
		{
			echo'
			<a href="amis-id'.$_GET['id'].'-'.urlencode(stripslashes(htmlspecialchars($_GET['pseudo']))).'">
				<div class="afficher_tout" title="Afficher tous les amis" ></div>
			</a>';
		}
		else
		{
			echo'
			<a href="amis-id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'">
				<div class="afficher_tout" title="Afficher tous les amis" ></div>
			</a>';
		}
	}
?> 	
    </div>
	<!-- FIN BLOC AMIS !-------------------------------------------------------> 
   
   
	<!-- BLOC IMAGES !---------------------------------------------------------> 
<?php
	$req_boucle = $bdd->prepare('SELECT adresse_stockage FROM photos 
								WHERE id_jeu=:id_jeu 
								ORDER BY date_enregistrement DESC LIMIT 0,9 ')
								or die(print_r($bdd->errorInfo()));
	$req_boucle->execute(array('id_jeu' => $donnees_jeu['id']))
								or die(print_r($bdd->errorInfo()));
	$i=0;
	while ($donneees2 = $req_boucle->fetch()) // on affiche message par message
	{
		$i++;
	}
	
	if($i>0)
		echo'<div id="bloc_images2">';
	else
      	echo'<div id="bloc_images">';  
	
	echo'
		<div id="images_profil">
			<span class="nbr_profil" style="margin-left:193px;">
				( '.$i.' )
			</span>
		</div>';
		
	$req = $bdd->prepare('SELECT adresse_stockage FROM photos 
						WHERE id_jeu=:id_jeu 
						ORDER BY date_enregistrement DESC LIMIT 0,8 ')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $donnees_jeu['id']))
						or die(print_r($bdd->errorInfo()));
	while ($donneees2 = $req->fetch()) // on affiche message par message
	{
		if ($donneees2['adresse_stockage'] != 0)
		{
			$source = getimagesize('images_utilisateurs/'.$donneees2['adresse_stockage']); // La photo est la source
			if($source[0] <= 900 AND $source[1] <= 600)
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donneees2['adresse_stockage'].'">';
			else
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$donneees2['adresse_stockage'].'">';
?>
	<!--[if gte IE 6]>
		<style type="text/css">
			.centre_image_bloc_images
			{
				width:0px;
				height:0px;
				margin-left:50px;
				margin-top:60px;
			}
			.centre_image_bloc_images img
			{
				margin-top:-90px;
				margin-left:-30px;
			}
		</style>
	<![endif]-->
<?php	
				echo'<div class="centre_image_bloc_images">';
				//[if IE 6]><span class="tampon"></span><![endif]
				if ($source[0] <= 60 AND $source[1] <= 60)
					echo '<img src="images_utilisateurs/'.$donnees2['adresse_stockage'].'" alt="image"/></span></a>'; 
				else		
					echo'<img src="images_utilisateurs/mini_2_'.$donneees2['adresse_stockage'].'"  alt="image"/></a>';
				echo'</div>';
			}
	}
	
	if($i==0)
		echo'<img src="images/pas_image.png"  alt=" Aucune image "/>';
		
	if($i>0)
	{
		echo'
		<a href="images-id'.$donnees_jeu['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_jeu['pseudo']))).'"> 
			<div class="afficher_tout" title="Afficher toutes les images" ></div>
		</a>';
	}
?>
	  
    </div>
	<!-- FIN BLOC IMAGES !-----------------------------------------------------> 
   
   
	<!-- BLOC VIDEOS !---------------------------------------------------------> 
<?php
	$req55 = $bdd->prepare('SELECT adresse_stockage FROM videos
							WHERE id_jeu=:id_jeu ORDER BY id DESC LIMIT 0,6')
							or die(print_r($bdd->errorInfo()));
	$req55->execute(array('id_jeu' => $donnees_jeu['id']))
							or die(print_r($bdd->errorInfo()));
	$v = 0;
	while ($donnees55 = $req55->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
	{
		$v++;
	}
	
	if($v>0)
		echo'<div id="bloc_videos2"> ';
	else
		echo'<div id="bloc_videos"> ';
		
	echo'
		<div id="videos_profil">
			<span class="nbr_profil" style="margin-left:193px;">
				( '.$v.' )
			</span>
		</div>';
		
	$req55 = $bdd->prepare('SELECT adresse_stockage FROM videos
							WHERE id_jeu=:id_jeu ORDER BY id DESC LIMIT 0,5')
							or die(print_r($bdd->errorInfo()));
	$req55->execute(array('id_jeu' => $donnees_jeu['id']))
							or die(print_r($bdd->errorInfo()));
	$v = 0;
	while ($donnees55 = $req55->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
	{
		$video = stripslashes(htmlspecialchars($donnees55['adresse_stockage']));
		if (isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
		{
			$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i',
			'<a title="Voir la video"  href="profil.php?watch_video=$1&id='.$_GET['id'].'&pseudo='.$_GET['pseudo'].'">
				<p class="petite_videos"> 
					<img style="width:75px;height:55px;" src="http://i1.ytimg.com/vi/$1/default.jpg"  alt="video youtube"/>
				</p>
			</a>',$video);	
			$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
		}
		else
		{
			$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i',
			'<a title="Voir la video"  href="profil.html?watch_video=$1&id='.$_SESSION['id_jeu'].'&pseudo='.$_SESSION['pseudo'].'">
				<p class="petite_videos"> 
					<img style="width:75px;height:55px;" src="http://i1.ytimg.com/vi/$1/default.jpg"  alt="video youtube"/>
				</p>
			</a>',$video);		
			$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
		}
		
		echo $video;	
		$v++;	
	 }
	 	if($v==0)
			echo'<img src="images/pas_video.png"  alt=" Aucune image "/>';
			
		if($v>0)
		{
			echo'
			<a href="videos-id'.$donnees_jeu['id'].'-'.urlencode(stripslashes(htmlspecialchars($donnees_jeu['pseudo']))).'"> 
				<div class="afficher_tout" title="Afficher toutes les vid�os" style="margin-top:28px;" >
				</div>
			</a>';
		}
			
	 ?>
    </div>
	<!-- FIN BLOC VIDEOS !-----------------------------------------------------> 

  </div> 	
<?php
if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
{
	$req = $bdd->prepare('SELECT COUNT(*) AS nbre FROM publication 
						WHERE lieu_publication_id_jeu=:lieu_publication_id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('lieu_publication_id_jeu' => $_GET['id']))
						or die(print_r($bdd->errorInfo()));
	$do = $req->fetch();
}
else
{
	$req = $bdd->prepare('SELECT COUNT(*) AS nbre FROM publication 
						WHERE lieu_publication_id_jeu=:lieu_publication_id_jeu')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('lieu_publication_id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$do = $req->fetch();
}
if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])					// SUR LE PROFIL D'UN AUTRE
{
	// AFFICHAGE DES DIFFERENTS CORPS PUBLICATION EN FONCTION AMIS OU NON 
	$requete = $bdd->prepare('SELECT * FROM amis 
							WHERE id_jeu=:id_jeu AND id_jeu_ami=:id_jeu_ami')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_GET['id'], 
							'id_jeu_ami' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$donnees_amis = $requete->fetch();	
	$requete2 = $bdd->prepare('SELECT * FROM amis 
							WHERE id_jeu=:id_jeu_ami AND id_jeu_ami=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$requete2->execute(array('id_jeu' => $_GET['id'], 
							'id_jeu_ami' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));	
	$donnees_amis2 = $requete2->fetch(); 
	
	if (isset($do['nbre']) AND $do['nbre'] == 0)
	{
		if ($donnees_amis['id_jeu'] != '' OR $donnees_amis2['id_jeu'] != '') 	// CONDITION POUR AFFICHER LE PROFIL SI ON EST AMI
		{
			echo '<div id="corps_publication2" >
			<img src="images/aucune_publication.png" alt=" Il n\'y a aucune publication sur cette page. "/>';
		}
		else
		{
			echo '<div id="corps_publication3" >
			<img src="images/aucune_publication_ami.png" alt=" Il n\'y a aucune publication sur cette page. "/>';
		}
	}
	else
	{
		echo '<div id="corps_publication1">';
	}
}
else
{	
	if (isset($do['nbre']) AND $do['nbre'] == 0)
	{
		echo '<div id="corps_publication4" >
		<img src="images/aucune_publication.png" 
		alt="Il n\'y a aucune publication sur cette page."/>';
	}
	else
	echo '<div id="corps_publication">';
}

if (isset($_GET['page']) AND $_GET['page'] > 0)
{
	$numero_page = $_GET['page'];
	$numero_page--;
	$numero_page = 15*$numero_page;
}
else
	$numero_page = 0;

	
$req = $bdd->prepare('SELECT lieu_publication_id_jeu, id_jeu,message,photo, 
					id,video, 
					DATE_FORMAT(date_publication, \'%d/%m/%Y � %H:%i \') 
					AS date_publication FROM publication 
					WHERE lieu_publication=:lieu_publication 
					AND lieu_publication_id_jeu=:lieu_publication_id_jeu 
					ORDER BY id DESC LIMIT '.$numero_page.',15')
					or die(print_r($bdd->errorInfo()));
$req->execute(array('lieu_publication' => $donnees['nom_de_compte'],
					'lieu_publication_id_jeu' => $donnees_jeu['id']))
					or die(print_r($bdd->errorInfo()));
while ($donnees2 = $req->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
{
	$r = $bdd->prepare('SELECT pseudo, photo_profil, id, nom_de_compte 
						FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$r->execute(array('id' => $donnees2['id_jeu']))		
						or die(print_r($bdd->errorInfo()));
	$d = $r->fetch(); 
	
echo '
<div class="bloc_publication_commentaire" id="'.$donnees2['id'].'"> ';

	if($donnees2['lieu_publication_id_jeu'] == $_SESSION['id_jeu'])
	{
		if(isset($_GET['page']))
		{
			echo'
			<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette publication ?\');
				if (sup == 0)return false;" href="profil.php?id_publication='.$donnees2['id'].'&supprimer_publication&page='.$_GET['page'].'" title="Supprimer la publication">
				<div id="supprimer_membre_clanp3"></div>
			</a>';
		}
		else
		{
			echo'
			<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette publication ?\');
				if (sup == 0)return false;" href="profil.php?id_publication='.$donnees2['id'].'&supprimer_publication" title="Supprimer la publication">
				<div id="supprimer_membre_clanp3"></div>
			</a>';
		}
	}
	elseif($donnees2['id_jeu'] == $_SESSION['id_jeu'])
	{
		if(isset($_GET['page']))
		{
			echo'
			<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette publication ?\');
				if (sup == 0)return false;" href="profil.php?id_ami='.$donnees2['lieu_publication_id_jeu'].'&pseudo='.$d['pseudo'].'&id_publication='.$donnees2['id'].'&supprimer_publication&page='.$_GET['page'].'" title="Supprimer la publication">
				<div id="supprimer_membre_clanp3"></div>
			</a>';
		}
		else
		{
			echo'
			<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette publication ?\');
				if (sup == 0)return false;" href="profil.php?id_ami='.$donnees2['lieu_publication_id_jeu'].'&pseudo='.$d['pseudo'].'&id_publication='.$donnees2['id'].'&supprimer_publication" title="Supprimer la publication">
				<div id="supprimer_membre_clanp3"></div>
			</a>';
		}
	}
	
	if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
		echo'<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'" title="'.stripslashes(htmlspecialchars($d['pseudo'])).'">';
	elseif($d['id'] == $_SESSION['id_jeu'])
		echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
	else
		echo'<span  title="Mon sous compte">';
		
	if ($d['photo_profil'] != 0)
	{
		$source = getimagesize('images_utilisateurs/'.$d['photo_profil']); 		// La photo est la source
		
	   echo'<div class="centre_image60">';
	   
		if ($source[0] <= 60 AND $source[1] <= 60)						
			echo'<img src="images_utilisateurs/'.$d['photo_profil'].'" alt="Photo de profil"/>';
		else
			echo'<img src="images_utilisateurs/mini_2_'.$d['photo_profil'].'" alt="Photo de profil"/>';
			
	   echo'</div>';
	}
	else
		echo '<img  class="image_publication" src="images/tete3.png" alt="Photo de profil"/></a>';
		
	if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
		echo'</a>';
	elseif($d['id'] == $_SESSION['id_jeu'])
		echo'</a>';
	else
		echo'</span>';

	if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
	{
		echo'
		<a href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">
			<span class="pseudo_publication">
				'.stripslashes(htmlspecialchars($d['pseudo'])).'
			</span>
		</a>';
	}
	elseif($d['id'] == $_SESSION['id_jeu'])
	{
		echo'
		<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
			<span class="pseudo_publication">
				'.stripslashes(htmlspecialchars($d['pseudo'])).'
			</span>
		</a>';	
	}
	else
	{
		echo'
		<span class="pseudo_publication" style="color:#102c3c;" title="Mon sous compte">
			'.stripslashes(htmlspecialchars($d['pseudo'])).'
		</span>';
	}

	echo'<p class="date_publication">le '.$donnees2['date_publication'].'</p>';
	
	echo '<div class="publication">';
	$message = nl2br(stripslashes(htmlspecialchars($donnees2['message'])));
	$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);	
	$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
	$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
	$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
	if (preg_match('#\[img\](.+)\[/img\]#', $message))
	{
		$message = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message);			
	}
	elseif (preg_match("#http://(.+)#isU", $message))
	{
		$message = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);					
	}
	elseif (preg_match("#https://(.+)#isU", $message))
	{
		$message = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);
	}
	else
	{
		$message = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$message);
	}
	$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
	$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
	$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
	$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
	$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
	$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
	$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
	$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
	$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
	$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);
	
	echo'<span class="texte_publication">'.$message.'</span>';	
		
	// TEXTE + IMAGE
	if ($donnees2['photo'] != 0 AND $donnees2['photo'] != '')
	{
		$source = getimagesize('images_utilisateurs/'.$donnees2['photo']); 		// La photo est la source
		if ($source[0] <= 900 AND $source[1] <= 600)
		{
			echo'<p class="img_bloc">';
			echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donnees2['photo'].'">';
		}
		else
		{
			echo'<p class="img_bloc">';	
			echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$donnees2['photo'].'">';
		}
	
		if ($source[0] <= 300 AND $source[1] <= 300)
			echo'<img class="img_publication" src="images_utilisateurs/'.$donnees2['photo'].'" />
				</a>
				</p>'; 
		else		
			echo'<img class="img_publication" src="images_utilisateurs/mini_1_'.$donnees2['photo'].'" />
				</a>
				</p>'; 
	}
	
	if (isset($donnees2['video']) AND $donnees2['video'] != '')
	{
		$video = $donnees2['video'];
		$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
		<object	type="application/x-shockwave-flash" width="500" height="350" data="http://www.youtube.com/v/$1&hl=fr">							
			<param name="wmode" value="transparent" />
			<param name="allowFullScreen" value="true" />
		</object>',$video);
		$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
		echo $video;
	}
	
	echo'</div>';
	
	// affichage j'aime 
	$nbre_jaime = 0;
	$like = 0;
	$tableau_pseudo_jaime = array();
	$tableau_id_jaime = array();
	$re_c = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_publication=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_publi' => $donnees2['id']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch()) 
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 1;
		}
		$re_c2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $donnees_c['id_jeu']))
		or die(print_r($bdd->errorInfo()));	
        $donnees_c2 = $re_c2->fetch();	
		$tableau_pseudo_jaime[$nbre_jaime] = $donnees_c2['pseudo'];
		$tableau_id_jaime[$nbre_jaime] = $donnees_c['id_jeu'];
		$nbre_jaime++; 
	}
		
	if($like == 0)
		echo'<p> <a id="'.$donnees2['id'].'"  class="like">
		<img src="images/pouce.png" title="J\'aime"/></a>';
	else
		echo'<p> <a id="'.$donnees2['id'].'"  class="like">
		<img src="images/pouce_vert.png" title="Je n\'aime plus"/></a>';

	if ($nbre_jaime > 1)
		echo ' (<span id="'.$donnees2['id'].'" class="nbre_jaime">'.$nbre_jaime.'</span>)';
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] != $_SESSION['id_jeu'])
		echo ' (<a href="profil-i'.$tableau_id_jaime[0].'.html">'.$tableau_pseudo_jaime[0].'</a>)';	
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] == $_SESSION['id_jeu'])
		echo '<span> Vous aimez</span>';		
	// fin affichage j'aime	
		
	// COMMENTAIRE
	if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu']) 				// si on est sur le profil de qqun d'autre
	{
		$requete = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu 
								AND id_jeu_ami=:id_jeu_ami')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('id_jeu' => $_GET['id'], 
								'id_jeu_ami' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));	
		$donnees_amis = $requete->fetch();	
		
		$requete2 = $bdd->prepare('SELECT * FROM amis WHERE id_jeu=:id_jeu_ami 
								AND id_jeu_ami=:id_jeu')
								or die(print_r($bdd->errorInfo()));
		$requete2->execute(array('id_jeu' => $_GET['id'], 
								'id_jeu_ami' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));	
		$donnees_amis2 = $requete2->fetch(); 
		
		if ($donnees_amis['id_jeu'] != '' OR $donnees_amis2['id_jeu'] != '') 	// CONDITION POUR AFFICHER LE PROFIL SI ON EST AMI
		{
			echo'
			<form action="profil_post.php" method="post">
				<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />
				<input type="hidden" name="id_publication" value="'.$donnees2['id'].'"/>';
				if(isset($_GET['page']) AND $_GET['page'] != 0)
					echo '<input type="hidden" name="page" value="'.$_GET['page'].'"/>';
			echo'
				<input type="hidden" name="profil_ou_clan" value="profil"/>
				<input type="hidden" name="id_ami" value="'.$_GET['id'].'"/>		
				<input type="hidden" name="id_jeu" value="'.$d['id'].'"/>
				<input type="hidden" name="pseudo" value="'.$_GET['pseudo'].'" />
			</form></span>';
		}
	}
	else
	{
		echo'
		<form action="profil_post.php" method="post">
			<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />
			<input type="hidden" name="id_publication" value="'.$donnees2['id'].'"/>';
			if(isset($_GET['page']) AND $_GET['page'] != 0)
				echo '<input type="hidden" name="page" value="'.$_GET['page'].'"/>';
		echo'
			<input type="hidden" name="profil_ou_clan" value="profil"/>	
			<input type="hidden" name="id_jeu" value="'.$d['id'].'"/>
		</form></span>';
	}	
	$reponse = $bdd->prepare('SELECT id, commentaire, id_jeu , 
							DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
							AS date_commentaire FROM commentaire 
							WHERE id_publication=:id_publication 
							ORDER BY id LIMIT 0,5')
							or die(print_r($bdd->errorInfo()));
	$reponse->execute(array('id_publication' => $donnees2['id']))
							or die(print_r($bdd->errorInfo()));
	$i=0;	
	while ($donnees3 = $reponse->fetch()) 										// on affiche commentaire par commentaire
	{
		$i++;
		$rs = $bdd->prepare('SELECT pseudo, photo_profil, id, nom_de_compte 
							FROM jeu WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$rs->execute(array('id' => $donnees3['id_jeu']))
							or die(print_r($bdd->errorInfo()));
		$ds = $rs->fetch();
		echo '
		<div class="bloc_commentaire">';
		if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
		OR $donnees2['lieu_publication_id_jeu'] == $_SESSION['id_jeu'] 
		OR $donnees2['id_jeu'] == $_SESSION['id_jeu'])
		{
			if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
			{
				if(isset($_GET['page']))
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
					if (sup == 0)return false;" 
					href="profil.php?id_ami='.$_GET['id'].'&pseudo='.$ds['pseudo'].'&id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'&page='.$_GET['page'].'" 
					title="Supprimer le commentaire">
						
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
				else
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
					if (sup == 0)return false;" 
					href="profil.php?id_ami='.$_GET['id'].'&pseudo='.$ds['pseudo'].'&id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'" 
					title="Supprimer le commentaire">
					
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
			}
			else
			{
				if(isset($_GET['page']))
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
					if (sup == 0)return false;" 
					href="profil.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'&page='.$_GET['page'].'" 
					title="Supprimer le commentaire">
					
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
				else
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
					if (sup == 0)return false;" 
					href="profil.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication='.$donnees2['id'].'" 
					title="Supprimer le commentaire">
					
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
			}
		}
		
		if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'<a href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'" title="'.stripslashes(htmlspecialchars($ds['pseudo'])).'">';
		elseif($ds['id'] == $_SESSION['id_jeu'])
			echo'<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
		else
			echo'<span title="Mon sous compte">';
			
		if ($ds['photo_profil'] != 0)
		{ 
			$source = getimagesize('images_utilisateurs/'.$ds['photo_profil']); // La photo est la source
			echo'<div class="centre_image40">';
			if ($source[0] <= 40 AND $source[1] <= 40)
				echo '<img src="images_utilisateurs/'.$ds['photo_profil'].'" />'; 
			else				
				echo'<img src="images_utilisateurs/mini_4_'.$ds['photo_profil'].'"  />';
			echo'</div>';
		}
		else
			echo '<img  class="img_commentaire" src="images/tete2.png" alt="Planete"/>';
		
		if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'</a>';
		elseif($ds['id'] == $_SESSION['id_jeu'])
			echo'</a>';
		else
			echo'</span>';

		if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'
			<a href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'">
				<span class="pseudo_commentaire">
					'.stripslashes(htmlspecialchars($ds['pseudo'])).'
				</span>
			</a>';
		}
		elseif($ds['id'] == $_SESSION['id_jeu'])
		{
			echo'
			<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
				<span class="pseudo_commentaire">
					'.stripslashes(htmlspecialchars($ds['pseudo'])).'
				</span>
			</a>';
		}
		else
		{
			echo'
			<span class="pseudo_commentaire" style="color:#102c3c;" title="Mon sous compte">
				'.stripslashes(htmlspecialchars($ds['pseudo'])).'
			</span>';
		}
		
		echo'
		<br/>
		<span class="date_commentaire">
			le '.$donnees3['date_commentaire'].'
		</span>';
		
		$com = stripslashes(htmlspecialchars($donnees3['commentaire']));
		
		if (preg_match("#http://(.+)#isU", $com))
		{
			$com = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);					
		}
		elseif (preg_match("#https://(.+)#isU", $com))
		{
			$com = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);
		}
		else
		{
			$com = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$com);
		}
		
		$com = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $com);		
		$com = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $com);	
		$com = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $com);		
		$com = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $com);
		$com = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $com);
		$com = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $com);
		$com = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $com);
		$com = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $com);
		$com = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $com);
		$com = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $com);
		echo'<span class="message_commentaire">'.$com.'</span>';

			
		echo '
		</div>';				
	}
	if ($i > 4)
	{
		if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu']) // SUR LE PROFIL D'UN AMI !
		{
			if(isset($_GET['page']))
			{
				echo '
				<a href="commentaires.php?id_ami='.$_GET['id'].'&pseudo_ami='.$_GET['pseudo'].'&id_table='.$donnees2['id'].'&lieu_publication='.$donnees2['lieu_publication_id_jeu'].'&page_profil='.$_GET['page'].'&lieu_page=profil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
			else
			{
				echo '
				<a href="commentaires.php?id_ami='.$_GET['id'].'&pseudo_ami='.$_GET['pseudo'].'&id_table='.$donnees2['id'].'&lieu_publication='.$donnees2['lieu_publication_id_jeu'].'&lieu_page=profil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
		}
		else // SUR SON PROFIL
		{
			if(isset($_GET['page']))
			{
				echo '
				<a href="commentaires.php?id_table='.$donnees2['id'].'&lieu_publication='.$donnees2['lieu_publication_id_jeu'].'&page_profil='.$_GET['page'].'&lieu_page=profil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
			else
			{
				echo '
				<a href="commentaires.php?id_table='.$donnees2['id'].'&lieu_publication='.$donnees2['lieu_publication_id_jeu'].'&lieu_page=profil"> 
					<div class="tous_commentaires"></div>
				</a>';
			}			
		}
	}	
echo '
</div>';

}
// GESTION AFFICHAGE PAGES
if(isset($_GET['id']) AND $_GET['id'] != $_SESSION['id_jeu'])
{
	$nbre_page = 1;
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM publication 
						WHERE lieu_publication_id_jeu=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_jeu' =>$_GET['id']))
						or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_publi'];
	$p->closeCursor();
}
else	
{	
	$nbre_page = 1;
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM publication 
						WHERE lieu_publication_id_jeu=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_jeu' =>$_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_publi'];
	$p->closeCursor();
}
if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
$r_pseudo= $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu ')
						or die(print_r($bdd->errorInfo()));
$r_pseudo->execute(array('id_jeu' => $_GET['id']))
						or die(print_r($bdd->errorInfo()));
$d_pseudo = $r_pseudo->fetch();
// DEFINIR LES VARIABLES AVANT LE INCLUDE///////////////////////////////////////	
$nom = ''.$d_pseudo['pseudo'].''; // PSEUDO ou NOM de la TEAM
$nbr_affichage = 15;

include('pagination_pseudo.php');
?>	
  
  </div>
  <div id="corps_invisible"></div> <!-- NECESSAIRE  POUR LES FLOAT ------------>
</div>
<script>	
////////////////////////////////////// Pour le bouton j'aime
var jaime = document.getElementsByTagName('a'),
jaimeTaille = jaime.length;
		for (var i = 0 ; i < jaimeTaille ; i++) { 	
			if (jaime[i] && jaime[i].className == 'like')
			{
				jaime[i].onclick = function() { 
				var id_publi = this.id;
				var jaime2 = this;
				// On lance la requ�te ajax
				$.get('like.php?id_publication='+id_publi, function(data) { 
				if (data == 1)
					jaime2.innerHTML = '<img src="images/pouce_vert.png" title="Je n\'aime plus"/>';
				else
					jaime2.innerHTML = '<img src="images/pouce.png" title="J\'aime"/>';
					
				});				
					return false; // on bloque la redirection
				};
			}
		}
var span = document.getElementsByTagName('span'),
spanTaille = span.length;
		for (var i = 0 ; i < jaimeTaille ; i++) {
			if (span[i] && span[i].className == 'nbre_jaime')
			{
				span[i].onclick = function() {
				var href_publi = this.id;
				var jaime3 = this;
				// On lance la requ�te ajax
				$.getJSON('like.php?nbre_jaime='+href_publi, function(data) { 
					jaime3.innerHTML = data['message'];
				});				
				};
			}
		}
//------------------------------------------------------------------------------
var publie = document.getElementById('publier');
var publie2 = document.getElementById('publier2');
var publie3 = document.getElementById('publier3');

if (publie)
{
	publie.onclick = function() {
	publie.style.display = 'none';
	};
}
if (publie2)
{
	publie2.onclick = function() {
	publie2.style.display = 'none';
	};
}
if (publie3)
{
	publie3.onclick = function() {
	publie3.style.display = 'none';
	};
}

var links = document.getElementsByTagName('a'),
    linksLen = links.length,
	overlay_v = document.getElementById('overlay_video');
	object = document.getElementById('object');
	
for (var i = 0 ; i < linksLen ; i++) {
	if (links[i].title == 'Afficher l\'image originale')
	{
		links[i].onclick = function() { 
			displayImg(this); 
			return false; // on bloque la redirection
		};
	}
}

function displayImg(link) {
    var img = new Image(),
        overlay = document.getElementById('overlay');
        profil = document.getElementById('contient_image_profil');
    img.onload = function() {
        profil.innerHTML = '';
        profil.appendChild(img);
    };
    img.src = link.href;
    overlay.style.display = 'block';
    profil.style.display = 'block';
    profil.innerHTML = '<span>Chargement en cours...</span>';
}

document.getElementById('overlay').onclick = function() {
    this.style.display = 'none';
    profil.style.display = 'none';	
};
//------------------------ POUR ENVOYER MESSAGE --------------------------------

	envoyer_message = document.getElementById('envoyer_message');
	envoyer_message.onclick = function() {
    overlay_envoyer_message.style.display = 'block';
	grand_overlay1.style.display = 'block';
	return false; // on bloque la redirection
	};	

	document.getElementById('fermer').onclick = function() {
    overlay_envoyer_message.style.display = 'none';
	grand_overlay1.style.display = 'none';
	return false; // on bloque la redirection
	};  

//------------------------------------------------------------------------------
	   	   
	function displayfilename(input)
	{
		filename = input.value;
		filename = filename.substring(filename.lastIndexOf('\\')+1); 			// Windows path
		filename = filename.substring(filename.lastIndexOf('/')+1); 			// Linux path
		document.getElementById('there').innerHTML = filename;
	}
	
//------------------------------------------------------------------------------
		var formulaire = document.getElementById('formulaire');
		var submit = document.getElementById('submit');		
			a = document.getElementById('a');
			video = document.getElementById('video');
			if (!br)
			{
			 var br = document.createElement('br');
			formulaire.insertBefore(br,submit);
			}


</script>

<?php include('pied_page.php'); ?>