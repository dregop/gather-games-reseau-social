<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
// ON DETRUIT LES SESSIONS PROVENANT DE COMMENTAIRE
unset($_SESSION['id_publi']);
unset($_SESSION['lieu_page']);
unset($_SESSION['page']);

if (empty($_SESSION['pseudo']))
{
	header('Location: informations-e1.html');
}
if (!isset($erreur_image, $photo) AND isset($_FILES['photos']['name']) 
AND $_FILES['photos']['size']<=1000000 AND $_FILES['photos']['error'] == 0)
{
	header('Location: communaute.html');
}
elseif( empty($_FILES['photos']['name']) 
AND isset($_POST['publication']) AND $_POST['publication'] != '')
{
	header('Location: communaute.html');
}
elseif(!empty($_FILES['photos']['name']))
{
	$erreur_image = 0;
	header('Location: communaute-e'.$erreur_image.'.html');
}
	
if (isset($_GET['page']) AND ($_GET['page'] == '' OR $_GET['page'] < 1))
{
	header('Location: communaute.html');
}
	
if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc 
									FROM jeu WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body> 
<!--[if gte IE 6]>
	<style type="text/css">
		.champs_publication_communaute{ height:98px;width:395px;} 
	</style>
<![endif]-->
<?php

//OVERLAY IMAGE
echo'
<div id="overlay"></div>
<div id="contient_image_profil"></div>'; /*SEPARER LES DEUX BLOCS POUR EVITER 
										   L'INCOMPREHENSION DU NAVIGATEUR */
//

include('menu_sans_doctype.php');	
	
if (isset($_FILES['photos']['name']) AND $_FILES['photos']['error'] == 0
AND !empty($_FILES['photos']['name']) AND $_FILES['photos']['size']<=1000000
AND isset($_POST['publication']))
{
	// Testons si l'extension est autoris�e
	$infosfichier = pathinfo($_FILES['photos']['name']);
	$extension_upload = $infosfichier['extension'];
	$extensions_autorisees = array('jpg', 'jpeg', 'png', 'PNG', 'JPEG', 'JPG');				
	if (in_array($extension_upload, $extensions_autorisees))
	{
		// ouverture du fichier compteur
		$monfichier = fopen('compteur.txt', 'r+');				 
		$compteur = fgets($monfichier); 
		$compteur++; 
		fseek($monfichier, 0); 
		fputs($monfichier, $compteur); 
		fclose($monfichier);
		// On peut valider le fichier et le stocker d�finitivement
		
		if ($extension_upload == 'png' OR $extension_upload == 'PNG')
		{
			move_uploaded_file($_FILES['photos']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefrompng('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		else
		{
			move_uploaded_file($_FILES['photos']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefromjpeg('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		
		include('redimention_image.php');
		
		$redimOK = fctredimimage(300,300,'images_utilisateurs/','mini_1_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(200,200,'images_utilisateurs/','mini_3_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(60,60,'images_utilisateurs/','mini_2_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(900,600,'images_utilisateurs/','grand_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
									

	}
	
	$photo = $compteur.'.'.$extension_upload;
	
	if ($source == '')
	{
		$photo = 0;
		$erreur_image = 0;
	}
	
	// on insert la photo dans la bdd
	$req = $bdd->prepare('INSERT INTO photos(id_jeu, adresse_stockage, 
						date_enregistrement) 
						VALUES(:id_jeu, :adresse_stockage,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'adresse_stockage' => $photo)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te

	if ($photo != 0)
	{
		// on insert le message dans la bdd
		$req = $bdd->prepare('INSERT INTO communaute(id_jeu,message,photo,time, 
							date_message) 
							VALUES(:id_jeu,:message,:photo,:time, NOW())') 
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'message' => $_POST['publication'],
							'photo' => $photo,'time' => time())) 
							or die(print_r($bdd->errorInfo()));
		$req->closeCursor(); // Termine le traitement de la requ�te
	}

}
elseif (isset($_POST['publication']) AND $_POST['publication'] != '' 
AND empty($_FILES['photos']['name']))
{
	// on insert le message ds la bdd
	if(preg_match("#^https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i", $_POST['publication']))
	{
		$message = $_POST['publication'];
		$message = preg_replace('#&list=[a-zA-Z0-9._/\-&\?\=]+#', '',$message);
		$message = preg_replace('#&feature=[a-zA-Z0-9._/\-&\?\=]+#', '',$message);
		$req = $bdd->prepare('INSERT INTO communaute(id_jeu,message,
						date_message,time) 
						VALUES(:id_jeu,:message,NOW(),:time)') 
						or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'message' => $message,'time' => time())) 
							or die(print_r($bdd->errorInfo()));
		$req->closeCursor(); // Termine le traitement de la requ�te	
	}
	else
	{
		$req = $bdd->prepare('INSERT INTO communaute(id_jeu,message,
							date_message,time) 
							VALUES(:id_jeu,:message,NOW(),:time)') 
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'message' => $_POST['publication'],'time' => time())) 
							or die(print_r($bdd->errorInfo()));
		$req->closeCursor(); // Termine le traitement de la requ�te	
	}
}
// PREMIERE BOUCLE POUR COMPTER (METTRE TOUT DANS UNE BOUCLE MARCHE PAS)
$p2 = $bdd->prepare('SELECT id,time FROM communaute WHERE id_jeu=:id_jeu')
					or die(print_r($bdd->errorInfo()));
$p2->execute(array('id_jeu' => $_SESSION['id_jeu']))
					or die(print_r($bdd->errorInfo()));
$i = 0;
while($do2 = $p2->fetch())	
{
	
	$temps = time();
	$temps_publi = $temps - $do2['time'];
	if($temps_publi < 3600*24)
	{
		$i++; // LA ON COMPTE JUSTE POUR AVOIR LE NOMBRE: 5 MAX
	}
}
// DEUXIEME BOUCLE POUR LE TEMPS RESTANT
$p3 = $bdd->prepare('SELECT id,time FROM communaute WHERE id_jeu=:id_jeu')
					or die(print_r($bdd->errorInfo()));
$p3->execute(array('id_jeu' => $_SESSION['id_jeu']))
					or die(print_r($bdd->errorInfo()));
$i2 = 0;
while($do3 = $p3->fetch())	
{
	
	$temps = time();
	$temps_publi = $temps - $do3['time'];
	if($temps_publi < 3600*24)
	{
		$i2++;		
		if($i2 = 5)
		{
			break; // LA ON SORT DE LA BOUCLE POUR RECUPERER LA DERNIERE DONNEE
				  // C'EST A DIRE LA PLUS ANCIENNE POUR DIRE LE TEMPS RESTANT
		}
	}
}
$r_joueur = $bdd->prepare('SELECT nom_de_compte FROM jeu
						WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$r_joueur->execute(array('id' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
$d_joueur = $r_joueur->fetch();
$r_admin = $bdd->prepare('SELECT admin FROM membres 
						WHERE nom_de_compte=:nom_de_compte')
						or die(print_r($bdd->errorInfo()));
$r_admin->execute(array('nom_de_compte' => $d_joueur['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));
$d_admin = $r_admin->fetch();

if($d_admin['admin'] == 'oui' OR $i < 5)
{
	echo'
<div id="corps_communaute">
	<img id="intro_communaute" src="images/intro_communaute.png" alt=" Fa�tes vous connaitre de la communaut� Gather Games " />
	<p id="intro_communaute2"> 
		Sur cette page, vos publications seront vues de toute la communaut�. 
		Elle vous permettera de rencontrer des joueurs, aux m�mes int�r�ts que 
		vous, plus facilement.<br /> 
		Pour lutter contre le flood inutil, vous n\'avez le droit qu\'� cinq 
		publications par jour.
	</p>';
	//MESSAGE D'ERREUR IMAGE INVALIDE
	if(isset($_GET['erreur_image']) AND $_GET['erreur_image'] == 0)
	{
	  echo'
		<p class="bloc_erreur">
			<img src="images/attention.png" alt="erreur"/> Image non valide.
			<br /> 
			(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
		</p>';
	}
?>
	<div class="bbcode_css_communaute">
		<script type="text/javascript" src="bbcode.js"></script>
		<a href="#">
			<img src="images/bbcode/gras.gif" alt="gif" title="Gras"
			onclick="javascript:bbcode('[g]', '[/g]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/italique.gif" alt="gif" title="Italique" 
			onclick="javascript:bbcode('[i]', '[/i]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/souligner.gif" alt="gif" title="Souligner"
			onclick="javascript:bbcode('[s]', '[/s]'); return(false)"/>
		</a>
		<a href="#">
			<img src="images/bbcode/image.gif" alt="gif" title="Image" 
			onclick="javascript:bbcode('[img]', '[/img]'); return(false)"/>
		</a>
		<!-- fin bbcode -->	
	</div>
<?php
	echo'
	<div id="debut_publication2"> 
	<form id="formulaire" action="communaute.php" method="post" enctype="multipart/form-data">
		<div id="contient_champs_publication2">
			<textarea type="text" id="publication" 
					class="champs_publication_communaute" name="publication" 
					placeholder="Publiez sur le profil de la communaut�, montrez de quoi vous �tes capable, fa�tes vous connaitre!" 
					style="color:#102c3c; font-weight:bolder;"></textarea>
			<div id="contient_publier_communaute">
				<input type="submit" id="publier" name="publiez" value="Publiez" class="publier2"/>
			</div>
		</div>';	
		// VIDEOS!
		echo'
		<span title="Prochainement ! Vous pouvez publiez vos vid�os You Tube.">
			<div id="conteneurfile_communaute">';
				/*<input type="file" name="videos" onmousedown="return false" 
						onkeydown="return false" class="inputfile" 
						onchange="displayfilename(this);"/> 
				*/   
		echo'
			</div>
		</span>';
		// PHOTOS
		echo'
		<div id="conteneurfile2">
			<input type="file" name="photos" onmousedown="return false" 
					onkeydown="return false" class="inputfile" 
					onchange="displayfilename(this);" />   
		</div>	
	</form>
		<div id="there2"></div>
	</div>	';
}
else
{
	$temps_restant = 3600*24 - $temps_publi;
	echo'
<div id="corps_communaute">
	<img id="intro_communaute" src="images/intro_communaute.png" alt="Fa�tes vous connaitre de la communaut� Gather Games" />
	<p id="intro_communaute2"> 
		Sur cette page, vos publications seront vues de toute la communaut�. 
		Elle vous permettera de rencontrer des joueurs, aux m�mes int�r�ts que 
		vous, plus facilement.<br /> 
		Pour lutter contre le flood inutil, vous n\'avez le droit qu\'� cinq 
		publications par jour.
	</p>';
	echo''.$i.'';
	//MESSAGE D'ERREUR IMAGE INVALIDE
	if(isset($_GET['erreur_image']) AND $_GET['erreur_image'] == 0)
	{
	  echo'
		<p class="bloc_erreur">
			<img src="images/attention.png" alt="erreur"/> Image non valide.
			<br /> 
			(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
		</p>';
	}
	if (floor(($temps_restant - 1)/3600) > 0)
	{
		echo'
		<div id="avant_publier" >
			<p id="nombre_heure">'.floor(($temps_restant - 1)/3600).' Heures</p>
		</div>';
	}
	else
	{
		echo'
		<div id="avant_publier" style="width:640px;background-repeat:no-repeat;">
			<p id="nombre_heure" style=""> moins d\'1 Heure</p>
		</div>';
	}
}
$p = $bdd->query('SELECT COUNT(*) AS nbre_publi FROM communaute ')
				or die(print_r($bdd->errorInfo()));
$do = $p->fetch();
if($do['nbre_publi'] > 0)
{
	echo '<div id="corps_publication_communaute" 
				style="margin:auto;margin-top:120px;">';
}
else
	echo '<div id="sous_corps_communaute">';

if (isset($_GET['page']) AND $_GET['page'] > 0)
{
	$numero_page = $_GET['page'];
	$numero_page--;
	$numero_page = 15*$numero_page;
}
else
	$numero_page = 0;

$requete = $bdd->query('SELECT *, 
						DATE_FORMAT(date_message, \'%d/%m/%Y � %H:%i \') 
						AS date_message FROM communaute 
						ORDER BY id DESC LIMIT '.$numero_page.',15')
						or die(print_r($bdd->errorInfo()));
while($donnees = $requete->fetch())
{
	$r = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$r->execute(array('id' => $donnees['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$d = $r->fetch(); 
	echo '
	<div class="bloc_publication_communaute" id="'.$donnees['id'].'"> ';
	
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
		{
			echo'
			<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'">
				<p class="pseudo_publication_communaute">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</p>
			</a>';
		}
		elseif($d['id'] == $_SESSION['id_jeu'])
		{
			echo'
			<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
				<p class="pseudo_publication_communaute">
					'.stripslashes(htmlspecialchars($d['pseudo'])).'
				</p>
			</a>';
		}
		else
		{
			echo'
			<p class="pseudo_publication_communaute" style="color:#102c3c;" title="Mon sous compte">
				'.stripslashes(htmlspecialchars($d['pseudo'])).'
			</p>';
		}
		
		echo'<p class="date_publication_communaute">
				le '.$donnees['date_message'].'
			</p>';
		
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'<a  href="id'.$d['id'].'-'.urlencode(stripslashes(htmlspecialchars($d['pseudo']))).'" title="'.stripslashes(htmlspecialchars($d['pseudo'])).'">';
		elseif($d['id'] == $_SESSION['id_jeu'])
			echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
		else
			echo'<span  title="Mon sous compte">';
			
		if ($d['photo_profil'] != 0)
		{
			$source = getimagesize('images_utilisateurs/'.$d['photo_profil']); 	// La photo est la source			
		   echo'
		   <div class="centre_image60">';
		   
			if ($source[0] <= 60 AND $source[1] <= 60)						
				echo'<img src="images_utilisateurs/'.$d['photo_profil'].'" alt="Photo de profil"/>';
			else
				echo'<img src="images_utilisateurs/mini_2_'.$d['photo_profil'].'" alt="Photo de profil"/>';
				
		   echo'
		   </div>';
		}
		else
			echo'<img  class="image_publication" src="images/tete3.png" alt="Photo de profil"/>';
		
		if($d['nom_de_compte'] != $_SESSION['nom_de_compte'])
			echo'</a>';
		elseif($d['id'] == $_SESSION['id_jeu'])
			echo'</a>';
		else
			echo'</span>';
		
		echo ' 
		<div class="info_joueur_communaute">
			<p>
				Jeu : '; 
				echo substr(stripslashes(htmlspecialchars($d['nom_jeu'])), 0, 20).'';
				if(strlen(stripslashes(htmlspecialchars($d['nom_jeu']))) > 20)
				{ 
					echo'...';
				} 
			echo'
			</p>
			
			<p>
				Plateforme : '.$d['plateforme'].'
			</p>
			
		</div>';
		echo '
		<div class="publication">';
		
		$message = nl2br(stripslashes(htmlspecialchars($donnees['message'])));
		$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);	
		$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
		$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
		$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
		if (preg_match('#\[img\](.+)\[/img\]#', $message))
		{
			$message = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message);			
		}
		elseif (preg_match("#http://(.+)#isU", $message))
		{
			$message = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);					
		}
		elseif (preg_match("#https://(.+)#isU", $message))
		{
			$message = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$message);
		}
		else
		{
			$message = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$message);
		}
		$message = preg_replace('#http://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
		<object	type="application/x-shockwave-flash" width="500" height="320" data="http://www.youtube.com/v/$1&hl=fr">							
			<param name="wmode" value="transparent" />
			<param name="allowFullScreen" value="true" />
		</object>',$message);
		$message = preg_replace('#https://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i','
		<object	type="application/x-shockwave-flash" width="500" height="320" data="http://www.youtube.com/v/$1&hl=fr">							
			<param name="wmode" value="transparent" />
			<param name="allowFullScreen" value="true" />
		</object>',$message);		
		if(preg_match('#<object#', $message))
		{
			$message = preg_replace('#">#', '',$message);
			$message = preg_replace('#" target="_blank#', '',$message);
			$message = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$message);
		}
		$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
		$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
		$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
		$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
		$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
		$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
		$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
		$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
		$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
		$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);
		echo'<span class="texte_publication">'.$message.'</span>';	
			
		// TEXTE + IMAGE
		if ($donnees['photo'] != 0 AND $donnees['photo'] != '')
		{
			$source = getimagesize('images_utilisateurs/'.$donnees['photo']); 	// La photo est la source
			if ($source[0] <= 900 AND $source[1] <= 600)
			{
				echo '
				<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/'.$donnees['photo'].'">';
			}
			else
			{
				echo '
				<p class="img_bloc">';
				echo'<a title="Afficher l\'image originale" href="images_utilisateurs/grand_'.$donnees['photo'].'">';
			}
		
			if ($source[0] <= 300 AND $source[1] <= 300)
			{
				echo '
						<img class="img_publication" src="images_utilisateurs/'.$donnees['photo'].'" />
					</a>
				</p>'; 
			}
			else	
			{
				echo '
						<img class="img_publication" src="images_utilisateurs/mini_1_'.$donnees['photo'].'" />
					</a>
				</p>'; 
			}
		}	
		echo'
		</div>';
		
	// affichage j'aime 
	$nbre_jaime = 0;
	$like = 0;
	$tableau_pseudo_jaime = array();
	$tableau_id_jaime = array();
	$re_c = $bdd->prepare('SELECT id_jeu FROM jaime WHERE id_communaute=:id_publi')
	or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id_publi' => $donnees['id']))
	or die(print_r($bdd->errorInfo()));
	while($donnees_c = $re_c->fetch()) 
	{
		if ($donnees_c['id_jeu'] == $_SESSION['id_jeu'])
		{
			$like = 1;
		}
		$re_c2 = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
		or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_jeu' => $donnees_c['id_jeu']))
		or die(print_r($bdd->errorInfo()));	
        $donnees_c2 = $re_c2->fetch();	
		$tableau_pseudo_jaime[$nbre_jaime] = $donnees_c2['pseudo'];
		$tableau_id_jaime[$nbre_jaime] = $donnees_c['id_jeu'];
		$nbre_jaime++; 
	}
		
	if($like == 0)
		echo'<p> <a id="'.$donnees['id'].'"  class="like">
		<img src="images/pouce.png" title="J\'aime"/></a>';
	else
		echo'<p> <a id="'.$donnees['id'].'"  class="like">
		<img src="images/pouce_vert.png" title="Je n\'aime plus"/></a>';

	if ($nbre_jaime > 1)
		echo ' (<span id="'.$donnees['id'].'" class="nbre_jaime">'.$nbre_jaime.'</span>)';
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] != $_SESSION['id_jeu'])
		echo ' (<a href="profil-i'.$tableau_id_jaime[0].'.html">'.$tableau_pseudo_jaime[0].'</a>)';	
	elseif ($nbre_jaime == 1 AND $tableau_id_jaime[0] == $_SESSION['id_jeu'])
		echo '<span> Vous aimez</span>';		
	// fin affichage j'aime	
		
		// !!!!COMMENTAIRE!!!! //
		echo '
		<form action="communaute_post.php" method="post">';
		echo'<input type="text" class="plus_commentaire" name="commentaire" placeholder=" Commentez cette publication " />';
		echo'<input type="hidden" name="id_publication_communaute" value="'.$donnees['id'].'"/>';
		if(isset($_GET['page']) AND $_GET['page'] != 0)
		{
			echo '
			 <input type="hidden" name="page" value="'.$_GET['page'].'"/>';
		}	
		echo'
		</form>';

		$reponse = $bdd->prepare('SELECT id, commentaire, id_jeu , 
								DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
								AS date_commentaire FROM commentaire 
								WHERE id_publication_communaute=:id_publication_communaute 
								ORDER BY id LIMIT 0,5')
								or die(print_r($bdd->errorInfo()));
		$reponse->execute(array('id_publication_communaute' => $donnees['id']))
								or die(print_r($bdd->errorInfo()));
		$i=0;	
		while ($donnees3 = $reponse->fetch()) 									// ON AFFICHE COMMENTAIRE PAR COMMENTAIRE
		{
			$i++;
			$rs = $bdd->prepare('SELECT pseudo, photo_profil, id, nom_de_compte 
								FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
			$rs->execute(array('id' => $donnees3['id_jeu']))
								or die(print_r($bdd->errorInfo()));
			$ds = $rs->fetch();
			echo '
			<div class="bloc_commentaire">';
			if($donnees3['id_jeu'] == $_SESSION['id_jeu'] 
			OR $donnees['id_jeu'] == $_SESSION['id_jeu'])
			{
				if(isset($_GET['page']))
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="communaute_post.php?page='.$_GET['page'].'&id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication_communaute='.$donnees['id'].'" 
						title="Supprimer le commentaire">
						
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
				else
				{
					echo'
					<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce commentaire ?\');
						if (sup == 0)return false;" 
						href="communaute_post.php?id_commentaire='.$donnees3['id'].'&supprimer_commentaire&id_publication_communaute='.$donnees['id'].'" 
						title="Supprimer le commentaire">
						
						<div id="supprimer_commentaire_profil"></div>
						
					</a>';
				}
			}
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'<a  href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'" title="'.stripslashes(htmlspecialchars($ds['pseudo'])).'">';
			elseif($ds['id'] == $_SESSION['id_jeu'])
				echo'<a  href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil">';
			else
				echo'<span  title="Mon sous compte">';
				
			if ($ds['photo_profil'] != 0)
			{ 
				$source = getimagesize('images_utilisateurs/'.$ds['photo_profil']); // La photo est la source
				echo'
				<div class="centre_image40">';
				
				if ($source[0] <= 40 AND $source[1] <= 40)
					echo '<img src="images_utilisateurs/'.$ds['photo_profil'].'" />'; 
				else				
					echo'<img src="images_utilisateurs/mini_4_'.$ds['photo_profil'].'"  />';
					
				echo'
				</div>';
			}
			else
				echo '<img  class="img_commentaire" src="images/tete2.png" alt="Planete"/>';
			
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'</a>';
			elseif($ds['id'] == $_SESSION['id_jeu'])
				echo'</a>';
			else
				echo'</span>';
	
			if($ds['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'
				<a  href="id'.$ds['id'].'-'.urlencode(stripslashes(htmlspecialchars($ds['pseudo']))).'">
					<span class="pseudo_commentaire">
						'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a>';
			}
			elseif($ds['id'] == $_SESSION['id_jeu'])
			{
				echo'
				<a href="id'.$_SESSION['id_jeu'].'-'.urlencode(stripslashes(htmlspecialchars($_SESSION['pseudo']))).'" title="Mon profil" >
					<span class="pseudo_commentaire">
						'.stripslashes(htmlspecialchars($ds['pseudo'])).'
					</span>
				</a>';
			}
			else
			{
				echo'
				<span class="pseudo_commentaire" style="color:#102c3c;" title="Mon sous compte">
					'.stripslashes(htmlspecialchars($ds['pseudo'])).'
				</span>';
			}
			
			echo'
			<br/>
			<span class="date_commentaire" style="color:#8a8989;">
				le '.$donnees3['date_commentaire'].'
			</span>';
			
			$com = stripslashes(htmlspecialchars($donnees3['commentaire']));				
			if (preg_match("#http://(.+)#isU", $com))
			{
				$com = preg_replace('#http://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);					
			}
			elseif (preg_match("#https://(.+)#isU", $com))
			{
				$com = preg_replace('#https://[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="$0" target="_blank">$0</a>',$com);
			}
			else
			{
				$com = preg_replace('#www.[a-zA-Z0-9.;_/\-&\#\?\=]+#i', '<a href="http://$0" target="_blank">$0</a>',$com);
			}
			
			$com = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $com);		
			$com = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $com);	
			$com = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $com);		
			$com = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $com);
			$com = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $com);
			$com = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $com);
			$com = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $com);
			$com = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $com);
			$com = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $com);
			$com = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $com);			
			echo '<span class="message_commentaire">'.$com.'</span>';	
			echo '
			</div>';				
		}
		if ($i > 4)
		{

			if(isset($_GET['page']))
			{
				echo '
				<a href="commentaires_communaute.php?id_table='.$donnees['id'].'&page_communaute='.$_GET['page'].'&lieu_page=communaute"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
			else
			{
				echo '
				<a href="commentaires_communaute.php?id_table='.$donnees['id'].'&lieu_page=communaute"> 
					<div class="tous_commentaires"></div>
				</a>';
			}
		}

	echo '
	</div>';
}
// GESTION AFFICHAGE PAGES
echo'<p class="nombre_page">';

	$nbre_page = 1;
	$p = $bdd->query('SELECT COUNT(*) AS nbre_com FROM communaute ')or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_com'];
	$p->closeCursor();

if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
$nom_page = 'communaute';
$nbr_affichage = 15;

include('pagination.php');
?>
	</div>

</div>

<script>
////////////////////////////////////// Pour le bouton j'aime
var jaime = document.getElementsByTagName('a'),
jaimeTaille = jaime.length;
		for (var i = 0 ; i < jaimeTaille ; i++) { 	
			if (jaime[i] && jaime[i].className == 'like')
			{
				jaime[i].onclick = function() { 
				var id_publi = this.id;
				var jaime2 = this;
				// On lance la requ�te ajax
				$.get('like.php?id_communaute='+id_publi, function(data) { 
				if (data == 1)
					jaime2.innerHTML = '<img src="images/pouce_vert.png" title="Je n\'aime plus"/>';
				else
					jaime2.innerHTML = '<img src="images/pouce.png" title="J\'aime"/>';
					
				});				
					return false; // on bloque la redirection
				};
			}
		}
var span = document.getElementsByTagName('span'),
spanTaille = span.length;
		for (var i = 0 ; i < jaimeTaille ; i++) {
			if (span[i] && span[i].className == 'nbre_jaime')
			{
				span[i].onclick = function() {
				var href_publi = this.id;
				var jaime3 = this;
				// On lance la requ�te ajax
				$.getJSON('like.php?nbre_jaime_commu='+href_publi, function(data) { 
					jaime3.innerHTML = data['message'];
				});				
				};
			}
		}	
//------------------------------------------------------------------------------
var publie = document.getElementById('publier');
if (publie)
{
publie.onclick = function() {
publie.style.display = 'none';
};
}
//------------------------------------------------------------------------------
var links = document.getElementsByTagName('a'),
    linksLen = links.length
for (var i = 0 ; i < linksLen ; i++) {
	if (links[i].title == 'Afficher l\'image originale')
	{
		links[i].onclick = function() { 
			displayImg(this); 
			return false; // on bloque la redirection
		};
	}

}

function displayImg(link) {
    var img = new Image(),
        overlay = document.getElementById('overlay');
        profil = document.getElementById('contient_image_profil');
    img.onload = function() {
        profil.innerHTML = '';
        profil.appendChild(img);
    };
    img.src = link.href;
    overlay.style.display = 'block';
    profil.style.display = 'block';
    profil.innerHTML = '<span>Chargement en cours...</span>';
}

document.getElementById('overlay').onclick = function() {
    this.style.display = 'none';
    profil.style.display = 'none';	
};   	   
	function displayfilename(input)
	{
		filename = input.value;
		filename = filename.substring(filename.lastIndexOf('\\')+1);			// Windows path
		filename = filename.substring(filename.lastIndexOf('/')+1); 			// Linux path
		document.getElementById('there2').innerHTML = filename;
	}
	
	
</script>	

<?php 
include('pied_page.php'); 
?>