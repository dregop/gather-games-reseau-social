<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
    die('Erreur : '.$e->getMessage());
}
if (isset($_GET['id_jeu']))
{ 
	$_SESSION['id_jeu_images'] = $_GET['id_jeu'];
}
elseif (isset($_GET['id_clan']))
{ 
	$_SESSION['id_clan_images'] = $_GET['id_clan'];
}
	
if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc 
								FROM jeu WHERE nom_de_compte=:ndc')
								or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
								or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
if (empty($_SESSION['pseudo']))
{
	header('Location: informations-pseudo.html');
}

if (isset($_GET['supprimer_image'], $_SESSION['statu']) 
AND $_SESSION['statu'] == 'meneur' AND isset($_SESSION['id_clan_images']))
{
	$req1 = $bdd->prepare('DELETE FROM photos WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id' => $_GET['supprimer_image'])) 
							or die(print_r($bdd->errorInfo()));
	$req1->closeCursor(); // Termine le traitement de la requ�te
	header('Location: images-it'.$_SESSION['id_clan_images'].'.html');
}
if (isset($_GET['supprimer_image']) 
AND $_SESSION['id_jeu_images'] == $_SESSION['id_jeu'] 
AND isset($_SESSION['id_jeu_images']))
{
	$req1 = $bdd->prepare('DELETE FROM photos WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id' => $_GET['supprimer_image'])) 
							or die(print_r($bdd->errorInfo()));
	$req1->closeCursor(); // Termine le traitement de la requ�te
	header('Location: images-ij'.$_SESSION['id_jeu_images'].'.html');
}
?>

<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />	
 </head>
   
<body>
<?php

if (isset($_GET['image']))
{		 
	echo'
	<div id="grand_overlay_videos">';
		if (isset($_SESSION['id_jeu_images']))
		{
			echo'
			<a href="images-ij'.$_SESSION['id_jeu_images'].'.html">
				<img id="fermer_message" src="images/quitter_video.png" alt=" Fermer "/>
			</a>';
		}
		if (isset($_SESSION['id_clan_images']))
		{
			echo'
			<a href="images-it'.$_SESSION['id_clan_images'].'.html">
				<img id="fermer_message" src="images/quitter_video.png" alt=" Fermer "/>
			</a>';
		}
	echo'
	</div>';
	echo'
	<div id="contient_img_overlay" style="display:block;">';
	echo'<div id="contient2_image_profil" style="display:block">';
	
	$source = getimagesize('images_utilisateurs/'.$_GET['image']); 				// LA PHOTO EST LA SOURCE
	if ($source[0] > 900 OR $source[1] > 600)
		echo '<img src="images_utilisateurs/grand_'.$_GET['image'].'" alt="image"/>';	
	else	
		echo '<img src="images_utilisateurs/'.$_GET['image'].'" alt="image" />';
		
	echo'</div>';
	if (isset($_SESSION['statu'], $_SESSION['id_clan_images']) 
	AND $_SESSION['statu'] == 'meneur')
	{
		echo '
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette image ?\');
			if (sup == 0)return false;"id="liens_suprimer_video" href="images.php?supprimer_image='.$_GET['id'].'&id_clan='.$_SESSION['id_clan_images'].'">
			<img id="supprimer_image" src="images/supprimer_image.png" alt=" Supprimer l\'image " />
		</a>';
	}
	elseif (isset($_SESSION['id_jeu_images']))
	{
		echo '
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette image ?\');
			if (sup == 0)return false;"id="liens_suprimer_video" href="images.php?supprimer_image='.$_GET['id'].'&id_jeu='.$_SESSION['id_jeu_images'].'">
			<img id="supprimer_image" src="images/supprimer_image.png" alt=" Supprimer l\'image " />
		</a>';
	}	
	echo'
	</div>';
}

include('menu_sans_doctype.php');

echo'
<div id="corps_images_clan">';
	if (isset($_SESSION['id_clan_images']))
	{
		if (isset($_GET['page']))
		{
			$numero_page = $_GET['page'];
			$numero_page--;
			$numero_page = 16*$numero_page; // RANGE DE 4 
		}
		else
			$numero_page = 0;

		$reqs = $bdd->prepare('SELECT * FROM clan WHERE id=:id_clan')
								or die(print_r($bdd->errorInfo()));
		$reqs->execute(array('id_clan' => $_SESSION['id_clan_images']))
								or die(print_r($bdd->errorInfo()));
		$donnees = $reqs->fetch();
		if ($donnees['id_jeu'] == $_SESSION['id_jeu'])
		{
			$_SESSION['statu'] = 'meneur';
		}
		echo'
		<img id="mosaique" src="images/mosaique.png" alt=" Mosa�que d\'images "/>';
		
		$re = $bdd->prepare('SELECT nom_clan FROM clan WHERE id=:id_clan')
							or die(print_r($bdd->errorInfo()));
		$re->execute(array('id_clan' => $_SESSION['id_clan_images']))
							or die(print_r($bdd->errorInfo()));
		$donnees = $re->fetch();
		echo'
		<div id="corps_images">';	
		$req = $bdd->prepare('SELECT adresse_stockage, id, id_jeu 
							FROM photos WHERE id_clan=:id_clan 
							ORDER BY id DESC LIMIT '.$numero_page.',16')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_clan' => $_SESSION['id_clan_images']))
							or die(print_r($bdd->errorInfo()));
		$i=0;
		while ($donneees2 = $req->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
		{
		$i++;
			$source = getimagesize('images_utilisateurs/'.$donneees2['adresse_stockage']); // LA PHOTO EST LA SOURCE
			echo'
			<div class="centreimg_images">';
			if ($source[0] > 200 OR $source[1] > 200)
			{
				echo'
				<a title="Afficher l\'image originale" 
				href="images.php?image='.$donneees2['adresse_stockage'].'&id_clan='.$_SESSION['id_clan_images'].'&id='.$donneees2['id'].'">
					<img src="images_utilisateurs/mini_3_'.$donneees2['adresse_stockage'].'" alt="Image"/>
				</a>';
			}
			else 
				echo'
				<a title="Afficher l\'image originale" 
				href="images.php?image='.$donneees2['adresse_stockage'].'&id_clan='.$_SESSION['id_clan_images'].'&id='.$donneees2['id'].'">
					<img src="images_utilisateurs/'.$donneees2['adresse_stockage'].'" alt="Image"/>
				</a>';
			echo'
			</div>';	
		}
		if($i==0)
		{
			echo '
			<div id="aucune_video_mosaique">
				Aucune image dans la mosa�que.
			</div>';
		}
		
	echo'</div>
	<div id="corps_invisible_images"></div>';	
	}
	elseif (isset($_SESSION['id_jeu_images']))
	{	
		if (isset($_GET['page']))
		{
			$numero_page = $_GET['page'];
			$numero_page--;
			$numero_page = 16*$numero_page;
		}
		else
			$numero_page = 0;

	echo'
	<img id="mosaique" src="images/mosaique.png" alt=" Mosa�que d\'images " />';
	$re = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id_jeu' => $_SESSION['id_jeu_images']))
						or die(print_r($bdd->errorInfo()));
	$donnees = $re->fetch();
	echo'
	<div id="corps_images">';	
		$req = $bdd->prepare('SELECT adresse_stockage, id, id_jeu FROM photos 
							WHERE id_jeu=:id_jeu 
							ORDER BY id 
							DESC LIMIT '.$numero_page.',16')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu_images']))
							or die(print_r($bdd->errorInfo()));
		$i=0;
		while ($donneees2 = $req->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
		{
			$i++;
			$source = getimagesize('images_utilisateurs/'.$donneees2['adresse_stockage']); // LA PHOTO EST LA SOURCE
			echo'
			<div class="centreimg_images">';
			if ($source[0] > 200 OR $source[1] > 200)
			{
				echo'
				<a title="Afficher l\'image originale" 
				href="images.php?image='.$donneees2['adresse_stockage'].'&id_jeu='.$_SESSION['id_jeu_images'].'&id='.$donneees2['id'].'">
					<img src="images_utilisateurs/mini_3_'.$donneees2['adresse_stockage'].'" alt="Image"/>
				</a>';
			}
			else 
				echo'
				<a title="Afficher l\'image originale" 
				href="images.php?image='.$donneees2['adresse_stockage'].'&id_jeu='.$_SESSION['id_jeu_images'].'&id='.$donneees2['id'].'">
					<img src="images_utilisateurs/'.$donneees2['adresse_stockage'].'" alt="Image"/>
				</a>';
			echo'
			</div>';	
		}
		if($i==0)
		{
			echo '
			<div id="aucune_video_mosaique">
				Aucune image dans la mosa�que.
			</div>';
		}
	echo'</div>
	<div id="corps_invisible_images"></div>
	';	
	}
// GESTION AFFICHAGE PAGES

$nbre_page = 1;
if (isset($_SESSION['id_jeu_images']))
{
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM photos 
						WHERE id_jeu=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_jeu' =>$_SESSION['id_jeu_images']))
						or die(print_r($bdd->errorInfo()));
}
elseif (isset($_SESSION['id_clan_images']))
{
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM photos 
						WHERE id_clan=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_clan' =>$_SESSION['id_clan_images']))
						or die(print_r($bdd->errorInfo()));
}	
$do = $p->fetch();
$nbr_entrees = $do['nbre_publi'];
$p->closeCursor();

if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
$nom_page = 'images';
$nbr_affichage = 16;

include('pagination.php');
	
$req_clan = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
$req_clan->execute(array('id' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
$donnees_clan = $req_clan->fetch();

if(isset($_SESSION['id_clan_images']) 
AND $donnees_clan['id_clan'] == $_SESSION['id_clan_images'])
{
	echo'
	<a href="team.html">
		<img src="images/retour_clan.png" alt="Retour Clan" style="margin-top:20px;"/>
	</a>';
}
elseif(isset($_SESSION['id_clan_images']) 
AND $donnees_clan['id_clan'] != $_SESSION['id_clan_images'])
{
	echo'
	<a href="team-i'.$_SESSION['id_clan_images'].'.html">
		<img src="images/retour_clan.png" alt="Retour Clan" style="margin-top:20px;" />
	</a>';
}
elseif(isset($_SESSION['id_jeu_images']) AND $_SESSION['id_jeu'] == $_SESSION['id_jeu_images'])
{
	echo'
	<a href="profil.html">
		<img src="images/retour_profil.png" alt="Retour profil" style="margin-top:20px;" />
	</a>';
}
else
{
	echo'
	<a href="profil-i'.$_SESSION['id_jeu_images'].'.html">
		<img src="images/retour_profil.png" alt="Retour profil" style="margin-top:20px;" />
	</a>';
}

echo '</div>';

include('pied_page.php');
?>