<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu
									WHERE nom_de_compte=:ndc')					
									or die(print_r($bdd->errorInfo()));			
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');

if (isset($_GET['page'],$_GET['id']) AND ($_GET['page'] == '' 
OR $_GET['page'] < 1 OR $_GET['id'] == '' OR $_GET['id'] < 1))
{
	header('Location: team.html');
}

if (isset($_GET['id']))
{
	$re2 = $bdd->prepare('SELECT * FROM forum_topic WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$re2->execute(array( 'id' => $_GET['id']))
						or die(print_r($bdd->errorInfo()));
	$donnees2 = $re2->fetch();
	
	if ($_SESSION['id_jeu'] == $donnees2['id_jeu'] AND $donnees2['view'] == 1)
	{
		$req2 = $bdd->prepare('UPDATE forum_topic SET view=0 WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$req2->execute(array('id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));	
		$req2->closeCursor(); // Termine le traitement de la requ�te 	
	}
	$re2->closeCursor(); // Termine le traitement de la requ�te
}

	$re2 = $bdd->prepare('SELECT * FROM clan WHERE id=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$re2->execute(array( 'id_clan' => $_SESSION['id_clan']))
						or die(print_r($bdd->errorInfo()));
	$donnees2 = $re2->fetch();
	$c = $bdd->query('SELECT COUNT(*) AS nbre FROM forum_topic')
						or die(print_r($bdd->errorInfo()));
	$dc = $c->fetch();
	
if (!isset($_SESSION['id_categorie']))
{
	header('Location: forum.html'); 
}
if (!isset($_SESSION['statut']))
{
	header('Location: team.html'); 
}
if (isset($_GET['id']) AND $_GET['id'] > 0 )
{
	$cm = $bdd->prepare('SELECT COUNT(*) AS nbre_com FROM commentaire_topic WHERE id_topic=:id')or die(print_r($bdd->errorInfo()));
	$cm->execute(array('id' =>$_GET['id']))or die(print_r($bdd->errorInfo()));
	$dcm = $cm->fetch();
	$dcm['nbre_com']/=4;
	if (($dcm['nbre_com']%4) != 0 OR $dcm['nbre_com'] == 0)
	{
		$dcm['nbre_com']++;
	}
	if (isset($_GET['page']) AND $_GET['page'] > 0)
	{
		$repo = $bdd->prepare('SELECT id, id_jeu, id_categorie, statut, 
								titre_topic, message_topic, 
								DATE_FORMAT(date_message, \'%d/%m/%Y � %H:%i \') 
								AS date_message FROM forum_topic WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$repo->execute(array('id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));
		$donnees = $repo->fetch();
		
		$rep = $bdd->prepare('SELECT nom_de_compte, id, pseudo, photo_profil, 
							nom_jeu, plateforme FROM jeu WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$rep->execute(array('id_jeu' =>$donnees['id_jeu']))
							or die(print_r($bdd->errorInfo()));
		$donnee = $rep->fetch();
	}
	else
		header('Location: forum.html'); 
}
else
	header('Location: forum.html'); 

		
if (isset($_POST['commentaire_topic']))
{
	$req = $bdd->prepare('INSERT INTO commentaire_topic(id_jeu, id_topic, 
						commentaire_topic, date_commentaire) 
						VALUES(:id_jeu, :id_topic, :commentaire_topic, NOW())')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'id_topic' => $_POST['id'],
						'commentaire_topic' => $_POST['commentaire_topic']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requ�te

	$re = $bdd->prepare('SELECT id_jeu FROM forum_topic WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id' => $_POST['id']))
						or die(print_r($bdd->errorInfo()));
	$donne = $re->fetch();
			
	if ($_SESSION['id_jeu'] != $donne['id_jeu'])
	{
		$req2 = $bdd->prepare('UPDATE forum_topic SET view=:one WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$req2->execute(array('id' => $_POST['id'], 'one' => 1))
								or die(print_r($bdd->errorInfo()));	
		$req2->closeCursor(); // Termine le traitement de la requ�te 
	}
	header('Location: topic-i'.$_POST['id'].'-p'.$_POST['page'].'.html'); 
}
?>
<!DOCTYPE html> 
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body> 
<?php
include('menu_sans_doctype.php');

	$re2 = $bdd->prepare('SELECT * FROM clan WHERE id=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$re2->execute(array( 'id_clan' => $_SESSION['id_clan']))
						or die(print_r($bdd->errorInfo()));
	$donnees2 = $re2->fetch();
	
echo '
<div id="corps_forum">';
// 1 ER BLOCK DU FORUM (NOM,IMAGE,MEMBRE etc...)////////////////////////////////
include('menu_forum.php');

if ($_GET['page'] == 1)
{
?>
<div id="bloc1_topic">
<?php
echo'
	<div id="debut_createur_topic">
		<p id="auteur_topic">Auteur</p>
		<p id="titre_topic">
			Titre du sujet : 
			'.stripslashes(htmlspecialchars($donnees['titre_topic'])).'
		</p>';
?>	
	</div>
<!--[if gte IE 6]>
	<style type="text/css">
		#bloc_message_topic
		{
			margin-top:0px;
		}
	</style>
<![endif]-->
	<div id="bloc_createur_topic">
		<div id="bloc_message_topic">
			<p class="date_message_topic" style="color:#b9cbd4;border-bottom:1px solid #b9cbd4;" >
				Publi� le <?php echo''.$donnees['date_message'].'';?> 
			</p>
<?php
			$message1 = nl2br(stripslashes(htmlspecialchars($donnees['message_topic'])));
			$message1 = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message1);
			$message1 = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message1);			
			$message1= preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message1);
			$message1 = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message1);
			$message1 = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message1);
			$message1 = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message1);
			$message1 = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="$1">$1</a>', $message1);
			$message1 = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message1);		
			$message1 = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message1);	
			$message1 = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message1);		
			$message1 = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message1);
			$message1 = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message1);
			$message1 = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message1);
			$message1 = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message1);
			$message1 = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message1);
			$message1 = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message1);
			$message1 = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message1);
		
?>
			<p class="message_topic"><?php echo''.$message1.'';?></p>
		</div>
		<div id="bloc_auteur">
<?php
		
			if($donnee['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'<a  href="profil-i'.$donnee['id'].'" title="'.stripslashes(htmlspecialchars($donnee['pseudo'])).'.html">';
			elseif($donnee['id'] == $_SESSION['id_jeu'])
				echo'<a href="profil.html"  title="Moi">';
			else
				echo'<span  title="Mon sous compte">';
					
			echo'
			<div id="centre_image200_topic" style="border-bottom:1px dashed #b9cbd4;">';
			if ($donnee['photo_profil'] != 0)
			{
				$source = getimagesize('images_utilisateurs/'.$donnee['photo_profil']); // La photo est la source
				if ($source[0] <= 200 AND $source[1] <= 200)
					echo'<img  src="images_utilisateurs/'.$donnee['photo_profil'].'" alt="Photo de profil" />';
				else
					echo'<img  src="images_utilisateurs/mini_3_'.$donnee['photo_profil'].'" alt="Photo de profil" />';
			}
			else 
				echo '<img src="images/tete1.png" alt="Photo de profil"/>';
			echo'
			</div>';
			
			if($donnee['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'</a>
				<a  href="profil-i'.$donnee['id'].'.html">
					<p id="pseudo_auteur" style="color:#b9cbd4;" >
						'.stripslashes(htmlspecialchars($donnee['pseudo'])).'
					</p>
				</a> ';
			}
				elseif($donnee['id'] == $_SESSION['id_jeu'])
				{
					echo'</a>
					<a  href="profil.html">
						<p id="pseudo_auteur" style="color:#b9cbd4;" >
							'.stripslashes(htmlspecialchars($donnee['pseudo'])).'
						</p>
					</a> ';
				}
				else
				{
					echo'</span>
					<p id="pseudo_auteur" title="Mon sous compte" style="color:#b9cbd4;" >
						'.stripslashes(htmlspecialchars($donnee['pseudo'])).'
					</p>';
				}
			echo '
			<p id="jeu_profil" style="margin-left:10px;">
				<span style="color:#b9cbd4;"> 
					Jeu :
				</span>
				<br />
				<span class="fond1_info_profil"> '; 
					echo substr(stripslashes(htmlspecialchars($donnee['nom_jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnee['nom_jeu']))) > 19)
					{ echo'...';} 
				echo'
				</span>
			</p>
			<p id="jeu_profil" style="margin-left:10px;">
				<span style="color:#b9cbd4;"> 
					Plateforme :
				</span>
				<br />
				<span class="fond1_info_profil">
					'.$donnee['plateforme'].'
				</span>
			</p>
		</div>
		<div id="corps_invisible"></div>
		<div id="corps_invisible2"></div>		
	</div>
</div>';
}

	$r = 0;
	$re = $bdd->prepare('SELECT * FROM commentaire_topic WHERE id_topic=:id')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id' =>$_GET['id']))
						or die(print_r($bdd->errorInfo()));
	while ($donnees_commentaire = $re->fetch())
	{
		$r++;
	}
	
	if ($_GET['page'] > 0 AND $r > 0)
	{
		if($_GET['page'] == 1 AND $r > 0)
		{
			echo'
			<img id="reponse_commentaire" src="images/reponse_commentaire.png" alt=" " />';
		}
?>
<div id="bloc2_topic">
		<div id="debut_commentaire_topic">
			<p id="auteur_commentaire">Auteur</p>
			<p id="reponses_commentaire">R�ponses</p>
		</div>
<?php

	if (isset($_GET['page']) AND $_GET['page'] > 0)
	{
		$numero_page = $_GET['page'];
		$numero_page--;
		$numero_page = 10*$numero_page;
	}
	else
		$numero_page = 0;
		
	$re = $bdd->prepare('SELECT id, id_topic, id_jeu, commentaire_topic, 
						DATE_FORMAT(date_commentaire, \'%d/%m/%Y � %H:%i \') 
						AS date_commentaire  
						FROM commentaire_topic 
						WHERE id_topic=:id LIMIT '.$numero_page.',10')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id' =>$_GET['id']))
						or die(print_r($bdd->errorInfo()));
	while ($donnees_commentaire = $re->fetch())
	{
		$r = $bdd->prepare('SELECT nom_de_compte, pseudo, photo_profil, 
							nom_jeu, id, plateforme FROM jeu WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$r->execute(array('id_jeu' =>$donnees_commentaire['id_jeu']))
							or die(print_r($bdd->errorInfo()));
		$donnee_commentaire = $r->fetch();
		if (!empty($donnees_commentaire))
		{
?>
		<div id="contient_commentaire_topic">
			<div id="bloc_commentaire_topic">
				<p class="date_message_topic">
					Publi� le <?php 
					echo''.$donnees_commentaire['date_commentaire'].'';?> 
				</p>
<?php
			$message = nl2br(stripslashes(htmlspecialchars($donnees_commentaire['commentaire_topic'])));			
			$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);			
			$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
			$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
			$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
			$message = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" style="max-height:200px;max-width=:200px;"/>', $message);
			$message = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="$1">$1</a>', $message);
			$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
			$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
			$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
			$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
			$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
			$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
			$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
			$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
			$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
			$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);		
?>
				<p class="message_topic">
					<?php echo''.$message.'';?>
				</p>
			</div>
			<div id="bloc_auteur_commentaire">
<?php
				if($donnee_commentaire['nom_de_compte'] != $_SESSION['nom_de_compte'])
					echo'<a  href="profil-i'.$donnee_commentaire['id'].'.html" title="'.stripslashes(htmlspecialchars($donnee_commentaire['pseudo'])).'">';
				elseif($donnee_commentaire['id'] == $_SESSION['id_jeu'])
					echo'<a href="profil.html"  title="Moi">';
				else
					echo'<span  title="Mon sous compte">';
					
				echo'
				<div id="centre_image200_topic">';
				if ($donnee_commentaire['photo_profil'] != 0)
				{
					$source = getimagesize('images_utilisateurs/'.$donnee_commentaire['photo_profil']); // La photo est la source
					if ($source[0] <= 200 AND $source[1] <= 200)
						echo'<img  src="images_utilisateurs/'.$donnee_commentaire['photo_profil'].'" alt="Photo de profil" />';
					else
						echo'<img  src="images_utilisateurs/mini_3_'.$donnee_commentaire['photo_profil'].'" alt="Photo de profil" />';
				}
				else 
					echo'<img src="images/tete1.png" alt="Photo de profil"/>';
				
				echo'
				</div>';
				
				if($donnee_commentaire['nom_de_compte'] != $_SESSION['nom_de_compte'])
				{
					echo'</a>
					<a  href="profil-i'.$donnee_commentaire['id'].'.html">
						<p id="pseudo_auteur">
							'.stripslashes(htmlspecialchars($donnee_commentaire['pseudo'])).'
						</p>
					</a> ';
				}
				elseif($donnee_commentaire['id'] == $_SESSION['id_jeu'])
				{
					echo'</a>
					<a  href="profil.html">
						<p id="pseudo_auteur">
							'.stripslashes(htmlspecialchars($donnee_commentaire['pseudo'])).'
						</p>
					</a> ';
				}
				else
				{
					echo'</span>
					<p id="pseudo_auteur" title="Mon sous compte">
						'.stripslashes(htmlspecialchars($donnee_commentaire['pseudo'])).'
					</p>';
				}
				
				echo'
				<p id="jeu_profil" style="margin-left:10px;">
					Jeu :
					<br />
					<span class="fond1_info_profil"> '; 
					echo substr(stripslashes(htmlspecialchars($donnee_commentaire['nom_jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnee_commentaire['nom_jeu']))) > 19)
					{ echo'...';} 
					echo'</span></p>
				<p id="jeu_profil" style="margin-left:10px;"> 
					Plateforme :
					<br />
					<span class="fond1_info_profil">
						'.$donnee_commentaire['plateforme'].'
					</span>
				</p>
			</div>
			<div id="corps_invisible"></div>
			<div id="corps_invisible2"></div>		
		</div>';
		}
	}
	}
echo'</div>';
// GESTION AFFICHAGE PAGES

	$nbre_page = 1;
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_commentaire 
						FROM commentaire_topic WHERE id_topic=:id')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id' =>$_GET['id']))	
						or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_commentaire'];
	
if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
// DEFINIR LES VARIABLES AVANT LE INCLUDE///////////////////////////////////////	
$nom_page = 'topic';
$nbr_affichage = 10;

include('pagination.php');
// FIN GESTION PAGES

$r_bloquer = $bdd->prepare('SELECT statut FROM forum_topic WHERE id=:id')
							or die(print_r($bdd->errorInfo())); // if bloquer
$r_bloquer->execute(array('id' => $_GET['id']))
							or die(print_r($bdd->errorInfo()));
$donnees_bloquer = $r_bloquer->fetch();

$r_recrutements = $bdd->prepare('SELECT id_categorie FROM forum_topic 
								WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
$r_recrutements->execute(array('id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));
$donnees_recrutement = $r_recrutements->fetch();
$r_recrutements2 = $bdd->prepare('SELECT bloc_categorie FROM forum_categorie 
								WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
$r_recrutements2->execute(array('id' => $donnees_recrutement['id_categorie']))
								or die(print_r($bdd->errorInfo()));
$donnees_recrutement2 = $r_recrutements2->fetch();
if ($donnees_recrutement2['bloc_categorie'] == 'recrutements') $visiteur_publi = 1;
	if (((isset($_GET['page']) AND $_SESSION['statut'] != 'visiteur')
	OR isset($visiteur_publi) )
	AND $donnees_bloquer['statut'] != 'bloquer')
	{
?>
		<form action="commentaire_topic.php" method="post">
		<div id="bloc_reponse_commentaire">
			<p>
<?php			
			echo '
			<div id="image_pseudo_commentaire">
			<div id="centre_image60_commentaire">';
			if(isset($_SESSION['photo_profil']) AND $_SESSION['photo_profil'] != '' AND $_SESSION['photo_profil'] != 0)
			{
				$source = getimagesize('images_utilisateurs/'.$_SESSION['photo_profil']); // La photo est la source
				if ($source[0] <= 60 AND $source[1] <= 60)
					echo'<img src="images_utilisateurs/'.$_SESSION['photo_profil'].'" alt="Photo de profil" />';
				else
					echo'<img  src="images_utilisateurs/mini_2_'.$_SESSION['photo_profil'].'" alt="Photo de profil" />';
			}
			else
			{
				echo'<img src="images/tete3.png" alt="Photo de profil" />';
			}
			
			echo'</div>'; 
			
			  echo'
			  <p id="pseudo_reponse_commentaire">
				'.stripslashes(htmlspecialchars($_SESSION['pseudo'])).'
			  </p>
			</div>';?>
			  

				<p id="champs_commentaire">
					<script type="text/javascript" src="bbcode.js"></script>
					<a href="#">
						<img src="images/bbcode/gras.gif" alt="gif" title="Gras"
						onclick="javascript:bbcode('[g]', '[/g]'); return(false)"/>
					</a>
					<a href="#">
						<img src="images/bbcode/italique.gif" alt="gif" title="Italique" 
						onclick="javascript:bbcode('[i]', '[/i]'); return(false)"/>
					</a>
					<a href="#">
						<img src="images/bbcode/souligner.gif" alt="gif" title="Souligner"
						onclick="javascript:bbcode('[s]', '[/s]'); return(false)"/>
					</a>
					<a href="#">
						<img src="images/bbcode/lien.gif" alt="gif" title="Lien" 
						onclick="javascript:bbcode('[a]', '[/a]'); return(false)"/>
					</a>
					<a href="#">
						<img src="images/bbcode/image.gif" alt="gif" title="Image" 
						onclick="javascript:bbcode('[img]', '[/img]'); return(false)"/>
					</a>
					<textarea id="input_commentaire_topic" name="commentaire_topic" rows="" cols="" placeholder="Entrer votre message ici"></textarea>
				</p>

				<br />
				<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
				<input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
				<input type="submit" id="publier" class="enregistrez_informations" name="Valider" value="R�pondre"/>
			</p>
		</div>
		</form>
<?php
	}
?>
 </div>
 </div>
<script>
var publie = document.getElementById('publier');
if (publie)
{
publie.onclick = function() {
publie.style.display = 'none';
};
}
var membre = document.getElementById('membre');
var overlay_guilde = document.getElementById('overlay_guilde');

	membre.onclick =  function()
	{
	overlay_guilde.style.display = 'block';
	
        return false; // on bloque la redirection
	};
	document.getElementById('fermer_membre').onclick = function() {
    overlay_guilde.style.display = 'none';
	return false; // on bloque la redirection
};
 var menu = document.getElementById('menu_gauche');
 menu.style.display = 'none';
var lien_categorie = document.getElementById('nouvelle_categorie');
var overlay_forum = document.getElementById('overlay_forum');
	lien_categorie.onclick =  function()
	{
	overlay_forum.style.display = 'block';
        return false; // on bloque la redirection
	};
document.getElementById('fermer').onclick = function() {
    overlay_forum.style.display = 'none';
	return false; // on bloque la redirection

};
</script>

<?php include('pied_page.php'); ?>