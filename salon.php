<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}
	
if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte
								FROM jeu WHERE nom_de_compte=:ndc')
								or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
								or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['nom_de_compte'])
		header('Location: informations.html');			
} 
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
if (empty($_SESSION['pseudo']))
{
	header('Location: informations-pseudo.html');
}
	// recup�rer id_clan
	$reqs_jeu = $bdd->prepare('SELECT id_clan 
								FROM jeu WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
	$reqs_jeu->execute(array('id' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
	$donnees_jeu = $reqs_jeu->fetch();
?>

<!DOCTYPE html>
  
<html> 
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />	
	<style>
body
{
color:black;
}
/* Pour que les liens ne soient pas soulign�s */
a {
	text-decoration: none;
}
img {
	vertical-align: middle;
}
/* Conteneur principal des blocs de la page */
#container {
	width: 80%;
	margin: 50px auto;
	padding: 2px 20px 20px 20px;
	background: #fff;
}

/* Bloc contenant la zone de texte et bouton */
.post_message  {
	width: 95%;
	margin: auto;
	border: 1px solid #d2d2d2;
	background: #f8fafd;
	padding: 3px;
}
/* Zone de texte */
.post_message #message {
	width: 80%;
}
/* Bouton d'envoi */
.post_message #post {
	width: 18%;
}

/* La zone où sont affich�s les messages
et utilisateurs connect�s */
.chat {
	width: 95%;
	margin: 10px auto;
	border: 1px solid #d2d2d2;
	padding: 0px;
}
/* Bloc de chargement */
.chat #loading {
	margin-top: 50px;
}
/* Annonce */
.chat #annonce {
	background: #f8fafd;
	margin: -6px -7px 5px -7px;
	padding: 5px;
	color:black;
	height:20px;
	box-shadow: 8px 8px 12px #aaa;
	-webkit-box-shadow: 0px 8px 15px #aaa;
}
/* Zone des messages */
.chat #text-td {
	margin: 0px; 
	padding: 5px; 
	width: 80%; 
	background: #fff;
	color:grey;
}
/* Zone des utilisateurs connect�s */
.chat #users-td, .chat #users-chat-td {
	margin: 0px; 
	padding: 5px; 
	width: 20%; 
	background: #ddd;
}
.chat #text, .chat #users, .chat #users-chat {
	height:300px; 
	overflow-y: auto;
}

/* Modification du statut */
.status {
	width: 95%;
	border: none;
	background: #fff;
	margin: auto;
	text-align: right;
}

.info {
	color: green;
}
	</style>
 </head>
   
<body>
<?php

include('menu_sans_doctype.php'); ?>

<div id="container">

				
<table class="chat"><tr>		
	<!-- zone des messages -->
	<td valign="top" id="text-td">
            	<div id="annonce">Chat de discussion 
				</div>
		<div id="text">
			<div id="loading">
				<center>
				<span class="info" id="info">Chargement du chat en cours...</span><br />
				<img src="images/ajax-loader.gif" alt="patientez...">
				</center>
			</div>
		</div>
 
	</td>
			
	<!-- colonne avec les membres connect�s au chat -->
	<td valign="top" id="users-td"><span style="color:grey;">Amis connect�s : </span>
	<div id="users">
			<div id="loading">
				<center>
				<span class="info" id="info">Chargement des joueurs en lignes</span><br />
				<img src="images/ajax-loader.gif" alt="patientez...">
				</center> 
			</div>	
	</div>
	<div id="responsePost" style="display:none"></div>	
	</td>
</tr></table>
<!-- Zone de texte //////////////////////////////////////////////////////// -->
        <a name="post"></a>
	<table class="post_message"><tr>
		<td>
		<form action="" method="" onsubmit="postMessage(); return false;">
			<select class="input1" name="affichage" id="affichage" >
				<option id="general" value="general">G�n�ral</option>
			<?php if($donnees_jeu['id_clan'] != 0)	
			echo'<option value="team">Team</option>'; ?>
			</select>
			<input type="text" name="message" id="message" maxlength="255" />
			<input type="hidden" name="id_jeu" id="id_jeu"/>
			<div id="responsePost" style="display:none"></div> 
			<p> Affichage : <br />
		<input type="checkbox" name="general" id="general" checked /> <label for="general">G�n�ral</label><br />
		<input type="checkbox" name="team" id="team" checked /> <label for="team">Team</label><br />
		    </p>
		</form>
		</td>
	</tr></table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script src="chat.js"></script>
	<?php
include('pied_page.php');
?>