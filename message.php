<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (isset($_GET['v']))
{
	$req1 = $bdd->prepare('UPDATE message SET view=0 
						WHERE destinataire=:id')
						or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));	
	$req1->closeCursor(); // Termine le traitement de la requ�te 	
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
$reqs = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$reqs->execute(array('id' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
$donnees = $reqs->fetch();

if (isset($_GET['supprimer_message'],$_GET['id_message']))
{
	$req = $bdd->prepare('DELETE FROM message WHERE id=:id_message 
						AND destinataire=:destinataire')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_message' => $_GET['id_message'],
						'destinataire' => $_SESSION['id_jeu'])) 
						or die(print_r($bdd->errorInfo()));
	header('Location: message.html');
}


?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body> 

<!---------------------------- BLOC MESSAGE !----------------------------> 
	<div id="grand_overlay1"></div>
	<div id="overlay_envoyer_message">
		<a id="fermer" href="#">
			<img id="fermer_message" src="images/fermer.png" alt=" Fermer " />
		</a>
		<p id="titre_overlay_message">
			Envoyer un message 
		</p>
		<form action="message_post.php" method="post">
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;"> 
			<textarea rows="10" cols="32" name="message2" style="color:#102c3c;font-size:1.2em;font-weight:bolder;"></textarea>
			<br />
			<input type="hidden" name="destinataire2" id="destinataire_id"/>
			<input type="submit" id="envoyer_profil" name="Valider" value="Envoyer"/>
		</p>
		</form>
	</div>	

<?php
include('menu_sans_doctype.php');
?>


<div id="corps_notification">
	<img id="g_news" src="images/g_news.png" alt="Gamers News"/> 
	<p id="intro_notification">  
		Cette page regroupe vos actualit�s. Vous pouvez y voir vos derniers 
		messages, vos dernieres demande d'ajout ami et enfin vos notifications 
		pour profiter au mieux de Gather Games.
	 <p>
<?php
	//ON RECUPERE LES VIEW DANS DEMANDE AMI
	$req2_notif = $bdd->prepare('SELECT view FROM demande_amis 
								WHERE id_jeu=:id_jeu AND view=1')
								or die(print_r($bdd->errorInfo()));
	$req2_notif->execute(array('id_jeu' => $_SESSION['id_jeu']))
								or die(print_r($bdd->errorInfo()));
	$n2=0;
	while ($do2_notif = $req2_notif->fetch())
	{
		$n2++;
	}

	echo'
	<div id="sous_corps_notification">
		<a href="notification.html">
			<div id="notif_notification"></div>
		</a> 
		<a href="message.html">
			<div id="message2_notification"></div>
		</a> ';
		
		if($n2>0)
		{
			echo'
			<a href="notification-demande-v.html">
				<div id="ajouter3_notification"></div>
			</a>';
		}
		else
		{
			echo'
			<a href="notification-demande.html">
				<div id="ajouter_notification"></div>
			</a>';
		}
	echo'
	</div>';
	
//MESSAGE D'ERREUR//
    if(isset($_GET['send'])) 
	{
		echo'
		<p class="bon_message">
			<img src="images/bon.png" alt=" "/> 
			Message envoy�. 
		</p>';	
	}
	elseif(isset($_GET['erreur']))
	{
		echo'
		<p class="erreur_message">
			<img src="images/attention.png" alt=" "/> 
			Le message ne peut pas �tre vide. 
		</p>';
	}
///////////////////

if (isset($_GET['page']))
{
	$numero_page = $_GET['page'];
	$numero_page--;
	$numero_page = 15*$numero_page;
}
else
	$numero_page = 0;

$re4 = $bdd->prepare('SELECT id,id_jeu,message,destinataire,
					DATE_FORMAT(date_message, \'%d/%m/%Y � %H:%i \') 
					AS date_message	FROM message 
					WHERE destinataire=:id_jeu 
					ORDER BY id DESC LIMIT '.$numero_page.',15')
					or die(print_r($bdd->errorInfo()));
$re4->execute(array('id_jeu' => $_SESSION['id_jeu']))
					or die(print_r($bdd->errorInfo()));
$i = 0;
while ($donnees4 = $re4->fetch()) // on affiche message par message
{
	if ($donnees4['id_jeu'] == 0)
	{
	echo'
	<div class="bloc_message">
	 <div class="bloc_pseudo_photo">';
	    echo'<img  class="image_publication" src="images/g_games.png" 
		alt="Photo de profil"/>';
		echo'
		<p class="pseudo_date_message">
				<span style="color:#102c3c;">
					L\'�quipe de Gather Games
				</span>';
		echo'
			<span style="font-size:small; color:white;"> 
				le '.$donnees4['date_message'].'
			</span>
		</p>
	 </div>';
		echo '
		<p class="message_message">'
			.$donnees4['message'].'
		</p>';
		echo '		
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce message ?\');
		if (sup == 0)return false;" href="message.php?id_message='.$donnees4['id'].'&supprimer_message">
			<div class="supprimer_message"></div>
		</a>
		
		<div id="corps_invisible"></div> 
	</div>';	
	}
	else
	{
	$re5 = $bdd->prepare('SELECT pseudo,photo_profil FROM jeu 
						WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$re5->execute(array('id_jeu' => $donnees4['id_jeu']))
						or die(print_r($bdd->errorInfo()));
	$donnees5 = $re5->fetch();
	echo'
	<div class="bloc_message">
	 <div class="bloc_pseudo_photo">';
		if ($donnees5['photo_profil'] != 0)
		{
	        echo'
			<a  href="profil-i'.$donnees4['id_jeu'].'html">';
			$source = getimagesize('images_utilisateurs/'.$donnees5['photo_profil']); // La photo est la source
			
		   echo'<div class="centre_image60">';
			if ($source[0] <= 60 AND $source[1] <= 60)						
				echo'
				<img src="images_utilisateurs/'.$donnees5['photo_profil'].'" alt="Photo de profil"/>
			</a>';
			else
				echo'
				<img src="images_utilisateurs/mini_2_'.$donnees5['photo_profil'].'" alt="Photo de profil"/>
			</a>';
		   echo'</div>';
		}
		else
		{
	        echo'
			<a  href="profil-i'.$donnees4['id_jeu'].'html">
				<img  class="image_publication" src="images/tete3.png" alt="Photo de profil"/>
			</a>';
		}
		echo'
		<p class="pseudo_date_message">
			<a  href="profil-i'.$donnees4['id_jeu'].'.html">
				<span style="color:#102c3c;">
					'.stripslashes(htmlspecialchars($donnees5['pseudo'])).'
				</span>
			</a>';
		echo'
			<span style="font-size:small; color:#375668;;"> 
				le '.$donnees4['date_message'].'
			</span>
		</p>
	 </div>';
		echo '
		<p class="message_message">'
			.$donnees4['message'].'
		</p>';
		echo '
		<p class="rep_supprimer_message">
			<a href="" name="'.$donnees4['id_jeu'].'"  id="envoyer_message">R�pondre</a>
		</p>
		
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce message ?\');
		if (sup == 0)return false;" href="message.php?id_message='.$donnees4['id'].'&supprimer_message">
			<div class="supprimer_message"></div>
		</a>
		
		<div id="corps_invisible"></div> 
	</div>';
	}
	$i++;

}
if ($i==0)
	echo'
		<div id="demande_notification2">
			Aucun message.
		</div>';

// GESTION AFFICHAGE PAGES

$nbre_page = 1;
$p = $bdd->prepare('SELECT COUNT(*) AS nbre_message FROM message 
					WHERE destinataire=:id_jeu')
					or die(print_r($bdd->errorInfo()));
$p->execute(array('id_jeu' =>$_SESSION['id_jeu']))
					or die(print_r($bdd->errorInfo()));
$do = $p->fetch();
$nbr_entrees = $do['nbre_message'];
$p->closeCursor();

if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
// DEFINIR LES VARIABLES AVANT LE INCLUDE///////////////////////////////////////	
$nom_page = 'message';
$nbr_affichage = 15;

include('pagination.php');
?>
</div>
	
<script>
	
//------------------------ POUR ENVOYER MESSAGE --------------------------------
var message = document.getElementsByTagName('a');
var id_jeu = document.getElementById('destinataire_id');
for (var i = 0, c = message.length ; i < c ; i++) {
	if (message[i].id == 'envoyer_message')
	{
		id_jeu.value = '';
		var mess = message[i];
		mess.indice = mess.name;
		mess.onclick = function() {
		id_jeu.value = this.indice;
		overlay_envoyer_message.style.display = 'block';
		grand_overlay1.style.display = 'block';
		return false; // on bloque la redirection
		};	
	}
}

	document.getElementById('fermer').onclick = function() {
    overlay_envoyer_message.style.display = 'none';
	grand_overlay1.style.display = 'none';
	return false; // on bloque la redirection
	}; 	
//------------------------------------------------------------------------------

</script>

<?php 
include('pied_page.php'); 
?>
