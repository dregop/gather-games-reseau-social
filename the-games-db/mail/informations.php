<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
	if (!isset($_SESSION['nom_de_compte']))
	{
	header('Location: index.html');
	}
	if (!isset($_SESSION['nom_de_compte']) AND !isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
	$req = $bdd->prepare('SELECT id, reponse, question FROM membres 
						WHERE nom_de_compte=:nom_de_compte')
						or die(print_r($bdd->errorInfo())); 					// L'ID POUR LA REDIRECTION VERS CHOIX PROFIL
	$req->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));
	$donnees = $req->fetch();
	$req->closeCursor(); // Termine le traitement de la requ�te
?>
 <!DOCTYPE html>
  
<html>

 <head>
 
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="index.css"/>    
    <link href="style.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
</head>
 
  <body style="background-image:url(images/bruit3.png);">  
 
 <!-- BLOC POUR ACCEDER AUX INFOS -->
	 <div id="fond_banniere">
	 
          <div id="banniere">   		  	           
	        <img id="grand_logo" src="images/grand_logo.png" alt="logo"/>
          </div>
          
		  <div id="bordure1"></div>
		  <div id="bordure2"></div>
		  
     </div>
    <div class="corps">
<?php 
if (!isset($_SESSION['photo_profil']))
{
  echo'<img id="etapes2" src="images/etapes2.png" alt="�tapes 2"/>';
}
// CES DONNEES SERVENT POUR LE IF LIGNE 30 ET LE IF LIGNE 84
$reqq = $bdd->prepare('SELECT * FROM jeu WHERE nom_de_compte=:nom_de_compte')
						or die(print_r($bdd->errorInfo()));
$reqq->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));
$donneess = $reqq->fetch();
$reqq->closeCursor(); // Termine le traitement de la requ�te
	
//MESSAGE D'ERREUR IMAGE INVALIDE
if (isset($_GET['erreur']) AND $_GET['erreur'] == 'e9')
{
	echo'
	<p class="bloc_erreur" style="height:40px">
		<img src="images/attention.png" alt="erreur">
		Image non valide, la photo de profil par d�faut du site est enregistr�e. 
		Vous pouvez supprimer votre sous-compte et recommencer. <br /> 
		(Formats autoris�s : JPEG, PNG / Taille maximale : 1 Mo)
	</p>';
}
//MESSAGE D'ERREUR BLOCK	
if (isset($_GET['erreur']) AND $_GET['erreur'] == 'e1')
{
	echo'
	<p class="bloc_erreur" style="height:40px">
		<img src="images/attention.png" alt="erreur"/> 
		Veuillez remplir tous les *champs tout en faisant attention � votre 
		pseudo qui doit �tre compris entre 2 et 20 caract�res (les (") et (\') 
		ne sont pas accept�s). La description doit contenir 250 caract�res 
		maximum 
	</p>';		
}

if (isset($_GET['max']))
{
	echo'
	<p class="bloc_erreur">
		<img src="images/attention.png" alt="erreur" /> 
		Vous ne pouvez avoir que 5 sous comptes.
	</p>';
}
	   
$requte = $bdd->prepare('SELECT * FROM jeu WHERE nom_de_compte=:nom_de_compte')
						or die(print_r($bdd->errorInfo()));
$requte->execute(array('nom_de_compte' => $_SESSION['nom_de_compte']))
						or die(print_r($bdd->errorInfo()));
while($donnes = $requte->fetch())
{
	echo '
	<div class="sous_corps3_informations_entier">
		<p class="pseudo">
			'.stripslashes(htmlspecialchars($donnes['pseudo'])).'
		</p>';
	
		//AFFICHAGE DE LA PHOTO DE PROFIL DANS LE BLOCK ENREGISTRE 
		if ($donnes['photo_profil'] != 0)
		{
			echo'
			<div class="centre_image_choix">';
			$source = getimagesize('images_utilisateurs/'.$donnes['photo_profil']); // La photo est la source
			if ($source[0] <= 200 AND $source[1] <= 200)
				echo'<img class="photo_profil" alt="Photo de profil" src="images_utilisateurs/'.$donnes['photo_profil'].'" alt="Photo de profil"/></div>';
			else
				echo'<img class="photo_profil" alt="Photo de profil" src="images_utilisateurs/mini_3_'.$donnes['photo_profil'].'" alt="Photo de profil"/></div>';	 
		}
	   else
		{
		  echo'<img class="photo_profil2" src="images/tete1.png" alt="Photo de profil"/>';
		}
		//FIN       
		echo' 
		<input type="hidden" name="id" value="'.$donnes['id'].'"/>		      
		<p class="info_jeu_enregistre">
			Plateforme : 
			<span style="color:#5a7d91">
				'.$donnes['plateforme'].'
			</span>
		</p>		
		<p class="info_jeu_enregistre">
			Jeu : 
			<span style="color:#5a7d91">
				'.substr(stripslashes(htmlspecialchars($donnes['nom_jeu'])), 0, 25).'';
				if(strlen(stripslashes(htmlspecialchars($donnes['nom_jeu']))) > 25)
				{ echo'...';} 
		echo' 
			</span>
		</p>';

	$selected = 'selected="selected"';
	$message3 = nl2br(stripslashes(htmlspecialchars($donnes['description'])));
	$message3 = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message3);
	if($donnes['description'] != '')
	{
		echo '
		<p class="info_jeu_enregistre">Description :
			<span style="color:#5a7d91">
				'.substr($message3, 0, 20).'';
				if(strlen($message3) > 20){ echo'...';}echo'
			</span>
		</p>';
	}
	else
	{
		echo '
		<p class="info_jeu_enregistre">Description :
			<span style="font-style: italic;">
				aucune
			</span>
		</p>';
	}

	if(isset($_SESSION['id_jeu']) 
	AND $donnes['id'] != $_SESSION['id_jeu'])
	{
		echo '
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce sous-compte ?\');
		if (sup == 0)return false;" href="informations_post.php?supprimer&id='.$donnes['id'].'">
			<div class="supprimer"></div>
		</a>';
	}
	elseif(isset($_SESSION['id_jeu']) 
	AND $donnes['id'] == $_SESSION['id_jeu'])
	{
		echo'<img id="connecte_informations" src="images/connecte_informations.png" alt="Vous y �tes connect�"/>';
	}
	else
	{
		echo '
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer ce sous-compte ?\');
		if (sup == 0)return false;" href="informations_post.php?supprimer&id='.$donnes['id'].'">
			<div class="supprimer"></div>
		</a>';
	}  
		echo'
	</div>';
}
	
if($donneess['photo_profil'] == '' AND$donneess['description'] == '' 
AND $donneess['pseudo'] == '' AND $donneess['plateforme'] == '' 
AND $donneess['nom_jeu'] == '')
{ 
  echo'
 <img id="premier" src="images/premier.png">
 <div id="sous_corps3_informations">
		<form action="informations_post.php" method="post" enctype="multipart/form-data">
			<p class="jeu1">Sous-compte :</p>
			<img class="tete" src="images/tete1.png" alt=" "/>
			<p id="selectionner"> 
				S�lectionner un avatar <br /> 
				<input type="file" name="photo_profil" value="Parcourir" id="file" />
			</p>
			<p class="info_jeu">
				<label for="plateforme">
					<span style="color:#102c3C" class="champs">*</span>
					Sur quel plateforme jouez-vous ? 
					<br />
				</label>
				<select class="input1" name="plateforme" id="plateforme" style="width:345px; height:32px;" >
				   <option value="S�lectionnez">S�lectionnez</option>
				   <option value="3DO">3DO</option>
				   <option value="Amiga CD32">Amiga CD32</option>
				   <option value="Atari 2600">Atari 2600</option>
				   <option value="Atari 5200">Atari 5200</option>
				   <option value="Atari 7800">Atari 7800</option>
				   <option value="Dreamcast">Dreamcast</option>
				   <option value="Famicom Disk">Famicom Disk</option>
				   <option value="GameBoy">GameBoy</option>
				   <option value="GameBoy Advance">GameBoy Advance</option>
				   <option value="GameBoy SP">GameBoy SP</option>
				   <option value="GameCube">GameCube</option>
				   <option value="Game Gear">Game Gear</option>
				   <option value="Iphone">Iphone</option>
				   <option value="Jaguar">Jaguar</option>
				   <option value="Lynx">Lynx</option>
				   <option value="Mac OS">Mac OS</option>
				   <option value="Master System">Master System</option>
				   <option value="Mega CD">Mega CD</option>
				   <option value="Megadrive">Megadrive</option>
				   <option value="NeoGeo">NeoGeo</option>
				   <option value="NeoGeo CD">NeoGeo CD</option>
				   <option value="NES">NES</option>
				   <option value="Nintendo 64">Nintendo 64</option>
				   <option value="Nintendo DS" >Nintendo DS</option>
				   <option value="Nintendo 3DS">Nintendo 3DS</option>
				   <option value="PC">PC</option>
				   <option value="Playstation">Playstation</option>
				   <option value="Playstation 2">Playstation 2</option>
				   <option value="Playstation 3">Playstation 3</option>
				   <option value="Playstation 4">Playstation 4</option>			   
				   <option value="PSP">PSP</option>
				   <option value="PSP GO">PSP GO</option>
				   <option value="PSP Vita">PSP Vita</option>
				   <option value="Saturn">Saturn</option>
				   <option value="Super NES" >Super NES</option>
				   <option value="Wii" >Wii</option>
				   <option value="Wii U" >Wii U</option>
				   <option value="WonderSwan" >WonderSwan</option>
				   <option value="Xbox" >Xbox</option>
				   <option value="Xbox 360" >Xbox 360</option>
				   <option value="Xbox One" >Xbox One</option>
				</select>
			</p>
			<p class="info_jeu">
				<span style="color:#102c3C">*</span>
				Votre jeu <br /> 
				<input class="input3" id="recherche" type="text"/ name="jeu" style="width:340px; height:25px;" />
			</p>
			<div id="results" style="display:none;"></div>
			<p class="info_jeu">
				<span style="color:#102c3C">*</span>
				Votre pseudo sur le jeu <br /> 
				<input class="input3" type="text" name="pseudo" style="width:340px; height:25px;"/>
			</p>
			<p class="info_jeu">
				Votre description en jeu 
				<span id="info_jeu"> (250 caract�res maximum)</span> <br />
				<textarea class="input3" name="description_jeu" rows="4" cols="35" maxlength="1000" ></textarea>
			</p>
			<input id="enregistrez" name="enregistrez2" type="submit"  value="Enregistrez"/>
		</form>';
 echo'</div>';
}
else
{
	if(isset($_GET['plus']))
	{
	echo' 
		<div id="scc">'; /* sous_corps3_informations_cache */
	}
	else
	{
	echo'
		<div id="scc" style="display:none;">'; /* sous_corps3_informations_cache */
	}
	echo'
		<form action="informations_post.php" method="post" enctype="multipart/form-data">
			<p class="jeu1">Sous-compte :</p>
			<img class="tete" src="images/tete1.png" alt=" "/>
			<p id="selectionner"> S�lectionner un avatar <br /> 
				<input type="file" name="photo_profil" value="Parcourir" id="file" />
			</p>
			<p class="info_jeu">
				<label for="plateforme">
					<span style="color:#102c3C" class="champs">*</span>
					Sur quel plateforme jouez-vous ? 
					<br />
				</label>
				<select class="input1" name="plateforme" id="plateforme" style="width:345px; height:32px;" >
				   <option value="S�lectionnez">S�lectionnez</option>
				   <option value="3DO">3DO</option>
				   <option value="Amiga CD32">Amiga CD32</option>
				   <option value="Atari 2600">Atari 2600</option>
				   <option value="Atari 5200">Atari 5200</option>
				   <option value="Atari 7800">Atari 7800</option>
				   <option value="Dreamcast">Dreamcast</option>
				   <option value="Famicom Disk">Famicom Disk</option>
				   <option value="GameBoy">GameBoy</option>
				   <option value="GameBoy Advance">GameBoy Advance</option>
				   <option value="GameBoy SP">GameBoy SP</option>
				   <option value="GameCube">GameCube</option>
				   <option value="Game Gear">Game Gear</option>
				   <option value="Iphone">Iphone</option>
				   <option value="Jaguar">Jaguar</option>
				   <option value="Lynx">Lynx</option>
				   <option value="Mac OS">Mac OS</option>
				   <option value="Master System">Master System</option>
				   <option value="Mega CD">Mega CD</option>
				   <option value="Megadrive">Megadrive</option>
				   <option value="NeoGeo">NeoGeo</option>
				   <option value="NeoGeo CD">NeoGeo CD</option>
				   <option value="NES">NES</option>
				   <option value="Nintendo 64">Nintendo 64</option>
				   <option value="Nintendo DS" >Nintendo DS</option>
				   <option value="Nintendo 3DS">Nintendo 3DS</option>
				   <option value="PC">PC</option>
				   <option value="Playstation">Playstation</option>
				   <option value="Playstation 2">Playstation 2</option>
				   <option value="Playstation 3">Playstation 3</option>
				   <option value="Playstation 4">Playstation 4</option>			   
				   <option value="PSP">PSP</option>
				   <option value="PSP GO">PSP GO</option>
				   <option value="PSP Vita">PSP Vita</option>
				   <option value="Saturn">Saturn</option>
				   <option value="Super NES" >Super NES</option>
				   <option value="Wii" >Wii</option>
				   <option value="Wii U" >Wii U</option>
				   <option value="WonderSwan" >WonderSwan</option>
				   <option value="Xbox" >Xbox</option>
				   <option value="Xbox 360" >Xbox 360</option>
				   <option value="Xbox One" >Xbox One</option>
				</select>
			</p>
			<p class="info_jeu">
				<span style="color:#102c3C">*</span>
				Votre jeu <br /> 
				<input id="recherche" class="input3" type="text"/ name="jeu" style="width:340px; height:25px;" >
			</p>
			<div id="results" style="display:none;"></div>
			<p class="info_jeu">
				<span style="color:#102c3C">*</span>
				Votre pseudo sur le jeu <br /> 
				<input class="input3" type="text" name="pseudo" style="width:340px; height:25px;"/>
			</p>
			<p class="info_jeu">
				 Votre description en jeu 
				<span id="info_jeu"> (250 caract�res maximum)</span> <br />
				<textarea class="input3" name="description_jeu" rows="4" cols="35" maxlength="1000" ></textarea>
			</p>
			<input id="enregistrez" name="enregistrez2" type="submit"  value="Enregistrer"/>
		</form>
	 </div>

	<div id="fin_corps">
	   <p id="texte_fin" > Vous pouvez d�sormais acc�der � votre profil ou ajouter un autre sous compte </p>

		<div id="plus"></div>
		<a href="choix-'.$donnees['id'].'.html">
			<div id="profil" ></div>
		</a>';
	echo'
	</div>';
}
?>
  
<script>
var results = document.getElementById('results');
var recherche = document.getElementById('recherche');
var mot_recherche, reponse;
var selection = -1;
recherche.addEventListener('keyup', function (event) 
{ 
var xdr = new XMLHttpRequest();  

	mot_recherche = recherche.value;
	xdr.open('GET', 'requete_ajax.php?name=' + mot_recherche);
	xdr.onreadystatechange = function(e) // On g�re ici une requ�te asynchrone
	{ 
        if (xdr.readyState == 4 && xdr.status == 200) // Si le fichier est charg� sans erreur
		{ 
var parser = new DOMParser();
var xmlDoc = parser.parseFromString(xdr.responseText, "application/xml");
            reponse = xmlDoc.getElementsByTagName('GameTitle');
			results.innerHTML = '';
			var taille_reponse = reponse.length;
			for (var i=0; i < taille_reponse; i++)
			{
				div = document.createElement('div');
				div.innerHTML = reponse[i].textContent;
				results.appendChild(div);
			}
			results.style.display = 'block';
        }
		else if(xdr.readyState == 4 && xdr.status != 200)
		{
			alert('Une erreur est survenue !\n\nCode :' + xdr.status + '\nTexte : ' + xdr.statusText);
		}
    }
	xdr.send(null);
},false);
results.addEventListener('click', function (event)
{
	texte = event.target.textContent;
	recherche.value = texte;
	results.style.display = 'none';
},false);

var sous_corps3 = document.getElementById('scc'); ///* sous_corps3_informations_cache */
var plus = document.getElementById('plus');
plus.addEventListener('click', function () 
{
	sous_corps3.style.display = 'block';
	fin_corps.style.display = 'none';
},false);
</script>

<?php
include('pied_copyright.php'); 
?>