-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 11 Mars 2014 à 14:25
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gather_ga`
--
CREATE DATABASE IF NOT EXISTS `gather_ga` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gather_ga`;

-- --------------------------------------------------------

--
-- Structure de la table `jeu`
--

CREATE TABLE IF NOT EXISTS `jeu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_de_compte` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `nom_jeu` varchar(255) NOT NULL,
  `id_clan` int(11) NOT NULL,
  `photo_profil` varchar(11) NOT NULL,
  `description` text NOT NULL,
  `plateforme` varchar(255) NOT NULL,
  `date_modification` datetime NOT NULL,
  `geth` int(11) NOT NULL,
  `clan_view` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_de_compte` varchar(255) NOT NULL,
  `admin` varchar(255) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL,
  `reponse` varchar(255) NOT NULL,
  `date_inscription` datetime NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `pays` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `twitch` varchar(255) NOT NULL,
  `eclypsia` varchar(255) NOT NULL,
  `daylimotion` varchar(255) NOT NULL,
  `site_web` varchar(255) NOT NULL,
  `nouveau` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
