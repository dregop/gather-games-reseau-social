<?php
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: index.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
?>

<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
 <body>  
 
<?php 
if (isset($_SESSION['id_jeu']))
{
?> 		
	<div id="fond_banniere" > 
		<div id="banniere">
		
			<div id="mon_compte">
<?php
			if ($_SESSION['photo_profil'] != 0 
			AND $_SESSION['photo_profil'] != '')
			{
				echo'<div class="centre_image40">';
				$source = getimagesize('images_utilisateurs/'.$_SESSION['photo_profil']); // La photo est la source
				if ($source[0] <= 40 AND $source[1] <= 40)
					echo'<img src="images_utilisateurs/'.$_SESSION['photo_profil'].'" alt="Photo de profil"/>';
				else
					echo'<img src="images_utilisateurs/mini_4_'.$_SESSION['photo_profil'].'" alt="Photo de profil"/>';
			    echo'</div>'; 
			}
			else 
			  echo '<img class="image40" src="images/tete2.png" alt="Photo de profil"/>';
			  
			  echo '<img class="fleche" src="images/fleche.png" alt="Fleche"/>
			  
					<div id="compte">
						<p id="texte_menu1">
							<a href="profil.html">
								Mon profil
							</a>
							<br />
							<a href="informations315.html">
								Mes informations
							</a>
							<br />
							<a href="choix-profil.html">
								Changez de sous-compte
							</a>
							<br />
							<a href="deconnection.php">
								Déconnexion
							</a> 
						</p>
					</div>
			</div>';
			
			if(isset($_SESSION['pseudo']) AND strlen($_SESSION['pseudo'])<11 
			AND strlen($_SESSION['pseudo'])>5)
			{
				echo'
				<a href="profil.html">
					<span id="pseudo_menu1">
						'.stripslashes(htmlspecialchars($_SESSION['pseudo'])).'
					</span> 
				</a>';
			}
			elseif(isset($_SESSION['pseudo']) AND strlen($_SESSION['pseudo'])<6)
			{
				echo'
				<a href="profil.html">
					<span id="pseudo_menu3">
						'.stripslashes(htmlspecialchars($_SESSION['pseudo'])).'
					</span> 
				</a>';
			}
			else
			{
				echo'
				<a href="profil.html">
					<span id="pseudo_menu2">
						'.stripslashes(htmlspecialchars($_SESSION['pseudo'])).'
					</span> 
				</a>';
			}
			
			//ON RECUPERE LES VIEW DANS MESSAGE  
			$req1_notif = $bdd->prepare('SELECT view FROM message 
										WHERE view = 1 
										AND destinataire=:destinataire')
										or die(print_r($bdd->errorInfo()));
			$req1_notif->execute(array('destinataire' => $_SESSION['id_jeu']))
										or die(print_r($bdd->errorInfo()));
			$n1=0;
			while ($do1_notif = $req1_notif->fetch())
			{
				$n1++;
			}
			//ON RECUPERE LES VIEW DANS DEMANDE AMI
			$req2_notif = $bdd->prepare('SELECT view FROM demande_amis 
										WHERE view = 1 AND id_jeu=:id_jeu ')
										or die(print_r($bdd->errorInfo()));
			$req2_notif->execute(array('id_jeu' => $_SESSION['id_jeu']))
										or die(print_r($bdd->errorInfo()));
			$n2=0;
			while ($do2_notif = $req2_notif->fetch())
			{
				$n2++;
			}
			
			// ON S'OCCUPE DES NOTIFICATIONS !!! 
			// UN AMI PUBLIE SUR SON MUR  ou COMMENTE UNE PUBLICATION SUR 
			// SON MUR
			$req3_notif = $bdd->prepare('SELECT view FROM notifications 
										WHERE view = 1 AND id_jeu_lieu=:id_jeu 
										AND view=1 AND id_publication=0 ')
										or die(print_r($bdd->errorInfo()));
			$req3_notif->execute(array('id_jeu' => $_SESSION['id_jeu']))
										or die(print_r($bdd->errorInfo()));
			$n3=0;
			while ($do3_notif = $req3_notif->fetch())
			{
				$n3++;
			}
			
			// ON S'OCCUPE DES NOTIFICATIONS !!! 
			// TOUS LES COMMENTAIRES ASSOCIE A SA PUBLICATION
			$req4_notif = $bdd->prepare('SELECT id FROM publication 
										WHERE id_jeu =:id_jeu ')
										or die(print_r($bdd->errorInfo()));
			$req4_notif->execute(array('id_jeu' => $_SESSION['id_jeu']))
										or die(print_r($bdd->errorInfo()));
			$n4=0;
			while ($do4_notif = $req4_notif->fetch())
			{
				$req5_notif = $bdd->prepare('SELECT view, id_jeu_lieu 
											FROM notifications 
											WHERE id_publication =:id_publication 
											AND view=1 
											AND id_jeu_com != :id_jeu_com ')
											or die(print_r($bdd->errorInfo()));
				$req5_notif->execute(array('id_publication' => $do4_notif['id'], 
											'id_jeu_com' => $_SESSION['id_jeu']))
											or die(print_r($bdd->errorInfo()));
				while ($do5_notif = $req5_notif->fetch())
				{
					$n4++;
				}
			}		
			
		if($n1 > 0 OR $n2 > 0 OR $n3 > 0 OR $n4 > 0)// notification.php?vu
		{
			echo'
			<div id="contient_notifs">
				<div id="notif2_banniere">
					<object type="application/x-shockwave-flash" height="52" width="30" data="images/notif_banniere3.swf"
					classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" 
					codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0"> 
							<param name="wmode" value="transparent">
							<param name="movie" value="images/notif_banniere3.swf">
							<param name="quality" value="high" />
							<embed src="images/notif_banniere3.swf" 
							wmode="transparent" quality="high" 
							pluginspage="http://www.macromedia.com/go/getflashplayer" 
							type="application/x-shockwave-flash" 
							width="30" height="52"></embed>
					</object>
				</div>';
			echo'	
				<a href="notification-v.html">
					<div id="devant">
					</div>
				</a>
			</div>';
		}
		else // notification.php
		{
			echo'
			<a href="notification.html">
				<div id="notif_banniere"></div>
			</a>';
		}
?>
		 
			<a href="classement.html">
				<img id="geth" src="images/geth.png" alt="Geth"/>
			</a>
			
			<div id="menu_clan">
				<img id="clan" src="images/clan.png" alt="Clans"/>
				<div id="sous_clan">
					<p id="texte_clan">
				     <a href="team.html">Ma Team</a><br />
				     <a href="recrutements.html">Recrutements</a>
					</p>		
				</div>
			</div>
			<div id="menu_actualites">
				<img id="actualites" src="images/actualites.png" alt="Actualités"/>
				<div id="sous_actualites">	
					<p id="texte_actualites">				
				     <a href="accueil.html">Mes amis</a><br />
				     <a href="communaute.html">La communauté</a>
					</p>
				</div>
			</div>
						
			<form action="recherche.html" method="post">
<?php
				if($n1 > 0 OR $n2 > 0 OR $n3 > 0 OR $n4 > 0)// notification.php?vu
				{
					echo'<input id="recherche2" name="recherche" type="text" placeholder=" Recherchez des joueurs mais aussi une team pour partagez vos expériences ... " /> <br /> ';
				}
				else
				{
					echo'<input id="recherche" name="recherche" type="text" placeholder=" Recherchez des joueurs mais aussi une team pour partagez vos expériences ... " /> <br /> ';
				}
?>
			</form>
				 		
		</div>  
		
        <div id="bordure1"></div>
		<div id="bordure2"></div>
		
		<!-- Pour les bordures qui suivent jusqu'au bout et cache le box-shadow -->		
		<div id="bordure3"></div>
		<div id="bordure4"></div>
		
    </div>
	
<?php
include('amis_connecte.php');
}
?>