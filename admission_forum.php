<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
if (empty($_SESSION['pseudo']))
{
	header('Location: informations-e1.html');
}
if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');

$requete = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id_jeu')
						or die(print_r($bdd->errorInfo()));
$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
$donnees_jeu = $requete->fetch();

$reqs = $bdd->prepare('SELECT * FROM clan WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$reqs->execute(array('id' => $donnees_jeu['id_clan']))
						or die(print_r($bdd->errorInfo()));
$donnees = $reqs->fetch();

if ($donnees['id_jeu'] != $_SESSION['id_jeu'])
{
	header('Location: team.html');
}
if (isset($_GET['candidature'], $_GET['id_jeu']))
{
	if ($_GET['candidature'] == 0)
	{
		$req = $bdd->prepare('UPDATE jeu SET id_clan=:id_clan  
								WHERE id=:id_jeu') 
								or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_clan' => $donnees['id'],
							'id_jeu' => $_GET['id_jeu'])) 
							or die(print_r($bdd->errorInfo()));
		$req->closeCursor(); // Termine le traitement de la requ�te	
		$req = $bdd->prepare('DELETE FROM candidature WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_GET['id_jeu'])) 
							or die(print_r($bdd->errorInfo()));
		header('Location: admission.html');
	}
	if ($_GET['candidature'] == 1)
	{
		$req = $bdd->prepare('DELETE FROM candidature WHERE id_jeu=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_GET['id_jeu'])) 
							or die(print_r($bdd->errorInfo()));
		header('Location: admission.html');
	}
}
include('menu.php'); 

echo '<div id="corps_admission">';

		echo'
		<a href="team.html">
			<div id="retour_admission"></div>
		</a>';
		
		if (isset($_GET['page']) AND $_GET['page'] > 0)
		{
			$numero_page = $_GET['page'];
			$numero_page--;
			$numero_page = 20*$numero_page;
		}
		else
			$numero_page = 0;
			
		$reqss = $bdd->prepare('SELECT * FROM candidature 
								WHERE id_clan=:id_clan 
								ORDER BY id 
								DESC LIMIT '.$numero_page.',20')
								or die(print_r($bdd->errorInfo()));
		$reqss->execute(array('id_clan' => $donnees['id']))
								or die(print_r($bdd->errorInfo()));
		$i=0;
		while ($donnees2 = $reqss->fetch())
		{
			$i++;
			$reqs3 = $bdd->prepare('SELECT * FROM jeu WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
			$reqs3->execute(array('id' => $donnees2['id_jeu']))
									or die(print_r($bdd->errorInfo()));
			$donnees3 = $reqs3->fetch();
			echo '
		<div class="bloc_admission">';
		
			if($donnees3['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'
				<a href="profil-i'.$donnees3['id'].'.html">
					<p class="pseudo_admission">
						'.htmlspecialchars(stripslashes($donnees3['pseudo'])).'
					</p>
				</a>';
			}
			elseif($donnees3['id'] == $_SESSION['id_jeu'])
			{
				echo'
				<a href="profil.html" title="Mon profil" >
					<p class="pseudo_admission">
						'.htmlspecialchars(stripslashes($donnees3['pseudo'])).'
					</p>
				</a>';	
			}
			else
			{
				echo'
				<p class="pseudo_admission" style="color:#102c3c;" title="Mon sous compte">
					'.htmlspecialchars(stripslashes($donnees3['pseudo'])).'
				</p>';
			}
			
			echo'	
			<div class="bloc_message_admission">
				<span style="color:#102c3c;font-weight:bolder;">
					Message da candidature :
				</span> 
				<br />';
				$message = nl2br(htmlspecialchars(stripslashes($donnees2['message'])));
				$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);
			echo'<p class="message_admission">
					'.$message.'
				</p>';
			echo'<p style="text-align:center;">
					<a href="admission_forum.php?candidature=0&id_jeu='.$donnees3['id'].'" id="accepter_admission">
						Accepter
					</a> / 
					<a href="admission_forum.php?candidature=1&id_jeu='.$donnees3['id'].'" id="refuser_admission">
						Refuser
					</a>
				</p>			
			</div>
			
			<div class="image_admission">';
			if($donnees3['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'<a  href="profil-i'.$donnees3['id'].'.html" title="'.htmlspecialchars(strip_tags($donnees3['pseudo'])).'">';
			elseif($donnees3['id'] == $_SESSION['id_jeu'])
				echo'<a  href="profil.html" title="Mon profil">';
			else
				echo'<span  title="Mon sous compte">';
				
			if ($donnees3['photo_profil'] != 0)
			{
				$source = getimagesize('images_utilisateurs/'.$donnees3['photo_profil']); // La photo est la source
				if ($source[0] <= 200 AND $source[1] <= 200)
					echo'<img class="photo_profil"  src="images_utilisateurs/'.$donnees3['photo_profil'].'" alt="Photo de profil" />';
				else
					echo'<img class="photo_profil"  src="images_utilisateurs/mini_3_'.$donnees3['photo_profil'].'" alt="Photo de profil" />';
			}
			else 
				echo'<img src="images/tete1.png" alt="Photo de profil"/>';
				
			if($donnees3['nom_de_compte'] != $_SESSION['nom_de_compte'])
				echo'</a>';
			elseif($donnees3['id'] == $_SESSION['id_jeu'])
				echo'</a>';
			else
				echo'</span>';
				
			echo'
			</a>
			</div>
			<div id="corps_invisible"></div>';
			
		echo'
		</div>';
		}
		if ($i == 0)
		{
			echo'
			<div id="bloc_aucune_candidature">
				<p id="aucune_admission">
					Aucune candidature... Parlez de votre Clan � vos ami(e)s !
				</p>
			</div>';
		}
		
	// GESTION AFFICHAGE PAGES
	echo'<p class="nombre_page">';

	$nbre_page = 1;
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_com FROM candidature 
					WHERE id_clan=:id_clan')
					or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_clan' => $donnees['id']))
					or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_com'];
	$p->closeCursor();

	if (isset ($_GET['page']))
		$current_page = $_GET['page'];
	else
		$current_page = 1;
		
	$nom_page = 'admission';
	$nbr_affichage = 20;

	include('pagination.php');

echo'</div>';

include('pied_page.php'); ?>