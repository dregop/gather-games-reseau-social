<?php 
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
    die('Erreur : '.$e->getMessage());
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (isset($_SESSION['id_jeu_videos']) OR isset($_SESSION['id_clan_videos']))
{
	$_SESSION['id_jeu_videos'] = '';
	$_SESSION['id_clan_videos'] = '';
}
if (isset($_GET['id_jeu']))
{ 
	$_SESSION['id_jeu_videos'] = $_GET['id_jeu'];
}
elseif (isset($_GET['id_clan']))
{ 
	$_SESSION['id_clan_videos'] = $_GET['id_clan'];
}

if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc 
								FROM jeu WHERE nom_de_compte=:ndc')
								or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
								or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}
if (!isset($_SESSION['nom_de_compte']))
{
header('Location: index.html');
}
if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
if (empty($_SESSION['pseudo']))
{
	header('Location: informations-pseudo.html');
}

if (isset($_SESSION['id_clan_videos']) AND $_SESSION['id_clan_videos'] != '')
{
	if (isset($_GET['supprimer_video'], $_SESSION['statu']) AND $_SESSION['statu'] == 'meneur')
	{
		$req1 = $bdd->prepare('DELETE FROM videos WHERE id=:id')or die(print_r($bdd->errorInfo()));
		$req1->execute(array(
		'id' => $_GET['supprimer_video'])) or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		header('Location: videos-it'.$_GET['id_clan'].'.html');
	}
}
elseif (isset($_SESSION['id_jeu_videos']) AND $_SESSION['id_jeu_videos'] != '')
{
	if (isset($_GET['supprimer_video']) AND $_SESSION['id_jeu_videos'] == $_SESSION['id_jeu'])
	{
		$req1 = $bdd->prepare('DELETE FROM videos WHERE id=:id')or die(print_r($bdd->errorInfo()));
		$req1->execute(array(
		'id' => $_GET['supprimer_video'])) or die(print_r($bdd->errorInfo()));
		$req1->closeCursor(); // Termine le traitement de la requ�te
		header('Location: videos-ij'.$_SESSION['id_jeu_videos'].'.html');
	}
}

?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body>
<?php

if (isset($_GET['watch_video']))
{		 
	echo'
	<div id="grand_overlay_videos">';
		if (isset($_SESSION['id_jeu_videos']) AND $_SESSION['id_jeu_videos'] != '')
		{
			echo'
			<a href="videos-ij'.$_SESSION['id_jeu_videos'].'.html">
				<img id="fermer_message" src="images/quitter_video.png" alt="Fermer"/>
			</a>';
		}
		if (isset($_SESSION['id_clan_videos']) AND $_SESSION['id_clan_videos'] != '')
		{
			echo'
			<a href="videos-it'.$_SESSION['id_clan_videos'].'.html">
				<img id="fermer_message" src="images/quitter_video.png" alt="Fermer"/>
			</a>';
		}
		
	echo'
	</div>';
	echo'
	<div id="overlay_videos" style="display:block">';
	
	if (isset($_SESSION['statu'], $_SESSION['id_clan_videos']) 
	AND $_SESSION['statu'] == 'meneur' AND $_SESSION['id_clan_videos'] != '')
	{
		echo '
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette vid�o ?\');
		if (sup == 0)return false;" id="liens_suprimer_video" href="videos.php?supprimer_video='.$_GET['id'].'&id_clan='.$_SESSION['id_clan_videos'].'">
			<img id="supprimer_video" src="images/supprimer_video.png" alt=" Supprimer la vid�o " />
		</a>';
	}
	elseif (isset($_SESSION['id_jeu_videos']) AND $_SESSION['id_jeu_videos'] != '')
	{
		echo '
		<a onclick ="var sup=confirm(\'�tes vous sur de vouloir supprimer cette vid�o ?\');
		if (sup == 0)return false;"id="liens_suprimer_video" href="videos.php?supprimer_video='.$_GET['id'].'&id_jeu='.$_SESSION['id_jeu_videos'].'">
			<img id="supprimer_video" src="images/supprimer_video.png" alt=" Supprimer la vid�o " />
		</a>';
	}	
	echo'
	<object type="application/x-shockwave-flash" width="700" height="450" data="http://www.youtube.com/v/'.stripslashes(htmlspecialchars($_GET['watch_video'])).'&hl=fr">							
		<param name="wmode" value="transparent" />
	</object> 				
		</div>';
}
	echo'
	<div id="overlay"></div>';
	//SEPARER LES DEUX BLOCS POUR EVITER L'INCOMPREHENSION DU NAVIGATEUR
	echo'
	<div id="contient_image_profil"></div>';
	
	include('menu_sans_doctype.php');
	
	echo'
	<div id="corps_images_clan">';
	if (isset($_SESSION['id_clan_videos']) AND $_SESSION['id_clan_videos'] != '')
	{
		if (isset($_GET['page']))
		{
			$numero_page = $_GET['page'];
			$numero_page--;
			$numero_page = 16*$numero_page;
		}
		else
			$numero_page = 0;

	$reqs = $bdd->prepare('SELECT * FROM clan WHERE id=:id_clan')
							or die(print_r($bdd->errorInfo()));
	$reqs->execute(array('id_clan' => $_SESSION['id_clan_videos']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $reqs->fetch();
	if ($donnees['id_jeu'] == $_SESSION['id_jeu'])
	{
		$_SESSION['statu'] = 'meneur';
	}
	echo'
	<img id="mosaique" src="images/mosaique_video.png" alt=" Mosa�que de vid�os " />';
	$re = $bdd->prepare('SELECT nom_clan FROM clan WHERE id=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id_clan' => $_SESSION['id_clan_videos']))
						or die(print_r($bdd->errorInfo()));
	$donnees = $re->fetch();
	
	echo'<div id="corps_images">';	
	$req = $bdd->prepare('SELECT adresse_stockage, id, id_jeu FROM videos 
						WHERE id_clan=:id_clan 
						ORDER BY id DESC LIMIT '.$numero_page.',16')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $_SESSION['id_clan_videos']))
						or die(print_r($bdd->errorInfo()));
	$i=0;
	while ($donneees2 = $req->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
	{
		$i++;
		$video = $donneees2['adresse_stockage'];
		$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i',
		'<a title="Voir la video"  href="videos.php?watch_video=$1&id_clan='.$_SESSION['id_clan_videos'].'&id='.$donneees2['id'].'">
			<img style="width:180px;height:120px;" src="http://i1.ytimg.com/vi/$1/default.jpg"  alt="video youtube"/>
		 </a>',$video);
		$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
		echo $video;
	}
	if($i==0)
	{
		echo '
		<div id="aucune_video_mosaique">
			Aucune vid�o dans la mosa�que.
		</div>';
	}
	echo'</div>
	<div id="corps_invisible_images"></div>';
	}
	elseif (isset($_SESSION['id_jeu_videos']) AND $_SESSION['id_jeu_videos'] != '')
	{
		if (isset($_GET['page']))
		{
			$numero_page = $_GET['page'];
			$numero_page--;
			$numero_page = 16*$numero_page;
		}
		else
			$numero_page = 0;	
			
		echo'
		<img id="mosaique" src="images/mosaique_video.png" alt=" Mosa�que de vid�os " />';
		$re = $bdd->prepare('SELECT pseudo FROM jeu WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
		$re->execute(array('id_jeu' => $_SESSION['id_jeu_videos']))
							or die(print_r($bdd->errorInfo()));
		$donnees = $re->fetch();
		echo'
		<div id="corps_images">';	
		$req = $bdd->prepare('SELECT adresse_stockage, id, id_jeu FROM videos 
							WHERE id_jeu=:id_jeu 
							ORDER BY id DESC LIMIT '.$numero_page.',16')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu_videos']))
							or die(print_r($bdd->errorInfo()));
		$i=0;
		while ($donneees2 = $req->fetch()) // ON AFFICHE MESSAGE PAR MESSAGE
		{
		$i++;
			$video = $donneees2['adresse_stockage'];
			echo'
			<div class="contient_video">';
				$video = preg_replace('#https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i',
				'<a title="Voir la video"  href="videos.php?watch_video=$1&id_jeu='.$_SESSION['id_jeu_videos'].'&id='.$donneees2['id'].'">
				 <img style="width:180px;height:120px;" src="http://i1.ytimg.com/vi/$1/default.jpg"  alt="video youtube"/>
				 </a>',$video);
			$video = preg_replace('#\#t=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
			echo $video;
			echo'
			</div>'; 
		}
		if($i==0)
		{
			echo '
			<div id="aucune_video_mosaique">
				Aucune vid�o dans la mosa�que.
			</div>';
		}
	echo'</div>
	<div id="corps_invisible_images"></div>
	';	
	}
// GESTION AFFICHAGE PAGES
$nbre_page = 1;
if (isset($_SESSION['id_jeu_videos']) AND $_SESSION['id_jeu_videos'] != '')
{
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM videos 
						WHERE id_jeu=:id_jeu')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_jeu' =>$_SESSION['id_jeu_videos']))
						or die(print_r($bdd->errorInfo()));
}
elseif (isset($_SESSION['id_clan_videos']) AND $_SESSION['id_clan_videos'] != '')
{
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publi FROM videos 
						WHERE id_clan=:id_clan')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('id_clan' =>$_SESSION['id_clan_videos']))
						or die(print_r($bdd->errorInfo()));
}	

$do = $p->fetch();
$nbr_entrees = $do['nbre_publi'];
$p->closeCursor();

if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;

// DEFINIR LES VARIABLES AVANT LE INCLUDE //////////////////////////////////////	
$nom_page = 'videos';
$nbr_affichage = 16;

include('pagination.php');

$req_clan = $bdd->prepare('SELECT id_clan FROM jeu WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
$req_clan->execute(array('id' => $_SESSION['id_jeu']))
						or die(print_r($bdd->errorInfo()));
$donnees_clan = $req_clan->fetch();

if(isset($_SESSION['id_clan_videos']) 
AND $donnees_clan['id_clan'] == $_SESSION['id_clan_videos'] AND $_SESSION['id_clan_videos'] != '' )
{
	echo'
	<a href="team.html">
		<img src="images/retour_clan.png" alt="Retour Clan" style="margin-top:20px;" />
	</a>';
}
elseif(isset($_SESSION['id_clan_videos']) 
AND $donnees_clan['id_clan'] != $_SESSION['id_clan_videos'] AND $_SESSION['id_clan_videos'] != '')
{
	echo'
	<a href="team-i'.$_SESSION['id_clan_videos'].'.html">
		<img src="images/retour_clan.png" alt="Retour Clan" style="margin-top:20px;" />
	</a>';
}
elseif(isset($_SESSION['id_jeu_videos']) AND $_SESSION['id_jeu'] == $_SESSION['id_jeu_videos'] 
AND $_SESSION['id_jeu_videos'] != '')
{
	echo'
	<a href="profil.html">
		<img src="images/retour_profil.png" alt="Retour profil" style="margin-top:20px;" />
	</a>';
}
elseif (isset($_SESSION['id_jeu_videos']) AND $_SESSION['id_jeu'] != $_SESSION['id_jeu_videos'] 
AND $_SESSION['id_jeu_videos'] != '')
{
	echo'
	<a href="profil-i'.$_SESSION['id_jeu_videos'].'.html">
		<img src="images/retour_profil.png" alt="Retour profil" style="margin-top:20px;" />
	</a>';
}
// FIN GESTION AFFICHAGE PAGES

echo'</div>';
include('pied_page.php');
?>