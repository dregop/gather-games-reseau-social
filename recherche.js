var results = document.getElementById('results');
var recherche = document.getElementById('recherche');
var mot_recherche, reponse;
var selection = -1;
recherche.addEventListener('keyup', function (event) 
{
	mot_recherche = recherche.value;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'ajax.php?recherche=' + mot_recherche);
	xhr.onreadystatechange = function(e) // On g�re ici une requ�te asynchrone
	{ 
        if (xhr.readyState == 4 && xhr.status == 200) // Si le fichier est charg� sans erreur
		{ 
			var results.className = 'recherche_en_cours';
            reponse = xhr.responseText;
			results.innerHTML = '';
			reponse = reponse.split('|');
			var taille_reponse = reponse.length;
			for (var i=0; i < taille_reponse; i++)
			{
				div = document.createElement('div');
				div.innerHTML = reponse[i];
				results.appendChild(div);
			}
        }
		else if(xhr.readyState == 4 && xhr.status != 200)
		{
			alert('Une erreur est survenue !\n\nCode :' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
    }
	xhr.send(null);
},false);
results.addEventListener('click', function (event)
{
	texte = event.target.textContent;
	recherche.value = texte;
},false);
