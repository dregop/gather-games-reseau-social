<?php session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
       die('Erreur : '.$e->getMessage());
}
if (isset($_SESSION['nom_de_compte']))
{
	$reqs_connecte = $bdd->prepare('SELECT nom_de_compte AS ndc 
									FROM jeu 
									WHERE nom_de_compte=:ndc')
									or die(print_r($bdd->errorInfo()));
	$reqs_connecte->execute(array('ndc' => $_SESSION['nom_de_compte']))
									or die(print_r($bdd->errorInfo()));
	$donnees_connecte = $reqs_connecte->fetch();
	if (!$donnees_connecte['ndc'])
		header('Location: informations.html');			
}

if (!isset($_SESSION['nom_de_compte']))
	header('Location: index.html');

if(!isset($_SESSION['id_jeu']))
	header('Location: index.html');
	
if (empty($_SESSION['pseudo']))
	header('Location: informations-e1.html');
	
	$requt1 = $bdd->prepare('SELECT id_clan 
							FROM jeu WHERE id=:id_jeu')
							or die(print_r($bdd->errorInfo()));
	$requt1->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
	$donnees_clan1 = $requt1->fetch();
	$requt2 = $bdd->prepare('SELECT * FROM clan WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$requt2->execute(array('id' => $donnees_clan1['id_clan']))
							or die(print_r($bdd->errorInfo()));
	$donnees_clan2 = $requt2->fetch();
	$_SESSION['statut'] = 'membre';
	
	if ($donnees_clan2['id_jeu'] == $_SESSION['id_jeu'])
	{
		$_SESSION['statut'] = 'meneur';
	}

////////////////////////// ON ENREGISTRE L'ANNONCE /////////////////////////////
if(isset($_POST['annonce'],$_POST['id_clan'], $_POST['id_jeu']) AND $_POST['annonce'] != '')
{
	if(strlen($_POST['annonce'])<501)
	{
		$req = $bdd->prepare('INSERT INTO recrutements 
							(id_clan,id_jeu,annonce,time, date_annonce) 
							VALUES (:id_clan,:id_jeu,:annonce,:time, NOW())')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_clan' => $_POST['id_clan'],
							'id_jeu' => $_POST['id_jeu'],
							'annonce' => $_POST['annonce'],
							'time' => time()))
							or die(print_r($bdd->errorInfo()));	
		$req->closeCursor(); // Termine le traitement de la requ�te
		header('Location: recrutements.html');
	}
	else
		header('Location: recrutements-fail.html');	
}
///////////////////// ON SUPPRIME L'ANNONCE 24H APRES ! ////////////////////////
$p2 = $bdd->prepare('SELECT time FROM recrutements 
					WHERE id_jeu=:id_jeu')
					or die(print_r($bdd->errorInfo()));
$p2->execute(array('id_jeu' => $_SESSION['id_jeu']))
					or die(print_r($bdd->errorInfo()));
$do2 = $p2->fetch();
$temps = time();
$temps_publi = $temps - $do2['time'];
if ($temps_publi > 3600 * 24)
{
	$req111 = $bdd->prepare('DELETE FROM recrutements 
							WHERE time >= :time')
							or die(print_r($bdd->errorInfo()));
	$req111->execute(array('time' => $temps_publi))
							or die(print_r($bdd->errorInfo()));
}
////////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
  
<html>
 <head>
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="small.css"  />	
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
 </head>
   
<body> 

<!--------------------- OVERLAY CANDIDATURE !---------------------->
<?php
if(isset($_GET['id_clan']))
{
?>
<div id="grand_overlay4"></div> 
<div id="overlay_devenir_membre">
	<a href="recrutements.html"><img id="fermer_message" src="images/fermer.png" alt=" Fermer " /></a>
	<p id="titre_overlay_membre">Devenir membre </p>
	<form action="clan_post.php" method="post">
<?php
	echo'
		<p style="text-align:left; color:#4e5f69; margin-left:25px; margin-top:30px;">
			Message de candidature au meneur :<br />
			<textarea rows="10" cols="37" name="candidature" style="color:#102c3c;font-size:1.2em;font-weight:bolder;"></textarea><br />
			<input type="submit" id="envoyer_profil" name="Valider" value="Envoyer"/>
		</p>
		<input type="hidden" name="id_clan" value="'.$_GET['id_clan'].'"/>
		<input type="hidden" name="annonce_recrutements"/>
	</form>';
?>
</div>
<!------------------ FIN OVERLAY CANDIDATURE!------------------->
<?php
}
	if(isset($_GET['annonce']))
	{
?>
		<div id="grand_overlay4"></div>
		<div id="overlay_annonce">
			<a href="recrutements.html">
				<img id="fermer_message" src="images/fermer.png" alt="Fermer"/>
			</a>
			<p id="titre_overlay_membre">Postez une annonce</p>
<?php			
			echo'<img id="votre_clan" src="images/votre_clan.png" alt="Votre Clan : "/>';
			echo'<div class="bloc_affichage2_recherche" style="margin-top:50px;">';
			echo'<span id="nom_clan_annonce">
					<img class="embleme_clan_recherche" src="images/embleme_clan_recherche.png" alt=" "/>
					<span class="nom2_clan_recherche" style="color:#102c3c;" >
						'.stripslashes(htmlspecialchars($donnees_clan2['nom_clan'])).'
					</span>
				</span>';
			if (isset($donnees_clan2['photo_clan']) 
			AND $donnees_clan2['photo_clan'] != '' 
			AND $donnees_clan2['photo_clan'] != 0)
			{	
				$source = getimagesize('images_utilisateurs/'.$donnees_clan2['photo_clan']); // La photo est la source
				echo'<div class="centre_image2_bloc_recherche">';
						if ($source[0] <= 120 AND $source[1] <= 60)
							echo'<img src="images_utilisateurs/'.$donnees_clan2['photo_clan'].'" alt="Photo de Clan" />';
						else
							echo'<img src="images_utilisateurs/clan_3_'.$donnees_clan2['photo_clan'].'" alt="Photo de Clan" />';
				echo'</div>';
			}
			else
				echo'<img class="defaut_clan2" src="images/defaut_clan2.png" alt="Photo de Clan"/>';	
				
			echo '<p class="info_clan_recherche">';
				if(strlen($donnees_clan2['jeu']) > 15)
				{
					echo'Jeu : 
					<span style="font-size:0.7em;">
						'.substr(stripslashes(htmlspecialchars($donnees_clan2['jeu'])), 0, 20).'';
						if(strlen(stripslashes(htmlspecialchars($donnees_clan2['jeu']))) > 20){ echo'...';}
					echo'
					</span><br />';
				}
				else
				{
					echo'Jeu : 
					'.substr(stripslashes(htmlspecialchars($donnees_clan2['jeu'])), 0, 20).'';
					if(strlen(stripslashes(htmlspecialchars($donnees_clan2['jeu']))) > 20){ echo'...';}
					echo'<br />';
				}		
					echo'Platerforme : '.$donnees_clan2['plateforme'].'
				 </p>';
			echo'</div>';
?>
			<div id="sous_overlay_recrutement">
				<img id="votre_annonce" src="images/votre_annonce.png" alt="Votre annonce : "/>
				<div class="bbcode_css_recrutement">
					<script type="text/javascript" src="bbcode.js"></script>
					<a href="#">
						<img src="images/bbcode/gras.gif" alt="gif" title="Gras"
						onclick="javascript:bbcode('[g]', '[/g]'); return(false)"/>
					</a>
					<a href="#">
						<img src="images/bbcode/italique.gif" alt="gif" title="Italique" 
						onclick="javascript:bbcode('[i]', '[/i]'); return(false)"/>
					</a>
					<a href="#">
						<img src="images/bbcode/souligner.gif" alt="gif" title="Souligner"
						onclick="javascript:bbcode('[s]', '[/s]'); return(false)"/>
					</a>
					<a href="#">
						<img src="images/bbcode/lien.gif" alt="gif" title="Lien" 
						onclick="javascript:bbcode('[a]', '[/a]'); return(false)"/>
					</a>
					<!-- fin bbcode -->	
				</div>
				<form action="recrutements.php" method="post">
					<textarea rows="9" cols="37" id="bbcode_recrutement" name="annonce" style="color:#102c3c;font-size:1.2em;font-weight:bolder;" placeholder="Quel type de joueur recherchez-vous?" ></textarea><br />
					<input type="submit" id="envoyer_profil" name="Valider" value="Poster"/>
					<input type="hidden" name="id_clan" value="<?php echo $donnees_clan2['id'] ;?>"/>
					<input type="hidden" name="id_jeu" value="<?php echo $_SESSION['id_jeu'] ;?>"/>
				</form>
			</div>
		</div>
<?php
	}
include('menu_sans_doctype.php');
?>
	<div id="corps_recrutement">
		<img id="recrutement" src="images/recrutement.png" alt="Annonces de recrutement"/>
		<p id="intro_recrutement"> 
			Cette page affiche toutes les annonces des teams pr�sentes sur Gather
			Games souhaitant recruter de nouveaux membres. Une annonce s'affiche
			pendant 24h. Son auteur doit la renouveler si necessaire. <br /> 
			Apr�s avoir regard� les crit�res de recrutement, vous pouvez d�poser
			votre candidature par un message d�di� au meneur ou sur le forum de 
			la team concern�e.
		</p>	
<?php
	if($_SESSION['statut'] == 'meneur')
	{
		$p2 = $bdd->prepare('SELECT time FROM recrutements 
							WHERE id_jeu=:id_jeu ORDER BY date_annonce DESC')
							or die(print_r($bdd->errorInfo()));
		$p2->execute(array('id_jeu' => $_SESSION['id_jeu']))
							or die(print_r($bdd->errorInfo()));
		$do2 = $p2->fetch();
		$temps = time();
		$temps_publi = $temps - $do2['time'];

		if ($temps_publi < 3600*24)
		{
			$temps_restant = 3600*24 - $temps_publi;

				if (floor(($temps_restant - 1)/3600) > 0)
				{
					echo'
					<div id="avant_publier" style="margin-left:30px;">
						<p id="nombre_heure">
							'.floor(($temps_restant - 1)/3600).' Heures
						</p>
					</div>';
				}
				else
				{
					echo'
					<div id="avant_publier" style="width:640px;background-repeat:no-repeat;margin-left:30px;">
						<p id="nombre_heure" style=""> 
							moins d\'1 Heure
						</p>
					</div>';
				}
					
				echo'<br />
				<div id="bloc_annonce2">';
		}
		else
		{
				echo'
				<a href="recrutements-annonce.html">
					<div id="nouvelle_annonce"></div>
				</a>
				<div id="bloc_annonce2">';
		}

	}	
	else
		echo'
		<div id="bloc_annonce">';
?>
	<div id="texte_annonce"> 
		Les annonces sont class�es en fonction de leur date de publication quel 
		que soit le jeu. 
		Faites une recherche approfondie par jeu: <br /><br /> 
		<form action="recrutements.php" method="post">
		<input style="color:#102c3c;font-size:1.0em;width:400px; height:20px;" 
			   name="recherche" value=" Recherche " 
			   title="Si votre jeu n'apparait pas dans la recherche c'est qu'il n'y a pas d'annonce de recrutement pour celui-ci" 
			   type="text" onFocus="this.value=''"
		/>
			<select class="input1" name="selection" id="selection" style="color:#102c3c;width:78px; height:26px;" >
				<option value="nom_clan">Par Clan</option>
				<option value="jeu">Par jeu</option>
			</select>
		<input type="submit" id="chercher_annonce" name="Valider" value="Rechercher"/>
		</form>
	</div>
<?php
if (isset($_GET['fail'])) 
{
	echo'<div id="bloc_erreur_annonce">
			<p class="erreur_modifs">
				<img src="images/attention.png" alt=""/> 
				L\'annonce doit contenir 500 caract�res maximum.
			</p>
		</div>';
}
			
if (isset($_GET['page']) AND $_GET['page'] > 0)
{
$numero_page = $_GET['page'];
$numero_page--;
$numero_page = 15*$numero_page;
}
else
$numero_page = 0;	

if (isset($_POST['selection']) AND $_POST['selection'] == 'nom_clan')
{
	$nom_clan = 1; $selection = $_POST['selection'];
}
elseif(isset($_GET['selection']) AND $_GET['selection'] == 'nom_clan')
{ 
	$nom_clan = 1; $selection = $_GET['selection'];
}
if (isset($_POST['selection']) AND $_POST['selection'] == 'jeu')
{
	$jeu = 1; $selection = $_POST['selection'];
}
elseif(isset($_GET['selection']) AND $_GET['selection'] == 'jeu')
{ 
	$jeu = 1; $selection = $_GET['selection'];
}

if (!isset($_GET['selection']) AND !isset($_POST['selection']))
	$selection = 1;
if (isset($_GET['page']))
	$recherche = $_GET['page'];
if (isset($_POST['recherche']))
	$recherche = $_POST['recherche'];
elseif (isset($_GET['recherche']))
	$recherche = $_GET['recherche'];
else
	$recherche = '';	
		
	$i=0;	
		
	if (isset($nom_clan)) 
	{
		$req_annonce = $bdd->prepare('SELECT r.id, id_clan, 
									annonce,date_annonce, 
									DATE_FORMAT(date_annonce, \'%d/%m/%Y � %Hh %imin \') 
									AS date_affichage 
									FROM recrutements r
									LEFT JOIN clan c
									ON r.id_clan = c.id   WHERE nom_clan 
									LIKE :recherche
									ORDER BY date_annonce  
									DESC LIMIT '.$numero_page.',15')
									or die(print_r($bdd->errorInfo()));
		$req_annonce->execute(array('recherche' => $recherche.'%'))
									or die(print_r($bdd->errorInfo()));
	}
	elseif (isset($jeu)) 
	{
		$req_annonce = $bdd->prepare('SELECT r.id, id_clan, 
									annonce,date_annonce, 
									DATE_FORMAT(date_annonce, \'%d/%m/%Y � %Hh %imin \') 
									AS date_affichage 
									FROM recrutements r
									LEFT JOIN clan c
									ON r.id_clan = c.id   
									WHERE jeu 
									LIKE :recherche
									ORDER BY date_annonce 
									DESC LIMIT '.$numero_page.',15')
									or die(print_r($bdd->errorInfo()));
		$req_annonce->execute(array('recherche' => $recherche.'%'))
									or die(print_r($bdd->errorInfo()));
	}
	else
	{
		$req_annonce = $bdd->query('SELECT r.id, id_clan, 
									annonce,date_annonce, 
									DATE_FORMAT(date_annonce, \'%d/%m/%Y � %Hh %imin \') 
									AS date_affichage 
									FROM recrutements r
									LEFT JOIN clan c
									ON r.id_clan = c.id 
									ORDER BY date_annonce 
									DESC LIMIT '.$numero_page.',15')
									or die(print_r($bdd->errorInfo()));
	}    	
	while ($donnees_annonce = $req_annonce->fetch()) 						// ON AFFICHE MESSAGE PAR MESSAGE
	{	
		if (isset($nom_clan)) 
		{
			$req_clan=$bdd->prepare('SELECT * FROM clan 
									WHERE nom_clan 
									LIKE :recherche 
									AND id=:id ')
									or die(print_r($bdd->errorInfo()));
			$req_clan->execute(array('recherche' => $recherche.'%', 
									'id' => $donnees_annonce['id_clan']))
									or die(print_r($bdd->errorInfo()));
		}
		elseif (isset($jeu)) 
		{
			$req_clan=$bdd->prepare('SELECT * FROM clan 
									WHERE jeu 
									LIKE :recherche 
									AND id=:id ')
									or die(print_r($bdd->errorInfo()));
			$req_clan->execute(array('recherche' => $recherche.'%',
									'id' => $donnees_annonce['id_clan']))
									or die(print_r($bdd->errorInfo()));
		}
		else
		{
			$req_clan=$bdd->prepare('SELECT * FROM clan WHERE id=:id ')
									or die(print_r($bdd->errorInfo()));
			$req_clan->execute(array( 'id' => $donnees_annonce['id_clan']))
									or die(print_r($bdd->errorInfo()));
		}
		
		$r=0;
		while ($donnees_clan= $req_clan->fetch()) 
		{
			echo'
			<div class="bloc_annonce_affichage">';
			echo'<div class="dessus_clan_annonce">';
			
			$requete = $bdd->prepare('SELECT id_clan FROM jeu 
									WHERE id=:id_jeu')
									or die(print_r($bdd->errorInfo()));
			$requete->execute(array('id_jeu' => $_SESSION['id_jeu']))
									or die(print_r($bdd->errorInfo()));
			$donnees_visiteur = $requete->fetch();
			
			
			if(isset($_GET['ec']) // erreur candidature
			AND $_GET['id'] == $donnees_annonce['id_clan'])
			{
				echo'
				<a href="recrutements-it'.$donnees_annonce['id_clan'].'.html">
					<div class="devenir_membre2_annonce"></div>
				</a>';
			}
			elseif ($_SESSION['statut'] == 'meneur' 
			AND $donnees_annonce['id_clan'] == $donnees_visiteur['id_clan'])
			{
				echo'<img class="haut_droit_annonce"src="images/meneur_clanp3_annonce.png" alt="Meneur" />';
			}
			elseif ($_SESSION['statut'] == 'membre'  
			AND $donnees_annonce['id_clan'] == $donnees_visiteur['id_clan'])
			{
				echo'<img class="haut_droit_annonce"src="images/membre_clanp3_annonce.png" alt="Membre" />';
			}
			elseif (isset($donnees_visiteur['id_clan']) 
			AND $donnees_visiteur['id_clan'] == 0) //VISITEUR SANS GUILDE
			{
				echo'
				<a href="recrutements-it'.$donnees_annonce['id_clan'].'.html">
					<div class="devenir_membre_annonce"></div>
				</a>';
			}
				echo'
					<p class="nom_clan_annonce">
						<img class="embleme_clan_recherche" src="images/embleme_clan_recherche.png" alt=" "/>
						<span class="nom2_clan_recherche" style="color:#102c3c;" >
							'.stripslashes(htmlspecialchars($donnees_clan['nom_clan'])).'
						</span>
					</p>
					<p class="jeu_annonce">';
					if(strlen($donnees_clan['jeu']) > 15)
					{
						echo'Jeu : 
						<span style="font-size:0.7em;">
							'.substr(stripslashes(htmlspecialchars($donnees_clan['jeu'])), 0, 20).'';
							if(strlen(stripslashes(htmlspecialchars($donnees_clan['jeu']))) > 20){ echo'...';}
						echo'
						</span>';
					}
					else
					{
						echo'Jeu : 
						'.substr(stripslashes(htmlspecialchars($donnees_clan['jeu'])), 0, 20).'';
						if(strlen(stripslashes(htmlspecialchars($donnees_clan['jeu']))) > 20){ echo'...';}
					}
					
					echo'
					</p>
				</div>
				<p class="date_notifs" style="font-size:small;color:#102c3c;margin-top:10px;margin-right:10px;">
					le '.$donnees_annonce['date_affichage'].'
				</p>';
				 
			echo'<div class="centre_image2_bloc_recherche">';
			if (isset($donnees_clan['photo_clan']) AND$donnees_clan['photo_clan'] != '' AND $donnees_clan['photo_clan'] != 0)
			{	
				$source = getimagesize('images_utilisateurs/'.$donnees_clan['photo_clan']); // La photo est la source
				if ($source[0] <= 120 AND $source[1] <= 60)
					echo'<img src="images_utilisateurs/'.$donnees_clan['photo_clan'].'" alt="Photo de Clan" />';
				else
					echo'<img src="images_utilisateurs/clan_3_'.$donnees_clan['photo_clan'].'" alt="Photo de Clan" />';	
			}
			else
				echo'<img src="images/defaut_clan2.png" alt="Photo de Clan"/>';
			
			echo'</div>';
			
			echo'<div class="info_clan_annonce">';
			
			$req_meneur = $bdd->prepare('SELECT * FROM jeu 
										WHERE id_clan=:id')
										or die(print_r($bdd->errorInfo()));
			$req_meneur->execute(array('id' => $donnees_annonce['id_clan']))
										or die(print_r($bdd->errorInfo()));	
			$donnees_meneur = $req_meneur->fetch();	
			
			if($donnees_meneur['nom_de_compte'] != $_SESSION['nom_de_compte'])
			{
				echo'
				<a  href="profil-i'.$donnees_meneur['id'].'.html">
					<span class="pseudo_annonce">
						Meneur : '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'
					</span>
				</a>';
			}
			elseif($donnees_meneur['id'] == $_SESSION['id_jeu'])
			{
				echo'
				<a  href="profil.html" title="Mon profil" >
					<span class="pseudo_annonce">
						Meneur : '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'
					</span>
				</a>';	
			}
			else
			{
				echo'
				<span class="pseudo_annonce" style="color:#102c3c;" title="Mon sous compte">
					Meneur : '.stripslashes(htmlspecialchars($donnees_meneur['pseudo'])).'
				</span>';
			}
			
			echo'Platerforme : '.$donnees_clan['plateforme'].'
				</div>';
				
			$message = nl2br(stripslashes(htmlspecialchars($donnees_annonce['annonce'])));
			$message = preg_replace('#<br />(?:\s*(?:<br />))+#i','<br /><br />',$message);		
			$message = preg_replace('#\[g\](.+)\[/g\]#isU', '<span style="font-size:1.1em">$1</span>', $message);
			$message = preg_replace('#\[i\](.+)\[/i\]#isU', '<em>$1</em>', $message);
			$message = preg_replace('#\[s\](.+)\[/s\]#isU', '<span style="text-decoration: underline;">$1</span>', $message);
			$message = preg_replace('#\[a\](.+)\[/a\]#isU', '<a href="$1">$1</a>', $message);
			$message = preg_replace('#;\)#', '<img class="bbcode_smiley" src="images/bbcode/wink.png"/>', $message);		
			$message = preg_replace('#:\)#', '<img class="bbcode_smiley" src="images/bbcode/shy.png"/>', $message);	
			$message = preg_replace('#:\(#', '<img class="bbcode_smiley" src="images/bbcode/sad.png"/>', $message);		
			$message = preg_replace('#:D#', '<img class="bbcode_smiley" src="images/bbcode/biggrin.png"/>', $message);
			$message = preg_replace('#:p#', '<img class="bbcode_smiley" src="images/bbcode/langue.png"/>', $message);
			$message = preg_replace('#\^\^#', '<img class="bbcode_smiley" src="images/bbcode/lol.png"/>', $message);
			$message = preg_replace('#:s#', '<img class="bbcode_smiley" src="images/bbcode/gene.png"/>', $message);
			$message = preg_replace('#:o#', '<img class="bbcode_smiley" src="images/bbcode/etonne.png"/>', $message);
			$message = preg_replace('#--\'#', '<img class="bbcode_smiley" src="images/bbcode/desempare.png"/>', $message);
			$message = preg_replace('#:\@#', '<img class="bbcode_smiley" src="images/bbcode/enerve.png"/>', $message);
			echo'
				<img class="type_joueur" src="images/type_joueur.png" alt="Type de joueur recherch� : "/>
				<div class="message_annonce">
					'.$message.'
				</div>
			</div>';
		$r++;
		}
	$i++;	
	}	
	if($i==0)
	{
		echo '
		<div id="demande_notification2"
			style="background-image: url(images/fond_sombre.png);">
			Aucune annonce de recrutement trouv�e suite � votre recherche.
		</div>';
	}
	if(isset($r) AND $r==0)
	{
		echo '
		<div id="demande_notification2"
			style="background-image: url(images/fond_sombre.png);">
			Aucune annonce de recrutement trouv�e suite � votre recherche.
		</div>';
	}
?>
	</div>
<?php
// GESTION AFFICHAGE PAGES
echo'<p class="nombre_page">';

if((isset($_POST['recherche'])) OR (isset($_GET['recherche']) AND $_GET['recherche'] != ''))
{
	$req_annonce = $bdd->query('SELECT id, id_clan, annonce,date_annonce, 
								DATE_FORMAT(date_annonce, \'%d/%m/%Y � %Hh %imin \') 
								AS date_affichage  
								FROM recrutements')
								or die(print_r($bdd->errorInfo()));
	while ($donnees_annonce = $req_annonce->fetch()) 							// ON AFFICHE MESSAGE PAR MESSAGE
	{	
		if (isset($nom_clan))
		{
			$req_clan = $bdd->prepare('SELECT * FROM clan WHERE nom_clan 
									LIKE :recherche AND id=:id')
									or die(print_r($bdd->errorInfo()));
			$req_clan->execute(array('recherche' => $recherche.'%', 
									'id' => $donnees_annonce['id_clan']))
									or die(print_r($bdd->errorInfo()));
			while ($donnees_clan= $req_clan->fetch())							
			{	
				if(isset($donnees_clan['id']))
				{
					$nbre_page = 1;
					$p = $bdd->prepare('SELECT COUNT(*) AS nbre_id_clan 
										FROM recrutements 
										WHERE id_clan=:id_clan')
										or die(print_r($bdd->errorInfo()));
					$p->execute(array('id_clan' => $donnees_clan['id']))
										or die(print_r($bdd->errorInfo()));
					$do = $p->fetch();
					$nbr_entrees = $do['nbre_id_clan'];
					$p->closeCursor();
				}
			}
		}
		elseif (isset($jeu))
		{
			$req_clan=$bdd->prepare('SELECT * FROM clan 
									WHERE jeu LIKE :recherche 
									AND id=:id')
									or die(print_r($bdd->errorInfo()));
			$req_clan->execute(array('recherche' => $recherche.'%', 
									'id' => $donnees_annonce['id_clan']))
									or die(print_r($bdd->errorInfo()));
			while ($donnees_clan= $req_clan->fetch()) 							
			{	
				if(isset($donnees_clan['id']))
				{
					$nbre_page = 1;
					$p = $bdd->prepare('SELECT COUNT(*) AS nbre_id_clan 
										FROM recrutements 
										WHERE id_clan=:id_clan')
										or die(print_r($bdd->errorInfo()));
					$p->execute(array('id_clan' => $donnees_clan['id']))
										or die(print_r($bdd->errorInfo()));
					$do = $p->fetch();
					$nbr_entrees = $do['nbre_id_clan'];
					$p->closeCursor();
				}
			}
		}
		else
		{
			$req_clan = $bdd->prepare('SELECT * FROM clan WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
			$req_clan->execute(array('id' => $donnees_annonce['id_clan']))
									or die(print_r($bdd->errorInfo()));
			while ($donnees_clan= $req_clan->fetch())
			{	
				if(isset($donnees_clan['id']))
				{
					$nbre_page = 1;
					$p = $bdd->prepare('SELECT COUNT(*) AS nbre_id_clan 
										FROM recrutements 
										WHERE id_clan=:id_clan')
										or die(print_r($bdd->errorInfo()));
					$p->execute(array('id_clan' => $donnees_clan['id']))
										or die(print_r($bdd->errorInfo()));
					$do = $p->fetch();
					$nbr_entrees = $do['nbre_id_clan'];
					$p->closeCursor();
				}
			}
		}
	}
}
else
{
	$nbre_page = 1;
	$p = $bdd->prepare('SELECT COUNT(*) AS nbre_publication 
						FROM recrutements 
						WHERE annonce !=:annonce')
						or die(print_r($bdd->errorInfo()));
	$p->execute(array('annonce' => ''))or die(print_r($bdd->errorInfo()));
	$do = $p->fetch();
	$nbr_entrees = $do['nbre_publication'];
	$p->closeCursor();
}

echo'<p class="nombre_page">';
		
if (isset ($_GET['page']))
	$current_page = $_GET['page'];
else
	$current_page = 1;
	
if (isset($nbr_entrees) AND $nbr_entrees != 0)
{
	$nbr_page = ceil($nbr_entrees / 15); //diviser puis arrondir au sup�rieur
	
	if ($current_page >= 1 AND $current_page <= $nbr_page)
	{
		if ($nbr_page > 1)
		{
			echo'
				<span style="color:#102c3c;">';
				if((isset($_POST['recherche'])) OR (isset($_GET['recherche']) AND $_GET['recherche'] != ''))
				{
					echo'
					<a href="recrutements-p1-'.$recherche.'-'.$selection.'.html">';
				}
				else
				{
					echo'
					<a href="recrutements-p1.html">';
				}
		
			if ($current_page - 3 > 1)
			{
				echo'
						<span class="numero_page1">1</span>
					</a>';
				echo '...';
			}
			else
			{
				if((isset($_GET['page']) AND $_GET['page'] ==1) 
				OR !isset($_GET['page']))
				{
					echo'
						<span class="numero_page2">1</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">1</span>
					</a>';
				}
			}
			
			echo '
				</span>';
			
			$count = -2;
			
			while ($count <= 2)
			{
				$affich_page = $current_page + $count ;
				
				if ($affich_page > 1
				AND $affich_page < $nbr_page)
				{
					if((isset($_POST['recherche'])) OR (isset($_GET['recherche']) AND $_GET['recherche'] != ''))
					{
						echo'
						<a href="recrutements-p'.$affich_page.'-'.$recherche.'-'.$selection.'.html">';
					}
					else
					{
						echo'
						<a href="recrutements-p'.$affich_page.'.html">';
					}
					
					if(isset($_GET['page']) AND $_GET['page'] == $affich_page)
					{
						echo'
							<span class="numero_page2">'.$affich_page.'</span>
						</a>';
					}
					else
					{
						echo'
							<span class="numero_page">'.$affich_page.'</span>
						</a>';
					}
				}
				$count ++;
			}
			
			echo'
				<span style="color:#102c3c;">';
			if ($current_page + 3 < $nbr_page)
				echo '...';
			if((isset($_POST['recherche'])) OR (isset($_GET['recherche']) AND $_GET['recherche'] != ''))
			{
				echo'	
				<a href="recrutements-p'.$nbr_page.'-'.$recherche.'-'.$selection.'.html">';
			}
			else
			{
				echo'	
				<a href="recrutements-p'.$nbr_page.'.html">';
			}
			
			if ($current_page + 3 < $nbr_page)
			{
				echo'
						<span class="numero_page1">'.$nbr_page.'</span>
					</a>';
			}
			else
			{
				if(isset($_GET['page']) AND $_GET['page'] == $nbr_page)
				{
					echo'
						<span class="numero_page2">'.$nbr_page.'</span>
					</a>';
				}
				else
				{
					echo'
						<span class="numero_page">'.$nbr_page.'</span>
					</a>';
				}
			}
				
				echo'
				</span>';
		}
	}
	else
	{
		echo 'Erreur de num�ro de page.'; // ERREUR
	}
}
echo'</p>';
// FIN GESTION AFFICHAGE PAGES
?>	
	</div>
<?php include('pied_page.php'); ?>