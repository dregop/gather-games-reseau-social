<?php // LES HEADER NE RESPECTENT PAS LA LIGNE DES 80 CARACTERES
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}

$req = $bdd->prepare('SELECT nom_de_compte FROM membres 
					WHERE nom_de_compte=:nom_de_compte')
					or die(print_r($bdd->errorInfo()));
$req->execute(array('nom_de_compte' => $_POST['nom_de_compte']))
					or die(print_r($bdd->errorInfo()));
$donnees = $req->fetch();

if (isset($_POST['nom_de_compte'],$_POST['mot_de_passe'],$_POST['mot_de_passe2']
,$_POST['email']) AND $_POST['nom_de_compte'] != '' 
AND $_POST['mot_de_passe'] != '' AND $_POST['mot_de_passe2'] != '' 
AND $_POST['email'] != '')
{
	if (
	!(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email']
	))) // si l'email ne correspond pas � ce format
	{
		$erreur_email = 'email';
		header('Location: index-'.$erreur_email.'-'.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
    elseif ($donnees['nom_de_compte'] == $_POST['nom_de_compte']) 				// s'il y a d�j� ce nom_de_compte ds la bdd 
	{
		$erreur_nom_de_compte = 'e1';
		header('Location: index-'.$erreur_nom_de_compte.','.stripslashes(htmlspecialchars($_POST['email'])).','.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	elseif (!(preg_match('#^[A-Za-z0-9]{1,1000}$#',$_POST['nom_de_compte']))) 	// si le nom_de_compte n'est pas conforme
	{
		$erreur2_nom_de_compte = 'e2';
		header('Location: index-'.$erreur2_nom_de_compte.','.stripslashes(htmlspecialchars($_POST['email'])).'.html');
	}	
	elseif (strlen($_POST['nom_de_compte']) <4) 								// si le nom de compte est inf�rieur � 4 caract�res
	{
		$erreur3_nom_de_compte = 'e3';
		header('Location: index-'.$erreur3_nom_de_compte.','.stripslashes(htmlspecialchars($_POST['email'])).','.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	elseif (strlen($_POST['nom_de_compte']) >12) 								// si le nom de compte est sup�rieur � 12 caract�res
	{
		$erreur4_nom_de_compte = 'e4';
		header('Location: index-'.$erreur4_nom_de_compte.','.stripslashes(htmlspecialchars($_POST['email'])).','.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	elseif ($_POST['mot_de_passe'] != $_POST['mot_de_passe2']) 					// si les deux mot de passe entr�es sont diff�rents
	{
		$erreur_mdp2 = 'mdp2';
		header('Location: index-'.$erreur_mdp2.','.stripslashes(htmlspecialchars($_POST['email'])).','.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	elseif (!(preg_match('#^[A-Za-z0-9�&�@��]{4,25}$#',$_POST['mot_de_passe'])))// si le mdp n'est pas conforme
	{
		$erreur_mdp = 'mdp';
		header('Location: index-'.$erreur_mdp.','.stripslashes(htmlspecialchars($_POST['email'])).','.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	elseif (!(isset($_POST['case']))) 											// si conditions g�n�ral ne sont pas accept�es
	{
		$erreur_case = 'case';
		header('Location: index-'.$erreur_case.','.stripslashes(htmlspecialchars($_POST['email'])).','.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	elseif (!(isset($erreur_mdp2)) AND !(isset($erreur_email)) 
	AND !(isset($erreur2_nom_de_compte)) AND !(isset($erreur_nom_de_compte)) 
	AND !(isset($erreur_mdp)) AND !(isset($erreur3_nom_de_compte)) 
	AND !(isset($erreur4_nom_de_compte)))	
	{
		
		$req->closeCursor(); // Termine le traitement de la requ�te
		
		// si tout c'est bien pass�
		$mdp_hache = sha1('qw' . $_POST['mot_de_passe']); // on hache le mdp
		
		// puis on enregistre les donnees de l'utilisateur dans la bdd
		$_SESSION['mot_de_passe'] = $mdp_hache;
		$_SESSION['email'] = $_POST['email'];	
		$_SESSION['nom_de_compte'] = $_POST['nom_de_compte']; 					// permet de savoir que l'inscription a �t� effectu�e
		
		header('Location: inscription.html'); 
	}

}
else
{
	$erreur5 = 'incomplet';
	if(isset($_POST['nom_de_compte'],$_POST['email']) AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email']))
	AND $_POST['nom_de_compte'] == '' AND $_POST['email'] != '')
	{
		header('Location: index-'.$erreur5.','.stripslashes(htmlspecialchars($_POST['email'])).'.html');
	}
	elseif(isset($_POST['email'],$_POST['nom_de_compte']) 
	AND $_POST['email'] != '' AND $_POST['nom_de_compte'] != ''
	AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email'])))
	{
		header('Location: index-'.$erreur5.','.stripslashes(htmlspecialchars($_POST['email'])).','.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	elseif(isset($_POST['nom_de_compte'])
	AND $_POST['nom_de_compte'] != '')
	{
		header('Location: index-'.$erreur5.'-'.stripslashes(htmlspecialchars($_POST['nom_de_compte'])).'.html');
	}
	else
	{
		header('Location: index-'.$erreur5.'.html');
	}
}
?>