<?php
session_start();
$menu = 0;
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
?>
    <!DOCTYPE html>
  
<html>

 <head>
 
    <title>Gather Games</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="index.css"/>    
    <link href="style.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/petit_logo.ico" />
	<!--[if IE]>
    <link type="text/css" rel="stylesheet" href="hack_ie.css" />
    <![endif]-->
 </head>
 
  <body> 
    
	 <div id="fond_banniere">
	 
          <div id="banniere">   		  	           
	      <a href="index.html">
			<img id="grand_logo" src="images/grand_logo.png" alt="logo"/>
			</a>
          </div>
          
		  <div id="bordure1"></div>
		  <div id="bordure2"></div>
		  
     </div>
 
    
    <div class="corps">
       <div id="corps_charte">	
           <p>
		        <strong>
					<span class="titre">
						Conditions g�n�rales d'utilisation
					</span>
				</strong>
				<br /><br />
				Une fois inscrit sur Gather Games, vous avez la possibilit� de 
				vous exprimer sur diff�rentes pages.
				<br />
				<ul>
				    <li>Vous �tes responsable et propri�taire de tout ce que 
						vous publiez sur celles-ci. 
						<br/>
                    <li>Gather Games ne pourra �tre tenu responsable des propos 
						publi�s.
						<br/>
                    <li>Les propos tenus doivent �tre mod�r�s, aussi il vous est 
						demand� de rester correct et courtois dans vos propos 
						vis-�-vis des autres utilisateurs.
						<br/>
                </ul>
		        <span class="titre">Sur notre site vous pouvez:</span><br />
                <ul>  
					<li>Partagez des photos, des vid�os ou des textes.<br />
                    <li>Ajouter des amis et discuter avec eux.<br />
                    <li>Parler dans un chat vocal.<br />
                    <li>Voir l'actualit� des jeux vid�os et celle de vos amis.
						<br />
                    <li>Cr�er la page de votre Team.<br />
                    <li>Inviter vos amis dans un chat vocal.<br />
                    <li>Faire partie d'un forum pour �tre membre d'une Team.
						<br />
                </ul>
                Ce site est �volutif, les services qui y sont propos�s peuvent 
				changer � tout moment. 
				<br /><br />
				
                <strong>
					<span class="titre">
						Mot de passe et nom d'utilisateur
					</span></strong>
					<br /><br />
                <ul>
				    <li>Lors de la cr�ation de votre compte, vous avez choisi un
						mot de passe et un nom d'utilisateur dont vous serez 
						enti�rement responsable. Votre compte et votre mot de 
						passe vous sont strictement personnel, ils ne doivent 
						pas �tre communiqu�s et doivent rester secrets...<br />
                    <li>Gather Games n'est pas responsable de la perte de votre 
						mot de passe ou de votre nom d'utilisateur ni de 
						l�utilisation qui pourrait �tre faite de votre compte.
						<br /><br /> 
                </ul>
                <strong>
					<span class="titre">
						Les droits de Gather Games
					</span>
				</strong>
				<br /><br />
                <ul>
				    <li>Gather Games avertira l'utilisateur d'un compte s'il ne 
						respecte pas les conditions g�n�rales d'utilisation.
						<br />
                    <li>Gather Games pourra supprimer un compte qui apr�s avoir 
						re�u un avertissement ne respecte toujours pas les 
						conditions g�n�rales d'utilisation.
						<br />
                    <li>Gather Games stocke les donn�es des comptes ainsi que 
						les informations personnelles s�y r�f�rant.
						<br />
                    <li>Il peut modifier et compl�ter la charte � tout moment.
						<br /><br />
                </ul>
				<strong>
					<span class="titre">
						Important: concernant vos donn�es personnelles
					</span>
				</strong>
				<br /><br />
                <ul>
				    <li>Conform�ment � la loi n� 78-17 du 6 janvier 1978 
					relative � l'informatique, aux fichiers et aux libert�s, 
					chaque membre de Gather Games dispose sur les donn�es 
					personnelles le concernant d'un droit d'acc�s (art. 34 � 38 
					de la loi), de rectification (art. 36 de la loi) et 
					d'opposition (art. 26 de la loi), qu'il peut exercer aupr�s
					de Gather Games.<br /><br />
                </ul>
                 <span class="fin_charte">				 
					Vous �tes maintenant inform� du fonctionnement du site, de ses 
					r�gles et de ce qu'il propose.
				 </span>
        
		</div>
		        
			
		<div id="pied_copyright">
		
		     <p class="copyright">
		      Copyright � 2013 Gather Games - Tous droits r�serv�s

		     </p>  
			 
	   </div>
   </div>
  
  </body>
</html> 