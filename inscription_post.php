<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}

if (isset($_POST['captcha'],$_POST['email2'],$_POST['question'],
$_POST['reponse'],$_POST['ville'],$_POST['sexe'],$_POST['pays']) 
AND $_POST['captcha'] != '' AND $_POST['email2'] != '' 
AND $_POST['question'] != 'S�lectionnez' AND $_POST['reponse'] != ''
AND $_POST['ville'] != '' AND $_POST['pays'] != '' AND $_POST['sexe'] != '')
{
	if (
	!(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email2']
	))) // si l'email ne correspond pas � ce format
	{
		$erreur_email1 = 'e';
		header('Location: inscription-'.$erreur_email1.'-'.stripslashes(htmlspecialchars($_POST['pays'])).'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif($_SESSION['email'] != $_POST['email2'])// si email2 != email 
	{
		$erreur_email2 = 'e2';
		header('Location: inscription-'.$erreur_email2.','.stripslashes(htmlspecialchars($_POST['email2'])).','.stripslashes(htmlspecialchars($_POST['pays'])).'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif( strlen($_POST['reponse']) > 250)// si la r�ponse secr�te depassent 250 caract�res
	{
		$erreur_qr = 'r';
		header('Location: inscription-'.$erreur_qr.','.stripslashes(htmlspecialchars($_POST['email2'])).','.stripslashes(htmlspecialchars($_POST['pays'])).'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif($_SESSION['captcha'] != $_POST['captcha'])// si captcha != du captcha entr�
	{
		$erreur_captcha = 'c';
		header('Location: inscription-'.$erreur_captcha.','.stripslashes(htmlspecialchars($_POST['email2'])).','.stripslashes(htmlspecialchars($_POST['pays'])).'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif (isset($_POST['youtube']) AND $_POST['youtube'] != ''
	AND !(preg_match("#^http://#",$_POST['youtube'])) 
	AND !(preg_match("#^https://#",$_POST['youtube'])))
	{
		$erreur_youtube = 'yt';
		header('Location: inscription-'.$erreur_youtube.','.stripslashes(htmlspecialchars($_POST['email2'])).','.stripslashes(htmlspecialchars($_POST['pays'])).'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif (!(isset($erreur_email2)) AND !(isset($erreur_captcha)) AND !(isset($erreur_qr)) AND !(isset($erreur_youtube)))	
	{
		// On enregistre dans SESSION si il n'y a pas d'erreur
		$_SESSION['question'] = $_POST['question'];
		$_SESSION['reponse'] = $_POST['reponse'];
		$_SESSION['sexe'] = $_POST['sexe'];
		$_SESSION['pays'] = $_POST['pays'];
		$_SESSION['ville'] = $_POST['ville'];
		$_SESSION['youtube'] = $_POST['youtube'];
		
		$requete = $bdd->prepare('INSERT INTO membres
								(nom_de_compte,mot_de_passe,email,question,
								reponse,sexe,pays,ville,youtube,nouveau,
								date_inscription) 
								VALUES(:nom_de_compte,:mot_de_passe,:email,
								:question,:reponse,:sexe,:pays,:ville,
								:youtube,:nouveau,NOW())')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('nom_de_compte' => $_SESSION['nom_de_compte'],
								'mot_de_passe' => $_SESSION['mot_de_passe'],
								'email' => $_SESSION['email'],
								'question'=> $_SESSION['question'],
								'reponse'=> $_SESSION['reponse'],
								'sexe'=> $_SESSION['sexe'],
								'pays'=> $_SESSION['pays'],
								'ville'=> $_SESSION['ville'],
								'youtube'=> $_SESSION['youtube'],
								'nouveau'=> 1,))
								or die(print_r($bdd->errorInfo()));
		$requete->closeCursor(); // Termine le traitement de la requ�te				
		header('Location: informations.html'); 
	}
}
elseif(isset($_SESSION['mot_de_passe']))
{	
	$erreur_incomplet = 'incomplet';
	

	if(isset($_POST['ville'],$_POST['ville'],$_POST['email2']) AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email2']))
	AND $_POST['pays'] != '' AND $_POST['ville'] != '')
	{
		header('Location: inscription-'.$erreur_incomplet.','.stripslashes(htmlspecialchars($_POST['email2'])).','.stripslashes(htmlspecialchars($_POST['pays'])).'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	if(isset($_POST['pays'],$_POST['ville'],$_POST['email2']) AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email2']))
	AND $_POST['ville'] == '' AND $_POST['pays'] != '')
	{
		header('Location: inscription-'.$erreur_incomplet.','.stripslashes(htmlspecialchars($_POST['email2'])).','.stripslashes(htmlspecialchars($_POST['pays'])).'.html');
	}
	elseif(isset($_POST['ville'],$_POST['ville'],$_POST['email2']) AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email2']))
	AND $_POST['pays'] == '' AND $_POST['ville'] != '')
	{
		header('Location: inscription-'.$erreur_incomplet.','.stripslashes(htmlspecialchars($_POST['email2'])).','.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif(isset($_POST['ville'],$_POST['ville']) AND $_POST['pays'] != '' 
	AND $_POST['ville'] != '')
	{
		header('Location: inscription-'.$erreur_incomplet.'-'.stripslashes(htmlspecialchars($_POST['pays'])).'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif(isset($_POST['ville'],$_POST['ville']) AND $_POST['pays'] != '' 
	AND $_POST['ville'] == '')
	{
		header('Location: inscription-'.$erreur_incomplet.'-'.stripslashes(htmlspecialchars($_POST['pays'])).'.html');
	}
	elseif(isset($_POST['ville'],$_POST['ville']) AND $_POST['pays'] == '' 
	AND $_POST['ville'] != '')
	{
		header('Location: inscription-'.$erreur_incomplet.'-'.stripslashes(htmlspecialchars($_POST['ville'])).'.html');
	}
	elseif(isset($_POST['email2']) AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email2']))
	AND $_POST['pays'] == '' AND $_POST['ville'] == '')
	{
		header('Location: inscription-'.$erreur_incomplet.','.stripslashes(htmlspecialchars($_POST['email2'])).'.html');
	}
	else
	{
		header('Location: inscription-'.$erreur_incomplet.'.html');
	}
}
else
	header('Location: index.html');
