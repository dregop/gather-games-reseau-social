<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arr�te tout
        die('Erreur : '.$e->getMessage());
}
////////////////////////////////////////////////////////////////////////////////
// LES REDIRECTIONS SONT A LA FIN, EN PREMIER ON PROCEDE AUX ENREGISTREMENTS ///
// DANS LA BDD /////////////////////////////////////////////////////////////////

// CONCERNANT PROFIL.PHP ///////////////////////////////////////////////////////
if (isset($_POST['you_tube']) 
AND preg_match("#^https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i", $_POST['you_tube'])
AND isset($_POST['publication'],$_POST['lieu_publication'], 
$_POST['lieu_publication_id_jeu']) AND $_POST['profil_ou_clan'] == 'profil')
{
	$video = $_POST['you_tube'];
	$video = preg_replace('#&list=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
	$video = preg_replace('#&feature=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
	// on insert la photo ds la bdd
	$req = $bdd->prepare('INSERT INTO videos(id_jeu, adresse_stockage, date_enregistrement) 
						VALUES(:id_jeu, :adresse_stockage,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'adresse_stockage' => $video)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te


	// on insert le message ds la bdd
	$req = $bdd->prepare('INSERT INTO publication(id_jeu,message,video,
						lieu_publication,lieu_publication_id_jeu,
						date_publication,profil_ou_clan) 
						VALUES(:id_jeu,:message,:video, :lieu_publication, 
						:lieu_publication_id_jeu, NOW(),:profil_ou_clan)') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'message' => $_POST['publication'],
						'video' => $video,
						'lieu_publication' => $_POST['lieu_publication'],
						'lieu_publication_id_jeu' => $_POST['lieu_publication_id_jeu'],
						'profil_ou_clan' => $_POST['profil_ou_clan']))
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te	

	$requete = $bdd->prepare('SELECT id FROM publication 
							WHERE id_jeu=:id_jeu AND video=:video 
							AND date_publication=NOW()')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_SESSION['id_jeu'], 'video' => $video))
							or die(print_r($bdd->errorInfo()));
	$donnees_t = $requete->fetch();	
	// On insert dans actualit�s ************************************************
	$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,id_table, date_actu) 
						VALUES(:id_jeu,:actu,:id_table, NOW())')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],'actu' => 'publication',
						'id_table' => $donnees_t['id'])) 
						or die(print_r($bdd->errorInfo()));
}

// Testons si le fichier a bien �t� envoy� et s'il n'y a pas d'erreur
// Testons si le fichier n'est pas trop gros

elseif (isset($_FILES['photos']['name']) AND $_FILES['photos']['error'] == 0 
AND !empty($_FILES['photos']['name']) AND $_FILES['photo']['size']<=1000000
AND isset($_POST['publication'],$_POST['lieu_publication'], 
$_POST['lieu_publication_id_jeu']) AND $_POST['profil_ou_clan'] == 'profil')
{
	// Testons si l'extension est autoris�e
	$infosfichier = pathinfo($_FILES['photos']['name']);
	$extension_upload = $infosfichier['extension'];
	$extensions_autorisees = array('jpg', 'jpeg', 'png', 'PNG', 'JPEG', 'JPG');				
	if (in_array($extension_upload, $extensions_autorisees))
	{
			// ouverture du fichier compteur
			$monfichier = fopen('compteur.txt', 'r+');				 
			$compteur = fgets($monfichier); 
			$compteur++; 
			fseek($monfichier, 0); 
			fputs($monfichier, $compteur); 
			fclose($monfichier);
			// On peut valider le fichier et le stocker d�finitivement
			
		if ($extension_upload == 'png' OR $extension_upload == 'PNG')
		{
			move_uploaded_file($_FILES['photos']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefrompng('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		else
		{
			move_uploaded_file($_FILES['photos']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefromjpeg('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		
		include('redimention_image.php');
		$redimOK = fctredimimage(300,300,'images_utilisateurs/','mini_1_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(200,200,'images_utilisateurs/','mini_3_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(60,60,'images_utilisateurs/','mini_2_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(900,600,'images_utilisateurs/','grand_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
	} 
	else
	{
		$photo = 0;
		$erreur_image = 0;
	}
	
	$photo = $compteur.'.'.$extension_upload;
	if ($source == '')
	{
		$photo = 0;
		$erreur_image = 0;
	}
	// on insert la photo ds la bdd
	$req = $bdd->prepare('INSERT INTO photos(id_jeu, adresse_stockage, 
						date_enregistrement) 
						VALUES(:id_jeu, :adresse_stockage,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'adresse_stockage' => $photo)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te

	if ($photo != 0)
	{
		// on insert le message ds la bdd
		$req = $bdd->prepare('INSERT INTO publication(id_jeu,message,photo,
							lieu_publication,lieu_publication_id_jeu,
							date_publication,profil_ou_clan) 
							VALUES(:id_jeu,:message,:photo, :lieu_publication, 
							:lieu_publication_id_jeu, NOW(),:profil_ou_clan)') 
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'message' => $_POST['publication'],
							'photo' => $photo,
							'lieu_publication' => $_POST['lieu_publication'],
							'lieu_publication_id_jeu' => $_POST['lieu_publication_id_jeu'],
							'profil_ou_clan' => $_POST['profil_ou_clan'])) 
							or die(print_r($bdd->errorInfo()));
		$req->closeCursor(); // Termine le traitement de la requ�te	
		
		$requete = $bdd->prepare('SELECT id FROM publication 
								WHERE id_jeu=:id_jeu AND message=:message 
								AND date_publication=NOW()')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('id_jeu' => $_SESSION['id_jeu'], 
								'message' => $_POST['publication']))
								or die(print_r($bdd->errorInfo()));
		$donnees_t = $requete->fetch();	
		
		// On insert dans actualit�s *******************************************
		$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,id_table, date_actu) 
							VALUES(:id_jeu,:actu,:id_table, NOW())') 
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
							'actu' => 'publication',
							'id_table' => $donnees_t['id'])) 
							or die(print_r($bdd->errorInfo()));	
	}
}	
elseif (isset($_POST['publication'],$_POST['lieu_publication'], 
$_POST['lieu_publication_id_jeu'])  AND $_POST['publication'] != ''
AND $_POST['profil_ou_clan'] == 'profil')
{	
	// on insert le message ds la bdd
	$req = $bdd->prepare('INSERT INTO publication(id_jeu,message,
						lieu_publication,lieu_publication_id_jeu, 
						date_publication,profil_ou_clan) 
						VALUES(:id_jeu,:message,:lieu_publication, 
						:lieu_publication_id_jeu, NOW(),:profil_ou_clan)') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'message' => $_POST['publication'],
						'lieu_publication' => $_POST['lieu_publication'],
						'lieu_publication_id_jeu' => $_POST['lieu_publication_id_jeu'],
						'profil_ou_clan' => $_POST['profil_ou_clan'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te	
	
	$requete = $bdd->prepare('SELECT id FROM publication 
							WHERE id_jeu=:id_jeu AND message=:message 
							AND date_publication=NOW()')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id_jeu' => $_SESSION['id_jeu'], 
							'message' => $_POST['publication']))
							or die(print_r($bdd->errorInfo()));
	$donnees_t = $requete->fetch();	
	// On insert dans actualit�s ***********************************************
	$req = $bdd->prepare('INSERT INTO actu(id_jeu,actu,id_table, date_actu) 
						VALUES(:id_jeu,:actu,:id_table, NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'actu' => 'publication',
						'id_table' => $donnees_t['id'])) 
						or die(print_r($bdd->errorInfo()));
}// CONCERNANT LE FICHIER CLAN.PHP (TEAM)
elseif (isset($_POST['publication'],$_POST['lieu_publication'], 
$_POST['lieu_publication_id_jeu'])  AND $_POST['publication'] != ''
AND $_POST['profil_ou_clan'] == 'clan')
{
	// on insert le message ds la bdd
	$req = $bdd->prepare('INSERT INTO publication(id_jeu,message,
						lieu_publication,lieu_publication_id_jeu, 
						date_publication,profil_ou_clan) 
						VALUES(:id_jeu,:message,:lieu_publication, 
						:lieu_publication_id_jeu, NOW(),:profil_ou_clan)') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_jeu' => $_SESSION['id_jeu'],
						'message' => $_POST['publication'],
						'lieu_publication' => $_POST['lieu_publication'],
						'lieu_publication_id_jeu' => $_POST['lieu_publication_id_jeu'],
						'profil_ou_clan' => $_POST['profil_ou_clan'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te		

}

if (isset($_POST['you_tube']) 
AND preg_match("#^https?://www.youtube.com/watch\?v\=([a-zA-Z0-9\?\=\-_&]{11})#i", $_POST['you_tube'])
AND isset($_POST['publication'],$_POST['id_clan']) 
AND $_POST['profil_ou_clan'] == 'clan')
{
	$video = $_POST['you_tube'];
	$video = preg_replace('#&list=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
	$video = preg_replace('#&feature=[a-zA-Z0-9._/\-&\?\=]+#', '',$video);
	// on insert la photo ds la bdd
	$req = $bdd->prepare('INSERT INTO videos(id_clan, adresse_stockage, 
						date_enregistrement) 
						VALUES(:id_clan, :adresse_stockage,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $_POST['id_clan'],
						'adresse_stockage' => $video)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te

	$req = $bdd->prepare('INSERT INTO publication(id_clan,id_jeu,message,
						video,profil_ou_clan, date_publication) 
						VALUES(:id_clan,:id_jeu, :message,:video, 
						:profil_ou_clan, NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $_POST['id_clan'],
						'id_jeu' => $_SESSION['id_jeu'],
						'message' => $_POST['publication'],
						'video' => $video,
						'profil_ou_clan' => $_POST['profil_ou_clan'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te

}
elseif ( isset($_FILES['photos']['name']) AND $_FILES['photos']['error'] == 0 
AND !empty($_FILES['photos']['name']) AND
isset($_POST['publication'],$_POST['id_clan']) 
AND $_POST['profil_ou_clan'] == 'clan')
{
	// Testons si l'extension est autoris�e
	$infosfichier = pathinfo($_FILES['photos']['name']);
	$extension_upload = $infosfichier['extension'];
	$extensions_autorisees = array('jpg', 'jpeg', 'png', 'PNG', 'JPEG', 'JPG');				
	if (in_array($extension_upload, $extensions_autorisees))
	{
			// ouverture du fichier compteur
			$monfichier = fopen('compteur.txt', 'r+');				 
			$compteur = fgets($monfichier); 
			$compteur++; 
			fseek($monfichier, 0); 
			fputs($monfichier, $compteur); 
			fclose($monfichier);
			// On peut valider le fichier et le stocker d�finitivement
		if ($extension_upload == 'png' OR $extension_upload == 'PNG')
		{
			move_uploaded_file($_FILES['photos']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefrompng('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		else
		{
			move_uploaded_file($_FILES['photos']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefromjpeg('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		include('redimention_image.php');
		$redimOK = fctredimimage(300,300,'images_utilisateurs/','mini_1_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(200,200,'images_utilisateurs/','mini_3_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(60,60,'images_utilisateurs/','mini_2_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(900,600,'images_utilisateurs/','grand_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
	}   
	$photo = $compteur.'.'.$extension_upload;
	// on insert le message ds la bdd
	$req = $bdd->prepare('INSERT INTO photos(id_clan, adresse_stockage, 	
						date_enregistrement) 
						VALUES(:id_clan, :adresse_stockage,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $_POST['id_clan'],
						'adresse_stockage' => $photo)) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	
	// on insert le message ds la bdd
	$req = $bdd->prepare('INSERT INTO publication(id_clan,id_jeu,message,
						photo,profil_ou_clan, date_publication) 
						VALUES(:id_clan,:id_jeu, :message,:photo, 
						:profil_ou_clan, NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $_POST['id_clan'],
						'id_jeu' => $_SESSION['id_jeu'],
						'message' => $_POST['publication'],
						'photo' => $photo,
						'profil_ou_clan' => $_POST['profil_ou_clan'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te	

		
}
elseif (isset($_POST['publication']) AND $_POST['publication'] != '' 
AND $_POST['profil_ou_clan'] == 'clan')
{	
	$req = $bdd->prepare('INSERT INTO publication(id_clan,id_jeu,message,
						profil_ou_clan, date_publication) 
						VALUES(:id_clan,:id_jeu, :message, :profil_ou_clan, 
						NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_clan' => $_POST['id_clan'],
						'id_jeu' => $_SESSION['id_jeu'],
						'message' => $_POST['publication'],
						'profil_ou_clan' => $_POST['profil_ou_clan'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te			
}

if (isset($_POST['commentaire']) AND isset($_POST['id_publication']) 
AND $_POST['commentaire'] != '')
{
	// on insert le message ds la bdd
	$req = $bdd->prepare('INSERT INTO commentaire(id_publication,id_jeu,
						commentaire,date_commentaire) 
						VALUES(:id_publication,:id_jeu,:commentaire,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('id_publication' => $_POST['id_publication'],
						'id_jeu' => $_SESSION['id_jeu'],
						'commentaire' => $_POST['commentaire'])) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	if (isset($_POST['accueil']))
	{
		header('Location: accueil.html');
	}
}

/////////////////////////// NOTIFICATIONS///////////////////////////////////////

// notif quand on publie sur ton mur
if (isset($_POST['lieu_publication_id_jeu'], $_POST['publication'],
$_POST['profil_ou_clan'], $_POST['id_ami']) 
AND $_POST['publication'] != '' AND $_POST['profil_ou_clan'] == 'profil') 
{
	$req = $bdd->prepare('INSERT INTO notifications(id_jeu_lieu,publication,
						id_jeu_publi,date_notification, profil_ou_clan,view) 
						VALUES(:id_jeu_lieu, :publication,:id_jeu_publi,NOW(), 
						:profil_ou_clan, :view)') 
						or die(print_r($bdd->errorInfo())); 					// ici commentaire = publication
	$req->execute(array('id_jeu_lieu' => $_POST['lieu_publication_id_jeu'],
						'publication' => $_POST['publication'],
						'id_jeu_publi' => $_SESSION['id_jeu'],
						'profil_ou_clan' => 'profil',
						'view' => 1)) or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
}

// notif quand on commente ta publication
if (isset($_POST['commentaire'],$_POST['profil_ou_clan']) 
AND $_POST['commentaire'] != '' AND $_POST['profil_ou_clan'] == 'profil')
{
	$requete = $bdd->prepare('SELECT lieu_publication_id_jeu 
							FROM publication WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id' => $_POST['id_publication']))
							or die(print_r($bdd->errorInfo()));
	$donnee_id = $requete->fetch();
	$req2 = $bdd->prepare('INSERT INTO notifications(id_jeu_lieu,id_publication,
						commentaire,date_notification, id_jeu_com, 
						profil_ou_clan,view) 
						VALUES(:id_jeu_lieu,:id_publication,:commentaire,NOW(), 
						:id_jeu_com, :profil_ou_clan,:view)') 
						or die(print_r($bdd->errorInfo()));
	$req2->execute(array('id_jeu_lieu' => $donnee_id['lieu_publication_id_jeu'],
						'id_publication' => $_POST['id_publication'],
						'commentaire' => $_POST['commentaire'],
						'id_jeu_com' => $_SESSION['id_jeu'],
						'profil_ou_clan' => 'profil',
						'view' => 1)) or die(print_r($bdd->errorInfo()));
	$req2->closeCursor(); // Termine le traitement de la requ�te
}

// notif publication dans ton clan
if (isset($_POST['publication'],$_POST['profil_ou_clan'], $_POST['id_clan']) 
AND $_POST['publication'] != '' AND $_POST['profil_ou_clan'] == 'clan') 
{
	$req = $bdd->prepare('INSERT INTO notifications(id_clan,publication,
						id_jeu_publi,date_notification, profil_ou_clan) 
						VALUES(:id_clan, :publication,:id_jeu_publi,NOW(),
						:profil_ou_clan)') 
						or die(print_r($bdd->errorInfo())); 					// ici commentaire = publication
	$req->execute(array('id_clan' => $_POST['id_clan'],
						'publication' => $_POST['publication'],
						'id_jeu_publi' => $_SESSION['id_jeu'],
						'profil_ou_clan' => 'clan')) 
						or die(print_r($bdd->errorInfo()));
	$req->closeCursor(); // Termine le traitement de la requ�te
	$req2 = $bdd->prepare('UPDATE jeu SET clan_view=1 WHERE id_clan =:id_clan 
						AND id != :id_jeu ') 
						or die(print_r($bdd->errorInfo())); 					// POUR LES PUBLICATIONS SUR LE MURS DU CLAN
	$req2->execute(array('id_clan' => $_POST['id_clan'], 
						'id_jeu' => $_SESSION['id_jeu'])) 
						or die(print_r($bdd->errorInfo()));
	$req2->closeCursor(); // Termine le traitement de la requ�te
}
// notif commentaire d'une publi dans ton clan
if (isset($_POST['commentaire'],$_POST['profil_ou_clan'],$_POST['id_clan'], 
$_POST['id_publication']) AND $_POST['commentaire'] != '' 
AND $_POST['profil_ou_clan'] == 'clan')
{
	$req2 = $bdd->prepare('INSERT INTO notifications(id_clan,id_publication,
						commentaire,date_notification, id_jeu_com, profil_ou_clan,view) 
						VALUES(:id_clan,:id_publication,:commentaire,NOW(), 
						:id_jeu_com, :profil_ou_clan,:view)') 
						or die(print_r($bdd->errorInfo()));
	$req2->execute(array('id_clan' => $_POST['id_clan'],
						'id_publication' => $_POST['id_publication'],
						'commentaire' => $_POST['commentaire'],
						'id_jeu_com' => $_SESSION['id_jeu'],
						'profil_ou_clan' => $_POST['profil_ou_clan'],
						'view' => 1)) or die(print_r($bdd->errorInfo()));
	$req2->closeCursor(); // Termine le traitement de la requ�te
}			

////////////////////////////////////////////////////////////////////////////////
// TOUTES LES REDIRECTIONS /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// SUR LA PAGE COMMENTAIRES
if(isset($_POST['commentaires']))
{
	if (isset($_POST['id_publication'],$_POST['page']))
	{
		header('Location: commentaires-p'.$_POST['page'].'.html');
	}
	else
	{
		header('Location: commentaires.html');
	}
}

/* 
	POUR PROFIL ET ACCUEIL (Car les publications d'accueil.php passent par 
	profil_post.php
*/
elseif (isset($_POST['profil_ou_clan']) AND $_POST['profil_ou_clan'] == 'profil' 
AND !isset($_POST['commentaires']))
{
	/* 
	TOUJOURS VERIFICATION IMAGE EN PREMIER !!! LOGIQUE POUR EVITER QUE D'AUTRE 
	VERIFICATION MARCHE AVANT DE VERIFIER L'IMAGE.
	*/
	if (isset($_POST['id_ami'],$erreur_image))
	{
		$erreur_image = 0;
		header('Location: profil-i'.$_POST['id_ami'].'-'.$erreur_image.'.html');
	}
	elseif (isset($erreur_image))
	{
		$erreur_image = 0;
		header('Location: profil-'.$erreur_image.'.html');
	}
	elseif (isset($_FILES['photos']['name']) 
	AND !empty($_FILES['photos']['name']) 
	AND ($_FILES['photos']['error'] != 0 
	OR $_FILES['photo_profil']['size']>1000000))
	{
		$erreur_image = 1;
		if(isset($_POST['id_ami']))
			header('Location: profil-i'.$_POST['id_ami'].'-'.$erreur_image.'.html');
		else
			header('Location: profil-'.$erreur_image.'.html');
	}
	elseif (isset($_POST['id_publication'],$_POST['accueil']) 
	AND  !isset($_POST['page']))
	{
		header('Location: accueil.html#'.$_POST['id_publication'].'');
	}
	elseif (isset($_POST['id_publication'],$_POST['accueil'], $_POST['page']))
	{
		header('Location: accueil-p'.$_POST['page'].'.html#'.$_POST['id_publication'].'');
	}
	elseif (isset($_POST['id_ami'],$_POST['lieu_publication'],
	$_POST['lieu_publication_id_jeu']))
	{
		header('Location: profil-i'.$_POST['id_ami'].'.html');
	}
	elseif (isset($_POST['lieu_publication'],$_POST['lieu_publication_id_jeu']) 
	AND !isset($_POST['id_ami']))
	{
		header('Location: profil.html');
	}
	elseif (isset($_POST['id_publication'],$_POST['page']) 
	AND !isset($_POST['id_ami']))
	{
		header('Location: profil-p'.$_POST['page'].'.html#'.$_POST['id_publication'].'');
	}
	elseif (isset($_POST['id_ami'],$_POST['id_publication'],$_POST['page']))
	{
		header('Location: profil-i'.$_POST['id_ami'].'-p'.$_POST['page'].'.html#'.$_POST['id_publication']);
	}	
	elseif (!isset($_POST['page']) 
	AND isset($_POST['id_ami'],$_POST['id_publication']))
	{
		header('Location: profil-i'.$_POST['id_ami'].'.html#'.$_POST['id_publication'].'');
	}
	elseif (!isset($_POST['page'],$_POST['id_ami']) 
	AND isset($_POST['id_publication']))
	{
		header('Location: profil.html#'.$_POST['id_publication'].'');
	}
	elseif (isset($_POST['id_ami']) 
	AND !isset($_POST['id_publication'],$_POST['page']))
	{
		header('Location: profil-i'.$_POST['id_ami'].'.html');
	}
	else
	{
		header('Location: profil.html');
	}
}
elseif (isset($_POST['profil_ou_clan']) AND $_POST['profil_ou_clan'] == 'clan' 
AND !isset($_POST['commentaires']))
{
	/* 
	TOUJOURS VERIFICATION IMAGE EN PREMIER !!! LOGIQUE POUR EVITER QUE D'AUTRE 
	VERIFICATION MARCHE AVANT DE VERIFIER L'IMAGE.
	*/
	if (isset($_POST['id_clan'],$erreur_image))
	{
		$erreur_image = 0;
		header('Location: team-i'.$_POST['id_ami'].'-'.$erreur_image.'.html');
	}
	elseif (isset($erreur_image))
	{
		$erreur_image = 0;
		header('Location: team-'.$erreur_image.'.html');
	}
	elseif (isset($_FILES['photos']['name']) 
	AND !empty($_FILES['photos']['name']) 
	AND ($_FILES['photos']['error'] != 0 
	OR $_FILES['photo_profil']['size']>1000000))
	{
		$erreur_image = 1;
		header('Location: team-'.$erreur_image.'.html');
	}
	elseif (isset($_POST['id_publication']) AND empty($_POST['page']))
	{
		header('Location: team.html#'.$_POST['id_publication'].'');
	}
	elseif (isset($_POST['id_publication'],$_POST['page']))
	{
	header('Location: team-p'.$_POST['page'].'.html#'.$_POST['id_publication'].'');
	}
	else
	{
	header('Location: team.html');
	}
}
else
{
	$erreur_image = 0;
	header('Location: profil-'.$erreur_image.'.html');
}